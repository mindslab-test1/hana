#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys

import pytz

reload(sys)
sys.setdefaultencoding('utf-8')

from pytz import timezone
from concurrent import futures
import time
import argparse
import grpc
from google.protobuf import empty_pb2
import pymysql
import os
exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(lib_path)

from proto import pool_pb2
from proto import sds_pb2
from proto import provider_pb2
from proto import userattr_pb2

# import MySQLdb.cursors

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


def paint(i):
    if i == 0:
        return "①"
    elif i == 1:
        return "②"
    elif i == 2:
        return "③"
    elif i == 3:
        return "④"
    elif i == 4:
        return "⑤"

class transactionInquiry(provider_pb2.DialogAgentProviderServicer):
    # STATE
    init_param = provider_pb2.InitParameter()

    # PROVIDER
    provider = pool_pb2.DialogAgentProviderParam()
    provider.name = 'transaction_inquiry'
    provider.description = 'transaction_inquiryDA'
    provider.version = '0.1'
    provider.single_turn = False
    provider.agent_kind = pool_pb2.AGENT_SDS
    provider.require_user_privacy = True
    provider.user_privacy_attributes.extend(["transaction"])

    # PARAMETER

    # SDS Stub
    sds_server_addr = ''
    sds_stub = None

    def __init__(self):
        self.state = provider_pb2.DIAG_STATE_IDLE

    #
    # INIT or TERM METHODS
    #
    def get_sds_server(self):
        sds_channel = grpc.insecure_channel(self.init_param.sds_remote_addr)

        # sds_channel = grpc.insecure_channel('127.0.0.1:9906')
        resolver_stub = sds_pb2.SpokenDialogServiceResolverStub(sds_channel)

        print 'stub'
        sq = sds_pb2.ServiceQuery()
        sq.path = self.sds_path
        sq.name = self.sds_domain
        print sq.path, sq.name

        svc_loc = resolver_stub.Find(sq)
        print 'find result', svc_loc
        # Create SpokenDialogService Stub
        print 'find result loc: ', svc_loc.server_address
        self.sds_stub = sds_pb2.SpokenDialogServiceStub(
            grpc.insecure_channel(svc_loc.server_address))
        self.sds_server_addr = svc_loc.server_address
        print 'stub sds ', svc_loc.server_address

    def IsReady(self, empty, context):
        print 'IsReady', 'called'
        status = provider_pb2.DialogAgentStatus()
        status.state = self.state
        return status

    def Init(self, init_param, context):
        print 'Init', 'called'
        self.state = provider_pb2.DIAG_STATE_INITIALIZING
        # COPY ALL
        self.init_param.CopyFrom(init_param)
        # DIRECT METHOD
        self.sds_path = init_param.params['sds_path']
        print 'path'
        self.sds_domain = init_param.params['sds_domain']
        print 'domain'
        self.db_host = init_param.params['db_host']
        print 'db_host'
        self.db_port = init_param.params["db_port"]
        print 'db_port'
        self.db_user = init_param.params['db_user']
        print 'db_user'
        self.db_pwd = init_param.params['db_pwd']
        print 'db_pwd'
        self.db_database = init_param.params['db_database']
        print 'db_database'
        self.db_table = init_param.params['db_table']
        print 'db_table'

        # CONNECT
        self.get_sds_server()
        print 'sds called'
        self.state = provider_pb2.DIAG_STATE_RUNNING
        # returns provider
        result = pool_pb2.DialogAgentProviderParam()
        result.CopyFrom(self.provider)
        print 'result called'
        return result

    def Terminate(self, empty, context):
        print 'Terminate', 'called'
        # DO NOTHING
        self.state = provider_pb2.DIAG_STATE_TERMINATED
        return empty_pb2.Empty()

    #
    # PROPERTY METHODS
    #
    def GetProviderParameter(self, empty, context):
        print 'GetProviderParameter', 'called'
        result = pool_pb2.DialogAgentProviderParam()
        result.CopyFrom(self.provider)
        return result

    def GetRuntimeParameters(self, empty, context):
        print 'GetRuntimeParameters', 'called'
        params = []
        result = provider_pb2.RuntimeParameterList()

        sds_path = provider_pb2.RuntimeParameter()
        sds_path.name = 'sds_path'
        sds_path.type = userattr_pb2.DATA_TYPE_STRING
        sds_path.desc = 'DM Path'
        sds_path.default_value = 'transaction_inquiry'
        sds_path.required = True
        params.append(sds_path)

        sds_domain = provider_pb2.RuntimeParameter()
        sds_domain.name = 'sds_domain'
        sds_domain.type = userattr_pb2.DATA_TYPE_STRING
        sds_domain.desc = 'DM Domain'
        sds_domain.default_value = 'transaction_inquiry'
        sds_domain.required = True
        params.append(sds_domain)

        db_host = provider_pb2.RuntimeParameter()
        db_host.name = 'db_host'
        db_host.type = userattr_pb2.DATA_TYPE_STRING
        db_host.desc = 'Database Host'
        db_host.default_value = '52.187.6.21'
        db_host.required = True
        params.append(db_host)

        db_port = provider_pb2.RuntimeParameter()
        db_port.name = 'db_port'
        db_port.type = userattr_pb2.DATA_TYPE_STRING
        db_port.desc = 'Database Port'
        db_port.default_value = '3306'
        db_port.required = True
        params.append(db_port)

        db_user = provider_pb2.RuntimeParameter()
        db_user.name = 'db_user'
        db_user.type = userattr_pb2.DATA_TYPE_STRING
        db_user.desc = 'Database User'
        db_user.default_value = 'tutor'
        db_user.required = True
        params.append(db_user)

        db_pwd = provider_pb2.RuntimeParameter()
        db_pwd.name = 'db_pwd'
        db_pwd.type = userattr_pb2.DATA_TYPE_STRING
        db_pwd.desc = 'Database Password'
        db_pwd.default_value = 'ggoggoma'
        db_pwd.required = True
        params.append(db_pwd)

        db_database = provider_pb2.RuntimeParameter()
        db_database.name = 'db_database'
        db_database.type = userattr_pb2.DATA_TYPE_STRING
        db_database.desc = 'Database Database name'
        db_database.default_value = 'hana'
        db_database.required = True
        params.append(db_database)

        db_table = provider_pb2.RuntimeParameter()
        db_table.name = 'db_table'
        db_table.type = userattr_pb2.DATA_TYPE_STRING
        db_table.desc = 'Database table'
        db_table.default_value = 'transaction'
        db_table.required = True
        params.append(db_table)

        result.params.extend(params)

        return result

    def GetUserAttributes(self, empty, context):
        print 'GetUserAttributes', 'called'
        result = userattr_pb2.UserAttributeList()
        #
        #  if not required, block the following lines
        #
        transaction = userattr_pb2.UserAttribute()
        transaction.name = 'transaction'
        # 이 이름은 DialogAgentProviderParam의 user_privacy_attributes에
        # 정의한 이름과 일치해야 한다.
        # 이 속성은 사용자의 기본 DB 외에 정의된 속성 외에 추가적으로 필요한
        # 속성을 정의하는 것입니다.
        transaction.title = '전화번호'
        transaction.type = userattr_pb2.DATA_TYPE_STRING
        transaction.desc = '기본 전화번호'
        result.attrs.extend([transaction])

        return result

    #
    # DIALOG METHODS
    #
    def Talk(self, talk, context):
        self.get_sds_server()
        session_id = talk.session_id
        print "Session ID : " + str(session_id)
        print "[Question] ", talk.text
        print "user_key : " + str(talk.user_key)
        print "user_phone" + str(talk.user_attributes.get('transaction'))

        # Create DialogSessionKey & set session_key
        dsk = sds_pb2.DialogueSessionKey()
        dsk.session_key = session_id
        print 'dsk==>', dsk
        # Dialog Open
        sds_session = self.sds_stub.Open(dsk)
        print 'sds_session==>', sds_session
        sq = sds_pb2.SdsQuery()
        sq.session_key = sds_session.session_key
        sq.utter = talk.text
        print 'sq==>', sq
        print 'sq.utter==>', sq.utter
        # Dialog UnderStand
        sds_act = self.sds_stub.Understand(sq)
        print 'sds_act==>', sds_act
        # DB Connection
        conn = pymysql.connect(user=self.db_user,
                               password=self.db_pwd,
                               host=self.db_host,
                               database=self.db_database,
                               charset='utf8',
                               use_unicode=False)
        curs = conn.cursor(pymysql.cursors.DictCursor)

        # Create SdsSlots & set Session Key
        sds_slots = sds_pb2.SdsSlots()
        sds_slots.session_key = sds_session.session_key
        print 'sds_slots==>', sds_slots
        user_phone = '01000000000'
        if talk.user_attributes.get('transaction') is not None:
            user_phone = str(talk.user_attributes.get('transaction'))

        for k, v in sds_act.filled_slots.items():
            sds_slots.slots[k] = v
        cnt = 0
        if len(sds_act.filled_slots.keys()) > 0:
            for i in range(0, len(sds_act.filled_slots.keys())):
                if sds_act.filled_slots.keys()[i] == 'cnt':
                    cnt = int(sds_act.filled_slots.get('cnt'))
        print 'cnt', cnt, type(cnt)

        # Dialog Act
        best_slu = sds_act.origin_best_slu
        dialog_act = best_slu[best_slu.find('#') + 1:best_slu.find('(')]
        print 'dialog_act : ' + dialog_act

        val_month = None
        val_day = None
        phone = []
        date = []
        category = []
        amount = []
        abstract = []
        balance = []
        account = ''

        if sds_act.filled_slots.get('month') is not None:
            val_month = sds_act.filled_slots.get('month').replace('월', '')
            print "[val_month]" + val_month
        if sds_act.filled_slots.get('day') is not None:
            val_day = sds_act.filled_slots.get('day').replace('일', '')
            print "[val_day]" + val_day

        #default_time = datetime.now(timezone('Asia/Seoul'))
        #print 'default_time', default_time
        if val_month and val_day is not None:
            val_date = val_month + '-' + val_day
            print 'val_date', val_date

        if dialog_act == 'negate':
            talk_res = provider_pb2.TalkResponse()
            output = '이용해 주셔서 감사합니다.'
            talk_res.text = output
            talk_res.state = provider_pb2.DIAG_CLOSED
            self.sds_stub.Close(dsk)
            return talk_res

        elif dialog_act == 'request' or dialog_act == 'inform':
            print '=================='
            if val_month is None and val_day is None:
                query = 'select * from ' + self.db_table + ' where phone=%s order by date desc limit 5'
                curs.execute(query, user_phone)
                result = curs.fetchall()
            elif val_month is not None and val_day is not None:
                #query = "select * from ' + self.db_table + 'where phone=%s and timestamp '2017-" + val_month + val_day + " 00:00:00' and 2017-" + val_month + val_day + " 23:59:59"
                query = "select * from " + self.db_table + " where phone=%s and date like %s order by date desc"
                curs.execute(query, (user_phone, "%"+val_date+"%"))
                result = curs.fetchall()
            print 'curs.rowcount', curs.rowcount
            cr = curs.rowcount
            print 'cr', cr

            if curs.rowcount != 0:
                for row in result:
                    for k in row.keys():
                        if str(row.get('date')).find('2017-') >= 0:
                            if k == 'phone':
                                phone.append(row.get(k))
                            elif k == 'account':
                                account = row.get(k)
                            elif k == 'balance':
                                if row.get('balance') is None:
                                    abstract.append('')
                                else:
                                    balance.append(row.get(k))
                            elif k == 'date':
                                if row.get('date') is None:
                                    abstract.append('')
                                else:
                                    date.append(str(row.get(k)))
                            elif k == 'category':
                                if row.get('category') is None:
                                    abstract.append('')
                                else:
                                    category.append(row.get(k))
                            elif k == 'amount':
                                if row.get('amount') is None:
                                    abstract.append('')
                                else:
                                    amount.append(str(row.get(k)))
                            elif k == 'abstract':
                                if row.get('abstract') is None:
                                    abstract.append('')
                                else:
                                    abstract.append(row.get(k))

                print 'phone', phone
                print 'account', account
                print 'balance', balance
                print 'date', date
                print 'category', category
                print 'amount', amount
                print 'abstract', abstract

                if not phone and not account and not balance and not date and not category and not amount and not abstract:
                    output = '이용하신 내역이 없습니다.[END]'
                else:
                    output = '[텍스트 뱅킹]\n' + account + '\n' + '[잔액] ' + str(balance[0]) + '원\n'
                    if cr == 5:
                        for i in range(0, 5):
                            output += paint(i)
                            output += ' ' + date[i] + '\n' + category[i] + ' ' + amount[i] + '원 ' + abstract[i] + '\n'
                        output += '[END]'
                    elif cr-cnt > 5:
                        for i in range(cnt, cnt+5):
                            output += paint(i-cnt)
                            output += ' ' + date[i] + '\n' + category[i] + ' ' + amount[i] + '원 ' + abstract[i] + '\n'
                        output += '추가로 더 내역조회를 원하시나요?'
                    elif cr-cnt < 5:
                        for i in range(cnt, cr):
                            output += paint(i-cnt)
                            output += ' ' + date[i] + '\n' + category[i] + ' ' + amount[i] + '원 ' + abstract[i] + '\n'
                        output += '[END]'
            elif curs.rowcount == 0:
                output = '이용하신 내역이 없습니다.[END]'
        print 'output', output

        sds_slots.slots['output'] = output
        curs.close()
        conn.close()
        # Send result slot & Get response
        sdsUtter = self.sds_stub.FillSlots(sds_slots)
        print "[System output] " + sdsUtter.response
        talk_res = provider_pb2.TalkResponse()
        talk_res.text = sdsUtter.response

        if str(talk_res.text[-6:-1]) == '[END]':
            talk_res.state = provider_pb2.DIAG_CLOSED
            self.sds_stub.Close(dsk)
            talk_res.text = talk_res.text[:-7]
            print 'talk_res.talk = ', talk_res.text
        return talk_res

    def Close(self, req, context):
        print 'Closing for ', req.session_id, req.agent_key
        talk_stat = provider_pb2.TalkStat()
        talk_stat.session_key = req.session_id
        talk_stat.agent_key = req.agent_key

        ses = sds_pb2.DialogueSessionKey()
        ses.session_key = req.session_id
        self.sds_stub.Close(ses)

        return talk_stat

def serve():
    parser = argparse.ArgumentParser(description='transactionInquiry DA')
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        required=True,
                        type=int,
                        help='port to access server')
    args = parser.parse_args()

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    provider_pb2.add_DialogAgentProviderServicer_to_server(
        transactionInquiry(), server)

    listen = '[::]' + ':' + str(args.port)
    server.add_insecure_port(listen)

    server.start()
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)

if __name__ == '__main__':
    serve()
