#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from datetime import datetime, timedelta
import time
from datetime import date

reload(sys)
sys.setdefaultencoding('utf-8')

from concurrent import futures
import time
import argparse
import grpc
from google.protobuf import empty_pb2
import pymysql
import os

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(lib_path)

from proto import pool_pb2
from proto import sds_pb2
from proto import provider_pb2
from proto import userattr_pb2

# import MySQLdb.cursors

_ONE_DAY_IN_SECONDS = 60 * 60 * 24
def yesterday_change_date():
    print '[change_day in]'
    # today = datetime.today()
    # yesterday = today - timedelta(days=1)
    yesterday = date.fromtimestamp(time.time() - 60 * 60 * 24)
    change_yesterday = yesterday.strftime("%m")
    # change_time = datetime.today().strftime("%m/%d, %H:%M")
    print change_yesterday
    return str(change_yesterday)
def today_change_date():
    print '[change_day in]'
    today = datetime.today()
    # yesterday = today - timedelta(days=1)
    # yesterday = date.fromtimestamp(time.time() - 60 * 60 * 24)
    # change_yesterday = yesterday.strftime("%m")
    change_time = datetime.today().strftime("%m")
    print change_time
    return str(change_time)

class card_payment_inquiry_DA(provider_pb2.DialogAgentProviderServicer):
    # STATE
    # state = provider_pb2.DIAG_STATE_IDLE
    init_param = provider_pb2.InitParameter()

    # PROVIDER
    provider = pool_pb2.DialogAgentProviderParam()
    provider.name = 'card_payment_inquiry'
    provider.description = 'Hi, card_payment_inquiry dialog agent'
    provider.version = '0.1'
    provider.single_turn = False
    provider.agent_kind = pool_pb2.AGENT_SDS
    provider.require_user_privacy = True
    provider.user_privacy_attributes.extend(["phone"])

    # PARAMETER
    sds_path = 'card_payment_inquiry'
    sds_domain = 'card_payment_inquiry'
    # SDS Stub
    sds_server_addr = ''
    sds_stub = None

    def __init__(self):
        self.state = provider_pb2.DIAG_STATE_IDLE

    #
    # INIT or TERM METHODS
    #
    def GetUserAttributes(self, empty, context):
        print 'GetUserAttributes', 'called'
        result = userattr_pb2.UserAttributeList()
        #
        #  if not required, block the following lines
        #
        phone = userattr_pb2.UserAttribute()
        print 'phone==>', phone
        phone.name = 'location'
        # 이 이름은 DialogAgentProviderParam의 user_privacy_attributes에
        # 정의한 이름과 일치해야 한다.
        # 이 속성은 사용자의 기본 DB 외에 정의된 속성 외에 추가적으로 필요한
        # 속성을 정의하는 것입니다.
        phone.title = '휴대폰 번호'
        phone.type = userattr_pb2.DATA_TYPE_STRING
        phone.desc = '휴대폰 번호를 입력해주세요.(ex 000-0000-0000)'
        result.attrs.extend([phone])

    def get_sds_server(self):
        sds_channel = grpc.insecure_channel(self.init_param.sds_remote_addr)
        # sds_channel = grpc.insecure_channel('127.0.0.1:9906')
        resolver_stub = sds_pb2.SpokenDialogServiceResolverStub(sds_channel)

        print 'stub'
        sq = sds_pb2.ServiceQuery()
        sq.path = self.sds_path
        sq.name = self.sds_domain
        print sq.path, sq.name

        svc_loc = resolver_stub.Find(sq)
        print 'find result', svc_loc
        # Create SpokenDialogService Stub
        print 'find result loc: ', svc_loc.server_address
        self.sds_stub = sds_pb2.SpokenDialogServiceStub(
            grpc.insecure_channel(svc_loc.server_address))
        self.sds_server_addr = svc_loc.server_address
        print 'stub sds ', svc_loc.server_address

    def IsReady(self, empty, context):
        print 'IsReady', 'called'
        status = provider_pb2.DialogAgentStatus()
        status.state = self.state
        return status

    def Init(self, init_param, context):
        print 'Init', 'called'
        self.state = provider_pb2.DIAG_STATE_INITIALIZING
        # COPY ALL
        self.init_param.CopyFrom(init_param)
        # DIRECT METHOD
        self.sds_path = init_param.params['sds_path']
        print 'path'
        self.sds_domain = init_param.params['sds_domain']
        print 'domain'

        self.db_host = init_param.params['db_host']
        print 'db_host'

        self.db_port = init_param.params["db_port"]
        print 'db_port'

        self.db_user = init_param.params['db_user']
        print 'db_user'

        self.db_pwd = init_param.params['db_pwd']
        print 'db_pwd'

        self.db_database = init_param.params['db_database']
        print 'db_database'

        self.db_table = init_param.params['db_table']
        print 'db_table'
        # CONNECT
        self.get_sds_server()
        print 'sds called'
        self.state = provider_pb2.DIAG_STATE_RUNNING
        # returns provider
        result = pool_pb2.DialogAgentProviderParam()
        result.CopyFrom(self.provider)
        print 'result called'
        return result

    def Terminate(self, empty, context):
        print 'Terminate', 'called'
        # DO NOTHING
        self.state = provider_pb2.DIAG_STATE_TERMINATED
        return empty_pb2.Empty()

    #
    # PROPERTY METHODS
    #

    def GetProviderParameter(self, empty, context):
        print 'GetProviderParameter', 'called'
        result = pool_pb2.DialogAgentProviderParam()
        result.CopyFrom(self.provider)
        return result

    def GetRuntimeParameters(self, empty, context):
        print 'GetRuntimeParameters', 'called'
        params = []
        result = provider_pb2.RuntimeParameterList()

        sds_path = provider_pb2.RuntimeParameter()
        sds_path.name = 'sds_path'
        sds_path.type = userattr_pb2.DATA_TYPE_STRING
        sds_path.desc = 'DM Path'
        sds_path.default_value = 'card_payment_inquiry'
        sds_path.required = True
        params.append(sds_path)

        sds_domain = provider_pb2.RuntimeParameter()
        sds_domain.name = 'sds_domain'
        sds_domain.type = userattr_pb2.DATA_TYPE_STRING
        sds_domain.desc = 'DM Domain'
        sds_domain.default_value = 'card_payment_inquiry'
        sds_domain.required = True
        params.append(sds_domain)

        db_host = provider_pb2.RuntimeParameter()
        db_host.name = 'db_host'
        db_host.type = userattr_pb2.DATA_TYPE_STRING
        db_host.desc = 'Database Host'
        db_host.default_value = '52.187.6.21'
        db_host.required = True
        params.append(db_host)

        db_port = provider_pb2.RuntimeParameter()
        db_port.name = 'db_port'
        db_port.type = userattr_pb2.DATA_TYPE_STRING
        db_port.desc = 'Database Port'
        db_port.default_value = '3306'
        db_port.required = True
        params.append(db_port)

        db_user = provider_pb2.RuntimeParameter()
        db_user.name = 'db_user'
        db_user.type = userattr_pb2.DATA_TYPE_STRING
        db_user.desc = 'Database User'
        db_user.default_value = 'tutor'
        db_user.required = True
        params.append(db_user)

        db_pwd = provider_pb2.RuntimeParameter()
        db_pwd.name = 'db_pwd'
        db_pwd.type = userattr_pb2.DATA_TYPE_STRING
        db_pwd.desc = 'Database Password'
        db_pwd.default_value = 'ggoggoma'
        db_pwd.required = True
        params.append(db_pwd)

        db_database = provider_pb2.RuntimeParameter()
        db_database.name = 'db_database'
        db_database.type = userattr_pb2.DATA_TYPE_STRING
        db_database.desc = 'Database Database name'
        db_database.default_value = 'hana'
        db_database.required = True
        params.append(db_database)

        db_table = provider_pb2.RuntimeParameter()
        db_table.name = 'db_table'
        db_table.type = userattr_pb2.DATA_TYPE_STRING
        db_table.desc = 'Database table'
        db_table.default_value = 'card_payment_inquiry'
        db_table.required = True
        params.append(db_table)

        result.params.extend(params)
        return result

    def Talk(self, talk, context):
        # meta_data = {"out.embed.type": "", "out.embed.data.body": ""}
        flag = False
        self.get_sds_server()
        print "Talk Func Start...."
        user_phone = ''  # 번호는 임의 생성
        user_name = ''
        month = ''
        due_date = ''
        user_amount = ''
        print '[talk 시작 부분]', user_phone, user_name, month, due_date, user_amount

        session_id = talk.session_id
        print "Session ID : " + str(session_id)  # 해당 session id
        print "[Question] ", talk.text  # talk.text -> 질문 내역
        print "Access_from", str(talk.access_from)  # 접속 경로
        print "user_attributes" + str(talk.user_attributes.get('phone'))        # 사용자 휴대폰 번호
        #
        # STEP #1
        #

        # Create DialogSessionKey & set session_key
        dsk = sds_pb2.DialogueSessionKey()
        dsk.session_key = session_id

        # Dialog Open
        sds_session = self.sds_stub.Open(dsk)
        print 'Dialog Open..........'

        sq = sds_pb2.SdsQuery()
        sq.session_key = sds_session.session_key
        sq.utter = talk.text

        # Dialog UnderStand
        sds_act = self.sds_stub.Understand(sq)

        #
        # STEP2
        #
        # DB Connection
        conn = pymysql.connect(user=self.db_user,
                               password=self.db_pwd,
                               host=self.db_host,
                               database=self.db_database,
                               charset='utf8',
                               use_unicode=False)
        curs = conn.cursor()

        # Create SdsSlots & set Session Key
        sds_slots = sds_pb2.SdsSlots()
        sds_slots.session_key = sds_session.session_key

        # Copy filled_slot to result Slot & Fill information slots
        for k, v in sds_act.filled_slots.items():  # sds_act.filled_slots == user에 의해서 채워진 slot
            sds_slots.slots[k] = v
        # Dialog Act
        best_slu = sds_act.origin_best_slu
        print 'best_slu : ' + best_slu
        dialog_act = best_slu[best_slu.find('#') + 1:best_slu.find('(')]
        print 'dialog_act : ' + dialog_act

        if talk.user_attributes.get('phone') is not None:
            print 'user_phone'
            user_phone = str(talk.user_attributes.get('phone'))
        else:
            user_phone = '01050589102'    # 사용자 정보 default

        output = 'Dialog Agent 오류 메시지 입니다.'
        # if 1:
        try:
            #
            # STEP3
            #
            print 'in'
            if dialog_act == 'inquiry':
                print 'user_phone', user_phone
                print 'yesterday', yesterday_change_date()
                query = "SELECT due_date, amount, name " \
                        "FROM " + self.db_table + "" \
                                                  " WHERE phone=%s and month=%s"
                curs.execute(query, (user_phone, yesterday_change_date()))    # 휴대폰 번호로 잔액 조회
                rows = curs.fetchall()
                if curs.rowcount != 0:
                    user_due_date = rows[0][0]
                    user_amount = rows[0][1]  # 대금(int형)
                    user_name = rows[0][2]
                    print '카드 대금', user_amount
                    output = today_change_date() + '월 카드대금, ' + user_name + '님의 ' + yesterday_change_date() + '월 카드 결제 금액은 ' + str(user_amount) + '원입니다. ' + user_due_date + '일 인출 예정입니다.'
                    sds_slots.slots['output'] = output
                else:
                    sds_slots.slots['output'] = '조회된 정보가 없습니다.'
            else:
                sds_slots.slots['output'] = '다른 분류값으로 떨어졌습니다.'
        except:
            flag = True
            print 'Dialog Agent 오류 메시지 입니다.'

        # Sedn result slot & Get response
        sdsUtter = self.sds_stub.FillSlots(sds_slots)
        #
        # STEP4
        #
        if flag == True:
            sdsUtter.response = output
            # print "[System output] " + sdsUtter.response

        print "[System output] " + sdsUtter.response
        talk_res = provider_pb2.TalkResponse()
        talk_res.text = sdsUtter.response

        if str(talk_res.text[-6:-1]) == '[END]':
            talk_res.state = provider_pb2.DIAG_CLOSED
            self.sds_stub.Close(dsk)
            talk_res.text = talk_res.text[:-7]
            print 'talk_res.talk = ', talk_res.text
        curs.close()
        conn.close()
        # talk_res.state = provider_pb2.DIAG_CLOSED
        # self.sds_stub.Close(dsk)

        return talk_res

    def Close(self, req, context):
        print 'Closing for ', req.session_id, req.agent_key
        talk_stat = provider_pb2.TalkStat()
        talk_stat.session_key = req.session_id
        talk_stat.agent_key = req.agent_key

        ses = sds_pb2.DialogueSessionKey()
        ses.session_key = req.session_id
        self.sds_stub.Close(ses)
        return talk_stat

def serve():
    parser = argparse.ArgumentParser(description='card_payment_inquiry DA')
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        required=True,
                        type=int,
                        help='port to access server')
    args = parser.parse_args()

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    provider_pb2.add_DialogAgentProviderServicer_to_server(card_payment_inquiry_DA(),
                                                           server)

    listen = '[::]' + ':' + str(args.port)
    server.add_insecure_port(listen)

    server.start()
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    serve()
