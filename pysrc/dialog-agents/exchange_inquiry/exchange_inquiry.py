#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from concurrent import futures
import argparse
import grpc
from google.protobuf import empty_pb2
import pymysql
import os
from urllib2 import Request, urlopen
import json
from datetime import datetime, timedelta
import time
from datetime import date
from pytz import timezone

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(lib_path)

from proto import pool_pb2
from proto import sds_pb2
from proto import provider_pb2
from proto import userattr_pb2

reload(sys)
sys.setdefaultencoding('utf-8')

_ONE_DAY_IN_SECONDS = 60 * 60 * 24
befor_exchange_en = ''
after_exchange_en = ''

def today_change_date():
    print '[change_day in]'
    today = datetime.today()
    change_time = datetime.today().strftime("%Y")
    print change_time
    return str(change_time)
def yesterday_change_day(count):   # 날짜 변환
    print '[change_day in]'
    print 'count', count
    today = datetime.today()
    yesterday = today - timedelta(days=count)
    change_yesterday = yesterday.strftime("%Y-%m-%d")
    return str(change_yesterday)


def exchange(value, after_exchange_en, key):
    currencies = after_exchange_en
    source = 'USD'
    format = '1'
    url = 'http://apilayer.net/api/live'
    url += '?access_key=' + key
    url += '&currencies=' + currencies
    url += '&suource=' + source
    url += '&format=' + format
    print url
    # standard_quote = []
    request = Request(url)
    request.get_method = lambda: 'GET'
    response_body = urlopen(request).read()

    ex_dict = response_body.replace("\n", "")
    json_dict = json.loads(ex_dict)
    # print ex_dict

    change_value = json_dict['quotes'].get(
        source + after_exchange_en)
    output = change_value
    result = float(value) * (1 / output)
    # print value, after_exchange_en, ' 은 ', result, 'USD 입니다.'
    print result
    return result

"""
def exchange(value, after_exchange_en):
           d43473d868569461e81bb167659d4b7d
    key = 'd43473d868569461e81bb167659d4b7d'
    currencies = after_exchange_en
    source = 'USD'
    format = '1'
    url = 'http://apilayer.net/api/live'
    url += '?access_key=' + key
    url += '&currencies=' + currencies
    url += '&suource=' + source
    url += '&format=' + format
    print url
    # standard_quote = []
    request = Request(url)
    request.get_method = lambda: 'GET'
    response_body = urlopen(request).read()

    ex_dict = response_body.replace("\n", "")
    json_dict = json.loads(ex_dict)
    # print ex_dict

    change_value = json_dict['quotes'].get(
        source + after_exchange_en)
    output = change_value
    result = float(value) * (1 / output)
    # print value, after_exchange_en, ' 은 ', result, 'USD 입니다.'
    print result
    return result
"""


class ExchangeDA(provider_pb2.DialogAgentProviderServicer):
    # STATE
    # state = provider_pb2.DIAG_STATE_IDLE
    init_param = provider_pb2.InitParameter()

    # PROVIDER
    provider = pool_pb2.DialogAgentProviderParam()
    provider.name = 'exchange_inquiry'
    provider.description = 'Hi, exchange dialog agent'
    provider.version = '0.1'
    provider.single_turn = False
    provider.agent_kind = pool_pb2.AGENT_SDS
    provider.require_user_privacy = True
    # PARAMETER

    # SDS Stub
    sds_server_addr = ''
    sds_stub = None

    def __init__(self):
        self.state = provider_pb2.DIAG_STATE_IDLE

    #
    # INIT or TERM METHODS
    #

    def GetUserAttributes(self, empty, context):
        print 'GetUserAttributes', 'called'
        result = userattr_pb2.UserAttributeList()
        #
        #  if not required, block the following lines
        #
        return result

    def get_sds_server(self):
        sds_channel = grpc.insecure_channel(self.init_param.sds_remote_addr)
        # sds_channel = grpc.insecure_channel('127.0.0.1:9906')
        resolver_stub = sds_pb2.SpokenDialogServiceResolverStub(sds_channel)

        print 'stub'
        sq = sds_pb2.ServiceQuery()
        sq.path = self.sds_path
        sq.name = self.sds_domain
        print sq.path, sq.name

        svc_loc = resolver_stub.Find(sq)
        print 'find result', svc_loc
        # Create SpokenDialogService Stub
        print 'find result loc: ', svc_loc.server_address
        self.sds_stub = sds_pb2.SpokenDialogServiceStub(
            grpc.insecure_channel(svc_loc.server_address))
        self.sds_server_addr = svc_loc.server_address
        print 'stub sds ', svc_loc.server_address

    def IsReady(self, empty, context):
        print 'IsReady', 'called'
        status = provider_pb2.DialogAgentStatus()
        status.state = self.state
        return status

    def Init(self, init_param, context):
        print 'Init', 'called'
        self.state = provider_pb2.DIAG_STATE_INITIALIZING
        # COPY ALL
        self.init_param.CopyFrom(init_param)
        # DIRECT METHOD
        self.sds_path = init_param.params['sds_path']
        print 'path'

        self.sds_domain = init_param.params['sds_domain']
        print 'domain'

        self.db_host = init_param.params['db_host']
        print 'db_host'

        self.db_port = init_param.params["db_port"]
        print 'db_port'

        self.db_user = init_param.params['db_user']
        print 'db_user'

        self.db_pwd = init_param.params['db_pwd']
        print 'db_pwd'

        self.db_database = init_param.params['db_database']
        print 'db_database'

        self.db_table = init_param.params['db_table']
        print 'db_table'

        self.apikey = init_param.params['apikey']
        print 'apikey'

        # CONNECT
        self.get_sds_server()
        print 'sds called'
        self.state = provider_pb2.DIAG_STATE_RUNNING
        # returns provider
        result = pool_pb2.DialogAgentProviderParam()
        result.CopyFrom(self.provider)
        print 'result called'
        return result

    def Terminate(self, empty, context):
        print 'Terminate', 'called'
        # DO NOTHING
        self.state = provider_pb2.DIAG_STATE_TERMINATED
        return empty_pb2.Empty()

    #
    # PROPERTY METHODS
    #
    def GetProviderParameter(self, empty, context):
        print 'GetProviderParameter', 'called'
        result = pool_pb2.DialogAgentProviderParam()
        result.CopyFrom(self.provider)
        return result

    def GetRuntimeParameters(self, empty, context):
        print 'GetRuntimeParameters', 'called'
        params = []
        result = provider_pb2.RuntimeParameterList()

        sds_path = provider_pb2.RuntimeParameter()
        sds_path.name = 'sds_path'
        sds_path.type = userattr_pb2.DATA_TYPE_STRING
        sds_path.desc = 'DM Path'
        sds_path.default_value = 'exchange_inquiry'
        sds_path.required = True
        params.append(sds_path)

        sds_domain = provider_pb2.RuntimeParameter()
        sds_domain.name = 'sds_domain'
        sds_domain.type = userattr_pb2.DATA_TYPE_STRING
        sds_domain.desc = 'DM Domain'
        sds_domain.default_value = 'exchange_inquiry'
        sds_domain.required = True
        params.append(sds_domain)

        db_host = provider_pb2.RuntimeParameter()
        db_host.name = 'db_host'
        db_host.type = userattr_pb2.DATA_TYPE_STRING
        db_host.desc = 'Database Host'
        db_host.default_value = '52.187.6.21'
        db_host.required = True
        params.append(db_host)

        db_port = provider_pb2.RuntimeParameter()
        db_port.name = 'db_port'
        db_port.type = userattr_pb2.DATA_TYPE_STRING
        db_port.desc = 'Database Port'
        db_port.default_value = '3306'
        db_port.required = True
        params.append(db_port)

        db_user = provider_pb2.RuntimeParameter()
        db_user.name = 'db_user'
        db_user.type = userattr_pb2.DATA_TYPE_STRING
        db_user.desc = 'Database User'
        db_user.default_value = 'tutor'
        db_user.required = True
        params.append(db_user)

        db_pwd = provider_pb2.RuntimeParameter()
        db_pwd.name = 'db_pwd'
        db_pwd.type = userattr_pb2.DATA_TYPE_STRING
        db_pwd.desc = 'Database Password'
        db_pwd.default_value = 'ggoggoma'
        db_pwd.required = True
        params.append(db_pwd)

        db_database = provider_pb2.RuntimeParameter()
        db_database.name = 'db_database'
        db_database.type = userattr_pb2.DATA_TYPE_STRING
        db_database.desc = 'Database Database name'
        db_database.default_value = 'hana'
        db_database.required = True
        params.append(db_database)

        db_table = provider_pb2.RuntimeParameter()
        db_table.name = 'db_table'
        db_table.type = userattr_pb2.DATA_TYPE_STRING
        db_table.desc = 'Database table'
        db_table.default_value = 'exchange_inquiry'
        db_table.required = True
        params.append(db_table)

        result.params.extend(params)
        return result


    #
    # DIALOG METHODS
    #
    def Talk(self, talk, context):
        flag = False
        self.get_sds_server()
        session_id = talk.session_id
        print "talk content :", talk
        print "Session ID : " + str(session_id)
        print "[Question] ", talk.text
        print "access_from : " + str(talk.access_from)
        #
        # STEP #1
        #

        # Create DialogSessionKey & set session_key
        dsk = sds_pb2.DialogueSessionKey()
        dsk.session_key = session_id

        # Dialog Open
        sds_session = self.sds_stub.Open(dsk)

        sq = sds_pb2.SdsQuery()
        sq.session_key = sds_session.session_key
        sq.utter = talk.text

        # Dialog UnderStand
        sds_act = self.sds_stub.Understand(sq)

        #
        # STEP2
        #
        # DB Connection
        conn = pymysql.connect(user=self.db_user,
                               password=self.db_pwd,
                               host=self.db_host,
                               database=self.db_database,
                               charset='utf8',
                               use_unicode=False)
        curs = conn.cursor()
        # Create SdsSlots & set Session Key
        sds_slots = sds_pb2.SdsSlots()
        sds_slots.session_key = sds_session.session_key
        print sds_act
        # Copy filled_slot to result Slot & Fill information slots
        for k, v in sds_act.filled_slots.items():  # sds_act.filled_slots == user에 의해서 채워진 slot
            sds_slots.slots[k] = v
            print k, v
        # Dialog Act
        best_slu = sds_act.origin_best_slu
        print 'best_slu : ' + best_slu
        dialog_act = best_slu[best_slu.find('#') + 1:best_slu.find('(')]
        print 'dialog_act : ' + dialog_act

        # if 1:
        try:
            if sds_act.origin_best_slu.find('unknown') > 0:
                sub_cmd = 'unknown'
                print sub_cmd
                error = {[1, 2]: 'hi'}  # TypeError 생성
                print error
                flag == True
            # default value
            year = None
            month = None
            day = None
            time = None
            currency = None
            print sds_slots.slots
            print 'start'
            # 채워진 slot 확인 and 비어있는 slot에 대한 처리
            print sds_act.filled_slots.get('time')
            if sds_act.filled_slots.get('time') is not None:
                time = sds_act.filled_slots.get('time')
            else:           # 언제인지 정확하게 입력 안하여 최근으로 입력
                print time
                print sds_slots.slots.get('time')
                time = '최근'
            if sds_act.filled_slots.get('year') is not None:
                year = sds_act.filled_slots.get('year')
            else:           # 년을 입력하지 않았으면 최근으로 입력
                year = today_change_date()
            if sds_act.filled_slots.get('month') is not None:
                month = sds_act.filled_slots.get('month')
            # else:           # 월을 입력하지 않았음 오류
            #     flag = True
            if sds_act.filled_slots.get('day') is not None:
                day = sds_act.filled_slots.get('day')
            # else:           # 날짜을 입력하지 않았음 오류
            #    flag = True
            if sds_act.filled_slots.get('currency') is not None:
                currency = sds_act.filled_slots.get('currency')
            # else:           # 통화명을 입력하지 않았음 오류
            #    flag = True
            #    print '통화명을 입력해 주세요.'
            print '----------------------'
            print 'year', year
            print 'month', month
            print 'day', day
            print 'time', time
            print 'currency', currency
            print '----------------------'
            if flag == False:
                if dialog_act == 'exchange_inquiry':
                    print 'diaglog_act In', dialog_act
                    if year is not None and month is not None and day is not None:  # 정확한 날짜가 있으면
                        date = year + '-' + month + '-' + day
                        print 'year + month + day -> date', date
                        if currency is not None:                                        # 통화명이 있으면
                            query = "SELECT currency_rate, currency" \
                                    " FROM " + self.db_table + " " \
                                                               "WHERE date = %s and currency like %s order by date desc limit 1"
                            currency = currency.replace('"','')
                            currency = currency + '%'
                            print currency
                            curs.execute(query, (date, currency))
                            rows = curs.fetchall()
                            currenct_rate = str(rows[0][0])
                            currency = str(rows[0][1])
                            output = '환율 정보 (' + date + ') ' + currency + ' = ' + currenct_rate + ' WON'
                            print 'output', output
                            sds_slots.slots['output'] = output
                        else:                           # 통화명이 없으면 5개 출력
                            query = "SELECT currency_rate, currency" \
                                    " FROM " + self.db_table + " " \
                                                               "WHERE date = %s order by date desc limit 5"
                            curs.execute(query, date)
                            rows = curs.fetchall()
                            output = ''
                            for row in range(len(rows)):
                                print row
                                print rows
                                currenct_rate = str(rows[row][0])
                                currency = str(rows[row][1])
                                output += '환율 정보 (' + date + ') ' + currency + ' = ' + currenct_rate + ' WON\n'
                            print 'output', output
                            sds_slots.slots['output'] = output
                    elif time is not None:                                             # 오늘 , 어제 , 엊그제 등등 정확한 날짜가 없는 경우
                        print 'Time', time
                        if time == '어제' or time == '어제':                            # 오늘, 어제, 엊그제에 따른 '0000-00-00' 포멧으로 변환
                            date = yesterday_change_day(1)
                            print 'yesterday', date
                        elif time == '엊그제' or time == '엊그재':
                            date = yesterday_change_day(2)
                            print 'defore_yesterday', date
                        elif time == '오늘' or time == '현재' or time == '지금' or time == '최근':
                            date = yesterday_change_day(0)
                            print 'today', date
                        print 'Time is not None -> date', date
                        if currency is not None:                                        # 통화명이 있는 경우
                            print currency, type(currency)
                            query = "SELECT currency_rate, currency" \
                                    " FROM " + self.db_table + " " \
                                                               "WHERE date = %s and currency like %s order by date desc limit 1"
                            print query
                            print date
                            currency = currency.replace('"', '')
                            currency = currency + '%'
                            print currency
                            curs.execute(query, (str(date), str(currency)))
                            rows = curs.fetchall()
                            print rows
                            currenct_rate = str(rows[0][0])                             # rows[0][0] == 환율
                            currency = str(rows[0][1])                                  # rows[0][1] == 통화명
                            output = '환율 정보 (' + date + ') ' + currency + ' = ' + currenct_rate + ' WON\n'
                            sds_slots.slots['output'] = output
                        else:                                                           # 통화명이 없는 경우 최근 5개 통화 제공
                            query = "SELECT currency_rate, currency " \
                                    "FROM " + self.db_table + " " \
                                                              "WHERE date = %s order by date desc limit 5"
                            curs.execute(query, date)
                            rows = curs.fetchall()
                            output = ''
                            for row in range(len(rows)):
                                print row
                                print rows
                                currenct_rate = str(rows[row][0])
                                currency = str(rows[row][1])
                                output += '환율 정보 (' + date + ') ' + currency + ' = ' + currenct_rate + ' WON\n'
                            print 'output', output
                            sds_slots.slots['output'] = output
                else:
                    output = '잘못된 대화 의도 입니다.'
                    sds_slots.slots['output'] = output
            elif flag == True:
                output = '날짜 or 통화명 확인바랍니다'
                sds_slots.slots['output'] = output
        except:
            flag = True
            print 'Dialog Agent 오류 메시지 입니다.'


        #
        # STEP3
        #
        curs.close()
        conn.close()
        # Send result slot & Get response
        sds_utter = self.sds_stub.FillSlots(sds_slots)
        if flag == True:
            output = '서버와의 통신에 문제가 발생했습니다. 바로 조취하겠습니다.[END]'
            sds_utter.response = output
        #
        # STEP4
        #

        print "[System output] " + sds_utter.response
        talk_res = provider_pb2.TalkResponse()
        talk_res.text = sds_utter.response
        talk_res.text = talk_res.text[:-7]
        talk_res.state = provider_pb2.DIAG_CLOSED
        self.sds_stub.Close(dsk)
        return talk_res

    def Close(self, req, context):
        print 'Closing for ', req.session_id, req.agent_key
        talk_stat = provider_pb2.TalkStat()
        talk_stat.session_key = req.session_id
        talk_stat.agent_key = req.agent_key

        ses = sds_pb2.DialogueSessionKey()
        self.sds_stub.Close(ses)
        return talk_stat

def serve():
    parser = argparse.ArgumentParser(description='exchangeDA')
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        required=True,
                        type=int,
                        help='port to access server')
    args = parser.parse_args()

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    provider_pb2.add_DialogAgentProviderServicer_to_server(
        ExchangeDA(), server)

    listen = '[::]' + ':' + str(args.port)
    server.add_insecure_port(listen)
    server.start()
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    serve()
