#!/bin/python
# _*_ coding: utf-8 _*_
import sys

reload(sys)
sys.setdefaultencoding('utf-8')
from concurrent import futures
import argparse
import traceback
import grpc
import os
import time
import datetime
import syslog
import json
import requests
from collections import OrderedDict
import random

apiBaseUrl = 'http://localhost:9990/api/v3'

chatbot = 'default'

device_info = {
    'id': '',
    'type': 'default',
    'version': '1',
    'channel': 'default',
    'support': {
        'support_render_text': True,
        'support_render_card': True
    }
}

location_info = {
    'latitude': 37.568226,
    'longitude': 126.981618,
    'location': 'hanoe building'
}


def restPost(url, pp, hh):
    if not hh:
        hh = {'Content-Type': 'application/json;charset=utf-8',
              'm2u-auth-internal': 'm2u-auth-internal'}
    result = requests.post(url, json=pp, headers=hh)
    return result


def korJson(uniJson):
    changeJsonDump = json.dumps(uniJson, ensure_ascii=False)
    changeJson = json.dumps(changeJsonDump)
    return changeJson


def getDeviceId():
    return 'MRC#{}'.format(str(random.randint(10000000, 99999999)))


def m2uSignIn():
    sign_in_api = apiBaseUrl + "auth/signIn"
    signInBody = {'userKey': 'admin', 'passPhrase': '1234'}

    result = restPost(sign_in_api, signInBody, '')

    # print '====== m2uSignIn result ======
    # print result
    output = result.json()

    auth_token = output['directive']['payload']['authSuccess']['authToken']
    # print '====== authToken :', auth_token
    return auth_token


def m2uOpen(auth_token, device_id):
    open_api = apiBaseUrl + 'dialog/open'
    open_body = {
        'payload': {
            'utter': '',
            'chatbot': chatbot,
            'lang': 'ko_KR'
        },
        'device': device_info,
        'location': location_info,
        'authToken': ''
    }
    open_body['device']['id'] = device_id
    open_body['authToken'] = auth_token

    result = restPost(open_api, open_body, '')

    # print '====== m2uOpen result ======'
    # print result


def m2uTextToTextTalk(auth_token, talk_text, device_id):
    text_to_text_talk_api = apiBaseUrl + 'dialog/textToTextTalk'
    text_to_text_talk_body = {
        'payload': {
            'utter': '',
            'lang': 'ko_KR',
            'inputType': 'SPEECH',
            'altUtters': ['', '', ''],
            'meta': {
                'kca-qa': 'mrc'
            }
        },
        'device': device_info,
        'location': location_info,
        'authToken': ''
    }
    text_to_text_talk_body['payload']['utter'] = talk_text
    text_to_text_talk_body['device']['id'] = device_id
    text_to_text_talk_body['authToken'] = auth_token

    result = restPost(text_to_text_talk_api, text_to_text_talk_body, '')

    # change_output = korJson(result.json())
    change_output = result.json()

    return change_output


def m2uClose(auth_token, device_id):
    close_api = apiBaseUrl + 'dialog/close'
    close_body = {
        'device': device_info,
        'location': location_info,
        'authToken': ''
    }
    close_body['device']['id'] = device_id
    close_body['authToken'] = auth_token

    result = restPost(close_api, close_body, '')

    # print '====== m2uClose result ======'
    # print result


def recordResult(fields, result, result_qa_file):
    question = fields[0]
    exp_aid = fields[1]
    passage = fields[2]
    exp_answer = fields[3].strip()
    # print 'question', question
    # print 'exp_aid', exp_aid
    # print 'passage', passage
    # print 'exp_answer', exp_answer

    try:
        # Q, AID, AStringView, Answer
        # Q, eAID, AID, top5(3), AScore, eAnswer, Answer, (O/X), probability
        cards = result['directive']['payload']['response']['cards']
        answers = cards[0]['custom']['cardData']['answers']
        index = 1
        for answer in answers:
            answer_text = answer['text']
            # print '_____', index, '_____',
            # print 'AID:', answer['aid']
            # print 'AScore:', answer['aScore']
            print 'Answer:', answer_text
            # print 'Probability:', answer['prob']
            result_line = question + '\t'
            result_line += exp_aid + '\t'
            result_line += answer['aid'] + '\t'
            # result_line += str(index) + '\t'
            result_line += str(answer['aScore']) + '\t'
            result_line += exp_answer + '\t'
            result_line += answer_text + '\t'
            if exp_answer.find(answer_text) != -1 \
                    or answer_text.find(exp_answer) != -1:
                result_line += 'O\t'
            else:
                result_line += 'X\t'
            result_line += '\t'
            result_line += str(answer['prob']) + '\n'
            result_qa_file.write(result_line)
            index += 1
        if index == 1:
            print 'No Answer'
            result_line = question + '\t'
            result_line += exp_aid + '\t'
            result_line += '-\t'
            # result_line += '-\t'
            result_line += '-\t'
            result_line += exp_answer + '\t'
            result_line += '-\t'
            result_line += '-\t'
            result_line += '-\t'
            result_line += '-\n'
            result_qa_file.write(result_line)
    except Exception as e:
        traceback.print_exc(e)
        print 'response = ', json.dumps(result, indent=2, sort_keys=True)
        print '\n############## Test Stopped! ##############'
        sys.exit(1)


def run(target):
    test_q_file = open(target, 'r')
    timestamp = datetime.datetime.now().strftime('%Y%m%d_%H%M%S')
    result_file = 'result_QA_' + timestamp + '.txt'
    result_qa_file = open(result_file, 'w')
    cut_cnt = 0
    cnt = 0
    auth_token = ''
    device_id = ''

    print '############## Test Start ##############'
    start_time = time.time()
    while True:
        # SignIn & Open Session
        if cut_cnt == 0:
            auth_token = m2uSignIn()
            device_id = getDeviceId()
            print '====== Open Session ======'
            print ('--- device_id : {} ---'.format(device_id))
            m2uOpen(auth_token, device_id)

        # Get a Line & TextToTalk
        line = test_q_file.readline()
        if not line:
            print '====== No More Line. Close Session ======'
            m2uClose(auth_token, device_id)
            break
        test_fields = line.split('\t')
        if len(test_fields) != 4:
            print 'Line', cnt, 'Fileds are not 4.'
            sys.exit(1)
        talk_result = m2uTextToTextTalk(auth_token, test_fields[0], device_id)
        print '====== TextToTextTalk result ======'
        recordResult(test_fields, talk_result, result_qa_file)
        cut_cnt += 1
        cnt += 1

        # Close Session
        if cut_cnt >= 50:
            print '====== Close Session ======'
            m2uClose(auth_token, device_id)
            auth_token = ''
            device_id = ''
            cut_cnt = 0
    elapsed_time = time.time() - start_time
    print '############### Test End ###############'
    test_q_file.close()
    result_qa_file.close()

    print 'Total Count:', cnt
    print('Total Elapsed Time: {}s'.format(elapsed_time))
    print('Average Elapsed Time: {}s'.format(elapsed_time / cnt))
    print 'Result File ==>', result_file


if __name__ == '__main__':
    parse = argparse.ArgumentParser()
    parse.add_argument('test_q_file', metavar='Test File', type=str,
                       help='Assign a file to test')
    args = parse.parse_args()

    if os.environ.has_key('MAUM_ROOT') is False:
        print '>>> MAUM_ROOT is not defined'
        sys.exit(1)

    maum_root = os.environ.get('MAUM_ROOT')
    lib_path = os.path.realpath(maum_root + '/lib/python')
    sys.path.append(lib_path)

    run(args.test_q_file)
