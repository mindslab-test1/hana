create table NLPADM.BRAIN_BQA_CATEGORY
(
    ID          VARCHAR2(40 char)  not null
        primary key,
    CATE_NAME   VARCHAR2(40 char)  not null,
    CATE_PATH   VARCHAR2(200 char) not null,
    CREATED_AT  TIMESTAMP(6),
    CREATOR_ID  VARCHAR2(40 char),
    PAR_CATE_ID VARCHAR2(40 char),
    UPDATED_AT  TIMESTAMP(6),
    UPDATER_ID  VARCHAR2(40 char)
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.BRAIN_BQA_PRPRS_MAIN_WD
(
    ID         NUMBER(10)        not null
        primary key,
    CREATED_AT TIMESTAMP(6),
    CREATOR_ID VARCHAR2(40 char),
    UPDATED_AT TIMESTAMP(6),
    UPDATER_ID VARCHAR2(40 char),
    USE_YN     VARCHAR2(5 char)  not null,
    WORD       VARCHAR2(40 char) not null
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.BRAIN_BQA_PRPRS_WD
(
    ID         NUMBER(10)        not null
        primary key,
    CREATED_AT TIMESTAMP(6),
    CREATOR_ID VARCHAR2(40 char),
    WORD       VARCHAR2(40 char) not null
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.BRAIN_BQA_PRPRS_REL
(
    MAIN_ID       NUMBER(10) not null
        constraint FK5VNTG21LOI0UQUYSVDWJO4E3V
            references BRAIN_BQA_PRPRS_MAIN_WD,
    PARAPHRASE_ID NUMBER(10) not null
        constraint FKNWST6MRMFFUUCQ7T09I86BDC0
            references BRAIN_BQA_PRPRS_WD,
    primary key (MAIN_ID, PARAPHRASE_ID)
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.BRAIN_BQA_SKILL
(
    ID         NUMBER(10)        not null
        primary key,
    CREATED_AT TIMESTAMP(6),
    CREATOR_ID VARCHAR2(40 char),
    NAME       VARCHAR2(40 char) not null,
    UPDATED_AT TIMESTAMP(6),
    UPDATER_ID VARCHAR2(40 char)
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.BRAIN_BQA_ITEM
(
    ID               NUMBER(10)        not null
        primary key,
    ANSWER           CLOB,
    C_ID             NUMBER(10),
    CATEGORY         VARCHAR2(40 char)
        constraint FK163YU1FFCKPHXNV2VEAD351OT
            references BRAIN_BQA_CATEGORY,
    CHANNEL          VARCHAR2(40 char) not null
        constraint FK8X9GHAL4OOJSHMC627YPXBT87
            references BRAIN_BQA_CATEGORY,
    CONFIRM_DTM      TIMESTAMP(6),
    CONFIRMER        VARCHAR2(40 char),
    CREATED_AT       TIMESTAMP(6),
    CREATOR_ID       VARCHAR2(40 char),
    MAIN_YN          VARCHAR2(1 char),
    QNA_ID           VARCHAR2(255 char),
    QUESTION         CLOB,
    SKILL_ID         NUMBER(10)
        constraint FKMEM6D38WVYCEYF0USV40CFS5E
            references BRAIN_BQA_SKILL,
    SRC              VARCHAR2(20 char),
    SUB_CATEGORY     VARCHAR2(40 char)
        constraint FK59TU1UK0RBK57MYAPXCMHTIEL
            references BRAIN_BQA_CATEGORY,
    SUB_SUB_CATEGORY VARCHAR2(40 char)
        constraint FKDWO5GU4TYD7EXDDS6FDOOP0GC
            references BRAIN_BQA_CATEGORY,
    UPDATED_AT       TIMESTAMP(6),
    UPDATER_ID       VARCHAR2(40 char),
    USE_YN           VARCHAR2(1 char),
    WEIGHT           NUMBER(2, 1)
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_BQA_INDEXING_HISTORY
(
    ID         NUMBER(10) not null
        primary key,
    ADDRESS    VARCHAR2(255 char),
    CREATED_AT TIMESTAMP(6),
    CREATOR_ID VARCHAR2(40 char),
    FETCHED    NUMBER(10),
    MESSAGE    VARCHAR2(100 char),
    PROCESSED  NUMBER(10),
    STATUS     NUMBER(1),
    STOP_YN    NUMBER(1),
    TOTAL      NUMBER(10),
    TYPE       VARCHAR2(30 char),
    UPDATED_AT TIMESTAMP(6),
    UPDATER_ID VARCHAR2(40 char)
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_NQA_INDEX_HISTORY
(
    ID         NUMBER(10) not null
        primary key,
    ADDRESS    VARCHAR2(255 char),
    CREATED_AT TIMESTAMP(6),
    CREATOR_ID VARCHAR2(40 char),
    FETCHED    NUMBER(10),
    MESSAGE    VARCHAR2(100 char),
    PROCESSED  NUMBER(10),
    STATUS     NUMBER(1),
    STOP_YN    NUMBER(1),
    TOTAL      NUMBER(10),
    TYPE       VARCHAR2(30 char),
    UPDATED_AT TIMESTAMP(6),
    UPDATER_ID VARCHAR2(40 char)
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_ACCESS_TOKEN
(
	ID VARCHAR2(255 char) not null
		primary key,
	ACCESS_TOKEN_ID VARCHAR2(255 char),
	CREATED_AT TIMESTAMP(6),
	SCOPES VARCHAR2(255 char),
	TTL NUMBER(10),
	USER_ID VARCHAR2(255 char)
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_ACL
(
    ID             NUMBER(10) not null
        primary key,
    ACCESS_TYPE    VARCHAR2(512 char),
    MODEL          VARCHAR2(512 char),
    PERMISSION     VARCHAR2(512 char),
    PRINCIPAL_ID   VARCHAR2(512 char),
    PRINCIPAL_TYPE VARCHAR2(512 char),
    PROPERTY       VARCHAR2(512 char)
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_CODE
(
    ID         VARCHAR2(10 char) not null
        primary key,
    GROUP_CODE VARCHAR2(40 char) not null,
    NAME       VARCHAR2(40 char) not null
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_COMMIT_HISTORY
(
    ID           NUMBER(19)        not null
        primary key,
    FILE_ID      VARCHAR2(40 char) not null,
    TYPE         VARCHAR2(40 char) not null,
    WORKSPACE_ID VARCHAR2(40 char) not null
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_GROUP
(
    ID          NUMBER(10)         not null
        primary key,
    DESCRIPTION VARCHAR2(512 char),
    NAME        VARCHAR2(512 char) not null,
    OWNER_ID    VARCHAR2(255 char)
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_GROUP_MEMBER
(
    ID          NUMBER(10) not null
        primary key,
    DESCRIPTION VARCHAR2(512 char),
    GROUP_ID    NUMBER(10),
    ROLE        VARCHAR2(512 char),
    STATE       VARCHAR2(512 char),
    USER_ID     VARCHAR2(512 char)
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_NER_CORPUS
(
    ID         NUMBER(19)         not null
        primary key,
    CREATED_AT TIMESTAMP(6),
    SENTENCE   VARCHAR2(200 char) not null
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_NER_CORPUS_TAG
(
    ID         NUMBER(19)        not null
        primary key,
    CREATED_AT TIMESTAMP(6),
    TAG        VARCHAR2(50 char) not null,
    WORD       VARCHAR2(50 char) not null
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_PASSWORD_HISTORY
(
    ID           NUMBER(10) not null
        primary key,
    LAST_UPDATED TIMESTAMP(6),
    PASSWORD     VARCHAR2(512 char),
    USER_ID      NUMBER(10)
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_ROLE
(
    ID          NUMBER(10)         not null
        primary key,
    CREATED_AT  TIMESTAMP(6),
    DESCRIPTION VARCHAR2(512 char),
    NAME        VARCHAR2(255 char) not null,
    UPDATED_AT  TIMESTAMP(6)
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_ROLE_MAPPING
(
    	ID NUMBER(10) not null
		primary key,
	PRINCIPAL_ID VARCHAR2(255 char),
	PRINCIPAL_TYPE VARCHAR2(512 char),
	ROLE_ID NUMBER(10),
	ROLE_MAPPING_ID VARCHAR2(255 char)
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_SIGNIN_HISTORY
(
    ID         NUMBER(10) not null
        primary key,
    AGENT      VARCHAR2(512 char),
    CREATED_AT TIMESTAMP(6),
    MSG        VARCHAR2(512 char),
    REMOTE     VARCHAR2(512 char),
    SIGNED VARCHAR2(255 char),
    USER_NAME  VARCHAR2(512 char)
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_STT_TRAINING
(
    ID           VARCHAR2(40 char) not null
        primary key,
    STT_KEY      VARCHAR2(255 char),
    WORKSPACE_ID VARCHAR2(40 char) not null
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_TWE_INDEXING_HISTORY
(
    ID         NUMBER(10) not null
        primary key,
    CREATED_AT TIMESTAMP(6),
    DELETE_CNT NUMBER(10),
    INSERT_CNT NUMBER(10),
    LEAD_TIME  VARCHAR2(255 char),
    UPDATE_CNT NUMBER(10)
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_TWE_SCHEDULE
(
    SEQ    NUMBER(10) not null
        primary key,
    HOUR   VARCHAR2(10 char),
    MINUTE VARCHAR2(10 char),
    PERIOD VARCHAR2(10 char),
    TYPE   VARCHAR2(10 char)
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_USER
(
    ID           VARCHAR2(255 char) not null
        primary key,
    ACTIVATED    VARCHAR2(255 char),
    EMAIL        VARCHAR2(512 char) not null,
    LAST_UPDATED TIMESTAMP(6)       not null,
    PASSWORD     VARCHAR2(512 char) not null,
    USER_NAME    VARCHAR2(512 char)
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_MRC_DATA_GROUP
(
    ID              VARCHAR2(40 char) not null
        primary key,
    CREATED_AT      TIMESTAMP(6),
    CREATOR_ID      VARCHAR2(40 char)
        constraint FK1TW6V3QF4NEIDUS5ST433E8KO
            references MAI_USER,
    DATA_GROUP_NAME VARCHAR2(40 char) not null,
    UPDATED_AT      TIMESTAMP(6),
    UPDATER_ID      VARCHAR2(40 char)
        constraint FKPKU7SKWXP2Q4RFVM6B18CEJDD
            references MAI_USER,
    WORKSPACE_ID    VARCHAR2(40 char) not null
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_SDS_ENTITY
(
    ID            VARCHAR2(40 char)  not null
        primary key,
    CLASS_NAME    VARCHAR2(255 char) not null,
    CLASS_SOURCE  VARCHAR2(40 char)  not null,
    CREATED_AT    TIMESTAMP(6)       not null,
    CREATOR_ID    VARCHAR2(40 char)  not null
        constraint FKB2GOI0GJIUIQHGM2QO0RULA79
            references MAI_USER,
    ENTITY_NAME   VARCHAR2(255 char) not null,
    ENTITY_SOURCE VARCHAR2(40 char)  not null,
    ENTITY_TYPE   VARCHAR2(40 char)  not null,
    SHARED_YN     VARCHAR2(1 char)   not null,
    UPDATED_AT    TIMESTAMP(6)       not null,
    UPDATER_ID    VARCHAR2(40 char)  not null
        constraint FK186C7U4NB3LWE5VIUE70FGYY
            references MAI_USER,
    WORKSPACEID   VARCHAR2(40 char)  not null
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_SDS_ENTITY_DATA
(
    ID          VARCHAR2(40 char)  not null
        primary key,
    CREATED_AT  TIMESTAMP(6)       not null,
    CREATOR_ID  VARCHAR2(40 char)  not null
        constraint FKAT901LD6OT9X25CTMPO0WTNVH
            references MAI_USER,
    ENTITY_DATA VARCHAR2(255 char) not null,
    UPDATED_AT  TIMESTAMP(6)       not null,
    UPDATER_ID  VARCHAR2(40 char)  not null
        constraint FKS9XWRCK2G3524TY8GD20WTGUU
            references MAI_USER,
    ENTITY_ID   VARCHAR2(40 char)  not null
        constraint FKMEHB7OL923NF50HBV9U8QCO66
            references MAI_SDS_ENTITY
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_SDS_ENTITY_DATA_SYNONYM
(
    ID                  VARCHAR2(40 char)  not null
        primary key,
    CREATED_AT          TIMESTAMP(6)       not null,
    CREATOR_ID          VARCHAR2(40 char)  not null
        constraint FKHSTWT4SJGSQ2LA2R724IKR5IE
            references MAI_USER,
    ENTITY_DATA_SYNONYM VARCHAR2(255 char) not null,
    UPDATED_AT          TIMESTAMP(6)       not null,
    UPDATER_ID          VARCHAR2(40 char)  not null
        constraint FK5TLNSME7II0PB8Y7SFBNNHOWO
            references MAI_USER,
    ENTITY_DATA_ID      VARCHAR2(40 char)  not null
        constraint FKBDAUNQDT0UT6OH4C6EGKDR1RT
            references MAI_SDS_ENTITY_DATA
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_SDS_INTENT
(
    ID          VARCHAR2(40 char)  not null
        primary key,
    CREATED_AT  TIMESTAMP(6)       not null,
    CREATOR_ID  VARCHAR2(40 char)  not null
        constraint FKL4W8L6072TQQIGD11K8AJH4OQ
            references MAI_USER,
    INTENT_NAME VARCHAR2(255 char) not null,
    SHARED_YN   VARCHAR2(1 char)   not null,
    UPDATED_AT  TIMESTAMP(6)       not null,
    UPDATER_ID  VARCHAR2(40 char)  not null
        constraint FKDLJRSCR5PCL0SOUEJKA1X85P
            references MAI_USER,
    WORKSPACEID VARCHAR2(40 char)  not null
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_SDS_INTENT_UTTER
(
    ID              VARCHAR2(40 char) not null
        primary key,
    CREATED_AT      TIMESTAMP(6)      not null,
    CREATOR_ID      VARCHAR2(40 char) not null
        constraint FKK461AD9T61KENP0RTFUNCG7KO
            references MAI_USER,
    MAPPED_USER_SAY VARCHAR2(255 char),
    UPDATED_AT      TIMESTAMP(6)      not null,
    UPDATER_ID      VARCHAR2(40 char) not null
        constraint FKET5NOFRLMX76AVRPE4EAXQOJ7
            references MAI_USER,
    USER_SAY        VARCHAR2(255 char),
    INTENT_ID       VARCHAR2(40 char) not null
        constraint FKLO17KFX59DB839SIR1NFCFS8Q
            references MAI_SDS_INTENT
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_SDS_INTENT_UTTER_DATA
(
    ID              VARCHAR2(40 char) not null
        primary key,
    CREATED_AT      TIMESTAMP(6)      not null,
    CREATOR_ID      VARCHAR2(40 char) not null
        constraint FK5IF7PFI76Y9FVMXFAQ5IS86GV
            references MAI_USER,
    ENTITY_ID       VARCHAR2(40 char),
    MAPPED_VALUE    VARCHAR2(255 char),
    UPDATED_AT      TIMESTAMP(6)      not null,
    UPDATER_ID      VARCHAR2(40 char) not null
        constraint FK1IYFLTKJ4PRLXKJT098B9DDK7
            references MAI_USER,
    INTENT_UTTER_ID VARCHAR2(40 char) not null
        constraint FK8JYN9F460EXTLTUNMAIKKI1E1
            references MAI_SDS_INTENT_UTTER
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_SDS_MODEL
(
    ID             VARCHAR2(40 char)  not null
        primary key,
    CREATED_AT     TIMESTAMP(6)       not null,
    CREATOR_ID     VARCHAR2(40 char)  not null
        constraint FKFW4LBSPRRGTBY60NQX3BP3QJT
            references MAI_USER,
    DESCRIPTION    VARCHAR2(255 char),
    LAST_TRAIN_KEY VARCHAR2(40 char),
    MODEL_NAME     VARCHAR2(255 char) not null,
    STATUS         VARCHAR2(10 char)  not null,
    UPDATED_AT     TIMESTAMP(6)       not null,
    UPDATER_ID     VARCHAR2(40 char)  not null
        constraint FKGJB8PH1X5RG4QOHIBFR8M848P
            references MAI_USER,
    WORKSPACE_ID   VARCHAR2(40 char)  not null
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_SDS_MODEL_ENTITY
(
    MODEL_ID  VARCHAR2(40 char) not null
        constraint FKQVE9HMK4F4PE7J5K58FW3SCK3
            references MAI_SDS_MODEL,
    ENTITY_ID VARCHAR2(40 char) not null
        constraint FKM98OLPR6248N2G7ADO79C8FCH
            references MAI_SDS_ENTITY
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_SDS_MODEL_INTENT
(
    MODEL_ID  VARCHAR2(40 char) not null
        constraint FKPCTTIFRRVRMW5T119MO6NCX30
            references MAI_SDS_MODEL,
    INTENT_ID VARCHAR2(40 char) not null
        constraint FKNK3KLG0DM5X08JMS7UI2XYWTX
            references MAI_SDS_INTENT
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_SDS_TASK
(
    ID            VARCHAR2(40 char)  not null
        primary key,
    CREATED_AT    TIMESTAMP(6)       not null,
    CREATOR_ID    VARCHAR2(40 char)  not null
        constraint FK7BR4YGAYHWVKMLBVB1K2L3H10
            references MAI_USER,
    RESET_SLOT    VARCHAR2(1 char)   not null,
    TASK_GOAL     CLOB,
    TASK_LOCATION VARCHAR2(255 char),
    TASK_NAME     VARCHAR2(255 char) not null,
    TASK_TYPE     VARCHAR2(40 char)  not null,
    UPDATED_AT    TIMESTAMP(6)       not null,
    UPDATER_ID    VARCHAR2(40 char)  not null
        constraint FK7ELCP6Q0I3W49GWR3KGGMR33H
            references MAI_USER,
    WORKSPACE_ID  VARCHAR2(40 char)  not null,
    MODEL_ID      VARCHAR2(40 char)  not null
        constraint FK18RVW5IA7HHJ67GVMTBKUX6BX
            references MAI_SDS_MODEL
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_SDS_EVENT
(
    ID         VARCHAR2(40 char) not null
        primary key,
    CREATED_AT TIMESTAMP(6)      not null,
    CREATOR_ID VARCHAR2(40 char) not null
        constraint FKMUB7O6RYS6JQ3M5HIBRP7ES6Y
            references MAI_USER,
    INTENT_ID  VARCHAR2(40 char),
    UPDATED_AT TIMESTAMP(6)      not null,
    UPDATER_ID VARCHAR2(40 char) not null
        constraint FK1IT4QA626L9UTU4YQDFBS0YEJ
            references MAI_USER,
    TASK_ID    VARCHAR2(40 char) not null
        constraint FKNXMJCRB9DQA1260ATKJ2Y2IMY
            references MAI_SDS_TASK
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_SDS_EVENT_CONDITION
(
    ID              VARCHAR2(40 char) not null
        primary key,
    CREATED_AT      TIMESTAMP(6)      not null,
    CREATOR_ID      VARCHAR2(40 char) not null
        constraint FKL8054CBQ3RNWD1Q8EO8EDD621
            references MAI_USER,
    EVENT_ACTION    CLOB,
    EVENT_CONDITION CLOB,
    EXTRA_DATA      VARCHAR2(255 char),
    UPDATED_AT      TIMESTAMP(6)      not null,
    UPDATER_ID      VARCHAR2(40 char) not null
        constraint FKF7FPQA188PAA4OR8LII9ILAEY
            references MAI_USER,
    EVENT_ID        VARCHAR2(40 char) not null
        constraint FK37HWE3VPI0QQR5MTX13KAP7FL
            references MAI_SDS_EVENT
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_SDS_EVENT_UTTER
(
    ID                 VARCHAR2(40 char) not null
        primary key,
    CREATED_AT         TIMESTAMP(6)      not null,
    CREATOR_ID         VARCHAR2(40 char) not null
        constraint FKSWVH3VRWI3VAY2QE7PDDSCJAO
            references MAI_USER,
    EXTRA_DATA         VARCHAR2(255 char),
    INTENT_ID          VARCHAR2(40 char),
    UPDATED_AT         TIMESTAMP(6)      not null,
    UPDATER_ID         VARCHAR2(40 char) not null
        constraint FKSGHPO24YTTK95KVTPL02HSE9K
            references MAI_USER,
    EVENT_CONDITION_ID VARCHAR2(40 char) not null
        constraint FK61E3YXCJJVFX2HXTXQJISBHTU
            references MAI_SDS_EVENT_CONDITION
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_SDS_EVENT_UTTER_DATA
(
    ID               VARCHAR2(40 char) not null
        primary key,
    CREATED_AT       TIMESTAMP(6)      not null,
    CREATOR_ID       VARCHAR2(40 char) not null
        constraint FK7DVYQ55CNQ1XFFEB0NTK28PFA
            references MAI_USER,
    EVENT_UTTER_DATA VARCHAR2(255 char),
    UPDATED_AT       TIMESTAMP(6)      not null,
    UPDATER_ID       VARCHAR2(40 char) not null
        constraint FKDP661WA55EFNDKICIPWC0EUGM
            references MAI_USER,
    EVENT_UTTER_ID   VARCHAR2(40 char) not null
        constraint FKQK2BM328MWC12WR83U9QMVUSH
            references MAI_SDS_EVENT_UTTER
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_SDS_NEXT_TASK
(
    ID                  VARCHAR2(40 char) not null
        primary key,
    CONTROLLER          VARCHAR2(40 char) not null,
    CREATED_AT          TIMESTAMP(6)      not null,
    CREATOR_ID          VARCHAR2(40 char) not null
        constraint FK1YD8ISPTSXO366W1GLTFEX5YD
            references MAI_USER,
    NEXT_TASK_CONDITION CLOB,
    NEXT_TASK_ID        VARCHAR2(40 char) not null,
    UPDATED_AT          TIMESTAMP(6)      not null,
    UPDATER_ID          VARCHAR2(40 char) not null
        constraint FKBD3TUVWA2JGCCSH620XW09UHV
            references MAI_USER,
    TASK_ID             VARCHAR2(40 char) not null
        constraint FKBGR72LE7YEOL8J2VJIV932DP3
            references MAI_SDS_TASK
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_SDS_START_EVENT
(
    ID                    VARCHAR2(40 char) not null
        primary key,
    CREATED_AT            TIMESTAMP(6)      not null,
    CREATOR_ID            VARCHAR2(40 char) not null
        constraint FKC73MY0EP3YASU4TVVICXLCDNY
            references MAI_USER,
    START_EVENT_ACTION    CLOB,
    START_EVENT_CONDITION CLOB,
    UPDATED_AT            TIMESTAMP(6)      not null,
    UPDATER_ID            VARCHAR2(40 char) not null
        constraint FKN6N2PNRF5YTRVYFP3PUYRKIOQ
            references MAI_USER,
    TASK_ID               VARCHAR2(40 char) not null
        constraint FKLKFHVKV8Y7WQV1RALY0NS2A1H
            references MAI_SDS_TASK
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_SDS_START_EVENT_UTTER
(
    ID             VARCHAR2(40 char) not null
        primary key,
    CREATED_AT     TIMESTAMP(6)      not null,
    CREATOR_ID     VARCHAR2(40 char) not null
        constraint FKCSLHA0QAXUQODJVMB36UA4RC7
            references MAI_USER,
    EXTRA_DATA     VARCHAR2(255 char),
    INTENT_ID      VARCHAR2(40 char),
    UPDATED_AT     TIMESTAMP(6)      not null,
    UPDATER_ID     VARCHAR2(40 char) not null
        constraint FK1YLKHG3JH7DD1EMD7QSAGRJ5O
            references MAI_USER,
    START_EVENT_ID VARCHAR2(40 char) not null
        constraint FKL559VIO3T9WRPQXKQQTNKI2F0
            references MAI_SDS_START_EVENT
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_SDS_START_EVENT_UTTER_DATA
(
    ID                     VARCHAR2(40 char) not null
        primary key,
    CREATED_AT             TIMESTAMP(6)      not null,
    CREATOR_ID             VARCHAR2(40 char) not null
        constraint FKGN5NVQDTDS9A16GAJHHWXTG07
            references MAI_USER,
    START_EVENT_UTTER_DATA VARCHAR2(255 char),
    UPDATED_AT             TIMESTAMP(6)      not null,
    UPDATER_ID             VARCHAR2(40 char) not null
        constraint FKTGJFDD1H3F9ENSBGQ56NVQADW
            references MAI_USER,
    START_EVENT_UTTER_ID   VARCHAR2(40 char) not null
        constraint FKQH7GE6X6MW3FGI2VDJC6O5VXE
            references MAI_SDS_START_EVENT_UTTER
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_WORKSPACE
(
    ID         VARCHAR2(255 char) not null
        primary key,
    CREATED_AT TIMESTAMP(6),
    OWNER_ID   VARCHAR2(512 char) not null,
    OWNER_TYPE VARCHAR2(512 char) not null,
    PATH       VARCHAR2(512 char) not null,
    UPDATED_AT TIMESTAMP(6)
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_DNN_DIC
(
    ID           VARCHAR2(40 char) not null
        primary key,
    APPLIED      VARCHAR2(512 char),
    CREATED_AT   TIMESTAMP(6),
    CREATOR_ID   VARCHAR2(40 char)
        constraint FKJJ79L86SHRQM407WVT67X7CYJ
            references MAI_USER,
    NAME         VARCHAR2(40 char) not null,
    UPDATED_AT   TIMESTAMP(6),
    UPDATER_ID   VARCHAR2(40 char)
        constraint FKROS2Y44RN0F1S67AFCQ32QK1G
            references MAI_USER,
    VERSIONS     CLOB,
    WORKSPACE_ID VARCHAR2(40 char) not null
        constraint FKAFAQNR87OIFVPCCHHBAOUABBO
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_DNN_CATEGORY
(
    ID           VARCHAR2(40 char)  not null
        primary key,
    CREATED_AT   TIMESTAMP(6),
    CREATOR_ID   VARCHAR2(40 char)
        constraint FK5G725OQBFV210PK9K1MOREJ2T
            references MAI_USER,
    DNN_DIC_ID   VARCHAR2(40 char)  not null
        constraint FKTCJVE58P2QMMN735GPDB9LSCH
            references MAI_DNN_DIC,
    NAME         VARCHAR2(512 char) not null,
    PARENT_ID    VARCHAR2(40 char),
    UPDATED_AT   TIMESTAMP(6),
    UPDATER_ID   VARCHAR2(40 char)
        constraint FKC26QBJWJY3EU7E8KLWEH907IH
            references MAI_USER,
    WORKSPACE_ID VARCHAR2(40 char)  not null
        constraint FK2VWXOJ6TVFEXDT3SMDM2DHDCL
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_DNN_DIC_LINE
(
    SEQ        NUMBER(10)         not null
        primary key,
    CATEGORY   VARCHAR2(512 char) not null,
    SENTENCE   CLOB,
    VERSION_ID VARCHAR2(40 char)  not null
        constraint FKVX4N0EMS8DI9LVW9EM74EVY9
            references MAI_DNN_DIC
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_DNN_MODEL
(
    DNN_KEY      VARCHAR2(36 char)  not null
        primary key,
    CREATED_AT   TIMESTAMP(6),
    CREATOR_ID   VARCHAR2(40 char)
        constraint FKC2CNOVBRMYPAEPC3R5ULIN24K
            references MAI_USER,
    DNNBINARY    VARCHAR2(512 char) not null,
    NAME         VARCHAR2(512 char) not null,
    UPDATED_AT   TIMESTAMP(6),
    UPDATER_ID   VARCHAR2(40 char)
        constraint FKR0HANUJ7Y6BQ37MI9KTCSVPH9
            references MAI_USER,
    WORKSPACE_ID VARCHAR2(40 char)  not null
        constraint FK46TDV009AIY29M6WBFKA58DOC
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_DNN_RESULT
(
    ID           VARCHAR2(40 char)  not null
        primary key,
    CATEGORY     VARCHAR2(512 char),
    CREATED_AT   TIMESTAMP(6),
    CREATOR_ID   VARCHAR2(40 char)
        constraint FKFQPEROT5KFAGA2TO9MI01OFI9
            references MAI_USER,
    FILE_GROUP   VARCHAR2(512 char) not null,
    MODEL        VARCHAR2(512 char) not null,
    PROBABILITY  NUMBER(17, 16),
    SENTENCE     CLOB               not null,
    UPDATED_AT   TIMESTAMP(6),
    UPDATER_ID   VARCHAR2(40 char)
        constraint FKJM8GFVSWG52T78BOLQ5SSOERA
            references MAI_USER,
    WORKSPACE_ID VARCHAR2(40 char)  not null
        constraint FKCS7HF7ERK5KQV7Q7EFO6M7VG1
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_DNN_TRAINING
(
    DNN_KEY      VARCHAR2(36 char) not null
        primary key,
    WORKSPACE_ID VARCHAR2(40 char) not null
        constraint FK2EQNI0RNAT0F9FONXQP2MIDEF
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_END_POST_DIC
(
    ID             VARCHAR2(40 char) not null
        primary key,
    CREATED_AT     TIMESTAMP(6),
    CREATOR_ID     VARCHAR2(40 char)
        constraint FKAB64KH2DH26OFRDC9CXX0BPU
            references MAI_USER,
    REPRESENTATION VARCHAR2(40 char) not null,
    TYPE           VARCHAR2(5 char)  not null,
    UPDATED_AT     TIMESTAMP(6),
    UPDATER_ID     VARCHAR2(40 char)
        constraint FKBXWPFO49FCKD7VOFGWQ0UGAG7
            references MAI_USER,
    WORKSPACE_ID   VARCHAR2(40 char) not null
        constraint FKLTAN3PRMOHK6OWLKR3N8R69GY
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_END_POST_DIC_REL
(
    ID                VARCHAR2(40 char) not null
        primary key,
    CREATED_AT        TIMESTAMP(6),
    CREATOR_ID        VARCHAR2(40 char)
        constraint FKC8YYEVW77P9JLDTF0Q9WOQG26
            references MAI_USER,
    REPRESENTATION_ID VARCHAR2(40 char)
        constraint FKNHRP9S4NJGGPRHKP76YYB13QR
            references MAI_END_POST_DIC,
    UPDATED_AT        TIMESTAMP(6),
    UPDATER_ID        VARCHAR2(40 char)
        constraint FK5AJBWWMDDBCRDSWMRMQ1WY3PG
            references MAI_USER,
    WORD              VARCHAR2(40 char) not null,
    WORKSPACE_ID      VARCHAR2(40 char) not null
        constraint FK9DLNSWBTIS7A23F9324BYRTJW
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_FILE
(
    ID           VARCHAR2(40 char)  not null
        primary key,
    CREATED_AT   TIMESTAMP(6),
    CREATOR_ID   VARCHAR2(40 char)
        constraint FK8HWQQI8C9BUCH4XSKS3IO0L5P
            references MAI_USER,
    DURATION     NUMBER(10),
    HASH         VARCHAR2(200 char) not null,
    META         VARCHAR2(255 char),
    NAME         VARCHAR2(200 char) not null,
    PURPOSE      VARCHAR2(200 char),
    FILE_SIZE    NUMBER(19)         not null,
    TYPE         VARCHAR2(200 char) not null,
    UPDATED_AT   TIMESTAMP(6),
    UPDATER_ID   VARCHAR2(40 char)
        constraint FKJTA3LJI9CG70U3PWQ2SR8Q3MM
            references MAI_USER,
    WORKSPACE_ID VARCHAR2(40 char)  not null
        constraint FKPYVK6NIK65TODK5BHAFWT044K
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_FILE_GROUP
(
    ID           VARCHAR2(40 char) not null
        primary key,
    CREATED_AT   TIMESTAMP(6),
    CREATOR_ID   VARCHAR2(40 char)
        constraint FK2OMLW4AL6Q3AU4HIKNMP8UBU4
            references MAI_USER,
    META         VARCHAR2(255 char),
    NAME         VARCHAR2(40 char) not null,
    PURPOSE      VARCHAR2(40 char) not null,
    UPDATED_AT   TIMESTAMP(6),
    UPDATER_ID   VARCHAR2(40 char)
        constraint FK8K8IPTOD1PTOY7K55L44TOP4U
            references MAI_USER,
    WORKSPACE_ID VARCHAR2(40 char) not null
        constraint FKHX7R3YW80WY5VMUQKE83XAA4R
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_FILE_GROUP_REL
(
    ID            VARCHAR2(40 char) not null
        primary key,
    CREATED_AT    TIMESTAMP(6),
    CREATOR_ID    VARCHAR2(40 char)
        constraint FKQ2R9QQBSG3T80KXU6TXYOOD9P
            references MAI_USER,
    FILE_GROUP_ID VARCHAR2(40 char) not null
        constraint FKDHA82IXU1696Q1QPUPEGMIF0T
            references MAI_FILE_GROUP,
    FILE_ID       VARCHAR2(40 char) not null
        constraint FKROGBB5KEB61C1VENANE5ORRBY
            references MAI_FILE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_HISTORY
(
    ID           VARCHAR2(40 char)  not null
        primary key,
    CODE         VARCHAR2(512 char) not null,
    CREATED_AT   TIMESTAMP(6),
    CREATOR_ID   VARCHAR2(40 char)
        constraint FK8P1LAFLHCSM95XNHXCXUR56FN
            references MAI_USER,
    DATA         CLOB,
    ENDED_AT     TIMESTAMP(6),
    MESSAGE      VARCHAR2(512 char),
    STARTED_AT   TIMESTAMP(6)       not null,
    UPDATED_AT   TIMESTAMP(6),
    UPDATER_ID   VARCHAR2(40 char)
        constraint FKGNSR0HRMD835KSRFBTW479N2Y
            references MAI_USER,
    WORKSPACE_ID VARCHAR2(40 char)
        constraint FK7VT7NYDGCDMRY2ULWTQK3O0HX
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_HMD_DIC
(
    ID           VARCHAR2(40 char) not null
        primary key,
    APPLIED      VARCHAR2(512 char),
    CREATED_AT   TIMESTAMP(6),
    CREATOR_ID   VARCHAR2(40 char)
        constraint FK4F92YVRLWCU9U67ER291YMVCX
            references MAI_USER,
    NAME         VARCHAR2(40 char) not null,
    UPDATED_AT   TIMESTAMP(6),
    UPDATER_ID   VARCHAR2(40 char)
        constraint FK9IYPFHARIIHUMV380LEHVOEHR
            references MAI_USER,
    VERSIONS     CLOB,
    WORKSPACE_ID VARCHAR2(40 char) not null
        constraint FKA63SFOTCN0U12BDXVYCNOAR8I
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_HMD_CATEGORY
(
    ID           VARCHAR2(40 char) not null
        primary key,
    CREATED_AT   TIMESTAMP(6),
    CREATOR_ID   VARCHAR2(40 char)
        constraint FKCW4O74S7XPSI535N381TWJ984
            references MAI_USER,
    HMD_DIC_ID   VARCHAR2(40 char) not null
        constraint FKBFHG28C70DYOAUIQ1U5NLCLPR
            references MAI_HMD_DIC,
    NAME         VARCHAR2(40 char) not null,
    PAR_CATE_ID  VARCHAR2(40 char),
    PARENT_ID    VARCHAR2(40 char),
    UPDATED_AT   TIMESTAMP(6),
    UPDATER_ID   VARCHAR2(40 char)
        constraint FKHMWBA1XWANU2YHPWLNL7HV3ML
            references MAI_USER,
    WORKSPACE_ID VARCHAR2(40 char) not null
        constraint FKCCY9TAYV7GXHS6ODDA1ADRDF5
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_HMD_DIC_LINE
(
    SEQ        NUMBER(10)        not null
        primary key,
    CATEGORY   VARCHAR2(40 char) not null,
    RULE       CLOB,
    VERSION_ID VARCHAR2(40 char) not null
        constraint FKLFD2YX7VUUYHKR8WHU6FW6C8G
            references MAI_HMD_DIC
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_HMD_RESULT
(
    ID           VARCHAR2(40 char)  not null
        primary key,
    CATEGORY     VARCHAR2(512 char) not null,
    CREATED_AT   TIMESTAMP(6),
    CREATOR_ID   VARCHAR2(40 char)
        constraint FK7SL090CDA91KN1LCN2D3CPUG
            references MAI_USER,
    FILE_GROUP   VARCHAR2(40 char)  not null,
    MODEL        VARCHAR2(512 char) not null,
    RULE         CLOB               not null,
    SENTENCE     CLOB               not null,
    UPDATED_AT   TIMESTAMP(6),
    UPDATER_ID   VARCHAR2(40 char)
        constraint FKCYV6QA47NOJLNW11CCI5FYML9
            references MAI_USER,
    WORKSPACE_ID VARCHAR2(40 char)  not null
        constraint FK2BQ7L97V7B1A3P1LBAQ69EQ7B
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_MRC_CATEGORY
(
    ID           VARCHAR2(40 char) not null
        primary key,
    CATE_NAME    VARCHAR2(40 char),
    CATE_PATH    VARCHAR2(164 char),
    CREATED_AT   TIMESTAMP(6),
    CREATOR_ID   VARCHAR2(40 char)
        constraint FKTL64V2VKPWLTUEDHMQ9G8DNVG
            references MAI_USER,
    PAR_CATE_ID  VARCHAR2(40 char),
    UPDATED_AT   TIMESTAMP(6),
    UPDATER_ID   VARCHAR2(40 char)
        constraint FKB54TE9I2O5KUUUNUT1HM9V8YK
            references MAI_USER,
    WORKSPACE_ID VARCHAR2(40 char)
        constraint FKM6J52A1HB6MY0HIIS14BF9TV6
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_MRC_PASSAGE_IDX_HISTORY
(
    ID           NUMBER(10)        not null
        primary key,
    ADDRESS      VARCHAR2(255 char),
    CREATED_AT   TIMESTAMP(6),
    CREATOR_ID   VARCHAR2(40 char)
        constraint FKMM79M9SMSRX1K7WBI5JRCCR04
            references MAI_USER,
    FETCHED      NUMBER(10),
    MESSAGE      VARCHAR2(100 char),
    PROCESSED    NUMBER(10),
    STATUS       NUMBER(1),
    STOP_YN      NUMBER(1),
    TOTAL        NUMBER(10),
    TYPE         VARCHAR2(30 char),
    UPDATED_AT   TIMESTAMP(6),
    UPDATER_ID   VARCHAR2(40 char)
        constraint FK4MSMIB7EQ0OXV5OAAK68MLTSE
            references MAI_USER,
    WORKSPACE_ID VARCHAR2(40 char) not null
        constraint FK42O1EK43L3W28C0C7KU9AIP1M
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_MRC_PASSAGE_SKILL
(
    ID           NUMBER(10)        not null
        primary key,
    CREATED_AT   TIMESTAMP(6),
    CREATOR_ID   VARCHAR2(40 char)
        constraint FKCUDH4QNR5DMUML8BYV81JI7XE
            references MAI_USER,
    NAME         VARCHAR2(40 char) not null,
    UPDATED_AT   TIMESTAMP(6),
    UPDATER_ID   VARCHAR2(40 char)
        constraint FKKWQNX56T92JURQB1U5G1TQSY3
            references MAI_USER,
    WORKSPACE_ID VARCHAR2(40 char) not null
        constraint FKLF1V99OWYPDMF40JPXPVUO2DM
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_MRC_PASSAGE
(
    ID            VARCHAR2(40 char) not null
        primary key,
    CREATED_AT    TIMESTAMP(6),
    CREATOR_ID    VARCHAR2(40 char)
        constraint FK6I2I99RQO8CNLPFQ2UI51MT2C
            references MAI_USER,
    PASSAGE       CLOB,
    PASSAGE_MORPH CLOB,
    SECTION       VARCHAR2(255 char),
    SKILL_ID      NUMBER(10)
        constraint FKGJOQTQJE0WQ7VDI3QOBGDICE4
            references MAI_MRC_PASSAGE_SKILL,
    SRC           VARCHAR2(255 char),
    UPDATED_AT    TIMESTAMP(6),
    UPDATER_ID    VARCHAR2(40 char)
        constraint FKIR6QADNGMRNPQPT3JJHVLE2E3
            references MAI_USER,
    WORD_LIST     CLOB,
    WORKSPACE_ID  VARCHAR2(40 char) not null
        constraint FK82WIISVJYTGY3FWORWFCKH9G2
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_MRC_PASSAGE_QUESTION
(
    ID             VARCHAR2(40 char) not null
        primary key,
    CREATED_AT     TIMESTAMP(6),
    CREATOR_ID     VARCHAR2(40 char)
        constraint FKG7E2PNPCGKLV9MOEDGJ8JBS1D
            references MAI_USER,
    PASSAGE_ID     VARCHAR2(40 char)
        constraint FK8SJ2YOAYO8V4DC1IGAYTI2RC
            references MAI_MRC_PASSAGE,
    QUESTION       VARCHAR2(255 char),
    QUESTION_MORPH CLOB,
    UPDATED_AT     TIMESTAMP(6),
    UPDATER_ID     VARCHAR2(40 char)
        constraint FKDAA3PX0KI8D9M1M4URCJPYBPX
            references MAI_USER,
    WORKSPACE_ID   VARCHAR2(40 char) not null
        constraint FKI4D7BBSXR2SQVPFPQOASG915Q
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_MRC_TRAINING_MODEL
(
    ID           VARCHAR2(255 char) not null
        primary key,
    BATCH_SIZE   NUMBER(10),
    CREATED_AT   TIMESTAMP(6),
    CREATOR_ID   VARCHAR2(40 char)
        constraint FKA6NCP20PYBQGCO2TOB9OY3Y3J
            references MAI_USER,
    EPOCHES      NUMBER(10),
    MODEL        VARCHAR2(512 char),
    MRC_BINARY   VARCHAR2(512 char),
    NAME         VARCHAR2(512 char),
    STATUS       VARCHAR2(40 char),
    UPDATED_AT   TIMESTAMP(6),
    UPDATER_ID   VARCHAR2(40 char)
        constraint FKFDTAHRFLIIP6CLX1VS3NOLUSD
            references MAI_USER,
    WORKSPACE_ID VARCHAR2(40 char)  not null
        constraint FK834HKDKWOHGDMIWOUIPG7XS46
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_MRC_WIKI
(
    ID           NUMBER(10)        not null
        primary key,
    CREATED_AT   TIMESTAMP(6),
    CREATOR_ID   VARCHAR2(40 char)
        constraint FKQV0GY61OQRBOP4G8NTEOLKSLV
            references MAI_USER,
    NAME         VARCHAR2(40 char) not null,
    UPDATED_AT   TIMESTAMP(6),
    UPDATER_ID   VARCHAR2(40 char)
        constraint FKHWUSD1DCCGQUGJFVEY6BUF1YI
            references MAI_USER,
    WORKSPACE_ID VARCHAR2(40 char) not null
        constraint FKE9QL971GRVWQDI4WFOR6RF3N4
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_MRC_CONTEXT
(
    ID            VARCHAR2(40 char) not null
        primary key,
    CATE_ID       VARCHAR2(40 char)
        constraint FKLAMG5KVV34SCGK37QSSM0W6J0
            references MAI_MRC_CATEGORY,
    CONTEXT       CLOB,
    CONTEXT_MORPH CLOB,
    CREATED_AT    TIMESTAMP(6),
    CREATOR_ID    VARCHAR2(40 char)
        constraint FKDAU0L9IE5UWGL6UKLC84N265H
            references MAI_USER,
    SEASON        NUMBER(10),
    TITLE         VARCHAR2(40 char),
    WIKI_ID       NUMBER(10)
        constraint FKAIBJTR2SCPURJPGSO7VF5LB0U
            references MAI_MRC_WIKI,
    WORKSPACE_ID  VARCHAR2(40 char) not null
        constraint FKQLX8GL2F2LW18OPA6B1BIIJSA
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_MRC_DATA_GROUP_REL
(
    CONTEXT_ID    VARCHAR2(40 char) not null
        constraint FK24H51FKKNDJN0I5ESBVXDR78B
            references MAI_MRC_CONTEXT,
    DATA_GROUP_ID VARCHAR2(40 char) not null
        constraint FKC94YVDKEHXV9LTN9CIJQ4VMTJ
            references MAI_MRC_DATA_GROUP,
    primary key (CONTEXT_ID, DATA_GROUP_ID)
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_MRC_QUESTION
(
    ID             VARCHAR2(40 char) not null
        primary key,
    CATE_ID        VARCHAR2(40 char)
        constraint FKX6HJP28DKAK0RS1EKLFR1VII
            references MAI_MRC_CATEGORY,
    CONTEXT_ID     VARCHAR2(40 char)
        constraint FKC6MPY8KEMUE8XIQI04NJFQXY1
            references MAI_MRC_CONTEXT,
    CREATED_AT     TIMESTAMP(6),
    QUESTION       CLOB,
    QUESTION_MORPH CLOB,
    WORKSPACE_ID   VARCHAR2(40 char) not null
        constraint FKFS1LNFDLH5R7E435PAA2JCV7N
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_MRC_ANSWER
(
    ID                 VARCHAR2(40 char) not null
        primary key,
    ANSWER             CLOB,
    ANSWER_END         NUMBER(10),
    ANSWER_END_MORPH   NUMBER(10),
    ANSWER_MORPH       CLOB,
    ANSWER_START       NUMBER(10),
    ANSWER_START_MORPH NUMBER(10),
    CREATED_AT         TIMESTAMP(6),
    QUESTION_ID        VARCHAR2(40 char)
        constraint FKP44EABQAB9OKJMUIPR5HTRCIE
            references MAI_MRC_QUESTION,
    WORKSPACE_ID       VARCHAR2(40 char) not null
        constraint FKQD223II8AG8LKD66GJEQ8BT2A
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_NER_DICTIONARY
(
    ID           VARCHAR2(40 char) not null
        primary key,
    CHANGE_CNT   NUMBER(10),
    CREATED_AT   TIMESTAMP(6),
    CREATOR_ID   VARCHAR2(40 char)
        constraint FK1OD9TTN6G1VJ2XLK00BO2847S
            references MAI_USER,
    DESCRIPTION  VARCHAR2(40 char),
    NAME         VARCHAR2(50 char) not null,
    WORKSPACE_ID VARCHAR2(40 char) not null
        constraint FKLVIOHTO5FRL7E1HD0CGGPK20H
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_NER_DICTIONARY_LINE
(
    ID           VARCHAR2(40 char) not null
        primary key,
    CHANGE_TAG   VARCHAR2(1000 char),
    CREATED_AT   TIMESTAMP(6),
    DESCRIPTION  VARCHAR2(1000 char),
    DIC_TYPE     NUMBER(10)        not null,
    ORIGIN_TAG   VARCHAR2(1000 char),
    PATTERN      VARCHAR2(1000 char),
    TAG          VARCHAR2(1000 char),
    VERSION_ID   VARCHAR2(40 char) not null
        constraint FKQUIOO3HR699PDXU3NQM2ISQKU
            references MAI_NER_DICTIONARY,
    WORD         VARCHAR2(1000 char),
    WORKSPACE_ID VARCHAR2(40 char) not null
        constraint FKT14472JBKWSJIXSYD2ABH9MJR
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_POS_DICTIONARY
(
    ID           VARCHAR2(40 char) not null
        primary key,
    CHANGE_CNT   NUMBER(10),
    CREATED_AT   TIMESTAMP(6),
    CREATOR_ID   VARCHAR2(40 char)
        constraint FK1X6X1QNCNKA57N0M0BJBGDGSA
            references MAI_USER,
    DESCRIPTION  VARCHAR2(40 char),
    NAME         VARCHAR2(50 char) not null,
    WORKSPACE_ID VARCHAR2(40 char) not null
        constraint FK8JCDWMESCXBP9RHOEK6CYXQJT
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_POS_DICTIONARY_LINE
(
    ID            VARCHAR2(40 char) not null
        primary key,
    COMPOUND_TYPE VARCHAR2(100 char),
    CREATED_AT    TIMESTAMP(6),
    DESCRIPTION   VARCHAR2(1000 char),
    DEST_POS      VARCHAR2(1000 char),
    DIC_TYPE      NUMBER(10)        not null,
    PATTERN       VARCHAR2(1000 char),
    SRC_POS       VARCHAR2(1000 char),
    VERSION_ID    VARCHAR2(40 char) not null
        constraint FKO7V33LE3NARX5JL4JCJ72MT9S
            references MAI_POS_DICTIONARY,
    WORD          VARCHAR2(1000 char),
    WORKSPACE_ID  VARCHAR2(40 char) not null
        constraint FKFUXKI1CJ5SN7Y3KL9I0EUXJXJ
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


CREATE TABLE NLPADM.MAI_REPLACEMENT_DICTIONARY
(
    ID VARCHAR2(40 char) not null
        primary key,
    APPLIED_YN VARCHAR2(1 char) not null,
    CREATED_AT TIMESTAMP(6),
    CREATOR_ID VARCHAR2(40 char)
        constraint FKO5PJSCBQEN3DRDCW24IIMOLH1
            references MAI_USER,
    DESCRIPTION VARCHAR2(40 char),
    NAME VARCHAR2(50 char) not null
        constraint UK_1X4SO1M3OWM5SVRMODF1KYK30
            unique,
    WORKSPACE_ID VARCHAR2(40 char) not null
        constraint FKM96XW4WQTV55Q86N5VNSC9MVU
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K;
/

CREATE TABLE NLPADM.MAI_REPLACEMENT_DICTIONARY_TYPE
(
    NAME VARCHAR2(10 char) not null
        primary key,
    USE_YN VARCHAR2(1 char) not null
) TABLESPACE NLP_DT01_8K;


CREATE TABLE NLPADM.MAI_REPLACEMENT_DICTIONARY_LINE
(
    ID VARCHAR2(40 char) not null
        primary key,
    CREATED_AT TIMESTAMP(6),
    CREATOR_ID VARCHAR2(40 char)
        constraint FK45VA25EF29SRH4DJOGFVEY8QE
            references MAI_USER,
    DESCRIPTION VARCHAR2(40 char),
    DEST_STR VARCHAR2(50 char),
    SRC_STR VARCHAR2(50 char),
    TYPE VARCHAR2(10 char) not null
        constraint FK4OLROU6TSFY9PH5PP53ITAPAT
            references MAI_REPLACEMENT_DICTIONARY_TYPE,
    USE_YN VARCHAR2(1 char) not null,
    VERSION_ID VARCHAR2(40 char) not null
        constraint FKOQ3SW34AD2XYQ2KKDNSDMS51J
            references MAI_REPLACEMENT_DICTIONARY
) TABLESPACE NLP_DT01_8K;

create table NLPADM.MAI_QUERY_PRPRS
(
    ID           VARCHAR2(40 char) not null
        primary key,
    ALL_STEP     VARCHAR2(20 char),
    CREATED_AT   TIMESTAMP(6),
    CREATOR_ID   VARCHAR2(40 char)
        constraint FKQTOR6XS02263NE53618FCMK2H
            references MAI_USER,
    CURRENT_STEP VARCHAR2(10 char),
    TITLE        VARCHAR2(40 char),
    WORKSPACE_ID VARCHAR2(40 char) not null
        constraint FKCFPHGEH045R2IVWWQ6B4RV06D
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_QUERY_PRPRS_STEP
(
    ID                  VARCHAR2(40 char)  not null
        primary key,
    CREATED_AT          TIMESTAMP(6),
    CREATOR_ID          VARCHAR2(40 char)
        constraint FKNSXGFNT580ME885TEP6U98Y69
            references MAI_USER,
    ORG_SENTENCE        VARCHAR2(512 char),
    QUERY_PARAPHRASE_ID VARCHAR2(40 char)
        constraint FKN1RQWLTYB8KWVFB8725P4RUA5
            references MAI_QUERY_PRPRS,
    SENTENCE            VARCHAR2(512 char) not null,
    STEP_CD             VARCHAR2(10 char),
    WORKSPACE_ID        VARCHAR2(40 char)  not null
        constraint FKO4T64GVU2KRULWD611HIDX3FL
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_STT_MODEL
(
    ID           VARCHAR2(40 char) not null
        primary key,
    CREATED_AT   TIMESTAMP(6),
    CREATOR_ID   VARCHAR2(40 char)
        constraint FK8VKIKVSRU8FDQQOLIRT9C2KEL
            references MAI_USER,
    NAME         VARCHAR2(512 char),
    PURPOSE      VARCHAR2(40 char),
    RATE         NUMBER(10),
    STATUS       VARCHAR2(40 char),
    STT_BINARY   VARCHAR2(512 char),
    STT_KEY      VARCHAR2(255 char),
    TRAIN_TYPE   VARCHAR2(40 char),
    UPDATED_AT   TIMESTAMP(6),
    UPDATER_ID   VARCHAR2(40 char)
        constraint FK5EVDLVT6P8X90OAL1KMGWAFCU
            references MAI_USER,
    WORKSPACE_ID VARCHAR2(40 char) not null
        constraint FK8MA58IU59BXETGY5N0P7P1SKR
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_STT_TRANSCRIPT
(
    ID             VARCHAR2(40 char) not null
        primary key,
    ALERT          VARCHAR2(200 char),
    APPLIED        VARCHAR2(200 char),
    CREATED_AT     TIMESTAMP(6),
    CREATOR_ID     VARCHAR2(40 char)
        constraint FKLD0K4GHMBFAMCK4YL9UYV9PHN
            references MAI_USER,
    FILE_GROUP_ID  VARCHAR2(255 char)
        constraint FKHDP5SSI8ACW67LTF2F0SOM225
            references MAI_FILE_GROUP,
    FILE_ID        VARCHAR2(40 char) not null
        constraint FK6N81SKEFFIT7ASQE89DEJT6W0
            references MAI_FILE,
    NAME           VARCHAR2(200 char),
    QAER_ID        VARCHAR2(50 char)
        constraint FKCPNBUOIKL5FM06DPLS75OVCHE
            references MAI_USER,
    TRANSCRIBER_ID NUMBER(10),
    UPDATED_AT     TIMESTAMP(6),
    UPDATER_ID     VARCHAR2(40 char)
        constraint FKRRHKAI95R1G8AOVJ20S2WCCJT
            references MAI_USER,
    VERSION        CLOB,
    WORKSPACE_ID   VARCHAR2(40 char) not null
        constraint FK5GMPWX3X90CXBJXFLO2BIX0JU
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_STT_ANALYSIS_RESULT
(
    ID            VARCHAR2(40 char) not null
        primary key,
    AM_MODEL_ID   VARCHAR2(200 char)
        constraint FKO3XHO42S6YBYGT78JTS3CRDAV
            references MAI_STT_MODEL,
    CREATED_AT    TIMESTAMP(6),
    CREATOR_ID    VARCHAR2(40 char)
        constraint FKJV2FU5UHS0CRN5PU468C1GAAS
            references MAI_USER,
    FILE_GROUP_ID VARCHAR2(40 char) not null
        constraint FKGRXM6CU5YCHSWWKQPP7CUCM5T
            references MAI_FILE_GROUP,
    FILE_ID       VARCHAR2(40 char) not null
        constraint FK5GOKMX10PGJB6CWYWD6PX3WG2
            references MAI_FILE,
    LM_MODEL_ID   VARCHAR2(200 char)
        constraint FKSPWYIEMVCTCO5UTK17YP7LM3G
            references MAI_STT_MODEL,
    TEXT          CLOB,
    TRANSCRIPT_ID VARCHAR2(40 char) not null
        constraint FKSU86DVCIAMXM1X3VFOIY2D0PN
            references MAI_STT_TRANSCRIPT,
    UPDATED_AT    TIMESTAMP(6),
    UPDATER_ID    VARCHAR2(40 char)
        constraint FKE2RSLP3D1MROJG3VOKQSVKYMJ
            references MAI_USER,
    WORKSPACE_ID  VARCHAR2(40 char) not null
        constraint FKJEWSOAN4FT162A1AKWGY36WPO
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_STT_EVAL_RESULT
(
    ID                 VARCHAR2(40 char) not null
        primary key,
    ANALYSIS_RESULT_ID VARCHAR2(40 char)
        constraint FKKRHWOMXKW9S0MLYUOYCKMR1UX
            references MAI_STT_ANALYSIS_RESULT,
    CREATED_AT         TIMESTAMP(6),
    CREATOR_ID         VARCHAR2(40 char)
        constraint FKJN9TELRQVAH32NR3N9JAMCQ6F
            references MAI_USER,
    RESULT             CLOB,
    UPDATED_AT         TIMESTAMP(6),
    UPDATER_ID         VARCHAR2(40 char)
        constraint FK42QIDAF1NF6AX8NB1Q25L5A4K
            references MAI_USER,
    WORKSPACE_ID       VARCHAR2(40 char) not null
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_STT_TRANSCRIPT_LINE
(
    SEQ        NUMBER(10)        not null
        primary key,
    TEXT       CLOB,
    VERSION_ID VARCHAR2(40 char) not null
        constraint FK55KO8Y1JRBSTIRHG8MV8A3ER
            references MAI_STT_TRANSCRIPT
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_SYNONYM_DIC
(
    ID             VARCHAR2(40 char) not null
        primary key,
    CREATED_AT     TIMESTAMP(6),
    CREATOR_ID     VARCHAR2(40 char)
        constraint FKA4W4J5XCYYNFFHJSP7E244V8N
            references MAI_USER,
    REPRESENTATION VARCHAR2(40 char) not null,
    UPDATED_AT     TIMESTAMP(6),
    UPDATER_ID     VARCHAR2(40 char)
        constraint FKS6XP6FJC771FI8767C39E6JQD
            references MAI_USER,
    WORKSPACE_ID   VARCHAR2(40 char) not null
        constraint FKG6FF6SOJCR9427VT7542SRE3A
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_SYNONYM_DIC_REL
(
    ID                VARCHAR2(40 char) not null
        primary key,
    CREATED_AT        TIMESTAMP(6),
    CREATOR_ID        VARCHAR2(40 char)
        constraint FK5AF08WY30X3APIGMUUSWCO6EQ
            references MAI_USER,
    REPRESENTATION_ID VARCHAR2(40 char)
        constraint FKE35NJRIHIPBCCSXCAXKSUV7I0
            references MAI_SYNONYM_DIC,
    SYNONYM_WORD      VARCHAR2(40 char) not null,
    UPDATED_AT        TIMESTAMP(6),
    UPDATER_ID        VARCHAR2(40 char)
        constraint FKFHYA2C51XD06AGP073B9CWXOL
            references MAI_USER,
    WORKSPACE_ID      VARCHAR2(40 char) not null
        constraint FK1PJBPOKDC27CTE92K9N9B36HR
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_TA_MODEL_MANAGEMENT
(
    ID                 VARCHAR2(40 char)  not null
        primary key,
    CREATED_AT         TIMESTAMP(6),
    CREATOR_ID         VARCHAR2(40 char)
        constraint FKCBGW2HW30A908I15U1R1VEVOV
            references MAI_USER,
    DNN_DIC_ID         VARCHAR2(40 char),
    DNN_USE            NUMBER(1)          not null,
    EXTERNAL_MODEL_SEQ VARCHAR2(512 char),
    GRPC_SERVER_SET_ID VARCHAR2(40 char),
    HMD_DIC_ID         VARCHAR2(40 char),
    HMD_USE            NUMBER(1)          not null,
    MAN_DESC           VARCHAR2(512 char),
    NAME               VARCHAR2(512 char) not null,
    UPDATED_AT         TIMESTAMP(6),
    UPDATER_ID         VARCHAR2(40 char)
        constraint FKEXC7MF8E0F9HYYESE91TYGYBU
            references MAI_USER,
    WORKSPACE_ID       VARCHAR2(40 char)  not null
        constraint FKMFCE0PNYEVOWA4VUX7I5O124P
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_TEST_FILES
(
    ID           NUMBER(19)        not null
        primary key,
    CREATED_AT   TIMESTAMP(6),
    CREATOR_ID   VARCHAR2(40 char)
        constraint FKHYOT2DCUKOKPAPHDYXNJFTFPG
            references MAI_USER,
    DESCRIPTION  VARCHAR2(40 char),
    FILE_ID      VARCHAR2(40 char)
        constraint FKEUQ3MMX4TAJKYNWVG51VBNE1Y
            references MAI_FILE,
    FILE_NAME    VARCHAR2(60 char) not null,
    LINE_CNT     NUMBER(10),
    TEST_TYPE    NUMBER(10)        not null,
    WORKSPACE_ID VARCHAR2(40 char) not null
        constraint FK1N9XG1HBMD38A0GRCF1WS5DR4
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_TEST_RESULT
(
    ID            NUMBER(19)        not null
        primary key,
    APPLY_CHECK   VARCHAR2(50 char),
    CREATED_AT    TIMESTAMP(6),
    DESCRIPTION   VARCHAR2(40 char),
    DICTIONARY_ID VARCHAR2(60 char) not null,
    FILE_ID       VARCHAR2(40 char),
    TEST_TYPE     NUMBER(10)        not null,
    TESTER_ID     VARCHAR2(40 char)
        constraint FKHSQJWSV1GNU2UCS0NE60TEHUW
            references MAI_USER,
    WORKSPACE_ID  VARCHAR2(40 char) not null
        constraint FKKWAERIUXOQHV5FHI17ADYX4EV
            references MAI_WORKSPACE,
    TEST_ID       NUMBER(19)
) TABLESPACE NLP_DT01_8K
;


create table NLPADM.MAI_TEST_RESULT_DETAIL
(
 ID NUMBER(19) not null
  primary key using index tablespace nlp_ix01_8k,
 CREATED_AT TIMESTAMP(6),
 DESCRIPTION VARCHAR2(40 char),
 DICTIONARY_ID VARCHAR2(60 char) not null,
 TEST_ID NUMBER(19),
 TEST_RESULT CLOB,
 TEST_TYPE NUMBER(10) not null,
 TESTER_ID VARCHAR2(40 char)
  constraint MAI_TEST_RESULT_DETAIL_FK01
   references MAI_USER,
 WORKSPACE_ID VARCHAR2(40 char) not null
  constraint MAI_TEST_RESULT_DETAIL_FK02
   references NLPADM.MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;

create table NLPADM.MAI_TEST_SENTENCES
(
    ID           NUMBER(19)         not null
        primary key,
    CREATED_AT   TIMESTAMP(6),
    DESCRIPTION  VARCHAR2(40 char),
    SENTENCE     VARCHAR2(200 char) not null,
    TEST_FILE_ID NUMBER(19)         not null
        constraint FKFBXSITBLD7TTYI3IRJUB9YUUR
            references MAI_TEST_FILES,
    TEST_TYPE    NUMBER(10)         not null,
    WORKSPACE_ID VARCHAR2(40 char)  not null
        constraint FK8N8FG1DTQ0UVDKTV0G37A9FVR
            references MAI_WORKSPACE
) TABLESPACE NLP_DT01_8K
;

create sequence NLPADM.BRAIN_BQA_PRPRS_WORD_SEQ
    increment by 5
;

create sequence NLPADM.BRAIN_BQA_SKILL_SEQ
    increment by 5
;

create sequence NLPADM.MAI_BQA_IDX_HISTORY_SEQ
    increment by 5
;

create sequence NLPADM.MAI_NQA_IDX_HISTORY_SEQ
    increment by 5
;

create sequence NLPADM.MAI_ACL_SEQ
    increment by 5
;

create sequence NLPADM.MAI_DNN_LINE_SEQ
    increment by 5
;

create sequence NLPADM.MAI_GROUP_MEMBER_SEQ
    increment by 5
;

create sequence NLPADM.MAI_GROUP_SEQ
    increment by 5
;

create sequence NLPADM.MAI_HMD_LINE_SEQ
    increment by 5
;

create sequence NLPADM.MAI_MRC_WIKI_SEQ
    increment by 5
;

create sequence NLPADM.MAI_NER_CORPUS_IDX_HSTY_SEQ
    increment by 5
;

create sequence NLPADM.MAI_NER_CORPUS_TAG_SEQ
    increment by 5
;

create sequence NLPADM.MAI_PASSAGE_IDX_HISTORY_SEQ
    increment by 5
;

create sequence NLPADM.MAI_PASSAGE_SKILL_SEQ
    increment by 5
;

create sequence NLPADM.MAI_PASSWORD_HISTORY_SEQ
    increment by 5
;

create sequence NLPADM.MAI_ROLE_MAPPING_SEQ
    increment by 5
;

create sequence NLPADM.MAI_ROLE_SEQ
    increment by 5
;

create sequence NLPADM.MAI_SIGNIN_HISTORY_SEQ
    increment by 5
;

create sequence NLPADM.MAI_STT_TRANSCRIPT_SEQ
    increment by 5
;

create sequence NLPADM.MAI_TEST_FILES_IDX_HSTY_SEQ
    increment by 5
;

create sequence NLPADM.MAI_TWE_HISTORY_SEQ
    increment by 5
;

create sequence MAI_COMMIT_IDX_HSTY_SEQ increment by 5;
create sequence MAI_TEST_SENT_IDX_HSTY_SEQ increment by 5;
create sequence MAI_TEST_RST_DT_IDX_HSTY_SEQ increment by 5;
create sequence MAI_TEST_RST_IDX_HSTY_SEQ increment by 5;

CREATE SYNONYM NLPCON.BRAIN_BQA_CATEGORY FOR NLPADM.BRAIN_BQA_CATEGORY;
CREATE SYNONYM NLPCON.BRAIN_BQA_ITEM FOR NLPADM.BRAIN_BQA_ITEM;
CREATE SYNONYM NLPCON.BRAIN_BQA_PRPRS_MAIN_WD FOR NLPADM.BRAIN_BQA_PRPRS_MAIN_WD;
CREATE SYNONYM NLPCON.BRAIN_BQA_PRPRS_REL FOR NLPADM.BRAIN_BQA_PRPRS_REL;
CREATE SYNONYM NLPCON.BRAIN_BQA_PRPRS_WD FOR NLPADM.BRAIN_BQA_PRPRS_WD;
CREATE SYNONYM NLPCON.BRAIN_BQA_SKILL FOR NLPADM.BRAIN_BQA_SKILL;
CREATE SYNONYM NLPCON.MAI_ACCESS_TOKEN FOR NLPADM.MAI_ACCESS_TOKEN;
CREATE SYNONYM NLPCON.MAI_ACL FOR NLPADM.MAI_ACL;
CREATE SYNONYM NLPCON.MAI_BQA_INDEXING_HISTORY FOR NLPADM.MAI_BQA_INDEXING_HISTORY;
CREATE SYNONYM NLPCON.MAI_CODE FOR NLPADM.MAI_CODE;
CREATE SYNONYM NLPCON.MAI_COMMIT_HISTORY FOR NLPADM.MAI_COMMIT_HISTORY;
CREATE SYNONYM NLPCON.MAI_DNN_CATEGORY FOR NLPADM.MAI_DNN_CATEGORY;
CREATE SYNONYM NLPCON.MAI_DNN_DIC FOR NLPADM.MAI_DNN_DIC;
CREATE SYNONYM NLPCON.MAI_DNN_DIC_LINE FOR NLPADM.MAI_DNN_DIC_LINE;
CREATE SYNONYM NLPCON.MAI_DNN_MODEL FOR NLPADM.MAI_DNN_MODEL;
CREATE SYNONYM NLPCON.MAI_DNN_RESULT FOR NLPADM.MAI_DNN_RESULT;
CREATE SYNONYM NLPCON.MAI_DNN_TRAINING FOR NLPADM.MAI_DNN_TRAINING;
CREATE SYNONYM NLPCON.MAI_END_POST_DIC FOR NLPADM.MAI_END_POST_DIC;
CREATE SYNONYM NLPCON.MAI_END_POST_DIC_REL FOR NLPADM.MAI_END_POST_DIC_REL;
CREATE SYNONYM NLPCON.MAI_FILE FOR NLPADM.MAI_FILE;
CREATE SYNONYM NLPCON.MAI_FILE_GROUP FOR NLPADM.MAI_FILE_GROUP;
CREATE SYNONYM NLPCON.MAI_FILE_GROUP_REL FOR NLPADM.MAI_FILE_GROUP_REL;
CREATE SYNONYM NLPCON.MAI_GROUP FOR NLPADM.MAI_GROUP;
CREATE SYNONYM NLPCON.MAI_GROUP_MEMBER FOR NLPADM.MAI_GROUP_MEMBER;
CREATE SYNONYM NLPCON.MAI_HISTORY FOR NLPADM.MAI_HISTORY;
CREATE SYNONYM NLPCON.MAI_HMD_CATEGORY FOR NLPADM.MAI_HMD_CATEGORY;
CREATE SYNONYM NLPCON.MAI_HMD_DIC FOR NLPADM.MAI_HMD_DIC;
CREATE SYNONYM NLPCON.MAI_HMD_DIC_LINE FOR NLPADM.MAI_HMD_DIC_LINE;
CREATE SYNONYM NLPCON.MAI_HMD_RESULT FOR NLPADM.MAI_HMD_RESULT;
CREATE SYNONYM NLPCON.MAI_MRC_ANSWER FOR NLPADM.MAI_MRC_ANSWER;
CREATE SYNONYM NLPCON.MAI_MRC_CATEGORY FOR NLPADM.MAI_MRC_CATEGORY;
CREATE SYNONYM NLPCON.MAI_MRC_CONTEXT FOR NLPADM.MAI_MRC_CONTEXT;
CREATE SYNONYM NLPCON.MAI_MRC_DATA_GROUP FOR NLPADM.MAI_MRC_DATA_GROUP;
CREATE SYNONYM NLPCON.MAI_MRC_DATA_GROUP_REL FOR NLPADM.MAI_MRC_DATA_GROUP_REL;
CREATE SYNONYM NLPCON.MAI_MRC_PASSAGE FOR NLPADM.MAI_MRC_PASSAGE;
CREATE SYNONYM NLPCON.MAI_MRC_PASSAGE_IDX_HISTORY FOR NLPADM.MAI_MRC_PASSAGE_IDX_HISTORY;
CREATE SYNONYM NLPCON.MAI_MRC_PASSAGE_QUESTION FOR NLPADM.MAI_MRC_PASSAGE_QUESTION;
CREATE SYNONYM NLPCON.MAI_MRC_PASSAGE_SKILL FOR NLPADM.MAI_MRC_PASSAGE_SKILL;
CREATE SYNONYM NLPCON.MAI_MRC_QUESTION FOR NLPADM.MAI_MRC_QUESTION;
CREATE SYNONYM NLPCON.MAI_MRC_TRAINING_MODEL FOR NLPADM.MAI_MRC_TRAINING_MODEL;
CREATE SYNONYM NLPCON.MAI_MRC_WIKI FOR NLPADM.MAI_MRC_WIKI;
CREATE SYNONYM NLPCON.MAI_NER_CORPUS FOR NLPADM.MAI_NER_CORPUS;
CREATE SYNONYM NLPCON.MAI_NER_CORPUS_TAG FOR NLPADM.MAI_NER_CORPUS_TAG;
CREATE SYNONYM NLPCON.MAI_NER_DICTIONARY FOR NLPADM.MAI_NER_DICTIONARY;
CREATE SYNONYM NLPCON.MAI_NER_DICTIONARY_LINE FOR NLPADM.MAI_NER_DICTIONARY_LINE;
CREATE SYNONYM NLPCON.MAI_NQA_INDEX_HISTORY FOR NLPADM.MAI_NQA_INDEX_HISTORY;
CREATE SYNONYM NLPCON.MAI_PASSWORD_HISTORY FOR NLPADM.MAI_PASSWORD_HISTORY;
CREATE SYNONYM NLPCON.MAI_POS_DICTIONARY FOR NLPADM.MAI_POS_DICTIONARY;
CREATE SYNONYM NLPCON.MAI_POS_DICTIONARY_LINE FOR NLPADM.MAI_POS_DICTIONARY_LINE;
CREATE SYNONYM NLPCON.MAI_REPLACEMENT_DICTIONARY FOR NLPADM.MAI_REPLACEMENT_DICTIONARY;
CREATE SYNONYM NLPCON.MAI_REPLACEMENT_DICTIONARY_TYPE FOR NLPADM.MAI_REPLACEMENT_DICTIONARY_TYPE;
CREATE SYNONYM NLPCON.MAI_REPLACEMENT_DICTIONARY_LINE FOR NLPADM.MAI_REPLACEMENT_DICTIONARY_LINE;
CREATE SYNONYM NLPCON.MAI_QUERY_PRPRS FOR NLPADM.MAI_QUERY_PRPRS;
CREATE SYNONYM NLPCON.MAI_QUERY_PRPRS_STEP FOR NLPADM.MAI_QUERY_PRPRS_STEP;
CREATE SYNONYM NLPCON.MAI_ROLE FOR NLPADM.MAI_ROLE;
CREATE SYNONYM NLPCON.MAI_ROLE_MAPPING FOR NLPADM.MAI_ROLE_MAPPING;
CREATE SYNONYM NLPCON.MAI_SDS_ENTITY FOR NLPADM.MAI_SDS_ENTITY;
CREATE SYNONYM NLPCON.MAI_SDS_ENTITY_DATA FOR NLPADM.MAI_SDS_ENTITY_DATA;
CREATE SYNONYM NLPCON.MAI_SDS_ENTITY_DATA_SYNONYM FOR NLPADM.MAI_SDS_ENTITY_DATA_SYNONYM;
CREATE SYNONYM NLPCON.MAI_SDS_EVENT FOR NLPADM.MAI_SDS_EVENT;
CREATE SYNONYM NLPCON.MAI_SDS_EVENT_CONDITION FOR NLPADM.MAI_SDS_EVENT_CONDITION;
CREATE SYNONYM NLPCON.MAI_SDS_EVENT_UTTER FOR NLPADM.MAI_SDS_EVENT_UTTER;
CREATE SYNONYM NLPCON.MAI_SDS_EVENT_UTTER_DATA FOR NLPADM.MAI_SDS_EVENT_UTTER_DATA;
CREATE SYNONYM NLPCON.MAI_SDS_INTENT FOR NLPADM.MAI_SDS_INTENT;
CREATE SYNONYM NLPCON.MAI_SDS_INTENT_UTTER FOR NLPADM.MAI_SDS_INTENT_UTTER;
CREATE SYNONYM NLPCON.MAI_SDS_INTENT_UTTER_DATA FOR NLPADM.MAI_SDS_INTENT_UTTER_DATA;
CREATE SYNONYM NLPCON.MAI_SDS_MODEL FOR NLPADM.MAI_SDS_MODEL;
CREATE SYNONYM NLPCON.MAI_SDS_MODEL_ENTITY FOR NLPADM.MAI_SDS_MODEL_ENTITY;
CREATE SYNONYM NLPCON.MAI_SDS_MODEL_INTENT FOR NLPADM.MAI_SDS_MODEL_INTENT;
CREATE SYNONYM NLPCON.MAI_SDS_NEXT_TASK FOR NLPADM.MAI_SDS_NEXT_TASK;
CREATE SYNONYM NLPCON.MAI_SDS_START_EVENT FOR NLPADM.MAI_SDS_START_EVENT;
CREATE SYNONYM NLPCON.MAI_SDS_START_EVENT_UTTER FOR NLPADM.MAI_SDS_START_EVENT_UTTER;
CREATE SYNONYM NLPCON.MAI_SDS_START_EVENT_UTTER_DATA FOR NLPADM.MAI_SDS_START_EVENT_UTTER_DATA;
CREATE SYNONYM NLPCON.MAI_SDS_TASK FOR NLPADM.MAI_SDS_TASK;
CREATE SYNONYM NLPCON.MAI_SIGNIN_HISTORY FOR NLPADM.MAI_SIGNIN_HISTORY;
CREATE SYNONYM NLPCON.MAI_STT_ANALYSIS_RESULT FOR NLPADM.MAI_STT_ANALYSIS_RESULT;
CREATE SYNONYM NLPCON.MAI_STT_EVAL_RESULT FOR NLPADM.MAI_STT_EVAL_RESULT;
CREATE SYNONYM NLPCON.MAI_STT_MODEL FOR NLPADM.MAI_STT_MODEL;
CREATE SYNONYM NLPCON.MAI_STT_TRAINING FOR NLPADM.MAI_STT_TRAINING;
CREATE SYNONYM NLPCON.MAI_STT_TRANSCRIPT FOR NLPADM.MAI_STT_TRANSCRIPT;
CREATE SYNONYM NLPCON.MAI_STT_TRANSCRIPT_LINE FOR NLPADM.MAI_STT_TRANSCRIPT_LINE;
CREATE SYNONYM NLPCON.MAI_SYNONYM_DIC FOR NLPADM.MAI_SYNONYM_DIC;
CREATE SYNONYM NLPCON.MAI_SYNONYM_DIC_REL FOR NLPADM.MAI_SYNONYM_DIC_REL;
CREATE SYNONYM NLPCON.MAI_TA_MODEL_MANAGEMENT FOR NLPADM.MAI_TA_MODEL_MANAGEMENT;
CREATE SYNONYM NLPCON.MAI_TEST_FILES FOR NLPADM.MAI_TEST_FILES;
CREATE SYNONYM NLPCON.MAI_TEST_RESULT FOR NLPADM.MAI_TEST_RESULT;
CREATE SYNONYM NLPCON.MAI_TEST_RESULT_DETAIL FOR NLPADM.MAI_TEST_RESULT_DETAIL;
CREATE SYNONYM NLPCON.MAI_TEST_SENTENCES FOR NLPADM.MAI_TEST_SENTENCES;
CREATE SYNONYM NLPCON.MAI_TWE_INDEXING_HISTORY FOR NLPADM.MAI_TWE_INDEXING_HISTORY;
CREATE SYNONYM NLPCON.MAI_TWE_SCHEDULE FOR NLPADM.MAI_TWE_SCHEDULE;
CREATE SYNONYM NLPCON.MAI_USER FOR NLPADM.MAI_USER;
CREATE SYNONYM NLPCON.MAI_WORKSPACE FOR NLPADM.MAI_WORKSPACE;

CREATE OR REPLACE SYNONYM NLPCON.BRAIN_BQA_PRPRS_WORD_SEQ FOR NLPADM.BRAIN_BQA_PRPRS_WORD_SEQ;
CREATE OR REPLACE SYNONYM NLPCON.BRAIN_BQA_SKILL_SEQ FOR NLPADM.BRAIN_BQA_SKILL_SEQ;
CREATE OR REPLACE SYNONYM NLPCON.MAI_ACL_SEQ FOR NLPADM.MAI_ACL_SEQ;
CREATE OR REPLACE SYNONYM NLPCON.MAI_BQA_IDX_HISTORY_SEQ FOR NLPADM.MAI_BQA_IDX_HISTORY_SEQ;
CREATE OR REPLACE SYNONYM NLPCON.MAI_DNN_LINE_SEQ FOR NLPADM.MAI_DNN_LINE_SEQ;
CREATE OR REPLACE SYNONYM NLPCON.MAI_GROUP_MEMBER_SEQ FOR NLPADM.MAI_GROUP_MEMBER_SEQ;
CREATE OR REPLACE SYNONYM NLPCON.MAI_GROUP_SEQ FOR NLPADM.MAI_GROUP_SEQ;
CREATE OR REPLACE SYNONYM NLPCON.MAI_HMD_LINE_SEQ FOR NLPADM.MAI_HMD_LINE_SEQ;
CREATE OR REPLACE SYNONYM NLPCON.MAI_MRC_WIKI_SEQ FOR NLPADM.MAI_MRC_WIKI_SEQ;
CREATE OR REPLACE SYNONYM NLPCON.MAI_NER_CORPUS_IDX_HSTY_SEQ FOR NLPADM.MAI_NER_CORPUS_IDX_HSTY_SEQ;
CREATE OR REPLACE SYNONYM NLPCON.MAI_NER_CORPUS_TAG_SEQ FOR NLPADM.MAI_NER_CORPUS_TAG_SEQ;
CREATE OR REPLACE SYNONYM NLPCON.MAI_NQA_IDX_HISTORY_SEQ FOR NLPADM.MAI_NQA_IDX_HISTORY_SEQ;
CREATE OR REPLACE SYNONYM NLPCON.MAI_PASSAGE_IDX_HISTORY_SEQ FOR NLPADM.MAI_PASSAGE_IDX_HISTORY_SEQ;
CREATE OR REPLACE SYNONYM NLPCON.MAI_PASSAGE_SKILL_SEQ FOR NLPADM.MAI_PASSAGE_SKILL_SEQ;
CREATE OR REPLACE SYNONYM NLPCON.MAI_PASSWORD_HISTORY_SEQ FOR NLPADM.MAI_PASSWORD_HISTORY_SEQ;
CREATE OR REPLACE SYNONYM NLPCON.MAI_ROLE_MAPPING_SEQ FOR NLPADM.MAI_ROLE_MAPPING_SEQ;
CREATE OR REPLACE SYNONYM NLPCON.MAI_ROLE_SEQ FOR NLPADM.MAI_ROLE_SEQ;
CREATE OR REPLACE SYNONYM NLPCON.MAI_SIGNIN_HISTORY_SEQ FOR NLPADM.MAI_SIGNIN_HISTORY_SEQ;
CREATE OR REPLACE SYNONYM NLPCON.MAI_STT_TRANSCRIPT_SEQ FOR NLPADM.MAI_STT_TRANSCRIPT_SEQ;
CREATE OR REPLACE SYNONYM NLPCON.MAI_TEST_FILES_IDX_HSTY_SEQ FOR NLPADM.MAI_TEST_FILES_IDX_HSTY_SEQ;
CREATE OR REPLACE SYNONYM NLPCON.MAI_TWE_HISTORY_SEQ FOR NLPADM.MAI_TWE_HISTORY_SEQ;
CREATE OR REPLACE SYNONYM NLPCON.MAI_XDC_LINE_SEQ  FOR NLPADM.MAI_XDC_LINE_SEQ;

CREATE OR REPLACE SYNONYM NLPCON.MAI_COMMIT_IDX_HSTY_SEQ FOR NLPADM.MAI_COMMIT_IDX_HSTY_SEQ;
CREATE OR REPLACE SYNONYM NLPCON.MAI_TEST_SENT_IDX_HSTY_SEQ FOR NLPADM.MAI_TEST_SENT_IDX_HSTY_SEQ;
CREATE OR REPLACE SYNONYM NLPCON.MAI_TEST_RST_DT_IDX_HSTY_SEQ FOR NLPADM.MAI_TEST_RST_DT_IDX_HSTY_SEQ;
CREATE OR REPLACE SYNONYM NLPCON.MAI_TEST_RST_IDX_HSTY_SEQ FOR NLPADM.MAI_TEST_RST_IDX_HSTY_SEQ;

GRANT DELETE ON NLPADM.BRAIN_BQA_CATEGORY TO NLPCON;
GRANT INSERT ON NLPADM.BRAIN_BQA_CATEGORY TO NLPCON;
GRANT SELECT ON NLPADM.BRAIN_BQA_CATEGORY TO NLPCON;
GRANT UPDATE ON NLPADM.BRAIN_BQA_CATEGORY TO NLPCON;

GRANT DELETE ON NLPADM.BRAIN_BQA_ITEM TO NLPCON;
GRANT INSERT ON NLPADM.BRAIN_BQA_ITEM TO NLPCON;
GRANT SELECT ON NLPADM.BRAIN_BQA_ITEM TO NLPCON;
GRANT UPDATE ON NLPADM.BRAIN_BQA_ITEM TO NLPCON;

GRANT DELETE ON NLPADM.BRAIN_BQA_PRPRS_MAIN_WD TO NLPCON;
GRANT INSERT ON NLPADM.BRAIN_BQA_PRPRS_MAIN_WD TO NLPCON;
GRANT SELECT ON NLPADM.BRAIN_BQA_PRPRS_MAIN_WD TO NLPCON;
GRANT UPDATE ON NLPADM.BRAIN_BQA_PRPRS_MAIN_WD TO NLPCON;

GRANT DELETE ON NLPADM.BRAIN_BQA_PRPRS_REL TO NLPCON;
GRANT INSERT ON NLPADM.BRAIN_BQA_PRPRS_REL TO NLPCON;
GRANT SELECT ON NLPADM.BRAIN_BQA_PRPRS_REL TO NLPCON;
GRANT UPDATE ON NLPADM.BRAIN_BQA_PRPRS_REL TO NLPCON;

GRANT DELETE ON NLPADM.BRAIN_BQA_PRPRS_WD TO NLPCON;
GRANT INSERT ON NLPADM.BRAIN_BQA_PRPRS_WD TO NLPCON;
GRANT SELECT ON NLPADM.BRAIN_BQA_PRPRS_WD TO NLPCON;
GRANT UPDATE ON NLPADM.BRAIN_BQA_PRPRS_WD TO NLPCON;

GRANT DELETE ON NLPADM.BRAIN_BQA_SKILL TO NLPCON;
GRANT INSERT ON NLPADM.BRAIN_BQA_SKILL TO NLPCON;
GRANT SELECT ON NLPADM.BRAIN_BQA_SKILL TO NLPCON;
GRANT UPDATE ON NLPADM.BRAIN_BQA_SKILL TO NLPCON;

GRANT DELETE ON NLPADM.MAI_ACCESS_TOKEN TO NLPCON;
GRANT INSERT ON NLPADM.MAI_ACCESS_TOKEN TO NLPCON;
GRANT SELECT ON NLPADM.MAI_ACCESS_TOKEN TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_ACCESS_TOKEN TO NLPCON;

GRANT DELETE ON NLPADM.MAI_ACL TO NLPCON;
GRANT INSERT ON NLPADM.MAI_ACL TO NLPCON;
GRANT SELECT ON NLPADM.MAI_ACL TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_ACL TO NLPCON;

GRANT DELETE ON NLPADM.MAI_BQA_INDEXING_HISTORY TO NLPCON;
GRANT INSERT ON NLPADM.MAI_BQA_INDEXING_HISTORY TO NLPCON;
GRANT SELECT ON NLPADM.MAI_BQA_INDEXING_HISTORY TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_BQA_INDEXING_HISTORY TO NLPCON;

GRANT DELETE ON NLPADM.MAI_CODE TO NLPCON;
GRANT INSERT ON NLPADM.MAI_CODE TO NLPCON;
GRANT SELECT ON NLPADM.MAI_CODE TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_CODE TO NLPCON;

GRANT DELETE ON NLPADM.MAI_COMMIT_HISTORY TO NLPCON;
GRANT INSERT ON NLPADM.MAI_COMMIT_HISTORY TO NLPCON;
GRANT SELECT ON NLPADM.MAI_COMMIT_HISTORY TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_COMMIT_HISTORY TO NLPCON;

GRANT DELETE ON NLPADM.MAI_DNN_CATEGORY TO NLPCON;
GRANT INSERT ON NLPADM.MAI_DNN_CATEGORY TO NLPCON;
GRANT SELECT ON NLPADM.MAI_DNN_CATEGORY TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_DNN_CATEGORY TO NLPCON;

GRANT DELETE ON NLPADM.MAI_DNN_DIC TO NLPCON;
GRANT INSERT ON NLPADM.MAI_DNN_DIC TO NLPCON;
GRANT SELECT ON NLPADM.MAI_DNN_DIC TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_DNN_DIC TO NLPCON;

GRANT DELETE ON NLPADM.MAI_DNN_DIC_LINE TO NLPCON;
GRANT INSERT ON NLPADM.MAI_DNN_DIC_LINE TO NLPCON;
GRANT SELECT ON NLPADM.MAI_DNN_DIC_LINE TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_DNN_DIC_LINE TO NLPCON;

GRANT DELETE ON NLPADM.MAI_DNN_MODEL TO NLPCON;
GRANT INSERT ON NLPADM.MAI_DNN_MODEL TO NLPCON;
GRANT SELECT ON NLPADM.MAI_DNN_MODEL TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_DNN_MODEL TO NLPCON;

GRANT DELETE ON NLPADM.MAI_DNN_RESULT TO NLPCON;
GRANT INSERT ON NLPADM.MAI_DNN_RESULT TO NLPCON;
GRANT SELECT ON NLPADM.MAI_DNN_RESULT TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_DNN_RESULT TO NLPCON;

GRANT DELETE ON NLPADM.MAI_DNN_TRAINING TO NLPCON;
GRANT INSERT ON NLPADM.MAI_DNN_TRAINING TO NLPCON;
GRANT SELECT ON NLPADM.MAI_DNN_TRAINING TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_DNN_TRAINING TO NLPCON;

GRANT DELETE ON NLPADM.MAI_END_POST_DIC TO NLPCON;
GRANT INSERT ON NLPADM.MAI_END_POST_DIC TO NLPCON;
GRANT SELECT ON NLPADM.MAI_END_POST_DIC TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_END_POST_DIC TO NLPCON;

GRANT DELETE ON NLPADM.MAI_END_POST_DIC_REL TO NLPCON;
GRANT INSERT ON NLPADM.MAI_END_POST_DIC_REL TO NLPCON;
GRANT SELECT ON NLPADM.MAI_END_POST_DIC_REL TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_END_POST_DIC_REL TO NLPCON;

GRANT DELETE ON NLPADM.MAI_FILE TO NLPCON;
GRANT INSERT ON NLPADM.MAI_FILE TO NLPCON;
GRANT SELECT ON NLPADM.MAI_FILE TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_FILE TO NLPCON;

GRANT DELETE ON NLPADM.MAI_FILE_GROUP TO NLPCON;
GRANT INSERT ON NLPADM.MAI_FILE_GROUP TO NLPCON;
GRANT SELECT ON NLPADM.MAI_FILE_GROUP TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_FILE_GROUP TO NLPCON;

GRANT DELETE ON NLPADM.MAI_FILE_GROUP_REL TO NLPCON;
GRANT INSERT ON NLPADM.MAI_FILE_GROUP_REL TO NLPCON;
GRANT SELECT ON NLPADM.MAI_FILE_GROUP_REL TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_FILE_GROUP_REL TO NLPCON;

GRANT DELETE ON NLPADM.MAI_GROUP TO NLPCON;
GRANT INSERT ON NLPADM.MAI_GROUP TO NLPCON;
GRANT SELECT ON NLPADM.MAI_GROUP TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_GROUP TO NLPCON;

GRANT DELETE ON NLPADM.MAI_GROUP_MEMBER TO NLPCON;
GRANT INSERT ON NLPADM.MAI_GROUP_MEMBER TO NLPCON;
GRANT SELECT ON NLPADM.MAI_GROUP_MEMBER TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_GROUP_MEMBER TO NLPCON;

GRANT DELETE ON NLPADM.MAI_HISTORY TO NLPCON;
GRANT INSERT ON NLPADM.MAI_HISTORY TO NLPCON;
GRANT SELECT ON NLPADM.MAI_HISTORY TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_HISTORY TO NLPCON;

GRANT DELETE ON NLPADM.MAI_HMD_CATEGORY TO NLPCON;
GRANT INSERT ON NLPADM.MAI_HMD_CATEGORY TO NLPCON;
GRANT SELECT ON NLPADM.MAI_HMD_CATEGORY TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_HMD_CATEGORY TO NLPCON;

GRANT DELETE ON NLPADM.MAI_HMD_DIC TO NLPCON;
GRANT INSERT ON NLPADM.MAI_HMD_DIC TO NLPCON;
GRANT SELECT ON NLPADM.MAI_HMD_DIC TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_HMD_DIC TO NLPCON;

GRANT DELETE ON NLPADM.MAI_HMD_DIC_LINE TO NLPCON;
GRANT INSERT ON NLPADM.MAI_HMD_DIC_LINE TO NLPCON;
GRANT SELECT ON NLPADM.MAI_HMD_DIC_LINE TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_HMD_DIC_LINE TO NLPCON;

GRANT DELETE ON NLPADM.MAI_HMD_RESULT TO NLPCON;
GRANT INSERT ON NLPADM.MAI_HMD_RESULT TO NLPCON;
GRANT SELECT ON NLPADM.MAI_HMD_RESULT TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_HMD_RESULT TO NLPCON;

GRANT DELETE ON NLPADM.MAI_MRC_ANSWER TO NLPCON;
GRANT INSERT ON NLPADM.MAI_MRC_ANSWER TO NLPCON;
GRANT SELECT ON NLPADM.MAI_MRC_ANSWER TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_MRC_ANSWER TO NLPCON;

GRANT DELETE ON NLPADM.MAI_MRC_CATEGORY TO NLPCON;
GRANT INSERT ON NLPADM.MAI_MRC_CATEGORY TO NLPCON;
GRANT SELECT ON NLPADM.MAI_MRC_CATEGORY TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_MRC_CATEGORY TO NLPCON;

GRANT DELETE ON NLPADM.MAI_MRC_CONTEXT TO NLPCON;
GRANT INSERT ON NLPADM.MAI_MRC_CONTEXT TO NLPCON;
GRANT SELECT ON NLPADM.MAI_MRC_CONTEXT TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_MRC_CONTEXT TO NLPCON;

GRANT DELETE ON NLPADM.MAI_MRC_DATA_GROUP TO NLPCON;
GRANT INSERT ON NLPADM.MAI_MRC_DATA_GROUP TO NLPCON;
GRANT SELECT ON NLPADM.MAI_MRC_DATA_GROUP TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_MRC_DATA_GROUP TO NLPCON;

GRANT DELETE ON NLPADM.MAI_MRC_DATA_GROUP_REL TO NLPCON;
GRANT INSERT ON NLPADM.MAI_MRC_DATA_GROUP_REL TO NLPCON;
GRANT SELECT ON NLPADM.MAI_MRC_DATA_GROUP_REL TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_MRC_DATA_GROUP_REL TO NLPCON;

GRANT DELETE ON NLPADM.MAI_MRC_PASSAGE TO NLPCON;
GRANT INSERT ON NLPADM.MAI_MRC_PASSAGE TO NLPCON;
GRANT SELECT ON NLPADM.MAI_MRC_PASSAGE TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_MRC_PASSAGE TO NLPCON;

GRANT DELETE ON NLPADM.MAI_MRC_PASSAGE_IDX_HISTORY TO NLPCON;
GRANT INSERT ON NLPADM.MAI_MRC_PASSAGE_IDX_HISTORY TO NLPCON;
GRANT SELECT ON NLPADM.MAI_MRC_PASSAGE_IDX_HISTORY TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_MRC_PASSAGE_IDX_HISTORY TO NLPCON;

GRANT DELETE ON NLPADM.MAI_MRC_PASSAGE_QUESTION TO NLPCON;
GRANT INSERT ON NLPADM.MAI_MRC_PASSAGE_QUESTION TO NLPCON;
GRANT SELECT ON NLPADM.MAI_MRC_PASSAGE_QUESTION TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_MRC_PASSAGE_QUESTION TO NLPCON;

GRANT DELETE ON NLPADM.MAI_MRC_PASSAGE_SKILL TO NLPCON;
GRANT INSERT ON NLPADM.MAI_MRC_PASSAGE_SKILL TO NLPCON;
GRANT SELECT ON NLPADM.MAI_MRC_PASSAGE_SKILL TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_MRC_PASSAGE_SKILL TO NLPCON;

GRANT DELETE ON NLPADM.MAI_MRC_QUESTION TO NLPCON;
GRANT INSERT ON NLPADM.MAI_MRC_QUESTION TO NLPCON;
GRANT SELECT ON NLPADM.MAI_MRC_QUESTION TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_MRC_QUESTION TO NLPCON;

GRANT DELETE ON NLPADM.MAI_MRC_TRAINING_MODEL TO NLPCON;
GRANT INSERT ON NLPADM.MAI_MRC_TRAINING_MODEL TO NLPCON;
GRANT SELECT ON NLPADM.MAI_MRC_TRAINING_MODEL TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_MRC_TRAINING_MODEL TO NLPCON;

GRANT DELETE ON NLPADM.MAI_MRC_WIKI TO NLPCON;
GRANT INSERT ON NLPADM.MAI_MRC_WIKI TO NLPCON;
GRANT SELECT ON NLPADM.MAI_MRC_WIKI TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_MRC_WIKI TO NLPCON;

GRANT DELETE ON NLPADM.MAI_NER_CORPUS TO NLPCON;
GRANT INSERT ON NLPADM.MAI_NER_CORPUS TO NLPCON;
GRANT SELECT ON NLPADM.MAI_NER_CORPUS TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_NER_CORPUS TO NLPCON;

GRANT DELETE ON NLPADM.MAI_NER_CORPUS_TAG TO NLPCON;
GRANT INSERT ON NLPADM.MAI_NER_CORPUS_TAG TO NLPCON;
GRANT SELECT ON NLPADM.MAI_NER_CORPUS_TAG TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_NER_CORPUS_TAG TO NLPCON;

GRANT DELETE ON NLPADM.MAI_NER_DICTIONARY TO NLPCON;
GRANT INSERT ON NLPADM.MAI_NER_DICTIONARY TO NLPCON;
GRANT SELECT ON NLPADM.MAI_NER_DICTIONARY TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_NER_DICTIONARY TO NLPCON;

GRANT DELETE ON NLPADM.MAI_NER_DICTIONARY_LINE TO NLPCON;
GRANT INSERT ON NLPADM.MAI_NER_DICTIONARY_LINE TO NLPCON;
GRANT SELECT ON NLPADM.MAI_NER_DICTIONARY_LINE TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_NER_DICTIONARY_LINE TO NLPCON;

GRANT DELETE ON NLPADM.MAI_NQA_INDEX_HISTORY TO NLPCON;
GRANT INSERT ON NLPADM.MAI_NQA_INDEX_HISTORY TO NLPCON;
GRANT SELECT ON NLPADM.MAI_NQA_INDEX_HISTORY TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_NQA_INDEX_HISTORY TO NLPCON;

GRANT DELETE ON NLPADM.MAI_PASSWORD_HISTORY TO NLPCON;
GRANT INSERT ON NLPADM.MAI_PASSWORD_HISTORY TO NLPCON;
GRANT SELECT ON NLPADM.MAI_PASSWORD_HISTORY TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_PASSWORD_HISTORY TO NLPCON;

GRANT DELETE ON NLPADM.MAI_POS_DICTIONARY TO NLPCON;
GRANT INSERT ON NLPADM.MAI_POS_DICTIONARY TO NLPCON;
GRANT SELECT ON NLPADM.MAI_POS_DICTIONARY TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_POS_DICTIONARY TO NLPCON;

GRANT DELETE ON NLPADM.MAI_POS_DICTIONARY_LINE TO NLPCON;
GRANT INSERT ON NLPADM.MAI_POS_DICTIONARY_LINE TO NLPCON;
GRANT SELECT ON NLPADM.MAI_POS_DICTIONARY_LINE TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_POS_DICTIONARY_LINE TO NLPCON;

GRANT DELETE ON NLPADM.MAI_REPLACEMENT_DICTIONARY TO NLPCON;
GRANT INSERT ON NLPADM.MAI_REPLACEMENT_DICTIONARY TO NLPCON;
GRANT SELECT ON NLPADM.MAI_REPLACEMENT_DICTIONARY TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_REPLACEMENT_DICTIONARY TO NLPCON;

GRANT DELETE ON NLPADM.MAI_REPLACEMENT_DICTIONARY_TYPE TO NLPCON;
GRANT INSERT ON NLPADM.MAI_REPLACEMENT_DICTIONARY_TYPE TO NLPCON;
GRANT SELECT ON NLPADM.MAI_REPLACEMENT_DICTIONARY_TYPE TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_REPLACEMENT_DICTIONARY_TYPE TO NLPCON;

GRANT DELETE ON NLPADM.MAI_REPLACEMENT_DICTIONARY_LINE TO NLPCON;
GRANT INSERT ON NLPADM.MAI_REPLACEMENT_DICTIONARY_LINE TO NLPCON;
GRANT SELECT ON NLPADM.MAI_REPLACEMENT_DICTIONARY_LINE TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_REPLACEMENT_DICTIONARY_LINE TO NLPCON;

GRANT DELETE ON NLPADM.MAI_QUERY_PRPRS TO NLPCON;
GRANT INSERT ON NLPADM.MAI_QUERY_PRPRS TO NLPCON;
GRANT SELECT ON NLPADM.MAI_QUERY_PRPRS TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_QUERY_PRPRS TO NLPCON;

GRANT DELETE ON NLPADM.MAI_QUERY_PRPRS_STEP TO NLPCON;
GRANT INSERT ON NLPADM.MAI_QUERY_PRPRS_STEP TO NLPCON;
GRANT SELECT ON NLPADM.MAI_QUERY_PRPRS_STEP TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_QUERY_PRPRS_STEP TO NLPCON;

GRANT DELETE ON NLPADM.MAI_ROLE TO NLPCON;
GRANT INSERT ON NLPADM.MAI_ROLE TO NLPCON;
GRANT SELECT ON NLPADM.MAI_ROLE TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_ROLE TO NLPCON;

GRANT DELETE ON NLPADM.MAI_ROLE_MAPPING TO NLPCON;
GRANT INSERT ON NLPADM.MAI_ROLE_MAPPING TO NLPCON;
GRANT SELECT ON NLPADM.MAI_ROLE_MAPPING TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_ROLE_MAPPING TO NLPCON;

GRANT DELETE ON NLPADM.MAI_SDS_ENTITY TO NLPCON;
GRANT INSERT ON NLPADM.MAI_SDS_ENTITY TO NLPCON;
GRANT SELECT ON NLPADM.MAI_SDS_ENTITY TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_SDS_ENTITY TO NLPCON;

GRANT DELETE ON NLPADM.MAI_SDS_ENTITY_DATA TO NLPCON;
GRANT INSERT ON NLPADM.MAI_SDS_ENTITY_DATA TO NLPCON;
GRANT SELECT ON NLPADM.MAI_SDS_ENTITY_DATA TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_SDS_ENTITY_DATA TO NLPCON;

GRANT DELETE ON NLPADM.MAI_SDS_ENTITY_DATA_SYNONYM TO NLPCON;
GRANT INSERT ON NLPADM.MAI_SDS_ENTITY_DATA_SYNONYM TO NLPCON;
GRANT SELECT ON NLPADM.MAI_SDS_ENTITY_DATA_SYNONYM TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_SDS_ENTITY_DATA_SYNONYM TO NLPCON;

GRANT DELETE ON NLPADM.MAI_SDS_EVENT TO NLPCON;
GRANT INSERT ON NLPADM.MAI_SDS_EVENT TO NLPCON;
GRANT SELECT ON NLPADM.MAI_SDS_EVENT TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_SDS_EVENT TO NLPCON;

GRANT DELETE ON NLPADM.MAI_SDS_EVENT_CONDITION TO NLPCON;
GRANT INSERT ON NLPADM.MAI_SDS_EVENT_CONDITION TO NLPCON;
GRANT SELECT ON NLPADM.MAI_SDS_EVENT_CONDITION TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_SDS_EVENT_CONDITION TO NLPCON;

GRANT DELETE ON NLPADM.MAI_SDS_EVENT_UTTER TO NLPCON;
GRANT INSERT ON NLPADM.MAI_SDS_EVENT_UTTER TO NLPCON;
GRANT SELECT ON NLPADM.MAI_SDS_EVENT_UTTER TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_SDS_EVENT_UTTER TO NLPCON;

GRANT DELETE ON NLPADM.MAI_SDS_EVENT_UTTER_DATA TO NLPCON;
GRANT INSERT ON NLPADM.MAI_SDS_EVENT_UTTER_DATA TO NLPCON;
GRANT SELECT ON NLPADM.MAI_SDS_EVENT_UTTER_DATA TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_SDS_EVENT_UTTER_DATA TO NLPCON;

GRANT DELETE ON NLPADM.MAI_SDS_INTENT TO NLPCON;
GRANT INSERT ON NLPADM.MAI_SDS_INTENT TO NLPCON;
GRANT SELECT ON NLPADM.MAI_SDS_INTENT TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_SDS_INTENT TO NLPCON;

GRANT DELETE ON NLPADM.MAI_SDS_INTENT_UTTER TO NLPCON;
GRANT INSERT ON NLPADM.MAI_SDS_INTENT_UTTER TO NLPCON;
GRANT SELECT ON NLPADM.MAI_SDS_INTENT_UTTER TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_SDS_INTENT_UTTER TO NLPCON;

GRANT DELETE ON NLPADM.MAI_SDS_INTENT_UTTER_DATA TO NLPCON;
GRANT INSERT ON NLPADM.MAI_SDS_INTENT_UTTER_DATA TO NLPCON;
GRANT SELECT ON NLPADM.MAI_SDS_INTENT_UTTER_DATA TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_SDS_INTENT_UTTER_DATA TO NLPCON;

GRANT DELETE ON NLPADM.MAI_SDS_MODEL TO NLPCON;
GRANT INSERT ON NLPADM.MAI_SDS_MODEL TO NLPCON;
GRANT SELECT ON NLPADM.MAI_SDS_MODEL TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_SDS_MODEL TO NLPCON;

GRANT DELETE ON NLPADM.MAI_SDS_MODEL_ENTITY TO NLPCON;
GRANT INSERT ON NLPADM.MAI_SDS_MODEL_ENTITY TO NLPCON;
GRANT SELECT ON NLPADM.MAI_SDS_MODEL_ENTITY TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_SDS_MODEL_ENTITY TO NLPCON;

GRANT DELETE ON NLPADM.MAI_SDS_MODEL_INTENT TO NLPCON;
GRANT INSERT ON NLPADM.MAI_SDS_MODEL_INTENT TO NLPCON;
GRANT SELECT ON NLPADM.MAI_SDS_MODEL_INTENT TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_SDS_MODEL_INTENT TO NLPCON;

GRANT DELETE ON NLPADM.MAI_SDS_NEXT_TASK TO NLPCON;
GRANT INSERT ON NLPADM.MAI_SDS_NEXT_TASK TO NLPCON;
GRANT SELECT ON NLPADM.MAI_SDS_NEXT_TASK TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_SDS_NEXT_TASK TO NLPCON;

GRANT DELETE ON NLPADM.MAI_SDS_START_EVENT TO NLPCON;
GRANT INSERT ON NLPADM.MAI_SDS_START_EVENT TO NLPCON;
GRANT SELECT ON NLPADM.MAI_SDS_START_EVENT TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_SDS_START_EVENT TO NLPCON;

GRANT DELETE ON NLPADM.MAI_SDS_START_EVENT_UTTER TO NLPCON;
GRANT INSERT ON NLPADM.MAI_SDS_START_EVENT_UTTER TO NLPCON;
GRANT SELECT ON NLPADM.MAI_SDS_START_EVENT_UTTER TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_SDS_START_EVENT_UTTER TO NLPCON;

GRANT DELETE ON NLPADM.MAI_SDS_START_EVENT_UTTER_DATA TO NLPCON;
GRANT INSERT ON NLPADM.MAI_SDS_START_EVENT_UTTER_DATA TO NLPCON;
GRANT SELECT ON NLPADM.MAI_SDS_START_EVENT_UTTER_DATA TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_SDS_START_EVENT_UTTER_DATA TO NLPCON;

GRANT DELETE ON NLPADM.MAI_SDS_TASK TO NLPCON;
GRANT INSERT ON NLPADM.MAI_SDS_TASK TO NLPCON;
GRANT SELECT ON NLPADM.MAI_SDS_TASK TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_SDS_TASK TO NLPCON;

GRANT DELETE ON NLPADM.MAI_SIGNIN_HISTORY TO NLPCON;
GRANT INSERT ON NLPADM.MAI_SIGNIN_HISTORY TO NLPCON;
GRANT SELECT ON NLPADM.MAI_SIGNIN_HISTORY TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_SIGNIN_HISTORY TO NLPCON;

GRANT DELETE ON NLPADM.MAI_STT_ANALYSIS_RESULT TO NLPCON;
GRANT INSERT ON NLPADM.MAI_STT_ANALYSIS_RESULT TO NLPCON;
GRANT SELECT ON NLPADM.MAI_STT_ANALYSIS_RESULT TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_STT_ANALYSIS_RESULT TO NLPCON;

GRANT DELETE ON NLPADM.MAI_STT_EVAL_RESULT TO NLPCON;
GRANT INSERT ON NLPADM.MAI_STT_EVAL_RESULT TO NLPCON;
GRANT SELECT ON NLPADM.MAI_STT_EVAL_RESULT TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_STT_EVAL_RESULT TO NLPCON;

GRANT DELETE ON NLPADM.MAI_STT_MODEL TO NLPCON;
GRANT INSERT ON NLPADM.MAI_STT_MODEL TO NLPCON;
GRANT SELECT ON NLPADM.MAI_STT_MODEL TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_STT_MODEL TO NLPCON;

GRANT DELETE ON NLPADM.MAI_STT_TRAINING TO NLPCON;
GRANT INSERT ON NLPADM.MAI_STT_TRAINING TO NLPCON;
GRANT SELECT ON NLPADM.MAI_STT_TRAINING TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_STT_TRAINING TO NLPCON;

GRANT DELETE ON NLPADM.MAI_STT_TRANSCRIPT TO NLPCON;
GRANT INSERT ON NLPADM.MAI_STT_TRANSCRIPT TO NLPCON;
GRANT SELECT ON NLPADM.MAI_STT_TRANSCRIPT TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_STT_TRANSCRIPT TO NLPCON;

GRANT DELETE ON NLPADM.MAI_STT_TRANSCRIPT_LINE TO NLPCON;
GRANT INSERT ON NLPADM.MAI_STT_TRANSCRIPT_LINE TO NLPCON;
GRANT SELECT ON NLPADM.MAI_STT_TRANSCRIPT_LINE TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_STT_TRANSCRIPT_LINE TO NLPCON;

GRANT DELETE ON NLPADM.MAI_SYNONYM_DIC TO NLPCON;
GRANT INSERT ON NLPADM.MAI_SYNONYM_DIC TO NLPCON;
GRANT SELECT ON NLPADM.MAI_SYNONYM_DIC TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_SYNONYM_DIC TO NLPCON;

GRANT DELETE ON NLPADM.MAI_SYNONYM_DIC_REL TO NLPCON;
GRANT INSERT ON NLPADM.MAI_SYNONYM_DIC_REL TO NLPCON;
GRANT SELECT ON NLPADM.MAI_SYNONYM_DIC_REL TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_SYNONYM_DIC_REL TO NLPCON;

GRANT DELETE ON NLPADM.MAI_TA_MODEL_MANAGEMENT TO NLPCON;
GRANT INSERT ON NLPADM.MAI_TA_MODEL_MANAGEMENT TO NLPCON;
GRANT SELECT ON NLPADM.MAI_TA_MODEL_MANAGEMENT TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_TA_MODEL_MANAGEMENT TO NLPCON;

GRANT DELETE ON NLPADM.MAI_TEST_FILES TO NLPCON;
GRANT INSERT ON NLPADM.MAI_TEST_FILES TO NLPCON;
GRANT SELECT ON NLPADM.MAI_TEST_FILES TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_TEST_FILES TO NLPCON;

GRANT DELETE ON NLPADM.MAI_TEST_RESULT TO NLPCON;
GRANT INSERT ON NLPADM.MAI_TEST_RESULT TO NLPCON;
GRANT SELECT ON NLPADM.MAI_TEST_RESULT TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_TEST_RESULT TO NLPCON;

GRANT DELETE ON NLPADM.MAI_TEST_RESULT_DETAIL TO NLPCON;
GRANT INSERT ON NLPADM.MAI_TEST_RESULT_DETAIL TO NLPCON;
GRANT SELECT ON NLPADM.MAI_TEST_RESULT_DETAIL TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_TEST_RESULT_DETAIL TO NLPCON;

GRANT DELETE ON NLPADM.MAI_TEST_SENTENCES TO NLPCON;
GRANT INSERT ON NLPADM.MAI_TEST_SENTENCES TO NLPCON;
GRANT SELECT ON NLPADM.MAI_TEST_SENTENCES TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_TEST_SENTENCES TO NLPCON;

GRANT DELETE ON NLPADM.MAI_TWE_INDEXING_HISTORY TO NLPCON;
GRANT INSERT ON NLPADM.MAI_TWE_INDEXING_HISTORY TO NLPCON;
GRANT SELECT ON NLPADM.MAI_TWE_INDEXING_HISTORY TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_TWE_INDEXING_HISTORY TO NLPCON;

GRANT DELETE ON NLPADM.MAI_TWE_SCHEDULE TO NLPCON;
GRANT INSERT ON NLPADM.MAI_TWE_SCHEDULE TO NLPCON;
GRANT SELECT ON NLPADM.MAI_TWE_SCHEDULE TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_TWE_SCHEDULE TO NLPCON;

GRANT DELETE ON NLPADM.MAI_USER TO NLPCON;
GRANT INSERT ON NLPADM.MAI_USER TO NLPCON;
GRANT SELECT ON NLPADM.MAI_USER TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_USER TO NLPCON;

GRANT DELETE ON NLPADM.MAI_WORKSPACE TO NLPCON;
GRANT INSERT ON NLPADM.MAI_WORKSPACE TO NLPCON;
GRANT SELECT ON NLPADM.MAI_WORKSPACE TO NLPCON;
GRANT UPDATE ON NLPADM.MAI_WORKSPACE TO NLPCON;

GRANT SELECT ON NLPADM.BRAIN_BQA_CATEGORY TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.BRAIN_BQA_ITEM TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.BRAIN_BQA_PRPRS_MAIN_WD TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.BRAIN_BQA_PRPRS_REL TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.BRAIN_BQA_PRPRS_WD TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.BRAIN_BQA_SKILL TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_ACCESS_TOKEN TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_ACL TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_BQA_INDEXING_HISTORY TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_CODE TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_COMMIT_HISTORY TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_DNN_CATEGORY TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_DNN_DIC TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_DNN_DIC_LINE TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_DNN_MODEL TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_DNN_RESULT TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_DNN_TRAINING TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_END_POST_DIC TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_END_POST_DIC_REL TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_FILE TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_FILE_GROUP TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_FILE_GROUP_REL TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_GROUP TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_GROUP_MEMBER TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_HISTORY TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_HMD_CATEGORY TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_HMD_DIC TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_HMD_DIC_LINE TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_HMD_RESULT TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_MRC_ANSWER TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_MRC_CATEGORY TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_MRC_CONTEXT TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_MRC_DATA_GROUP TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_MRC_DATA_GROUP_REL TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_MRC_PASSAGE TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_MRC_PASSAGE_IDX_HISTORY TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_MRC_PASSAGE_QUESTION TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_MRC_PASSAGE_SKILL TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_MRC_QUESTION TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_MRC_TRAINING_MODEL TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_MRC_WIKI TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_NER_CORPUS TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_NER_CORPUS_TAG TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_NER_DICTIONARY TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_NER_DICTIONARY_LINE TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_NQA_INDEX_HISTORY TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_PASSWORD_HISTORY TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_POS_DICTIONARY TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_POS_DICTIONARY_LINE TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_QUERY_PRPRS TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_QUERY_PRPRS_STEP TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_ROLE TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_ROLE_MAPPING TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_SDS_ENTITY TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_SDS_ENTITY_DATA TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_SDS_ENTITY_DATA_SYNONYM TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_SDS_EVENT TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_SDS_EVENT_CONDITION TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_SDS_EVENT_UTTER TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_SDS_EVENT_UTTER_DATA TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_SDS_INTENT TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_SDS_INTENT_UTTER TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_SDS_INTENT_UTTER_DATA TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_SDS_MODEL TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_SDS_MODEL_ENTITY TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_SDS_MODEL_INTENT TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_SDS_NEXT_TASK TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_SDS_START_EVENT TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_SDS_START_EVENT_UTTER TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_SDS_START_EVENT_UTTER_DATA TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_SDS_TASK TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_SIGNIN_HISTORY TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_STT_ANALYSIS_RESULT TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_STT_EVAL_RESULT TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_STT_MODEL TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_STT_TRAINING TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_STT_TRANSCRIPT TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_STT_TRANSCRIPT_LINE TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_SYNONYM_DIC TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_SYNONYM_DIC_REL TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_TA_MODEL_MANAGEMENT TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_TEST_FILES TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_TEST_RESULT TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_TEST_SENTENCES TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_TWE_INDEXING_HISTORY TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_TWE_SCHEDULE TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_USER TO RL_NLP_SEL;
GRANT SELECT ON NLPADM.MAI_WORKSPACE TO RL_NLP_SEL;

COMMENT ON TABLE NLPADM.MAI_ACL IS 'ACL';
COMMENT ON COLUMN NLPADM.MAI_ACL.ACCESS_TYPE IS '접근 타입';
COMMENT ON COLUMN NLPADM.MAI_ACL.ID IS 'ACL ID';
COMMENT ON COLUMN NLPADM.MAI_ACL.MODEL IS 'ACL 모델';
COMMENT ON COLUMN NLPADM.MAI_ACL.PERMISSION IS 'ACL 권한';
COMMENT ON COLUMN NLPADM.MAI_ACL.PRINCIPAL_ID IS '';
COMMENT ON COLUMN NLPADM.MAI_ACL.PRINCIPAL_TYPE IS '';
COMMENT ON COLUMN NLPADM.MAI_ACL.PROPERTY IS 'ACL 속성';

COMMENT ON TABLE NLPADM.MAI_ACCESS_TOKEN IS 'ACCESS TOKEN';
COMMENT ON COLUMN NLPADM.MAI_ACCESS_TOKEN.ID IS 'access token ID';
COMMENT ON COLUMN NLPADM.MAI_ACCESS_TOKEN.CREATED_AT IS '생성일';
COMMENT ON COLUMN NLPADM.MAI_ACCESS_TOKEN.SCOPES IS '';
COMMENT ON COLUMN NLPADM.MAI_ACCESS_TOKEN.TTL IS '';
COMMENT ON COLUMN NLPADM.MAI_ACCESS_TOKEN.USER_ID IS '사용자 ID';
COMMENT ON COLUMN NLPADM.MAI_ACCESS_TOKEN.ACCESS_TOKEN_ID IS 'access token ID 값';

COMMENT ON TABLE NLPADM.MAI_ROLE IS '권한';
COMMENT ON COLUMN NLPADM.MAI_ROLE.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_ROLE.DESCRIPTION IS '권한 설명';
COMMENT ON COLUMN NLPADM.MAI_ROLE.ID IS '권한 ID';
COMMENT ON COLUMN NLPADM.MAI_ROLE.NAME IS '권한명';
COMMENT ON COLUMN NLPADM.MAI_ROLE.UPDATED_AT IS '수정일자';

COMMENT ON TABLE NLPADM.MAI_ROLE_MAPPING IS '권한 매핑';
COMMENT ON COLUMN NLPADM.MAI_ROLE_MAPPING.ID IS '권한 매핑 ID';
COMMENT ON COLUMN NLPADM.MAI_ROLE_MAPPING.PRINCIPAL_ID IS '';
COMMENT ON COLUMN NLPADM.MAI_ROLE_MAPPING.PRINCIPAL_TYPE IS '';
COMMENT ON COLUMN NLPADM.MAI_ROLE_MAPPING.ROLE_ID IS '권한 ID';
COMMENT ON COLUMN NLPADM.MAI_ROLE_MAPPING.ROLE_MAPPING_ID IS '권한 매핑 ID 값';

COMMENT ON TABLE NLPADM.MAI_SIGNIN_HISTORY IS '로그인 기록';
COMMENT ON COLUMN NLPADM.MAI_SIGNIN_HISTORY.AGENT IS '';
COMMENT ON COLUMN NLPADM.MAI_SIGNIN_HISTORY.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_SIGNIN_HISTORY.ID IS '접속 히스토리 ID';
COMMENT ON COLUMN NLPADM.MAI_SIGNIN_HISTORY.MSG IS '접속 히스토리 메세지';
COMMENT ON COLUMN NLPADM.MAI_SIGNIN_HISTORY.REMOTE IS '';
COMMENT ON COLUMN NLPADM.MAI_SIGNIN_HISTORY.SIGNED IS '';
COMMENT ON COLUMN NLPADM.MAI_SIGNIN_HISTORY.USER_NAME IS '사용자 이름';

COMMENT ON TABLE NLPADM.MAI_CODE IS '코드';
COMMENT ON COLUMN NLPADM.MAI_CODE.GROUP_CODE IS '공통코드 그룹';
COMMENT ON COLUMN NLPADM.MAI_CODE.ID IS '공통코드 ID';
COMMENT ON COLUMN NLPADM.MAI_CODE.NAME IS '공통코드명';

COMMENT ON TABLE NLPADM.MAI_COMMIT_HISTORY IS '커밋 히스토리';
COMMENT ON COLUMN NLPADM.MAI_COMMIT_HISTORY.FILE_ID IS '커밋 히스토리 파일 ID';
COMMENT ON COLUMN NLPADM.MAI_COMMIT_HISTORY.ID IS '커밋 히스토리 ID';
COMMENT ON COLUMN NLPADM.MAI_COMMIT_HISTORY.TYPE IS '커밋 히스토리 타입';
COMMENT ON COLUMN NLPADM.MAI_COMMIT_HISTORY.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_FILE IS '파일';
COMMENT ON COLUMN NLPADM.MAI_FILE.CREATED_AT IS '생성일';
COMMENT ON COLUMN NLPADM.MAI_FILE.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_FILE.DURATION IS '';
COMMENT ON COLUMN NLPADM.MAI_FILE.FILE_SIZE IS '파일 사이즈';
COMMENT ON COLUMN NLPADM.MAI_FILE.HASH IS '파일 해시값';
COMMENT ON COLUMN NLPADM.MAI_FILE.ID IS '파일 ID';
COMMENT ON COLUMN NLPADM.MAI_FILE.META IS '파일 메타값';
COMMENT ON COLUMN NLPADM.MAI_FILE.NAME IS '파일명';
COMMENT ON COLUMN NLPADM.MAI_FILE.PURPOSE IS '파일 목적';
COMMENT ON COLUMN NLPADM.MAI_FILE.TYPE IS '파일 타입';
COMMENT ON COLUMN NLPADM.MAI_FILE.UPDATED_AT IS '수정일';
COMMENT ON COLUMN NLPADM.MAI_FILE.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_FILE.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_FILE_GROUP IS '파일 그룹';
COMMENT ON COLUMN NLPADM.MAI_FILE_GROUP.CREATED_AT IS '생성일';
COMMENT ON COLUMN NLPADM.MAI_FILE_GROUP.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_FILE_GROUP.ID IS '파일 그룹 ID';
COMMENT ON COLUMN NLPADM.MAI_FILE_GROUP.META IS '파일 그룹 메타값';
COMMENT ON COLUMN NLPADM.MAI_FILE_GROUP.NAME IS '파일 그룹명';
COMMENT ON COLUMN NLPADM.MAI_FILE_GROUP.PURPOSE IS '파일 그룹 목적';
COMMENT ON COLUMN NLPADM.MAI_FILE_GROUP.UPDATED_AT IS '수정일';
COMMENT ON COLUMN NLPADM.MAI_FILE_GROUP.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_FILE_GROUP.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_FILE_GROUP_REL IS '파일 그룹 관계';
COMMENT ON COLUMN NLPADM.MAI_FILE_GROUP_REL.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_FILE_GROUP_REL.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_FILE_GROUP_REL.FILE_GROUP_ID IS '파일 그룹 ID';
COMMENT ON COLUMN NLPADM.MAI_FILE_GROUP_REL.FILE_ID IS '파일 ID';
COMMENT ON COLUMN NLPADM.MAI_FILE_GROUP_REL.ID IS '파일 그룹 관계 ID';

COMMENT ON TABLE NLPADM.MAI_GROUP IS '그룹';
COMMENT ON COLUMN NLPADM.MAI_GROUP.DESCRIPTION IS '그룹 설명';
COMMENT ON COLUMN NLPADM.MAI_GROUP.ID IS '그룹 ID';
COMMENT ON COLUMN NLPADM.MAI_GROUP.NAME IS '그룹명';
COMMENT ON COLUMN NLPADM.MAI_GROUP.OWNER_ID IS '그룹 관리자';

COMMENT ON TABLE NLPADM.MAI_GROUP_MEMBER IS '그룹 멤버';
COMMENT ON COLUMN NLPADM.MAI_GROUP_MEMBER.DESCRIPTION IS '그룹원 설명';
COMMENT ON COLUMN NLPADM.MAI_GROUP_MEMBER.GROUP_ID IS '그룹 ID';
COMMENT ON COLUMN NLPADM.MAI_GROUP_MEMBER.ID IS '그룹원 ID';
COMMENT ON COLUMN NLPADM.MAI_GROUP_MEMBER.ROLE IS '그룹원 권한';
COMMENT ON COLUMN NLPADM.MAI_GROUP_MEMBER.STATE IS '그룹원 상태';
COMMENT ON COLUMN NLPADM.MAI_GROUP_MEMBER.USER_ID IS '사용자 ID';

COMMENT ON TABLE NLPADM.MAI_HISTORY IS '히스토리';
COMMENT ON COLUMN NLPADM.MAI_HISTORY.CODE IS '히스토리 코드';
COMMENT ON COLUMN NLPADM.MAI_HISTORY.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_HISTORY.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_HISTORY.DATA IS '히스토리 데이터';
COMMENT ON COLUMN NLPADM.MAI_HISTORY.ENDED_AT IS '히스토리 완료일자';
COMMENT ON COLUMN NLPADM.MAI_HISTORY.ID IS '히스토리 ID';
COMMENT ON COLUMN NLPADM.MAI_HISTORY.MESSAGE IS '히스토리 메세지';
COMMENT ON COLUMN NLPADM.MAI_HISTORY.STARTED_AT IS '히스토리 시작일자';
COMMENT ON COLUMN NLPADM.MAI_HISTORY.UPDATED_AT IS '수정일';
COMMENT ON COLUMN NLPADM.MAI_HISTORY.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_HISTORY.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_NER_CORPUS IS '개체명 사전 테스트 대상 코퍼스';
COMMENT ON COLUMN NLPADM.MAI_NER_CORPUS.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_NER_CORPUS.ID IS '코퍼스 ID';
COMMENT ON COLUMN NLPADM.MAI_NER_CORPUS.SENTENCE IS '코퍼스 문장';

COMMENT ON TABLE NLPADM.MAI_NER_CORPUS_TAG IS '개체명 사전 테스트 대상 코퍼스 태그';
COMMENT ON COLUMN NLPADM.MAI_NER_CORPUS_TAG.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_NER_CORPUS_TAG.ID IS '코퍼스 태그 ID';
COMMENT ON COLUMN NLPADM.MAI_NER_CORPUS_TAG.TAG IS '코퍼스 태그명';
COMMENT ON COLUMN NLPADM.MAI_NER_CORPUS_TAG.WORD IS '단어';

COMMENT ON TABLE NLPADM.MAI_NER_DICTIONARY IS '개체명 사전';
COMMENT ON COLUMN NLPADM.MAI_NER_DICTIONARY.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_NER_DICTIONARY.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_NER_DICTIONARY.DESCRIPTION IS '개체명 사전 설명';
COMMENT ON COLUMN NLPADM.MAI_NER_DICTIONARY.ID IS '개체명 사전 ID';
COMMENT ON COLUMN NLPADM.MAI_NER_DICTIONARY.NAME IS '개체명 사전명';
COMMENT ON COLUMN NLPADM.MAI_NER_DICTIONARY.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_NER_DICTIONARY_LINE IS '개체명 사전 데이터';
COMMENT ON COLUMN NLPADM.MAI_NER_DICTIONARY_LINE.CHANGE_TAG IS '개체명 변환 태그';
COMMENT ON COLUMN NLPADM.MAI_NER_DICTIONARY_LINE.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_NER_DICTIONARY_LINE.DESCRIPTION IS '개체명 사전 데이터 설명';
COMMENT ON COLUMN NLPADM.MAI_NER_DICTIONARY_LINE.DIC_TYPE IS '개체명 사전 데이터 타입';
COMMENT ON COLUMN NLPADM.MAI_NER_DICTIONARY_LINE.ID IS '개체명 사전 데이터 ID';
COMMENT ON COLUMN NLPADM.MAI_NER_DICTIONARY_LINE.ORIGIN_TAG IS '개체명 사전 데이터 원래 태그';
COMMENT ON COLUMN NLPADM.MAI_NER_DICTIONARY_LINE.PATTERN IS '개체명 사전 데이터 패턴';
COMMENT ON COLUMN NLPADM.MAI_NER_DICTIONARY_LINE.TAG IS '개체명 사전 데이터 태그';
COMMENT ON COLUMN NLPADM.MAI_NER_DICTIONARY_LINE.VERSION_ID IS '개체명 사전 ID';
COMMENT ON COLUMN NLPADM.MAI_NER_DICTIONARY_LINE.WORD IS '개체명 사전 데이터 단어';
COMMENT ON COLUMN NLPADM.MAI_NER_DICTIONARY_LINE.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_NQA_INDEX_HISTORY IS 'NQA 인덱스 히스토리';
COMMENT ON COLUMN NLPADM.MAI_NQA_INDEX_HISTORY.ADDRESS IS 'NQA 인덱싱 API 주소';
COMMENT ON COLUMN NLPADM.MAI_NQA_INDEX_HISTORY.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_NQA_INDEX_HISTORY.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_NQA_INDEX_HISTORY.FETCHED IS 'NQA 인덱싱 가지고온 item 갯수'';';
COMMENT ON COLUMN NLPADM.MAI_NQA_INDEX_HISTORY.ID IS 'NQA 인덱싱 히스토리 ID';
COMMENT ON COLUMN NLPADM.MAI_NQA_INDEX_HISTORY.MESSAGE IS 'NQA 인덱싱 히스토리 메세지';
COMMENT ON COLUMN NLPADM.MAI_NQA_INDEX_HISTORY.PROCESSED IS 'NQA 인덱싱 완료된 갯수';
COMMENT ON COLUMN NLPADM.MAI_NQA_INDEX_HISTORY.STATUS IS 'NQA 인덱싱 히스토리 상태';
COMMENT ON COLUMN NLPADM.MAI_NQA_INDEX_HISTORY.STOP_YN IS 'NQA 인덱싱 중단 여부';
COMMENT ON COLUMN NLPADM.MAI_NQA_INDEX_HISTORY.TOTAL IS 'NQA 인덱싱 item 총 갯수';
COMMENT ON COLUMN NLPADM.MAI_NQA_INDEX_HISTORY.TYPE IS 'NQA 인덱싱 히스토리 타입';
COMMENT ON COLUMN NLPADM.MAI_NQA_INDEX_HISTORY.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_NQA_INDEX_HISTORY.UPDATER_ID IS '수정자';

COMMENT ON TABLE NLPADM.MAI_PASSWORD_HISTORY IS '비밀번호 히스토리';
COMMENT ON COLUMN NLPADM.MAI_PASSWORD_HISTORY.ID IS '비밀번호 히스토리 ID';
COMMENT ON COLUMN NLPADM.MAI_PASSWORD_HISTORY.LAST_UPDATED IS '최근 수정 일자';
COMMENT ON COLUMN NLPADM.MAI_PASSWORD_HISTORY.PASSWORD IS '비밀번호';
COMMENT ON COLUMN NLPADM.MAI_PASSWORD_HISTORY.USER_ID IS '사용자 ID';

COMMENT ON TABLE NLPADM.MAI_POS_DICTIONARY IS '형태소 사전';
COMMENT ON COLUMN NLPADM.MAI_POS_DICTIONARY.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_POS_DICTIONARY.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_POS_DICTIONARY.DESCRIPTION IS '형태소 사전 설명';
COMMENT ON COLUMN NLPADM.MAI_POS_DICTIONARY.ID IS '형태소 사전 ID';
COMMENT ON COLUMN NLPADM.MAI_POS_DICTIONARY.NAME IS '형태소 사전명';
COMMENT ON COLUMN NLPADM.MAI_POS_DICTIONARY.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_POS_DICTIONARY_LINE IS '형태소 사전 데이터';
COMMENT ON COLUMN NLPADM.MAI_POS_DICTIONARY_LINE.COMPOUND_TYPE IS '형태소 사전 ';
COMMENT ON COLUMN NLPADM.MAI_POS_DICTIONARY_LINE.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_POS_DICTIONARY_LINE.DESCRIPTION IS '형태소 사전 데이터 설명';
COMMENT ON COLUMN NLPADM.MAI_POS_DICTIONARY_LINE.DEST_POS IS '형태소 사전 데이터 변경할 형태소';
COMMENT ON COLUMN NLPADM.MAI_POS_DICTIONARY_LINE.DIC_TYPE IS '형태소 사전 데이터 타입';
COMMENT ON COLUMN NLPADM.MAI_POS_DICTIONARY_LINE.ID IS '형태소 사전 데이터 ID';
COMMENT ON COLUMN NLPADM.MAI_POS_DICTIONARY_LINE.PATTERN IS '형태소 사전 데이터 패턴';
COMMENT ON COLUMN NLPADM.MAI_POS_DICTIONARY_LINE.SRC_POS IS '형태소 사전 데이터 원래 형태소';
COMMENT ON COLUMN NLPADM.MAI_POS_DICTIONARY_LINE.VERSION_ID IS '형태소 사전 ID';
COMMENT ON COLUMN NLPADM.MAI_POS_DICTIONARY_LINE.WORD IS '형태소 사전 데이터 단어';
COMMENT ON COLUMN NLPADM.MAI_POS_DICTIONARY_LINE.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_REPLACEMENT_DICTIONARY IS '치환 사전 ';
COMMENT ON COLUMN NLPADM.MAI_REPLACEMENT_DICTIONARY.APPLIED_YN IS '적용 여부';
COMMENT ON COLUMN NLPADM.MAI_REPLACEMENT_DICTIONARY.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_REPLACEMENT_DICTIONARY.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_REPLACEMENT_DICTIONARY.DESCRIPTION IS '치환 사전 설명';
COMMENT ON COLUMN NLPADM.MAI_REPLACEMENT_DICTIONARY.ID IS '치환 사전 ID';
COMMENT ON COLUMN NLPADM.MAI_REPLACEMENT_DICTIONARY.NAME IS '치환 사전명';
COMMENT ON COLUMN NLPADM.MAI_REPLACEMENT_DICTIONARY.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_REPLACEMENT_DICTIONARY_TYPE IS '치환 타입';
COMMENT ON COLUMN NLPADM.MAI_REPLACEMENT_DICTIONARY_TYPE.NAME IS '치환 타입 이름';
COMMENT ON COLUMN NLPADM.MAI_REPLACEMENT_DICTIONARY_TYPE.USE_YN IS '사용여부';

COMMENT ON TABLE NLPADM.MAI_REPLACEMENT_DICTIONARY_LINE IS '치환 사전 데이터';
COMMENT ON COLUMN NLPADM.MAI_REPLACEMENT_DICTIONARY_LINE.ID IS '치환 사전 데이터 ID';
COMMENT ON COLUMN NLPADM.MAI_REPLACEMENT_DICTIONARY_LINE.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_REPLACEMENT_DICTIONARY_LINE.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_REPLACEMENT_DICTIONARY_LINE.DESCRIPTION IS '형태소 사전 데이터 설명';
COMMENT ON COLUMN NLPADM.MAI_REPLACEMENT_DICTIONARY_LINE.DEST_STR IS '치환 사전 데이터 변경할 문자열';
COMMENT ON COLUMN NLPADM.MAI_REPLACEMENT_DICTIONARY_LINE.SRC_STR IS '치환 사전 데이터 원래 문자열';
COMMENT ON COLUMN NLPADM.MAI_REPLACEMENT_DICTIONARY_LINE.TYPE IS '치환 타입';
COMMENT ON COLUMN NLPADM.MAI_REPLACEMENT_DICTIONARY_LINE.USE_YN IS '사용 여부';
COMMENT ON COLUMN NLPADM.MAI_REPLACEMENT_DICTIONARY_LINE.VERSION_ID IS '치환 사전 ID';

COMMENT ON TABLE NLPADM.MAI_QUERY_PRPRS IS 'Query Paraphrase';
COMMENT ON COLUMN NLPADM.MAI_QUERY_PRPRS.ALL_STEP IS 'Query Paraphrase Step';
COMMENT ON COLUMN NLPADM.MAI_QUERY_PRPRS.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_QUERY_PRPRS.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_QUERY_PRPRS.CURRENT_STEP IS 'Query Paraphrase 현재 Step';
COMMENT ON COLUMN NLPADM.MAI_QUERY_PRPRS.ID IS 'Query Paraphrase ID';
COMMENT ON COLUMN NLPADM.MAI_QUERY_PRPRS.TITLE IS 'Query Paraphrase 이름';
COMMENT ON COLUMN NLPADM.MAI_QUERY_PRPRS.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_QUERY_PRPRS_STEP IS 'Query Paraphrase Step';
COMMENT ON COLUMN NLPADM.MAI_QUERY_PRPRS_STEP.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_QUERY_PRPRS_STEP.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_QUERY_PRPRS_STEP.ID IS 'Query Paraphrase Step ID';
COMMENT ON COLUMN NLPADM.MAI_QUERY_PRPRS_STEP.ORG_SENTENCE IS 'Query Paraphrase Step 기존문장';
COMMENT ON COLUMN NLPADM.MAI_QUERY_PRPRS_STEP.QUERY_PARAPHRASE_ID IS 'Query Paraphrase ID';
COMMENT ON COLUMN NLPADM.MAI_QUERY_PRPRS_STEP.SENTENCE IS 'Query Paraphrase Step 문장';
COMMENT ON COLUMN NLPADM.MAI_QUERY_PRPRS_STEP.STEP_CD IS 'Query Paraphrase Step Stepcd';
COMMENT ON COLUMN NLPADM.MAI_QUERY_PRPRS_STEP.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_STT_ANALYSIS_RESULT IS 'STT 분석결과';
COMMENT ON COLUMN NLPADM.MAI_STT_ANALYSIS_RESULT.AM_MODEL_ID IS 'AM 모델 ID';
COMMENT ON COLUMN NLPADM.MAI_STT_ANALYSIS_RESULT.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_STT_ANALYSIS_RESULT.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_STT_ANALYSIS_RESULT.FILE_GROUP_ID IS '파일 그룹 ID';
COMMENT ON COLUMN NLPADM.MAI_STT_ANALYSIS_RESULT.FILE_ID IS '파일 ID';
COMMENT ON COLUMN NLPADM.MAI_STT_ANALYSIS_RESULT.ID IS 'STT 분석결과 ID';
COMMENT ON COLUMN NLPADM.MAI_STT_ANALYSIS_RESULT.LM_MODEL_ID IS 'LM 모델 ID';
COMMENT ON COLUMN NLPADM.MAI_STT_ANALYSIS_RESULT.TEXT IS 'STT 분석결과 내용';
COMMENT ON COLUMN NLPADM.MAI_STT_ANALYSIS_RESULT.TRANSCRIPT_ID IS 'STT 전사자';
COMMENT ON COLUMN NLPADM.MAI_STT_ANALYSIS_RESULT.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_STT_ANALYSIS_RESULT.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_STT_ANALYSIS_RESULT.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_STT_EVAL_RESULT IS 'STT 평가결과';
COMMENT ON COLUMN NLPADM.MAI_STT_EVAL_RESULT.ANALYSIS_RESULT_ID IS '분석 결과 ID';
COMMENT ON COLUMN NLPADM.MAI_STT_EVAL_RESULT.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_STT_EVAL_RESULT.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_STT_EVAL_RESULT.ID IS 'STT 평가결과 ID';
COMMENT ON COLUMN NLPADM.MAI_STT_EVAL_RESULT.RESULT IS 'STT 평가결과';
COMMENT ON COLUMN NLPADM.MAI_STT_EVAL_RESULT.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_STT_EVAL_RESULT.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_STT_EVAL_RESULT.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_DNN_DIC IS 'DNN 사전';
COMMENT ON COLUMN NLPADM.MAI_DNN_DIC.APPLIED IS '';
COMMENT ON COLUMN NLPADM.MAI_DNN_DIC.CREATED_AT IS '생성일';
COMMENT ON COLUMN NLPADM.MAI_DNN_DIC.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_DNN_DIC.ID IS 'DNN 사전 ID';
COMMENT ON COLUMN NLPADM.MAI_DNN_DIC.NAME IS 'DNN 사전명';
COMMENT ON COLUMN NLPADM.MAI_DNN_DIC.UPDATED_AT IS '수정일';
COMMENT ON COLUMN NLPADM.MAI_DNN_DIC.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_DNN_DIC.VERSIONS IS '';
COMMENT ON COLUMN NLPADM.MAI_DNN_DIC.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_DNN_CATEGORY IS 'DNN 카테고리';
COMMENT ON COLUMN NLPADM.MAI_DNN_CATEGORY.CREATED_AT IS '생성일';
COMMENT ON COLUMN NLPADM.MAI_DNN_CATEGORY.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_DNN_CATEGORY.DNN_DIC_ID IS 'DNN 사전 ID';
COMMENT ON COLUMN NLPADM.MAI_DNN_CATEGORY.ID IS 'DNN 카테고리 ID';
COMMENT ON COLUMN NLPADM.MAI_DNN_CATEGORY.NAME IS 'DNN 카테고리명';
COMMENT ON COLUMN NLPADM.MAI_DNN_CATEGORY.PARENT_ID IS '';
COMMENT ON COLUMN NLPADM.MAI_DNN_CATEGORY.UPDATED_AT IS '수정일';
COMMENT ON COLUMN NLPADM.MAI_DNN_CATEGORY.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_DNN_CATEGORY.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_DNN_DIC_LINE IS 'DNN 데이터';
COMMENT ON COLUMN NLPADM.MAI_DNN_DIC_LINE.CATEGORY IS 'DNN 사전 데이터 카테고리';
COMMENT ON COLUMN NLPADM.MAI_DNN_DIC_LINE.SENTENCE IS 'DNN 사전 데이터 문장';
COMMENT ON COLUMN NLPADM.MAI_DNN_DIC_LINE.SEQ IS 'DNN 사전 데이터 시퀀스';
COMMENT ON COLUMN NLPADM.MAI_DNN_DIC_LINE.VERSION_ID IS 'DNN 사전 ID';

COMMENT ON TABLE NLPADM.MAI_DNN_MODEL IS 'DNN 모델';
COMMENT ON COLUMN NLPADM.MAI_DNN_MODEL.CREATED_AT IS '생성일';
COMMENT ON COLUMN NLPADM.MAI_DNN_MODEL.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_DNN_MODEL.DNNBINARY IS 'DNN 모델 바이너리';
COMMENT ON COLUMN NLPADM.MAI_DNN_MODEL.DNN_KEY IS 'DNN 모델 키';
COMMENT ON COLUMN NLPADM.MAI_DNN_MODEL.NAME IS 'DNN 모델 명';
COMMENT ON COLUMN NLPADM.MAI_DNN_MODEL.UPDATED_AT IS '수정일';
COMMENT ON COLUMN NLPADM.MAI_DNN_MODEL.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_DNN_MODEL.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_DNN_RESULT IS 'DNN 테스트 결과';
COMMENT ON COLUMN NLPADM.MAI_DNN_RESULT.CATEGORY IS 'DNN 테스트 결과 카테고리';
COMMENT ON COLUMN NLPADM.MAI_DNN_RESULT.CREATED_AT IS '생성일';
COMMENT ON COLUMN NLPADM.MAI_DNN_RESULT.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_DNN_RESULT.FILE_GROUP IS 'DNN 테스트 대상 파일 그룹';
COMMENT ON COLUMN NLPADM.MAI_DNN_RESULT.ID IS 'DNN 테스트 결과 ID';
COMMENT ON COLUMN NLPADM.MAI_DNN_RESULT.MODEL IS 'DNN 테스트 대상 모델';
COMMENT ON COLUMN NLPADM.MAI_DNN_RESULT.PROBABILITY IS 'DNN 테스트 Probability 값';
COMMENT ON COLUMN NLPADM.MAI_DNN_RESULT.SENTENCE IS 'DNN 테스트 문장';
COMMENT ON COLUMN NLPADM.MAI_DNN_RESULT.UPDATED_AT IS '수정일';
COMMENT ON COLUMN NLPADM.MAI_DNN_RESULT.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_DNN_RESULT.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_DNN_TRAINING IS 'DNN 학습';
COMMENT ON COLUMN NLPADM.MAI_DNN_TRAINING.DNN_KEY IS 'DNN 모델 키';
COMMENT ON COLUMN NLPADM.MAI_DNN_TRAINING.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_HMD_DIC IS 'HMD 사전';
COMMENT ON COLUMN NLPADM.MAI_HMD_DIC.APPLIED IS '적용 번호';
COMMENT ON COLUMN NLPADM.MAI_HMD_DIC.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_HMD_DIC.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_HMD_DIC.ID IS 'HMD ID';
COMMENT ON COLUMN NLPADM.MAI_HMD_DIC.NAME IS 'HMD 명';
COMMENT ON COLUMN NLPADM.MAI_HMD_DIC.UPDATED_AT IS '수정일';
COMMENT ON COLUMN NLPADM.MAI_HMD_DIC.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_HMD_DIC.VERSIONS IS '';
COMMENT ON COLUMN NLPADM.MAI_HMD_DIC.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_HMD_CATEGORY IS 'HMD 카테고리';
COMMENT ON COLUMN NLPADM.MAI_HMD_CATEGORY.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_HMD_CATEGORY.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_HMD_CATEGORY.HMD_DIC_ID IS 'HMD ID';
COMMENT ON COLUMN NLPADM.MAI_HMD_CATEGORY.ID IS 'HMD 카테고리 ID';
COMMENT ON COLUMN NLPADM.MAI_HMD_CATEGORY.NAME IS 'HMD 카테고리명';
COMMENT ON COLUMN NLPADM.MAI_HMD_CATEGORY.PARENT_ID IS '';
COMMENT ON COLUMN NLPADM.MAI_HMD_CATEGORY.PAR_CATE_ID IS '';
COMMENT ON COLUMN NLPADM.MAI_HMD_CATEGORY.UPDATED_AT IS '수정일';
COMMENT ON COLUMN NLPADM.MAI_HMD_CATEGORY.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_HMD_CATEGORY.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_HMD_DIC_LINE IS 'HMD 데이터';
COMMENT ON COLUMN NLPADM.MAI_HMD_DIC_LINE.CATEGORY IS 'HMD 카테고리';
COMMENT ON COLUMN NLPADM.MAI_HMD_DIC_LINE.RULE IS 'HMD 규칙';
COMMENT ON COLUMN NLPADM.MAI_HMD_DIC_LINE.SEQ IS 'HMD 데이터 시퀀스';
COMMENT ON COLUMN NLPADM.MAI_HMD_DIC_LINE.VERSION_ID IS 'HMD ID';

COMMENT ON TABLE NLPADM.MAI_HMD_RESULT IS 'HMD 테스트 결과';
COMMENT ON COLUMN NLPADM.MAI_HMD_RESULT.CATEGORY IS 'HMD 분류 결과 카테고리';
COMMENT ON COLUMN NLPADM.MAI_HMD_RESULT.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_HMD_RESULT.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_HMD_RESULT.FILE_GROUP IS '파일 그룹';
COMMENT ON COLUMN NLPADM.MAI_HMD_RESULT.ID IS 'HMD 분류 결과 ID';
COMMENT ON COLUMN NLPADM.MAI_HMD_RESULT.MODEL IS 'HMD 분류 기준 모델명';
COMMENT ON COLUMN NLPADM.MAI_HMD_RESULT.RULE IS 'HMD 분류된 규칙';
COMMENT ON COLUMN NLPADM.MAI_HMD_RESULT.SENTENCE IS 'HMD 분류 결과';
COMMENT ON COLUMN NLPADM.MAI_HMD_RESULT.UPDATED_AT IS '수정일';
COMMENT ON COLUMN NLPADM.MAI_HMD_RESULT.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_HMD_RESULT.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_STT_MODEL IS 'STT 학습 모델';
COMMENT ON COLUMN NLPADM.MAI_STT_MODEL.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_STT_MODEL.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_STT_MODEL.ID IS 'STT 모델 ID';
COMMENT ON COLUMN NLPADM.MAI_STT_MODEL.NAME IS 'STT 모델명';
COMMENT ON COLUMN NLPADM.MAI_STT_MODEL.PURPOSE IS 'lm,am 구분';
COMMENT ON COLUMN NLPADM.MAI_STT_MODEL.RATE IS '8k, 16k 구';
COMMENT ON COLUMN NLPADM.MAI_STT_MODEL.STATUS IS 'STT 모델 상태';
COMMENT ON COLUMN NLPADM.MAI_STT_MODEL.STT_BINARY IS 'STT 학습모델 Binary';
COMMENT ON COLUMN NLPADM.MAI_STT_MODEL.STT_KEY IS 'STT 학습모델 Key';
COMMENT ON COLUMN NLPADM.MAI_STT_MODEL.TRAIN_TYPE IS 'STT 모델 학습 키';
COMMENT ON COLUMN NLPADM.MAI_STT_MODEL.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_STT_MODEL.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_STT_MODEL.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_STT_TRAINING IS 'STT 학습';
COMMENT ON COLUMN NLPADM.MAI_STT_TRAINING.ID IS 'STT 학습 ID';
COMMENT ON COLUMN NLPADM.MAI_STT_TRAINING.STT_KEY IS 'STT Key';
COMMENT ON COLUMN NLPADM.MAI_STT_TRAINING.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_STT_TRANSCRIPT IS 'STT 전사';
COMMENT ON COLUMN NLPADM.MAI_STT_TRANSCRIPT.ALERT IS 'STT 전사 Alert';
COMMENT ON COLUMN NLPADM.MAI_STT_TRANSCRIPT.APPLIED IS 'STT 전사 Applied';
COMMENT ON COLUMN NLPADM.MAI_STT_TRANSCRIPT.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_STT_TRANSCRIPT.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_STT_TRANSCRIPT.FILE_GROUP_ID IS 'STT 파일 그룹 ID';
COMMENT ON COLUMN NLPADM.MAI_STT_TRANSCRIPT.FILE_ID IS 'STT 전사 파일 ID';
COMMENT ON COLUMN NLPADM.MAI_STT_TRANSCRIPT.ID IS 'STT 전사 ID';
COMMENT ON COLUMN NLPADM.MAI_STT_TRANSCRIPT.NAME IS 'STT 전사 이름';
COMMENT ON COLUMN NLPADM.MAI_STT_TRANSCRIPT.QAER_ID IS 'STT 전사 QaerID';
COMMENT ON COLUMN NLPADM.MAI_STT_TRANSCRIPT.TRANSCRIBER_ID IS 'STT 전사자';
COMMENT ON COLUMN NLPADM.MAI_STT_TRANSCRIPT.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_STT_TRANSCRIPT.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_STT_TRANSCRIPT.VERSION IS 'STT 전사 Version';
COMMENT ON COLUMN NLPADM.MAI_STT_TRANSCRIPT.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_STT_TRANSCRIPT_LINE IS 'STT 전사 내용';
COMMENT ON COLUMN NLPADM.MAI_STT_TRANSCRIPT_LINE.SEQ IS 'STT 전사 내용 ID';
COMMENT ON COLUMN NLPADM.MAI_STT_TRANSCRIPT_LINE.TEXT IS 'STT 전사 내용';
COMMENT ON COLUMN NLPADM.MAI_STT_TRANSCRIPT_LINE.VERSION_ID IS 'STT 전사 내용 VersionID';

COMMENT ON TABLE NLPADM.MAI_SYNONYM_DIC IS '동의어 사전 대표어';
COMMENT ON COLUMN NLPADM.MAI_SYNONYM_DIC.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_SYNONYM_DIC.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_SYNONYM_DIC.ID IS '동의어 사전 ID';
COMMENT ON COLUMN NLPADM.MAI_SYNONYM_DIC.REPRESENTATION IS '동의어 사전 대표어';
COMMENT ON COLUMN NLPADM.MAI_SYNONYM_DIC.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_SYNONYM_DIC.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_SYNONYM_DIC.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_SYNONYM_DIC_REL IS '동의어 사전 동의어';
COMMENT ON COLUMN NLPADM.MAI_SYNONYM_DIC_REL.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_SYNONYM_DIC_REL.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_SYNONYM_DIC_REL.ID IS '동의어 사전 연관 ID';
COMMENT ON COLUMN NLPADM.MAI_SYNONYM_DIC_REL.REPRESENTATION_ID IS '동의어 사전 대표어 ID';
COMMENT ON COLUMN NLPADM.MAI_SYNONYM_DIC_REL.SYNONYM_WORD IS '동의어 사전 단어';
COMMENT ON COLUMN NLPADM.MAI_SYNONYM_DIC_REL.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_SYNONYM_DIC_REL.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_SYNONYM_DIC_REL.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_TA_MODEL_MANAGEMENT IS 'TA 모델';
COMMENT ON COLUMN NLPADM.MAI_TA_MODEL_MANAGEMENT.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_TA_MODEL_MANAGEMENT.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_TA_MODEL_MANAGEMENT.DNN_DIC_ID IS 'DNN 사전 ID';
COMMENT ON COLUMN NLPADM.MAI_TA_MODEL_MANAGEMENT.DNN_USE IS 'DNN 사용 여부';
COMMENT ON COLUMN NLPADM.MAI_TA_MODEL_MANAGEMENT.EXTERNAL_MODEL_SEQ IS '외부모델 SEQ';
COMMENT ON COLUMN NLPADM.MAI_TA_MODEL_MANAGEMENT.GRPC_SERVER_SET_ID IS 'Grpc Server set ID';
COMMENT ON COLUMN NLPADM.MAI_TA_MODEL_MANAGEMENT.HMD_DIC_ID IS 'HMD 사전 ID';
COMMENT ON COLUMN NLPADM.MAI_TA_MODEL_MANAGEMENT.HMD_USE IS 'HMD 사용 여부';
COMMENT ON COLUMN NLPADM.MAI_TA_MODEL_MANAGEMENT.ID IS '모델 ID';
COMMENT ON COLUMN NLPADM.MAI_TA_MODEL_MANAGEMENT.MAN_DESC IS '모델 설명';
COMMENT ON COLUMN NLPADM.MAI_TA_MODEL_MANAGEMENT.NAME IS '모델명';
COMMENT ON COLUMN NLPADM.MAI_TA_MODEL_MANAGEMENT.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_TA_MODEL_MANAGEMENT.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_TA_MODEL_MANAGEMENT.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_TEST_FILES IS '테스트 파일';
COMMENT ON COLUMN NLPADM.MAI_TEST_FILES.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_TEST_FILES.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_TEST_FILES.DESCRIPTION IS '파일 설명';
COMMENT ON COLUMN NLPADM.MAI_TEST_FILES.FILE_ID IS '파일 ID';
COMMENT ON COLUMN NLPADM.MAI_TEST_FILES.FILE_NAME IS '파일명';
COMMENT ON COLUMN NLPADM.MAI_TEST_FILES.ID IS '테스트 파일 ID';
COMMENT ON COLUMN NLPADM.MAI_TEST_FILES.LINE_CNT IS '파일 내용 라인 수';
COMMENT ON COLUMN NLPADM.MAI_TEST_FILES.TEST_TYPE IS '테스트 타입';
COMMENT ON COLUMN NLPADM.MAI_TEST_FILES.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_TEST_RESULT IS '사전 테스트 결과';
COMMENT ON COLUMN NLPADM.MAI_TEST_RESULT.APPLY_CHECK IS '적용할 사전 데이터 타입 리스트';
COMMENT ON COLUMN NLPADM.MAI_TEST_RESULT.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_TEST_RESULT.DESCRIPTION IS '테스트 결과 설명';
COMMENT ON COLUMN NLPADM.MAI_TEST_RESULT.DICTIONARY_ID IS '테스트할 사전 ID';
COMMENT ON COLUMN NLPADM.MAI_TEST_RESULT.FILE_ID IS '파일 ID';
COMMENT ON COLUMN NLPADM.MAI_TEST_RESULT.ID IS '테스트 결과 ID';
COMMENT ON COLUMN NLPADM.MAI_TEST_RESULT.TESTER_ID IS '테스터  ID';
COMMENT ON COLUMN NLPADM.MAI_TEST_RESULT.TEST_ID IS '테스트 ID';
COMMENT ON COLUMN NLPADM.MAI_TEST_RESULT.TEST_TYPE IS '테스트 타입';
COMMENT ON COLUMN NLPADM.MAI_TEST_RESULT.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_TEST_SENTENCES IS 'Workspace';
COMMENT ON COLUMN NLPADM.MAI_TEST_SENTENCES.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_TEST_SENTENCES.DESCRIPTION IS '테스트 문장 설명';
COMMENT ON COLUMN NLPADM.MAI_TEST_SENTENCES.ID IS '테스트 문장 ID';
COMMENT ON COLUMN NLPADM.MAI_TEST_SENTENCES.SENTENCE IS '테스트 문장';
COMMENT ON COLUMN NLPADM.MAI_TEST_SENTENCES.TEST_FILE_ID IS '테스트 파일 ID';
COMMENT ON COLUMN NLPADM.MAI_TEST_SENTENCES.TEST_TYPE IS '테스트 타입';
COMMENT ON COLUMN NLPADM.MAI_TEST_SENTENCES.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_TWE_INDEXING_HISTORY IS 'Workspace';
COMMENT ON COLUMN NLPADM.MAI_TWE_INDEXING_HISTORY.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_TWE_INDEXING_HISTORY.DELETE_CNT IS 'Triple Wiki 삭제된 갯수';
COMMENT ON COLUMN NLPADM.MAI_TWE_INDEXING_HISTORY.ID IS 'TWE 인덱스 히스토리 ID';
COMMENT ON COLUMN NLPADM.MAI_TWE_INDEXING_HISTORY.INSERT_CNT IS 'Triple Wiki 등록된 갯수';
COMMENT ON COLUMN NLPADM.MAI_TWE_INDEXING_HISTORY.LEAD_TIME IS 'Triple Wiki LeadTime';
COMMENT ON COLUMN NLPADM.MAI_TWE_INDEXING_HISTORY.UPDATE_CNT IS 'Triple Wiki 수정된 갯수';

COMMENT ON TABLE NLPADM.MAI_TWE_SCHEDULE IS 'Triple Wiki Indexing 스케줄';
COMMENT ON COLUMN NLPADM.MAI_TWE_SCHEDULE.HOUR IS '시';
COMMENT ON COLUMN NLPADM.MAI_TWE_SCHEDULE.MINUTE IS '분';
COMMENT ON COLUMN NLPADM.MAI_TWE_SCHEDULE.PERIOD IS '주기';
COMMENT ON COLUMN NLPADM.MAI_TWE_SCHEDULE.SEQ IS '스케쥴 시퀀스';
COMMENT ON COLUMN NLPADM.MAI_TWE_SCHEDULE.TYPE IS '스케쥴 타입';

COMMENT ON TABLE NLPADM.MAI_USER IS 'maum admin 사용자';
COMMENT ON COLUMN NLPADM.MAI_USER.ACTIVATED IS '';
COMMENT ON COLUMN NLPADM.MAI_USER.EMAIL IS '사용자 메일주소';
COMMENT ON COLUMN NLPADM.MAI_USER.ID IS '사용자 ID';
COMMENT ON COLUMN NLPADM.MAI_USER.LAST_UPDATED IS '최근 수정 일자';
COMMENT ON COLUMN NLPADM.MAI_USER.PASSWORD IS '비밀번호';
COMMENT ON COLUMN NLPADM.MAI_USER.USER_NAME IS '사용자 이름';

COMMENT ON TABLE NLPADM.MAI_WORKSPACE IS 'Workspace';
COMMENT ON COLUMN NLPADM.MAI_WORKSPACE.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_WORKSPACE.ID IS 'WORKSPACE ID';
COMMENT ON COLUMN NLPADM.MAI_WORKSPACE.OWNER_ID IS 'WORKSPACE 소유자';
COMMENT ON COLUMN NLPADM.MAI_WORKSPACE.OWNER_TYPE IS 'WORKSPACE 소유자 타입';
COMMENT ON COLUMN NLPADM.MAI_WORKSPACE.PATH IS 'WORKSPACE 경로명';
COMMENT ON COLUMN NLPADM.MAI_WORKSPACE.UPDATED_AT IS '수정일자';

COMMENT ON TABLE NLPADM.MAI_SDS_ENTITY IS 'SDS entity';
COMMENT ON COLUMN NLPADM.MAI_SDS_ENTITY.CLASS_NAME IS 'entity class name';
COMMENT ON COLUMN NLPADM.MAI_SDS_ENTITY.CLASS_SOURCE IS 'default = "SYSTEM"';
COMMENT ON COLUMN NLPADM.MAI_SDS_ENTITY.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_ENTITY.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_SDS_ENTITY.ENTITY_NAME IS 'entity name';
COMMENT ON COLUMN NLPADM.MAI_SDS_ENTITY.ENTITY_SOURCE IS 'entity 값을 채우는 주체(default = "KB")';
COMMENT ON COLUMN NLPADM.MAI_SDS_ENTITY.ENTITY_TYPE IS 'entity 자료형';
COMMENT ON COLUMN NLPADM.MAI_SDS_ENTITY.ID IS 'entity ID';
COMMENT ON COLUMN NLPADM.MAI_SDS_ENTITY.SHARED_YN IS 'shared 유/무(Y or N)';
COMMENT ON COLUMN NLPADM.MAI_SDS_ENTITY.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_ENTITY.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_SDS_ENTITY.WORKSPACEID IS 'workspace ID';

COMMENT ON TABLE NLPADM.MAI_SDS_ENTITY_DATA IS 'SDS entity data';
COMMENT ON COLUMN NLPADM.MAI_SDS_ENTITY_DATA.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_ENTITY_DATA.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_SDS_ENTITY_DATA.ENTITY_DATA IS '기준이 되는 값';
COMMENT ON COLUMN NLPADM.MAI_SDS_ENTITY_DATA.ENTITY_ID IS '';
COMMENT ON COLUMN NLPADM.MAI_SDS_ENTITY_DATA.ID IS 'entity data ID';
COMMENT ON COLUMN NLPADM.MAI_SDS_ENTITY_DATA.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_ENTITY_DATA.UPDATER_ID IS '수정자';

COMMENT ON TABLE NLPADM.MAI_SDS_ENTITY_DATA_SYNONYM IS 'SDS entity data synonym';
COMMENT ON COLUMN NLPADM.MAI_SDS_ENTITY_DATA_SYNONYM.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_ENTITY_DATA_SYNONYM.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_SDS_ENTITY_DATA_SYNONYM.ENTITY_DATA_ID IS '';
COMMENT ON COLUMN NLPADM.MAI_SDS_ENTITY_DATA_SYNONYM.ENTITY_DATA_SYNONYM IS '동의어';
COMMENT ON COLUMN NLPADM.MAI_SDS_ENTITY_DATA_SYNONYM.ID IS 'entity data synonym ID';
COMMENT ON COLUMN NLPADM.MAI_SDS_ENTITY_DATA_SYNONYM.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_ENTITY_DATA_SYNONYM.UPDATER_ID IS '수정자';

COMMENT ON TABLE NLPADM.MAI_SDS_EVENT IS 'SDS event';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT.ID IS 'task event ID';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT.INTENT_ID IS 'response에서 사용되는 intent';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT.TASK_ID IS 'task ID';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT.UPDATER_ID IS '수정자';

COMMENT ON TABLE NLPADM.MAI_SDS_EVENT_CONDITION IS 'SDS event condition';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT_CONDITION.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT_CONDITION.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT_CONDITION.EVENT_ACTION IS 'response 이후 수행할 내용';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT_CONDITION.EVENT_CONDITION IS 'response 응답 조건';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT_CONDITION.EVENT_ID IS '';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT_CONDITION.EXTRA_DATA IS '';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT_CONDITION.ID IS 'task event conditon ID';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT_CONDITION.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT_CONDITION.UPDATER_ID IS '수정자';

COMMENT ON TABLE NLPADM.MAI_SDS_EVENT_UTTER IS 'SDS event utter';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT_UTTER.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT_UTTER.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT_UTTER.EVENT_CONDITION_ID IS '';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT_UTTER.EXTRA_DATA IS 'task event condition intent ID';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT_UTTER.ID IS 'task event condition intent ID';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT_UTTER.INTENT_ID IS 'response 응답 시 사용되는 intent';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT_UTTER.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT_UTTER.UPDATER_ID IS '수정자';

COMMENT ON TABLE NLPADM.MAI_SDS_EVENT_UTTER_DATA IS 'SDS event utter data';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT_UTTER_DATA.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT_UTTER_DATA.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT_UTTER_DATA.EVENT_UTTER_DATA IS 'system 응답 메시지';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT_UTTER_DATA.EVENT_UTTER_ID IS '';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT_UTTER_DATA.ID IS 'task event condition intent utter ID';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT_UTTER_DATA.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_EVENT_UTTER_DATA.UPDATER_ID IS '수정자';

COMMENT ON TABLE NLPADM.MAI_SDS_INTENT IS 'SDS intent';
COMMENT ON COLUMN NLPADM.MAI_SDS_INTENT.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_INTENT.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_SDS_INTENT.ID IS 'intent ID';
COMMENT ON COLUMN NLPADM.MAI_SDS_INTENT.INTENT_NAME IS 'intent Name';
COMMENT ON COLUMN NLPADM.MAI_SDS_INTENT.SHARED_YN IS 'shared 유/무(Y or N)';
COMMENT ON COLUMN NLPADM.MAI_SDS_INTENT.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_INTENT.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_SDS_INTENT.WORKSPACEID IS 'workspace ID';

COMMENT ON TABLE NLPADM.MAI_SDS_INTENT_UTTER IS 'SDS intent utter';
COMMENT ON COLUMN NLPADM.MAI_SDS_INTENT_UTTER.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_INTENT_UTTER.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_SDS_INTENT_UTTER.ID IS 'intent utter ID';
COMMENT ON COLUMN NLPADM.MAI_SDS_INTENT_UTTER.INTENT_ID IS 'intent id';
COMMENT ON COLUMN NLPADM.MAI_SDS_INTENT_UTTER.MAPPED_USER_SAY IS 'entity로 매핑된 사용자 발화 메시지';
COMMENT ON COLUMN NLPADM.MAI_SDS_INTENT_UTTER.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_INTENT_UTTER.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_SDS_INTENT_UTTER.USER_SAY IS '사용자 발화 메시지';

COMMENT ON TABLE NLPADM.MAI_SDS_INTENT_UTTER_DATA IS 'SDS intent utter data';
COMMENT ON COLUMN NLPADM.MAI_SDS_INTENT_UTTER_DATA.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_INTENT_UTTER_DATA.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_SDS_INTENT_UTTER_DATA.ENTITY_ID IS '사용자 발화 메시지에서 매핑된 entity name';
COMMENT ON COLUMN NLPADM.MAI_SDS_INTENT_UTTER_DATA.ID IS 'intent corpus data ID';
COMMENT ON COLUMN NLPADM.MAI_SDS_INTENT_UTTER_DATA.INTENT_UTTER_ID IS '';
COMMENT ON COLUMN NLPADM.MAI_SDS_INTENT_UTTER_DATA.MAPPED_VALUE IS '사용자 발화 메시지에서 entity에 매핑된 값';
COMMENT ON COLUMN NLPADM.MAI_SDS_INTENT_UTTER_DATA.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_INTENT_UTTER_DATA.UPDATER_ID IS '수정자';

COMMENT ON TABLE NLPADM.MAI_SDS_MODEL IS 'SDS next task event';
COMMENT ON COLUMN NLPADM.MAI_SDS_MODEL.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_MODEL.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_SDS_MODEL.DESCRIPTION IS 'model description';
COMMENT ON COLUMN NLPADM.MAI_SDS_MODEL.ID IS 'model ID';
COMMENT ON COLUMN NLPADM.MAI_SDS_MODEL.LAST_TRAIN_KEY IS 'model name';
COMMENT ON COLUMN NLPADM.MAI_SDS_MODEL.MODEL_NAME IS 'model name';
COMMENT ON COLUMN NLPADM.MAI_SDS_MODEL.STATUS IS 'model status(default = "edit")';
COMMENT ON COLUMN NLPADM.MAI_SDS_MODEL.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_MODEL.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_SDS_MODEL.WORKSPACE_ID IS 'workspace ID';

COMMENT ON TABLE NLPADM.MAI_SDS_MODEL_ENTITY IS 'SDS model entity';
COMMENT ON COLUMN NLPADM.MAI_SDS_MODEL_ENTITY.ENTITY_ID IS '엔터티 ID';
COMMENT ON COLUMN NLPADM.MAI_SDS_MODEL_ENTITY.MODEL_ID IS '모델 ID';

COMMENT ON TABLE NLPADM.MAI_SDS_MODEL_INTENT IS 'SDS model intent';
COMMENT ON COLUMN NLPADM.MAI_SDS_MODEL_INTENT.INTENT_ID IS '인텐트 ID';
COMMENT ON COLUMN NLPADM.MAI_SDS_MODEL_INTENT.MODEL_ID IS '모델 ID';

COMMENT ON TABLE NLPADM.MAI_SDS_NEXT_TASK IS 'SDS next task event';
COMMENT ON COLUMN NLPADM.MAI_SDS_NEXT_TASK.CONTROLLER IS 'task 진행 주체(default = "system")';
COMMENT ON COLUMN NLPADM.MAI_SDS_NEXT_TASK.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_NEXT_TASK.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_SDS_NEXT_TASK.ID IS 'task next task ID';
COMMENT ON COLUMN NLPADM.MAI_SDS_NEXT_TASK.NEXT_TASK_CONDITION IS '다음 task로의 진입 조건';
COMMENT ON COLUMN NLPADM.MAI_SDS_NEXT_TASK.NEXT_TASK_ID IS '다음 task의 이름';
COMMENT ON COLUMN NLPADM.MAI_SDS_NEXT_TASK.TASK_ID IS 'task ID';
COMMENT ON COLUMN NLPADM.MAI_SDS_NEXT_TASK.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_NEXT_TASK.UPDATER_ID IS '수정자';

COMMENT ON TABLE NLPADM.MAI_SDS_START_EVENT IS 'SDS task start event';
COMMENT ON COLUMN NLPADM.MAI_SDS_START_EVENT.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_START_EVENT.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_SDS_START_EVENT.ID IS 'task start event ID';
COMMENT ON COLUMN NLPADM.MAI_SDS_START_EVENT.START_EVENT_ACTION IS '시스템 발화 이후 시스템에서 수행할 내용';
COMMENT ON COLUMN NLPADM.MAI_SDS_START_EVENT.START_EVENT_CONDITION IS 'task 시작 조건';
COMMENT ON COLUMN NLPADM.MAI_SDS_START_EVENT.TASK_ID IS 'task ID';
COMMENT ON COLUMN NLPADM.MAI_SDS_START_EVENT.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_START_EVENT.UPDATER_ID IS '수정자';

COMMENT ON TABLE NLPADM.MAI_SDS_START_EVENT_UTTER IS 'SDS task start event utter';
COMMENT ON COLUMN NLPADM.MAI_SDS_START_EVENT_UTTER.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_START_EVENT_UTTER.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_SDS_START_EVENT_UTTER.EXTRA_DATA IS '';
COMMENT ON COLUMN NLPADM.MAI_SDS_START_EVENT_UTTER.ID IS 'task start event intent ID';
COMMENT ON COLUMN NLPADM.MAI_SDS_START_EVENT_UTTER.INTENT_ID IS 'task 시작 시에 사용되는 intent';
COMMENT ON COLUMN NLPADM.MAI_SDS_START_EVENT_UTTER.START_EVENT_ID IS '';
COMMENT ON COLUMN NLPADM.MAI_SDS_START_EVENT_UTTER.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_START_EVENT_UTTER.UPDATER_ID IS '수정자';

COMMENT ON TABLE NLPADM.MAI_SDS_START_EVENT_UTTER_DATA IS 'SDS task start event utter data';
COMMENT ON COLUMN NLPADM.MAI_SDS_START_EVENT_UTTER_DATA.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_START_EVENT_UTTER_DATA.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_SDS_START_EVENT_UTTER_DATA.ID IS 'task start event intent utter ID';
COMMENT ON COLUMN NLPADM.MAI_SDS_START_EVENT_UTTER_DATA.START_EVENT_UTTER_DATA IS 'system 응답 메시지';
COMMENT ON COLUMN NLPADM.MAI_SDS_START_EVENT_UTTER_DATA.START_EVENT_UTTER_ID IS '';
COMMENT ON COLUMN NLPADM.MAI_SDS_START_EVENT_UTTER_DATA.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_START_EVENT_UTTER_DATA.UPDATER_ID IS '수정자';

COMMENT ON TABLE NLPADM.MAI_SDS_TASK IS 'SDS Task';
COMMENT ON COLUMN NLPADM.MAI_SDS_TASK.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_TASK.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_SDS_TASK.ID IS 'task ID';
COMMENT ON COLUMN NLPADM.MAI_SDS_TASK.MODEL_ID IS 'model ID';
COMMENT ON COLUMN NLPADM.MAI_SDS_TASK.RESET_SLOT IS '슬롯 초기화 여부(default = "N")';
COMMENT ON COLUMN NLPADM.MAI_SDS_TASK.TASK_GOAL IS 'task를 벗어날 조건';
COMMENT ON COLUMN NLPADM.MAI_SDS_TASK.TASK_LOCATION IS '';
COMMENT ON COLUMN NLPADM.MAI_SDS_TASK.TASK_NAME IS 'task name';
COMMENT ON COLUMN NLPADM.MAI_SDS_TASK.TASK_TYPE IS 'task 진행 필수값(default = "essential")';
COMMENT ON COLUMN NLPADM.MAI_SDS_TASK.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_SDS_TASK.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_SDS_TASK.WORKSPACE_ID IS 'workspace ID';

COMMENT ON TABLE NLPADM.BRAIN_BQA_CATEGORY IS 'BasicQA Category';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_CATEGORY.CATE_NAME IS '카테고리 이름';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_CATEGORY.CATE_PATH IS 'BasicQA Category 경로';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_CATEGORY.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_CATEGORY.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_CATEGORY.ID IS '카테고리ID';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_CATEGORY.PAR_CATE_ID IS 'BasicQA Category 부모 ID';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_CATEGORY.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_CATEGORY.UPDATER_ID IS '수정자';

COMMENT ON TABLE NLPADM.BRAIN_BQA_ITEM IS 'BasicQA Item';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_ITEM.ANSWER IS '답변';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_ITEM.CATEGORY IS '카테고리';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_ITEM.CHANNEL IS '채널';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_ITEM.CONFIRMER IS '승인자';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_ITEM.CONFIRM_DTM IS '승인날짜';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_ITEM.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_ITEM.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_ITEM.C_ID IS '문서ID';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_ITEM.ID IS 'ID';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_ITEM.MAIN_YN IS '대표질문여부';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_ITEM.QNA_ID IS '질문ID';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_ITEM.QUESTION IS '질문';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_ITEM.SKILL_ID IS '스킬ID';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_ITEM.SRC IS '출처';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_ITEM.SUB_CATEGORY IS '서브카테고리';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_ITEM.SUB_SUB_CATEGORY IS '서브서브카테고리';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_ITEM.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_ITEM.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_ITEM.USE_YN IS '사용여부';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_ITEM.WEIGHT IS '가중치';

COMMENT ON TABLE NLPADM.BRAIN_BQA_PRPRS_MAIN_WD IS 'BasicQA paraphrase 메인 단어';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_PRPRS_MAIN_WD.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_PRPRS_MAIN_WD.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_PRPRS_MAIN_WD.ID IS '대표어미ID';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_PRPRS_MAIN_WD.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_PRPRS_MAIN_WD.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_PRPRS_MAIN_WD.USE_YN IS '사용여부';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_PRPRS_MAIN_WD.WORD IS '어미단어';

COMMENT ON TABLE NLPADM.BRAIN_BQA_PRPRS_REL IS 'BasicQA paraphrase 메인단어 와 단어 관게매핑';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_PRPRS_REL.MAIN_ID IS '대표어미ID';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_PRPRS_REL.PARAPHRASE_ID IS '유사어미ID';

COMMENT ON TABLE NLPADM.BRAIN_BQA_PRPRS_WD IS 'BasicQA paraphrase 단어';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_PRPRS_WD.CREATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_PRPRS_WD.CREATOR_ID IS '수정자';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_PRPRS_WD.ID IS '유사어미ID';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_PRPRS_WD.WORD IS '유사어미단어';

COMMENT ON TABLE NLPADM.BRAIN_BQA_SKILL IS 'BasicQA Skill';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_SKILL.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_SKILL.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_SKILL.ID IS '스킬ID';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_SKILL.NAME IS '스킬이름';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_SKILL.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.BRAIN_BQA_SKILL.UPDATER_ID IS '수정자';

COMMENT ON TABLE NLPADM.MAI_BQA_INDEXING_HISTORY IS 'BQA 인덱싱 히스토리';
COMMENT ON COLUMN NLPADM.MAI_BQA_INDEXING_HISTORY.ADDRESS IS 'BQA 인덱싱 히스토리 서버 주소';
COMMENT ON COLUMN NLPADM.MAI_BQA_INDEXING_HISTORY.CREATED_AT IS '생성일';
COMMENT ON COLUMN NLPADM.MAI_BQA_INDEXING_HISTORY.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_BQA_INDEXING_HISTORY.FETCHED IS 'BQA 인덱싱 가지고온 item 갯수';
COMMENT ON COLUMN NLPADM.MAI_BQA_INDEXING_HISTORY.ID IS 'BQA 인덱싱 히스토리ID';
COMMENT ON COLUMN NLPADM.MAI_BQA_INDEXING_HISTORY.MESSAGE IS 'BQA 인덱싱 엔진 메세지';
COMMENT ON COLUMN NLPADM.MAI_BQA_INDEXING_HISTORY.PROCESSED IS 'BQA 인덱싱 완료된 갯수';
COMMENT ON COLUMN NLPADM.MAI_BQA_INDEXING_HISTORY.STATUS IS 'BQA 인덱싱 히스토리 상태';
COMMENT ON COLUMN NLPADM.MAI_BQA_INDEXING_HISTORY.STOP_YN IS 'BQA 인덱싱 stop 여부';
COMMENT ON COLUMN NLPADM.MAI_BQA_INDEXING_HISTORY.TOTAL IS 'BQA 인덱싱 item 총 갯수';
COMMENT ON COLUMN NLPADM.MAI_BQA_INDEXING_HISTORY.TYPE IS 'BQA 인덱싱  full, incremental index 구분';
COMMENT ON COLUMN NLPADM.MAI_BQA_INDEXING_HISTORY.UPDATED_AT IS '수정일';
COMMENT ON COLUMN NLPADM.MAI_BQA_INDEXING_HISTORY.UPDATER_ID IS '수정자';

COMMENT ON TABLE NLPADM.MAI_MRC_ANSWER IS 'MRC 답';
COMMENT ON COLUMN NLPADM.MAI_MRC_ANSWER.ANSWER IS 'MRC 답변';
COMMENT ON COLUMN NLPADM.MAI_MRC_ANSWER.ANSWER_END IS 'MRC 답변 끝 위치';
COMMENT ON COLUMN NLPADM.MAI_MRC_ANSWER.ANSWER_END_MORPH IS 'MRC 답변의 형태소단위 끝 위치';
COMMENT ON COLUMN NLPADM.MAI_MRC_ANSWER.ANSWER_MORPH IS 'MRC 답변 형태소 분석 결과';
COMMENT ON COLUMN NLPADM.MAI_MRC_ANSWER.ANSWER_START IS 'MRC 답변 시작 위치';
COMMENT ON COLUMN NLPADM.MAI_MRC_ANSWER.ANSWER_START_MORPH IS 'MRC 답변의 형태소단위 시작 위치';
COMMENT ON COLUMN NLPADM.MAI_MRC_ANSWER.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_MRC_ANSWER.ID IS 'MRC 답변 ID';
COMMENT ON COLUMN NLPADM.MAI_MRC_ANSWER.QUESTION_ID IS 'MRC 질문 ID';
COMMENT ON COLUMN NLPADM.MAI_MRC_ANSWER.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_MRC_CATEGORY IS 'MRC Category';
COMMENT ON COLUMN NLPADM.MAI_MRC_CATEGORY.CATE_NAME IS '카테고리 이름';
COMMENT ON COLUMN NLPADM.MAI_MRC_CATEGORY.CATE_PATH IS 'MRC Category 경로';
COMMENT ON COLUMN NLPADM.MAI_MRC_CATEGORY.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_MRC_CATEGORY.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_MRC_CATEGORY.ID IS '카테고리 ID';
COMMENT ON COLUMN NLPADM.MAI_MRC_CATEGORY.PAR_CATE_ID IS 'MRC Category 부모 ID';
COMMENT ON COLUMN NLPADM.MAI_MRC_CATEGORY.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_MRC_CATEGORY.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_MRC_CATEGORY.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_MRC_CONTEXT IS 'MRC 본문';
COMMENT ON COLUMN NLPADM.MAI_MRC_CONTEXT.CATE_ID IS 'MRC 본문 카테고리 ID';
COMMENT ON COLUMN NLPADM.MAI_MRC_CONTEXT.CONTEXT IS 'MRC 단락 ';
COMMENT ON COLUMN NLPADM.MAI_MRC_CONTEXT.CONTEXT_MORPH IS 'MRC 단락 형태소 분석 결과';
COMMENT ON COLUMN NLPADM.MAI_MRC_CONTEXT.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_MRC_CONTEXT.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_MRC_CONTEXT.ID IS 'MRC 단락 ID';
COMMENT ON COLUMN NLPADM.MAI_MRC_CONTEXT.SEASON IS 'MRC 데이터 제작 시즌';
COMMENT ON COLUMN NLPADM.MAI_MRC_CONTEXT.TITLE IS 'MRC 단락 제목';
COMMENT ON COLUMN NLPADM.MAI_MRC_CONTEXT.WIKI_ID IS 'MRC WIKI ID';
COMMENT ON COLUMN NLPADM.MAI_MRC_CONTEXT.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_MRC_DATA_GROUP IS 'MRC DataGroup';
COMMENT ON COLUMN NLPADM.MAI_MRC_DATA_GROUP.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_MRC_DATA_GROUP.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_MRC_DATA_GROUP.DATA_GROUP_NAME IS 'MRC 데이터 그룹명';
COMMENT ON COLUMN NLPADM.MAI_MRC_DATA_GROUP.ID IS 'MRC 데이터 그룹 ID';
COMMENT ON COLUMN NLPADM.MAI_MRC_DATA_GROUP.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_MRC_DATA_GROUP.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_MRC_DATA_GROUP.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_MRC_DATA_GROUP_REL IS 'MRC DataGroup 관계매핑';
COMMENT ON COLUMN NLPADM.MAI_MRC_DATA_GROUP_REL.CONTEXT_ID IS 'MRC 단락 ID';
COMMENT ON COLUMN NLPADM.MAI_MRC_DATA_GROUP_REL.DATA_GROUP_ID IS 'MRC 데이터 그룹 ID';

--done
COMMENT ON TABLE NLPADM.MAI_MRC_PASSAGE IS 'MRC Passage';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE.ID IS '문서 ID';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE.PASSAGE IS 'MRC Passage 본문';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE.PASSAGE_MORPH IS 'MRC Passage 본문 형태소 문서';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE.SECTION IS '섹션';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE.SKILL_ID IS '스킬ID';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE.SRC IS '출처';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE.WORD_LIST IS 'MRC Passage Word list(엔진사용)';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_MRC_PASSAGE_IDX_HISTORY IS 'MRC Passage Indexing 기록';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_IDX_HISTORY.ID IS 'MRC Passage Indexing ID'
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_IDX_HISTORY.CREATED_AT IS '실행날짜';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_IDX_HISTORY.CREATOR_ID IS '실행자';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_IDX_HISTORY.FETCHED IS 'MRC Passage Indexing 가지고온 item 갯수';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_IDX_HISTORY.MESSAGE IS 'MRC Passage Indexing engine message';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_IDX_HISTORY.PROCESSED IS 'MRC Passage Indexing 완료된 갯수';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_IDX_HISTORY.STATUS IS 'MRC Passage Indexing 상태';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_IDX_HISTORY.STOP_YN IS 'MRC Passage Indexing stop 여부';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_IDX_HISTORY.TOTAL IS 'MRC Passage Indexing item 총 갯수';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_IDX_HISTORY.TYPE IS 'MRC Passage Indexing full, incremental 구분 ';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_IDX_HISTORY.ADDRESS IS 'MRC Passage Indexing 서버 주소';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_IDX_HISTORY.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_IDX_HISTORY.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_IDX_HISTORY.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_MRC_PASSAGE_QUESTION IS 'MRC 질문';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_QUESTION.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_QUESTION.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_QUESTION.ID IS '질문ID';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_QUESTION.PASSAGE_ID IS '문서ID';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_QUESTION.QUESTION IS '질문';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_QUESTION.QUESTION_MORPH IS '질문 형태소';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_QUESTION.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_QUESTION.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_QUESTION.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_MRC_PASSAGE_SKILL IS 'MRC Passage Skill';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_SKILL.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_SKILL.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_SKILL.ID IS '문단 스킬 ID';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_SKILL.NAME IS '문단 스킬 이름';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_SKILL.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_SKILL.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_MRC_PASSAGE_SKILL.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_MRC_QUESTION IS 'MRC 질문';
COMMENT ON COLUMN NLPADM.MAI_MRC_QUESTION.CATE_ID IS 'MRC 카테고리 ID';
COMMENT ON COLUMN NLPADM.MAI_MRC_QUESTION.CONTEXT_ID IS 'MRC 단락 ID';
COMMENT ON COLUMN NLPADM.MAI_MRC_QUESTION.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_MRC_QUESTION.ID IS 'MRC 질문 ID';
COMMENT ON COLUMN NLPADM.MAI_MRC_QUESTION.QUESTION IS 'MRC 질문';
COMMENT ON COLUMN NLPADM.MAI_MRC_QUESTION.QUESTION_MORPH IS 'MRC 질문 형태소분석 결과';
COMMENT ON COLUMN NLPADM.MAI_MRC_QUESTION.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_MRC_TRAINING_MODEL IS 'MRC 학습 모델';
COMMENT ON COLUMN NLPADM.MAI_MRC_TRAINING_MODEL.BATCH_SIZE IS 'MRC 학습 배치 크기';
COMMENT ON COLUMN NLPADM.MAI_MRC_TRAINING_MODEL.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_MRC_TRAINING_MODEL.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_MRC_TRAINING_MODEL.EPOCHES IS 'MRC 학습 반복 횟수';
COMMENT ON COLUMN NLPADM.MAI_MRC_TRAINING_MODEL.ID IS 'MRC 학습 ID';
COMMENT ON COLUMN NLPADM.MAI_MRC_TRAINING_MODEL.MODEL IS 'MRC 학습 모델명';
COMMENT ON COLUMN NLPADM.MAI_MRC_TRAINING_MODEL.MRC_BINARY IS 'MRC 학습 바이너리';
COMMENT ON COLUMN NLPADM.MAI_MRC_TRAINING_MODEL.NAME IS 'MRC 모델 이름';
COMMENT ON COLUMN NLPADM.MAI_MRC_TRAINING_MODEL.STATUS IS '';
COMMENT ON COLUMN NLPADM.MAI_MRC_TRAINING_MODEL.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_MRC_TRAINING_MODEL.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_MRC_TRAINING_MODEL.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_MRC_WIKI IS 'MRC Wiki';
COMMENT ON COLUMN NLPADM.MAI_MRC_WIKI.CREATED_AT IS '생성일자';
COMMENT ON COLUMN NLPADM.MAI_MRC_WIKI.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_MRC_WIKI.ID IS 'MRC Wiki ID';;
COMMENT ON COLUMN NLPADM.MAI_MRC_WIKI.NAME IS 'MRC Wiki 이름';
COMMENT ON COLUMN NLPADM.MAI_MRC_WIKI.UPDATED_AT IS '수정일자';
COMMENT ON COLUMN NLPADM.MAI_MRC_WIKI.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_MRC_WIKI.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_END_POST_DIC IS '어미 사전';
COMMENT ON COLUMN NLPADM.MAI_END_POST_DIC.CREATED_AT IS '생성일';
COMMENT ON COLUMN NLPADM.MAI_END_POST_DIC.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_END_POST_DIC.ID IS '어미 사전 ID';
COMMENT ON COLUMN NLPADM.MAI_END_POST_DIC.REPRESENTATION IS '';
COMMENT ON COLUMN NLPADM.MAI_END_POST_DIC.TYPE IS '어미 타입';
COMMENT ON COLUMN NLPADM.MAI_END_POST_DIC.UPDATED_AT IS '수정일';
COMMENT ON COLUMN NLPADM.MAI_END_POST_DIC.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_END_POST_DIC.WORKSPACE_ID IS 'WORKSPACE ID';

COMMENT ON TABLE NLPADM.MAI_END_POST_DIC_REL IS '어미 사전 관계';
COMMENT ON COLUMN NLPADM.MAI_END_POST_DIC_REL.CREATED_AT IS '생성일';
COMMENT ON COLUMN NLPADM.MAI_END_POST_DIC_REL.CREATOR_ID IS '생성자';
COMMENT ON COLUMN NLPADM.MAI_END_POST_DIC_REL.ID IS '어미사전 ID';
COMMENT ON COLUMN NLPADM.MAI_END_POST_DIC_REL.REPRESENTATION_ID IS '대표 어미사전 ID';
COMMENT ON COLUMN NLPADM.MAI_END_POST_DIC_REL.UPDATED_AT IS '수정일';
COMMENT ON COLUMN NLPADM.MAI_END_POST_DIC_REL.UPDATER_ID IS '수정자';
COMMENT ON COLUMN NLPADM.MAI_END_POST_DIC_REL.WORD IS '단어';
COMMENT ON COLUMN NLPADM.MAI_END_POST_DIC_REL.WORKSPACE_ID IS 'WORKSPACE ID';
