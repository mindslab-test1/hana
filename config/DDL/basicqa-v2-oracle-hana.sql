CREATE TABLE NLPADM.BRAIN_BQA2_ANSWER (
  ID NUMBER(10) NOT NULL,
  ANSWER CLOB NOT NULL,
  SUMMARY CLOB,
  SRC VARCHAR2(512 char),
  CATEGORY_ID NUMBER(10),
  ATTR1 VARCHAR2(256),
  ATTR2 VARCHAR2(256),
  ATTR3 VARCHAR2(256),
  ATTR4 VARCHAR2(256),
  LAYER1_ID NUMBER(10),
  LAYER2_ID NUMBER(10),
  LAYER3_ID NUMBER(10),
  LAYER4_ID NUMBER(10),
  CREATOR_ID VARCHAR2(40 char),
  UPDATER_ID VARCHAR2(40 char),
  CREATE_DTM TIMESTAMP(6),
  UPDATE_DTM TIMESTAMP(6)
) TABLESPACE NLP_DT01_8K;

ALTER TABLE NLPADM.BRAIN_BQA2_ANSWER
  ADD CONSTRAINT BRAIN_BQA2_ANSWER_PK
  PRIMARY KEY (ID)
  USING INDEX TABLESPACE NLP_IX01_8K;

CREATE SEQUENCE NLPADM.BRAIN_BQA2_ANSWER_SEQUENCE
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  INCREMENT BY 1
  START WITH 1
  CACHE 100
  NOORDER
  NOCYCLE;

CREATE SYNONYM NLPCON.BRAIN_BQA2_ANSWER FOR NLPADM.BRAIN_BQA2_ANSWER;
CREATE OR REPLACE SYNONYM NLPCON.BRAIN_BQA2_ANSWER_SEQUENCE FOR NLPADM.BRAIN_BQA2_ANSWER_SEQUENCE;
GRANT ALTER, SELECT ON NLPADM.BRAIN_BQA2_ANSWER_SEQUENCE TO NLPCON, RL_NLP_ALL;

COMMENT ON TABLE NLPADM.BRAIN_BQA2_ANSWER IS '답변필드';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_ANSWER.ID IS '아이디';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_ANSWER.ANSWER IS '답변';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_ANSWER.SUMMARY IS '요약답변';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_ANSWER.SRC IS '출처';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_ANSWER.CATEGORY_ID IS '카테고리ID';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_ANSWER.ATTR1 IS '속성1';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_ANSWER.ATTR2 IS '속성2';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_ANSWER.ATTR3 IS '속성3';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_ANSWER.ATTR4 IS '속성4';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_ANSWER.LAYER1_ID IS '계층1 ID';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_ANSWER.LAYER2_ID IS '계층2 ID';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_ANSWER.LAYER3_ID IS '계층3 ID';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_ANSWER.LAYER4_ID IS '계층4 ID';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_ANSWER.CREATOR_ID IS '만든사람';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_ANSWER.UPDATER_ID IS '수정한사람';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_ANSWER.CREATE_DTM IS '만든날짜';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_ANSWER.UPDATE_DTM IS '수정날짜';

GRANT DELETE ON NLPADM.BRAIN_BQA2_ANSWER TO NLPCON;
GRANT INSERT ON NLPADM.BRAIN_BQA2_ANSWER TO NLPCON;
GRANT SELECT ON NLPADM.BRAIN_BQA2_ANSWER TO NLPCON;
GRANT UPDATE ON NLPADM.BRAIN_BQA2_ANSWER TO NLPCON;

CREATE TABLE NLPADM.BRAIN_BQA2_QUESTION (
  ID NUMBER(10) NOT NULL,
  ANSWER_ID NUMBER(10) NOT NULL,
  QUESTION CLOB NOT NULL,
  SRC VARCHAR2(512 char),
  CATEGORY_ID NUMBER(10) NOT NULL,
  ATTR1 VARCHAR2(256),
  ATTR2 VARCHAR2(256),
  ATTR3 VARCHAR2(256),
  ATTR4 VARCHAR2(256),
  CREATOR_ID VARCHAR2(40 char),
  UPDATER_ID VARCHAR2(40 char),
  CREATE_DTM TIMESTAMP(6),
  UPDATE_DTM TIMESTAMP(6)
) TABLESPACE NLP_DT01_8K;

ALTER TABLE NLPADM.BRAIN_BQA2_QUESTION
  ADD CONSTRAINT BRAIN_BQA2_QUESTION_PK
  PRIMARY KEY (ID)
  USING INDEX TABLESPACE NLP_IX01_8K;

CREATE SEQUENCE NLPADM.BRAIN_BQA2_QUESTION_SEQUENCE
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  INCREMENT BY 1
  START WITH 101
  CACHE 100
  NOORDER
  NOCYCLE;

CREATE SYNONYM NLPCON.BRAIN_BQA2_QUESTION FOR NLPADM.BRAIN_BQA2_QUESTION;
CREATE OR REPLACE SYNONYM NLPCON.BRAIN_BQA2_QUESTION_SEQUENCE FOR NLPADM.BRAIN_BQA2_QUESTION_SEQUENCE;
GRANT ALTER, SELECT ON NLPADM.BRAIN_BQA2_QUESTION_SEQUENCE TO NLPCON, RL_NLP_ALL;

COMMENT ON TABLE NLPADM.BRAIN_BQA2_QUESTION IS '질문필드';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_QUESTION.ID IS '아이디';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_QUESTION.QUESTION IS '질문';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_QUESTION.SRC IS '출처';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_QUESTION.CATEGORY_ID IS '카테고리ID';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_QUESTION.ATTR1 IS '속성1';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_QUESTION.ATTR2 IS '속성2';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_QUESTION.ATTR3 IS '속성3';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_QUESTION.ATTR4 IS '속성4';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_QUESTION.CREATOR_ID IS '만든사람';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_QUESTION.UPDATER_ID IS '수정한사람';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_QUESTION.CREATE_DTM IS '만든날짜';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_QUESTION.UPDATE_DTM IS '수정날짜';

GRANT DELETE ON NLPADM.BRAIN_BQA2_QUESTION TO NLPCON;
GRANT INSERT ON NLPADM.BRAIN_BQA2_QUESTION TO NLPCON;
GRANT SELECT ON NLPADM.BRAIN_BQA2_QUESTION TO NLPCON;
GRANT UPDATE ON NLPADM.BRAIN_BQA2_QUESTION TO NLPCON;

create table NLPADM.BRAIN_BQA2_LAYER (
  ID NUMBER(10) NOT NULL,
  NAME VARCHAR2(256) not null,
  LAYER_SECTION NUMBER(10) NOT NULL,
  CREATOR_ID VARCHAR2(40 char),
  UPDATER_ID VARCHAR2(40 char),
  CREATE_DTM TIMESTAMP(6),
  UPDATE_DTM TIMESTAMP(6)
) TABLESPACE NLP_DT01_8K;

ALTER TABLE NLPADM.BRAIN_BQA2_LAYER
  ADD CONSTRAINT BRAIN_BQA2_LAYER_PK
  PRIMARY KEY (ID)
  USING INDEX TABLESPACE NLP_IX01_8K;

CREATE SEQUENCE NLPADM.BRAIN_BQA2_LAYER_SEQUENCE
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  INCREMENT BY 1
  START WITH 101
  CACHE 100
  NOORDER
  NOCYCLE;

CREATE SYNONYM NLPCON.BRAIN_BQA2_LAYER FOR NLPADM.BRAIN_BQA2_LAYER;
CREATE OR REPLACE SYNONYM NLPCON.BRAIN_BQA2_LAYER_SEQUENCE FOR NLPADM.BRAIN_BQA2_LAYER_SEQUENCE;
GRANT ALTER, SELECT ON NLPADM.BRAIN_BQA2_LAYER_SEQUENCE TO NLPCON, RL_NLP_ALL;

COMMENT ON TABLE NLPADM.BRAIN_BQA2_LAYER IS 'Layer';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_LAYER.ID IS '아이디';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_LAYER.NAME IS 'Layer 이름';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_LAYER.LAYER_SECTION IS 'Layer 구분(1,2,3,4)';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_LAYER.CREATOR_ID IS '만든사람';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_LAYER.UPDATER_ID IS '수정한사람';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_LAYER.CREATE_DTM IS '만든날짜';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_LAYER.UPDATE_DTM IS '수정날짜';

GRANT DELETE ON NLPADM.BRAIN_BQA2_LAYER TO NLPCON;
GRANT INSERT ON NLPADM.BRAIN_BQA2_LAYER TO NLPCON;
GRANT SELECT ON NLPADM.BRAIN_BQA2_LAYER TO NLPCON;
GRANT UPDATE ON NLPADM.BRAIN_BQA2_LAYER TO NLPCON;

CREATE TABLE NLPADM.BRAIN_BQA2_CATEGORY (
  ID NUMBER(10) NOT NULL,
  NAME VARCHAR(256) NOT NULL,
  CREATOR_ID VARCHAR2(40 char),
  UPDATER_ID VARCHAR2(40 char),
  CREATE_DTM TIMESTAMP(6),
  UPDATE_DTM TIMESTAMP(6)
) TABLESPACE NLP_DT01_8K;

ALTER TABLE NLPADM.BRAIN_BQA2_CATEGORY
  ADD CONSTRAINT BRAIN_BQA2_CATEGORY_PK
  PRIMARY KEY (ID)
  USING INDEX TABLESPACE NLP_IX01_8K;

CREATE SEQUENCE NLPADM.BRAIN_BQA2_CATEGORY_SEQUENCE
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  INCREMENT BY 1
  START WITH 1
  CACHE 100
  NOORDER
  NOCYCLE;

CREATE SYNONYM NLPCON.BRAIN_BQA2_CATEGORY FOR NLPADM.BRAIN_BQA2_CATEGORY;
CREATE OR REPLACE SYNONYM NLPCON.BRAIN_BQA2_CATEGORY_SEQUENCE FOR NLPADM.BRAIN_BQA2_CATEGORY_SEQUENCE;
GRANT ALTER, SELECT ON NLPADM.BRAIN_BQA2_CATEGORY_SEQUENCE TO NLPCON, RL_NLP_ALL;

COMMENT ON TABLE NLPADM.BRAIN_BQA2_CATEGORY IS '카테고리';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_CATEGORY.ID IS '아이디';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_CATEGORY.NAME IS '카테고리 명';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_CATEGORY.CREATOR_ID IS '만든사람';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_CATEGORY.UPDATER_ID IS '수정한사람';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_CATEGORY.CREATE_DTM IS '만든날짜';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_CATEGORY.UPDATE_DTM IS '수정날짜';

GRANT DELETE ON NLPADM.BRAIN_BQA2_CATEGORY TO NLPCON;
GRANT INSERT ON NLPADM.BRAIN_BQA2_CATEGORY TO NLPCON;
GRANT SELECT ON NLPADM.BRAIN_BQA2_CATEGORY TO NLPCON;
GRANT UPDATE ON NLPADM.BRAIN_BQA2_CATEGORY TO NLPCON;

CREATE TABLE NLPADM.BRAIN_BQA2_TAG (
  ID NUMBER(10) NOT NULL,
  NAME VARCHAR(256) NOT NULL,
  CREATOR_ID VARCHAR2(40 char),
  UPDATER_ID VARCHAR2(40 char),
  CREATE_DTM TIMESTAMP(6),
  UPDATE_DTM TIMESTAMP(6)
) TABLESPACE NLP_DT01_8K;

ALTER TABLE NLPADM.BRAIN_BQA2_TAG
  ADD CONSTRAINT BRAIN_BQA2_TAG_PK
  PRIMARY KEY (ID)
  USING INDEX TABLESPACE NLP_IX01_8K;

CREATE SEQUENCE NLPADM.BRAIN_BQA2_TAG_SEQUENCE
  MINVALUE 1
  MAXVALUE 9999999999999999999999999999
  INCREMENT BY 1
  START WITH 1
  CACHE 100
  NOORDER
  NOCYCLE;

CREATE SYNONYM NLPCON.BRAIN_BQA2_TAG FOR NLPADM.BRAIN_BQA2_TAG;
CREATE OR REPLACE SYNONYM NLPCON.BRAIN_BQA2_TAG_SEQUENCE FOR NLPADM.BRAIN_BQA2_TAG_SEQUENCE;
GRANT ALTER, SELECT ON NLPADM.BRAIN_BQA2_TAG_SEQUENCE TO NLPCON, RL_NLP_ALL;

COMMENT ON TABLE NLPADM.BRAIN_BQA2_TAG IS '태그명';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_TAG.ID IS '아이디';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_TAG.NAME IS '태그명';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_TAG.CREATOR_ID IS '만든사람';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_TAG.UPDATER_ID IS '수정한사람';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_TAG.CREATE_DTM IS '만든날짜';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_TAG.UPDATE_DTM IS '수정날짜';

GRANT DELETE ON NLPADM.BRAIN_BQA2_TAG TO NLPCON;
GRANT INSERT ON NLPADM.BRAIN_BQA2_TAG TO NLPCON;
GRANT SELECT ON NLPADM.BRAIN_BQA2_TAG TO NLPCON;
GRANT UPDATE ON NLPADM.BRAIN_BQA2_TAG TO NLPCON;

CREATE TABLE NLPADM.BRAIN_BQA2_ANSWER_TAG_REL (
  ANSWER_ID NUMBER(10) NOT NULL,
  TAG_ID NUMBER(10) NOT NULL,
  CREATE_DTM TIMESTAMP(6),
  UPDATE_DTM TIMESTAMP(6)
) TABLESPACE NLP_DT01_8K;

ALTER TABLE NLPADM.BRAIN_BQA2_ANSWER_TAG_REL
  ADD CONSTRAINT BRAIN_BQA2_ANSWER_TAG_REL_PK
  PRIMARY KEY (ANSWER_ID, TAG_ID)
  USING INDEX TABLESPACE NLP_IX01_8K;

CREATE SYNONYM NLPCON.BRAIN_BQA2_ANSWER_TAG_REL FOR NLPADM.BRAIN_BQA2_ANSWER_TAG_REL;

COMMENT ON TABLE NLPADM.BRAIN_BQA2_ANSWER_TAG_REL IS '답변 태그 Rel';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_ANSWER_TAG_REL.ANSWER_ID IS '답변 ID';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_ANSWER_TAG_REL.TAG_ID IS '태그 ID';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_ANSWER_TAG_REL.CREATE_DTM IS '만든날짜';
COMMENT ON COLUMN NLPADM.BRAIN_BQA2_ANSWER_TAG_REL.UPDATE_DTM IS '수정날짜';

GRANT DELETE ON NLPADM.BRAIN_BQA2_ANSWER_TAG_REL TO NLPCON;
GRANT INSERT ON NLPADM.BRAIN_BQA2_ANSWER_TAG_REL TO NLPCON;
GRANT SELECT ON NLPADM.BRAIN_BQA2_ANSWER_TAG_REL TO NLPCON;
GRANT UPDATE ON NLPADM.BRAIN_BQA2_ANSWER_TAG_REL TO NLPCON;