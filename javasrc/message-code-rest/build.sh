#!/usr/bin/env bash

OS=
if [ -f /etc/lsb-release ]; then
  OS=ubuntu
elif [ -f /etc/centos-release ]; then
  OS=centos
elif [ -f /etc/redhat-release ]; then
  OS=centos
else
  echo "Illegal OS detected. Set centos as default."
  OS=centos
fi

if [ "${OS}" = "centos" ]; then
  __CC=/usr/bin/gcc
  __CXX=/usr/bin/g++
  CMAKE=/usr/bin/cmake3
else
  __CC=/usr/bin/gcc-4.8
  __CXX=/usr/bin/g++-4.8
  CMAKE=/usr/bin/cmake
fi

if [ ! -d ${MAUM_ROOT} ]; then
  echo ${MAUM_ROOT} is not directory.
  echo Use valid directory.
  exit 1
fi

## argument parameters
shift

maum_root=${MAUM_ROOT}

REPO_ROOT=$(pwd)
# repo root is used in submodule build scirpt
export REPO_ROOT

function main() {
build_rest
build_config
}

## mlt-message
function build_rest() {
  base_dir=$(pwd)
  build_base=${REPO_ROOT}
  shadow_dist_base=${REPO_ROOT}/target
  out_dir=${MAUM_ROOT}
  cd ${build_base}
  gradlew clean shadowJar -Pconfig=prod
  test -d ${out_dir}/lib || mkdir -p ${out_dir}/lib
  cp ${shadow_dist_base}/libs/* ${out_dir}/lib
  cd ${base_dir}
}

## config
function build_config() {
  test -d ${MAUM_ROOT}/etc || mkdir -p ${MAUM_ROOT}/etc
  (cd config/supervisor.conf.d && rsync -ar * ${MAUM_ROOT}/etc/supervisor/conf.d)
  cp config/message-code.conf ${MAUM_ROOT}/etc
  cp config/logback/message-code-api-logback.xml ${MAUM_ROOT}/etc/logback
  if [ "${OS}" = "centos" ]; then
    sudo chcon -Rv --type=httpd_log_t ${MAUM_ROOT}/logs
  fi
}

main