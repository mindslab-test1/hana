package message.api.vo.messagecode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@EqualsAndHashCode
@Data
public class MessageCode implements Serializable {

  private static final String SVC_GRP_CD_ECT = "'ECT'";
  private static final String SVC_GRP_CD_ECA = "'ECA'";

  // HAI서비스그룹코드 : group1
  // CCA[대손님] ECA[대직원]
  private String haiSvcGrpCd;

  // HAI채널구분코드 : group2
  // ZZZ[전체] NEA[2단창] SEA[3단창]
  private String haiChnlDvCd;

  // 스킬코드 : skillId
  // 6자리 fix [HAI채널구분코드 마다 000000부터 생성]
  // common msg들은 999999 스킬에 등록이 되어있음 [대직원은 정리가 잘되 있지만, 대손님은 안되어 있음]
  private String skillCd;

  // 메세지 순번 : nodeAttrSeq
  // 메세지 등록순서 [스킬코드에 등록된]
  private BigDecimal msgSeq;

  // 메세지 코드 : [ex : NOR + A + 99999]
  // msgMgntCd + dtlsMsgMgntCd + msgNo
  private String msgCd;

  // 관리 메세지 코드
  // NOR[정상메세지], ERR[오류메세지], COM[공통메세지]
  private String msgMgmtCd;

  // 세부 관리 메세지코드
  // [A, Q, B, D, S]
  private String dtlsMsgMgntCd;

  // 메시지 번호 [답변번호]
  // 사용자가 직접입력하여 생성 [임의의 5자리 숫자]
  private String msgNo;

  // 정렬 순서(수정)
  private BigDecimal orderSeq;

  // 메세지 유형 코드 [답변의 유형을 만들어주는 코드] : nodeAttrCode
  // ex : 버튼, 텍스트, 이미지 등등등....
  // 2자리 숫자로 생성
  private String msgTypeCd;

  // 세부메시지 유형 코드
  // ex : 텍스트출력 기본, 카드출력타입1 등등등...
  // msgTypeCd + 타입2자리 [총 4자리숫자]
  private String dtlsMsgTypeCd;

  // 메세지 TEXT
  private String msgCtt;

  // 메세지 Template
  private String tmplCtt;

  // 사용여부
  private String useYn;

  // 비고
  private String rmk;

  // 7자리
  private String regEmpNo;
  private Date sysRegDtm;
  private String updEmpNo;
  private Date sysUpdDtm;

  // 신규 메세지 생성시 사용
  public String generateMsgCd() {
    return this.getMsgMgmtCd() + this.getDtlsMsgMgntCd() + this.getMsgNo();
  }
}
