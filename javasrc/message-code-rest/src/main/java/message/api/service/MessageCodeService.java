package message.api.service;

import java.util.ArrayList;
import lombok.extern.slf4j.Slf4j;
import message.api.mapper.MessageCodeMapper;
import message.api.vo.messagecode.MessageCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class MessageCodeService {

  private final MessageCodeMapper messageCodeMapper;

  @Autowired
  public MessageCodeService(MessageCodeMapper messageCodeMapper) {
    this.messageCodeMapper = messageCodeMapper;
  }

  public ArrayList<MessageCode> findMessageCodes(MessageCode messageCode) {
    // HAI_SVC_GRP_CD + HAI_CHNL_DV_CD + SKILL_CD
    return this.messageCodeMapper.findMessageCodes(messageCode);
  }

  public String getMaxMsgNo(MessageCode messageCode) {
    String noStr = this.messageCodeMapper.getMaxMsgNo(messageCode);

    if (noStr == null) {
      noStr = "0";
    }

    int no = Integer.parseInt(noStr);
    no++;
    return String.format("%05d", no);
  }

  public void addMessageCode(MessageCode messageCode) {
    messageCodeMapper.insertMessageCode(messageCode);
  }

  public void updateMessageCode(MessageCode messageCode) {
    messageCodeMapper.updateMessageCode(messageCode);
  }

  public void deleteMessageCode(MessageCode messageCode) {
    messageCodeMapper.deleteMessageCode(messageCode);
  }

  public void copyMessageCode(MessageCode messageCode) {
    messageCodeMapper.copyMessageCode(messageCode);
  }
}
