package message.api.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.google.protobuf.Empty;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;
import com.google.protobuf.util.JsonFormat;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import maum.brain.qa.nqa.ApiService.AbortIndexingRequest;
import maum.brain.qa.nqa.ApiService.IndexingQaSet;
import maum.brain.qa.nqa.ApiService.IndexingResponse;
import maum.brain.qa.nqa.Nqa.CheckAnswerResponse;
import maum.brain.qa.nqa.Nqa.CheckQuestionResponse;
import maum.brain.qa.nqa.Nqa.SearchAnswerRequest;
import maum.brain.qa.nqa.Nqa.SearchQuestionRequest;
import message.common.NQAGrpcInterfaceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class NQAGrpcService {

  private static final Logger logger = LoggerFactory.getLogger(NQAGrpcService.class);
  private static final MapType ReturnType = TypeFactory.defaultInstance()
      .constructMapType(HashMap.class, String.class, Object.class);

  @Autowired
  private Environment env;

  private static final Map<String, List<String>> nqaChannelAddrs = new HashMap<>();

  private static synchronized List<String> getNqaChannelAddresses(Environment env, String channel) {
    String propertyKey = "nqa.server." + channel.toLowerCase() + ".addr";
    if (!env.containsProperty(propertyKey)) {
      propertyKey = "nqa.server.addr";
    }

    if (!nqaChannelAddrs.containsKey(propertyKey)) {
      final String channelAddrs = env.getProperty(propertyKey);
      logger.info("{} = {}", propertyKey, channelAddrs);
      nqaChannelAddrs.put(propertyKey, Arrays.asList(channelAddrs.split(",")));
    }

    return nqaChannelAddrs.get(propertyKey);
  }

  private static HashMap<String, Object> getHashMapFromMessage(Message message) throws IOException {
    String json = JsonFormat.printer().includingDefaultValueFields().print(message);
    ObjectMapper mapper = new ObjectMapper();
    return mapper.readValue(json, ReturnType);
  }

  public ArrayList<HashMap<String, Object>> indexing(HashMap<String, Object> param) {
    logger.debug("===== indexing :: param {}", param);

    ArrayList<HashMap<String, Object>> resultArr = new ArrayList<>();

    List<String> nqaAddrs = getNqaChannelAddresses(env, (String) param.get("channel"));

    for (String addr : nqaAddrs) {
      try {
        String ip = addr.split(":")[0];
        int port = Integer.parseInt(addr.split(":")[1]);

        NQAGrpcInterfaceManager client = new NQAGrpcInterfaceManager(ip, port);

        IndexingQaSet.Builder indexingRequest = IndexingQaSet.newBuilder();
        indexingRequest.setChannel(param.get("channel") + "");
        indexingRequest.setCategory(param.get("category") + "");
        indexingRequest.setIndexId(param.get("indexId") + "");

        IndexingResponse indexingResponse = client.indexing(indexingRequest.build());

        HashMap<String, Object> map = getHashMapFromMessage(indexingResponse);
        map.put("resultCode", 1);
        map.put("nqaAddr", addr);
        resultArr.add(map);
      } catch (Exception e) {
        logger.error("===== indexing :: {}", e.getMessage());
        HashMap<String, Object> map = new HashMap<>();
        map.put("resultCode", 0);
        map.put("nqaAddr", addr);
        resultArr.add(map);
      }
    }
    return resultArr;
  }

  public ArrayList<HashMap<String, Object>> abortIndexing(HashMap<String, Object> param) {
    logger.debug("===== abortIndexing :: param {}", param);

    ArrayList<HashMap<String, Object>> resultArr = new ArrayList<>();

    List<String> nqaAddrs = getNqaChannelAddresses(env, (String) param.get("channel"));
    for (String addr : nqaAddrs) {
      try {
        String ip = addr.split(":")[0];
        int port = Integer.parseInt(addr.split(":")[1]);

        NQAGrpcInterfaceManager client = new NQAGrpcInterfaceManager(ip, port);

        AbortIndexingRequest.Builder abortIndexingRequest = AbortIndexingRequest.newBuilder();
        abortIndexingRequest.setChannel(param.get("channel") + "");
        abortIndexingRequest.setCategory(param.get("category") + "");

        IndexingResponse indexingResponse = client.abortIndexing(abortIndexingRequest.build());

        HashMap<String, Object> map = getHashMapFromMessage(indexingResponse);
        map.put("resultCode", 1);
        map.put("nqaAddr", addr);
        resultArr.add(map);
      } catch (Exception e) {
        logger.error("===== abortIndexing :: {}", e.getMessage());
        HashMap<String, Object> map = new HashMap<>();
        map.put("resultCode", 0);
        map.put("nqaAddr", addr);
        resultArr.add(map);
      }
    }
    return resultArr;
  }

  public ArrayList<HashMap<String, Object>> getIndexingStatus(HashMap<String, Object> param) {
    logger.debug("===== getIndexingStatus:: param {}", param);

    ArrayList<HashMap<String, Object>> resultArr = new ArrayList<>();

    List<String> nqaAddrs = getNqaChannelAddresses(env, (String) param.get("channel"));
    for (String addr : nqaAddrs) {
      try {
        String ip = addr.split(":")[0];
        int port = Integer.parseInt(addr.split(":")[1]);

        NQAGrpcInterfaceManager client = new NQAGrpcInterfaceManager(ip, port);

        IndexingResponse indexingResponse = client.getIndexingStatus(Empty.newBuilder().build());

        HashMap<String, Object> map = getHashMapFromMessage(indexingResponse);
        map.put("resultCode", 1);
        map.put("nqaAddr", addr);
        resultArr.add(map);
      } catch (Exception e) {
        logger.error("===== getIndexingStatus :: {}", e.getMessage());
        HashMap<String, Object> map = new HashMap<>();
        map.put("resultCode", 0);
        map.put("nqaAddr", addr);
        resultArr.add(map);
      }
    }
    return resultArr;
  }

  public ArrayList<HashMap<String, Object>> checkQuestion(HashMap<String, Object> param) {
    logger.debug("===== checkQuestion :: param {}", param);

    ArrayList<HashMap<String, Object>> resultArr = new ArrayList<>();

    List<String> nqaAddrs = getNqaChannelAddresses(env, (String) param.get("channel"));
    for (String addr : nqaAddrs) {
      try {
        String ip = addr.split(":")[0];
        int port = Integer.parseInt(addr.split(":")[1]);

        NQAGrpcInterfaceManager client = new NQAGrpcInterfaceManager(ip, port);

        SearchQuestionRequest.Builder request = SearchQuestionRequest.newBuilder();
        String jsonRequest = new ObjectMapper().writeValueAsString(param);
        logger.trace("checkQuestion jsonRequest: {}", jsonRequest);
        JsonFormat.parser().ignoringUnknownFields().merge(jsonRequest, request);

        CheckQuestionResponse response = client.checkQuestion(request.build());

        HashMap<String, Object> map = getHashMapFromMessage(response);
        map.put("resultCode", 1);
        map.put("nqaAddr", addr);
        resultArr.add(map);
      } catch (Exception e) {
        if (e instanceof InvalidProtocolBufferException) {
          logger.error("===== checkQuestion :: proto Message parsing Error {}", e.getMessage());
        } else if (e instanceof JsonProcessingException) {
          logger.error("===== checkQuestion :: jackson Json Processing Error {}", e.getMessage());
        } else {
          logger.error("===== checkQuestion :: {}", e.getMessage());
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("resultCode", 0);
        map.put("nqaAddr", addr);
        resultArr.add(map);
      }
    }
    return resultArr;
  }

  public ArrayList<HashMap<String, Object>> checkAnswer(HashMap<String, Object> param) {
    logger.debug("===== checkAnswer :: param {}", param);

    ArrayList<HashMap<String, Object>> resultArr = new ArrayList<>();

    List<String> nqaAddrs = getNqaChannelAddresses(env, (String) param.get("channel"));
    for (String addr : nqaAddrs) {
      try {
        String ip = addr.split(":")[0];
        int port = Integer.parseInt(addr.split(":")[1]);

        NQAGrpcInterfaceManager client = new NQAGrpcInterfaceManager(ip, port);

        SearchAnswerRequest.Builder request = SearchAnswerRequest.newBuilder();
        String jsonRequest = new ObjectMapper().writeValueAsString(param);
        logger.trace("checkAnswer jsonRequest: {}", jsonRequest);
        JsonFormat.parser().ignoringUnknownFields().merge(jsonRequest, request);

        CheckAnswerResponse response = client.checkAnswer(request.build());

        HashMap<String, Object> map = getHashMapFromMessage(response);
        map.put("resultCode", 1);
        map.put("nqaAddr", addr);
        resultArr.add(map);
      } catch (Exception e) {
        if (e instanceof InvalidProtocolBufferException) {
          logger.error("===== checkAnswer :: proto Message parsing Error {}", e.getMessage());
        } else if (e instanceof JsonProcessingException) {
          logger.error("===== checkAnswer :: jackson Json Processing Error {}", e.getMessage());
        } else {
          logger.error("===== checkAnswer :: {}", e.getMessage());
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("resultCode", 0);
        map.put("nqaAddr", addr);
        resultArr.add(map);
      }
    }
    return resultArr;
  }
}
