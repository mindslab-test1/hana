package message.api.mapper;

import java.util.ArrayList;
import message.api.vo.messagecode.MessageCode;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface MessageCodeMapper {

  ArrayList<MessageCode> findMessageCodes(MessageCode messageCode) throws DataAccessException;

  String getMaxMsgNo(MessageCode messageCode) throws DataAccessException;

  void insertMessageCode(MessageCode messageCode) throws DataAccessException;

  void updateMessageCode(MessageCode messageCode) throws DataAccessException;

  void deleteMessageCode(MessageCode messageCode) throws DataAccessException;

  void copyMessageCode(MessageCode messageCode) throws DataAccessException;
}
