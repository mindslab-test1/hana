package message.api.controller;

import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import message.api.service.NQAGrpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("api/nqa/third-party")
public class NqaController {

  private static final String CHANNEL_IS_MISSING_OR_EMPTY = "'channel' is missing or empty.";
  private static final String CATEGORY_IS_MISSING_OR_EMPTY = "'category' is missing or empty.";
  private static final String INDEX_ID_IS_MISSING_OR_EMPTY = "'id' to index is missing or empty.";
  private static final String NTOP_IS_INVALID = "'ntop' is invalid .";
  private static final String CATEGORY_IS_MISSING = "'category' is missing.";
  private static final String QUERY_TYPE_IS_MISSING = "'query_type' is missing.";
  private static final String QUERY_TARGET_IS_MISSING = "'query_target' is missing.";
  private static final String USER_QUERY_IS_MISSING = "'user_query' is missing.";
  private static final String ATTRIBUTES_IS_MISSING = "'attributes' is missing.";

  @Autowired
  private NQAGrpcService nqaGrpcService;

  private boolean checkEmpty(Object param) {
    return param != null && !"".equals(param);
  }

  @CrossOrigin
  @RequestMapping(value = "/indexing", method = RequestMethod.POST)
  public ResponseEntity<?> indexing(@RequestBody HashMap<String, Object> param) {
    log.info("===== call api  POST [[/api/nqa/indexing]] :: {}", param);

    try {
      if (!this.checkEmpty(param.get("channel"))) {
        return new ResponseEntity<>(CHANNEL_IS_MISSING_OR_EMPTY, HttpStatus.BAD_REQUEST);
      }
      if (!this.checkEmpty(param.get("category"))) {
        return new ResponseEntity<>(CATEGORY_IS_MISSING_OR_EMPTY, HttpStatus.BAD_REQUEST);
      }
      if (!this.checkEmpty(param.get("indexId"))) {
        return new ResponseEntity<>(INDEX_ID_IS_MISSING_OR_EMPTY, HttpStatus.BAD_REQUEST);
      }

      return new ResponseEntity<>(nqaGrpcService.indexing(param), HttpStatus.OK);
    } catch (Exception e) {
      log.error("[ERROR] /api/nqa/indexing :: {}", e.getMessage());
      return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @RequestMapping(value = "/abort-indexing", method = RequestMethod.POST)
  public ResponseEntity<?> abortIndexing(@RequestBody HashMap<String, Object> param) {
    log.info("===== call api  POST [[/api/nqa/abort-indexing]] :: {}", param);

    try {
      if (!this.checkEmpty(param.get("channel"))) {
        return new ResponseEntity<>(CHANNEL_IS_MISSING_OR_EMPTY, HttpStatus.BAD_REQUEST);
      }
      if (!this.checkEmpty(param.get("category"))) {
        return new ResponseEntity<>(CATEGORY_IS_MISSING_OR_EMPTY, HttpStatus.BAD_REQUEST);
      }

      return new ResponseEntity<>(nqaGrpcService.abortIndexing(param), HttpStatus.OK);
    } catch (Exception e) {
      log.error("[ERROR] /api/nqa/abort-indexing :: {}", e.getMessage());
      return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @RequestMapping(value = "/indexing-status", method = RequestMethod.POST)
  public ResponseEntity<?> getIndexingStatus(@RequestBody HashMap<String, Object> param) {
    log.info("===== call api  POST [[/api/nqa/indexing-status]]");

    try {
      if (!this.checkEmpty(param.get("channel"))) {
        return new ResponseEntity<>(CHANNEL_IS_MISSING_OR_EMPTY, HttpStatus.BAD_REQUEST);
      }
      if (!this.checkEmpty(param.get("category"))) {
        return new ResponseEntity<>(CATEGORY_IS_MISSING_OR_EMPTY, HttpStatus.BAD_REQUEST);
      }

      return new ResponseEntity<>(nqaGrpcService.getIndexingStatus(param), HttpStatus.OK);
    } catch (Exception e) {
      log.error("[ERROR] /api/nqa/indexing-status :: {}", e.getMessage());
      return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @RequestMapping(value = "/check-question", method = RequestMethod.POST)
  public ResponseEntity<?> checkQuestion(@RequestBody HashMap<String, Object> param) {
    log.info("===== call api  POST [[/api/nqa/check-question]] :: {}", param);

    try {
      if (Integer.parseInt((String) param.getOrDefault("ntop", "-1")) < 1) {
        return new ResponseEntity<>(NTOP_IS_INVALID, HttpStatus.BAD_REQUEST);
      }
      if (!this.checkEmpty(param.get("channel"))) {
        return new ResponseEntity<>(CHANNEL_IS_MISSING_OR_EMPTY, HttpStatus.BAD_REQUEST);
      }
      if (param.get("category") == null) {
        return new ResponseEntity<>(CATEGORY_IS_MISSING, HttpStatus.BAD_REQUEST);
      }
      if (param.get("query_type") == null) {
        return new ResponseEntity<>(QUERY_TYPE_IS_MISSING, HttpStatus.BAD_REQUEST);
      }
      if (param.get("query_target") == null) {
        return new ResponseEntity<>(QUERY_TARGET_IS_MISSING, HttpStatus.BAD_REQUEST);
      }
      if (param.get("user_query") == null) {
        return new ResponseEntity<>(USER_QUERY_IS_MISSING, HttpStatus.BAD_REQUEST);
      }
      if (param.get("attributes") == null) {
        return new ResponseEntity<>(ATTRIBUTES_IS_MISSING, HttpStatus.BAD_REQUEST);
      }

      return new ResponseEntity<>(nqaGrpcService.checkQuestion(param), HttpStatus.OK);
    } catch (Exception e) {
      log.error("[ERROR] /api/nqa/check-question :: {}", e.getMessage());
      return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @RequestMapping(value = "/check-answer", method = RequestMethod.POST)
  public ResponseEntity<?> checkAnswer(@RequestBody HashMap<String, Object> param) {
    log.info("===== call api  POST [[/api/nqa/check-answer]] :: {}", param);

    try {
      if (Integer.parseInt((String) param.getOrDefault("ntop", "-1")) < 1) {
        return new ResponseEntity<>(NTOP_IS_INVALID, HttpStatus.BAD_REQUEST);
      }
      if (!this.checkEmpty(param.get("channel"))) {
        return new ResponseEntity<>(CHANNEL_IS_MISSING_OR_EMPTY, HttpStatus.BAD_REQUEST);
      }
      if (param.get("category") == null) {
        return new ResponseEntity<>(CATEGORY_IS_MISSING, HttpStatus.BAD_REQUEST);
      }
      if (param.get("query_type") == null) {
        return new ResponseEntity<>(QUERY_TYPE_IS_MISSING, HttpStatus.BAD_REQUEST);
      }
      if (param.get("query_target") == null) {
        return new ResponseEntity<>(QUERY_TARGET_IS_MISSING, HttpStatus.BAD_REQUEST);
      }
      if (param.get("user_query") == null) {
        return new ResponseEntity<>(USER_QUERY_IS_MISSING, HttpStatus.BAD_REQUEST);
      }
      if (param.get("attributes") == null) {
        return new ResponseEntity<>(ATTRIBUTES_IS_MISSING, HttpStatus.BAD_REQUEST);
      }

      return new ResponseEntity<>(nqaGrpcService.checkAnswer(param), HttpStatus.OK);
    } catch (Exception e) {
      log.error("[ERROR] /api/nqa/check-answer :: {}", e.getMessage());
      return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
