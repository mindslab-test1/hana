package message.api.controller;

import java.util.ArrayList;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;
import message.api.service.MessageCodeService;
import message.api.vo.messagecode.MessageCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("api/msg-code")
public class MessageCodeController {

  /**
   * 통합 메세지관리에서 사용하는 코드들을 정의해 놓았음[모두 정의된 것은 아님] 데이터 Insert시에 하드코딩으로 사용해야하는 코드들을 정의 함 주석 처리 되어있는 부분은
   * API SERVER에서 사용하지 않는 코드들 [참고용, 추후 추가 개발될 수 있는 코드들]
   */
  //private static final String SVC_GRP_CD_CCA = "CCA"; // 대손님
  private static final String SVC_GRP_CD_ECA = "ECA"; // 대직원
  private static final String SVC_GRP_CD_ECT = "ECT"; // 현

  private static final String CHN_DV_CD_ZZZ = "ZZZ"; // 전체
  //private static final String CHN_DV_CD_NEA = "NEA"; // 2단창
  //private static final String CHN_DV_CD_SEA = "SEA"; // 3단창

  private static final String MSG_MGMT_CD_NOR = "NOR"; // 정상메세지
  //private static final String MSG_MGNT_CD_ERR = "ERR"; // 오류메세지
  //private static final String MSG_MGNT_CD_COM = "COM"; // 공통메세지

  private static final String DTLS_MSG_MGNT_CD_A = "A"; // Answer
  //private static final String DTLS_MSG_MGNT_CD_Q = "Q"; // Question
  //private static final String DTLS_MSG_MGNT_CD_B = "B"; // Business
  //private static final String DTLS_MSG_MGNT_CD_D = "D"; // Database
  //private static final String DTLS_MSG_MGNT_CD_S = "S"; // System

  private static final String COMMON_SKILL_CD = "999999"; // 공통메세지코드 관리 스킬코드
  private static final String DEFAULT_MSG_NO = "99999";
  private static final String DEFAULT_MSG_TYPE_CD = "01";
  private static final String DEFAULT_MSG_DTLS_TYPE_CD = "0101";

  @Autowired
  private MessageCodeService messageCodeService;

  @CrossOrigin
  @RequestMapping(value = "/test", method = RequestMethod.GET)
  public ResponseEntity<?> test() {
    log.info("===== call api  GET [[/api/msg-code/test]] :: {}");

    try {

      // 연동 테스트용

      return new ResponseEntity<>("Hello Java World!!", HttpStatus.OK);
    } catch (Exception e) {
      log.error("[ERROR] /api/msg-code/test :: {}", e.getMessage());
      return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @RequestMapping(value = "/skill-msgs", method = RequestMethod.POST)
  public ResponseEntity<?> getSkillMessageCodes(@RequestBody MessageCode param) {
    log.info("===== call api  POST [[/api/msg-code/skill-msgs]] :: {}", param);

    try {

      String errMsg;
      ArrayList<MessageCode> skillMsgs = new ArrayList<>();
      ArrayList<MessageCode> commonSkillMsgs = new ArrayList<>();
      ArrayList<MessageCode> rsltMsgs = new ArrayList<>();

      if (!this.checkEmpty(param.getHaiSvcGrpCd())) {
        errMsg = "haiSvcGrpCd is null or empty.";
        return new ResponseEntity<>(errMsg, HttpStatus.BAD_REQUEST);
      }
      if (!this.checkEmpty(param.getHaiChnlDvCd())) {
        errMsg = "haiChnlDvCd is null or empty.";
        return new ResponseEntity<>(errMsg, HttpStatus.BAD_REQUEST);
      }

      if (this.checkEmpty(param.getSkillCd())) {
        log.info("param1: {}", param);
        skillMsgs = messageCodeService.findMessageCodes(param);
      }

      param.setSkillCd(COMMON_SKILL_CD);
      log.info("param2: {}", param);
      commonSkillMsgs = messageCodeService.findMessageCodes(param);

      rsltMsgs.addAll(skillMsgs);
      rsltMsgs.addAll(commonSkillMsgs);

      return new ResponseEntity<>(rsltMsgs, HttpStatus.OK);
    } catch (Exception e) {
      log.error("[ERROR] /api/msg-code/skill-msgs :: {}", e.getMessage());
      return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @RequestMapping(value = "/msgs", method = RequestMethod.POST)
  public ResponseEntity<?> getDetailMessageCodes(@RequestBody MessageCode param) {
    log.info("===== call api  POST [[/api/msg-code/msgs]] :: {}", param);

    try {

      String errMsg;

      if (!this.checkEmpty(param.getHaiSvcGrpCd())) {
        errMsg = "haiSvcGrpCd is null or empty.";
        return new ResponseEntity<>(errMsg, HttpStatus.BAD_REQUEST);
      }
      if (!this.checkEmpty(param.getHaiChnlDvCd())) {
        errMsg = "haiChnlDvCd is null or empty.";
        return new ResponseEntity<>(errMsg, HttpStatus.BAD_REQUEST);
      }
      if (!this.checkEmpty(param.getSkillCd())) {
        errMsg = "skillCd is null or empty.";
        return new ResponseEntity<>(errMsg, HttpStatus.BAD_REQUEST);
      }
      if (!this.checkEmpty(param.getMsgCd())) {
        errMsg = "msgCd is null or empty.";
        return new ResponseEntity<>(errMsg, HttpStatus.BAD_REQUEST);
      }

      return new ResponseEntity<>(messageCodeService.findMessageCodes(param), HttpStatus.OK);
    } catch (Exception e) {
      log.error("[ERROR] /api/msg-code/msgs :: {}", e.getMessage());
      return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @RequestMapping(value = "/insert", method = RequestMethod.POST)
  public ResponseEntity<?> insertMessageCode(@RequestBody MessageCode param) {
    log.info("===== call api  POST [[/api/msg-code/insert]] :: {}", param);

    try {
      String errMsg;

      /*if (!this.checkEmpty(param.getHaiSvcGrpCd())) {
        errMsg = "haiSvcGrpCd is null or empty.";
        return new ResponseEntity<>(errMsg, HttpStatus.BAD_REQUEST);
      }
      if (!this.checkEmpty(param.getHaiChnlDvCd())) {
        errMsg = "haiChnlDvCd is null or empty.";
        return new ResponseEntity<>(errMsg, HttpStatus.BAD_REQUEST);
      }*/
      if (!this.checkEmpty(param.getSkillCd())) {
        errMsg = "skillCd is null or empty.";
        return new ResponseEntity<>(errMsg, HttpStatus.BAD_REQUEST);
      }

      // 신규 메세지 코드의 경우 아래의 값으로 default 셋팅
      param.setHaiSvcGrpCd(SVC_GRP_CD_ECT);
      param.setHaiChnlDvCd(CHN_DV_CD_ZZZ);

      // msgCd 값이 없는 경우에만 기본값으로 생성
      if (!this.checkEmpty(param.getMsgCd())) {
        param.setMsgMgmtCd(MSG_MGMT_CD_NOR);
        param.setDtlsMsgMgntCd(DTLS_MSG_MGNT_CD_A);
        if (!this.checkEmpty(param.getMsgNo())) {
          param.setMsgNo(messageCodeService.getMaxMsgNo(param));
        }
        param.setMsgCd(param.generateMsgCd());
      }

      if (!this.checkEmpty(param.getDtlsMsgTypeCd())) {
        param.setMsgTypeCd(DEFAULT_MSG_TYPE_CD);
        param.setDtlsMsgTypeCd(DEFAULT_MSG_DTLS_TYPE_CD);
      } else {
        param.setMsgTypeCd(param.getDtlsMsgTypeCd().substring(0, 2));
        param.setDtlsMsgTypeCd(param.getDtlsMsgTypeCd());
      }

      param.setUseYn("Y");
      param.setRmk("시나리오 빌더를 통해 메세지코드 생성");
      param.setRegEmpNo("9999999");
      param.setSysRegDtm(new Date());
      param.setUpdEmpNo("9999999");
      param.setSysUpdDtm(new Date());

      log.info("param: {}", param);

      messageCodeService.addMessageCode(param);

      return new ResponseEntity<>(messageCodeService.findMessageCodes(param), HttpStatus.OK);
    } catch (Exception e) {
      log.error("[ERROR] /api/msg-code/insert :: {}", e.getMessage());
      return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @RequestMapping(value = "/update", method = RequestMethod.POST)
  public ResponseEntity<?> updateMessageCode(@RequestBody MessageCode param) {
    log.info("===== call api  POST [[/api/msg-code/update]] :: {}", param);

    try {
      String errMsg;

      if (!this.checkEmpty(param.getHaiSvcGrpCd())) {
        errMsg = "haiSvcGrpCd is null or empty.";
        return new ResponseEntity<>(errMsg, HttpStatus.BAD_REQUEST);
      }
      if (!this.checkEmpty(param.getHaiChnlDvCd())) {
        errMsg = "haiChnlDvCd is null or empty.";
        return new ResponseEntity<>(errMsg, HttpStatus.BAD_REQUEST);
      }
      if (!this.checkEmpty(param.getSkillCd())) {
        errMsg = "skillCd is null or empty.";
        return new ResponseEntity<>(errMsg, HttpStatus.BAD_REQUEST);
      }
      if (!this.checkEmpty(param.getMsgCd())) {
        errMsg = "msgCd is null or empty.";
        return new ResponseEntity<>(errMsg, HttpStatus.BAD_REQUEST);
      }

      param.setUpdEmpNo("9999999");
      param.setMsgTypeCd(param.getDtlsMsgTypeCd().substring(0, 2));
      messageCodeService.updateMessageCode(param);

      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      log.error("[ERROR] /api/msg-code/update :: {}", e.getMessage());
      return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @RequestMapping(value = "/delete", method = RequestMethod.POST)
  public ResponseEntity<?> deleteMessageCode(@RequestBody MessageCode param) {
    log.info("===== call api  POST [[/api/msg-code/delete]] :: {}", param);

    try {
      String errMsg;

      if (!this.checkEmpty(param.getHaiSvcGrpCd())) {
        errMsg = "haiSvcGrpCd is null or empty.";
        return new ResponseEntity<>(errMsg, HttpStatus.BAD_REQUEST);
      }
      if (!this.checkEmpty(param.getHaiChnlDvCd())) {
        errMsg = "haiChnlDvCd is null or empty.";
        return new ResponseEntity<>(errMsg, HttpStatus.BAD_REQUEST);
      }
      if (!this.checkEmpty(param.getSkillCd())) {
        errMsg = "skillCd is null or empty.";
        return new ResponseEntity<>(errMsg, HttpStatus.BAD_REQUEST);
      }
      if (!this.checkEmpty(param.getMsgCd())) {
        errMsg = "msgCd is null or empty.";
        return new ResponseEntity<>(errMsg, HttpStatus.BAD_REQUEST);
      }

      messageCodeService.deleteMessageCode(param);

      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      log.error("[ERROR] /api/msg-code/delete :: {}", e.getMessage());
      return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @CrossOrigin
  @RequestMapping(value = "/deploy_msgs", method = RequestMethod.POST)
  public ResponseEntity<?> updateMessageCodes(@RequestBody MessageCode param) {
    log.info("===== call api  POST [[/api/msg-code/deploy_msgs]] :: {}", param);

    try {
      String errMsg;

      if (!this.checkEmpty(param.getHaiSvcGrpCd())) {
        errMsg = "haiSvcGrpCd is null or empty.";
        return new ResponseEntity<>(errMsg, HttpStatus.BAD_REQUEST);
      }
      if (!this.checkEmpty(param.getHaiChnlDvCd())) {
        errMsg = "haiChnlDvCd is null or empty.";
        return new ResponseEntity<>(errMsg, HttpStatus.BAD_REQUEST);
      }
      if (!this.checkEmpty(param.getSkillCd())) {
        errMsg = "skillCd is null or empty.";
        return new ResponseEntity<>(errMsg, HttpStatus.BAD_REQUEST);
      }

      param.setHaiSvcGrpCd(SVC_GRP_CD_ECA);

      messageCodeService.deleteMessageCode(param);
      messageCodeService.copyMessageCode(param);

      return new ResponseEntity<>(HttpStatus.OK);
    } catch (Exception e) {
      log.error("[ERROR] /api/msg-code/deploy_msgs :: {}", e.getMessage());
      return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  private boolean checkEmpty(String param) {
    return param != null && !"".equals(param);
  }

}
