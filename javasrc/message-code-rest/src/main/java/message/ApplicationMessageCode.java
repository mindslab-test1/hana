package message;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@Configuration
@MapperScan(basePackages = "message/**")
@PropertySource(value = "file:${MAUM_ROOT}/etc/message-code.conf", encoding = "UTF-8")
public class ApplicationMessageCode {

  public static void main(String[] args) {
    SpringApplication.run(ApplicationMessageCode.class, args);
  }
}
