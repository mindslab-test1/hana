package message.common;

import com.google.protobuf.Empty;
import io.grpc.ManagedChannel;
import io.grpc.StatusRuntimeException;
import maum.brain.qa.nqa.ApiService.AbortIndexingRequest;
import maum.brain.qa.nqa.ApiService.IndexingQaSet;
import maum.brain.qa.nqa.ApiService.IndexingResponse;
import maum.brain.qa.nqa.NQaApiServiceGrpc;
import maum.brain.qa.nqa.NQaApiServiceGrpc.NQaApiServiceBlockingStub;
import maum.brain.qa.nqa.NQaServiceGrpc;
import maum.brain.qa.nqa.NQaServiceGrpc.NQaServiceBlockingStub;
import maum.brain.qa.nqa.Nqa.CheckAnswerResponse;
import maum.brain.qa.nqa.Nqa.CheckQuestionResponse;
import maum.brain.qa.nqa.Nqa.SearchAnswerRequest;
import maum.brain.qa.nqa.Nqa.SearchQuestionRequest;
import org.slf4j.LoggerFactory;

public class NQAGrpcInterfaceManager {

  static final org.slf4j.Logger logger = LoggerFactory.getLogger(NQAGrpcInterfaceManager.class);

  private final String host;
  private final int port;

  public NQAGrpcInterfaceManager(String host, int port) {
    this.host = host;
    this.port = port;
  }

  public IndexingResponse abortIndexing(AbortIndexingRequest request) {
    logger.debug("===== NQAGrpcInterfaceManager abortIndexing :: AbortIndexingRequest {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NQaApiServiceBlockingStub stub = NQaApiServiceGrpc.newBlockingStub(channel);
      IndexingResponse result = stub.abortIndexing(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== NQAGrpcInterfaceManager abortIndexing :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("fullIndexing e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public IndexingResponse getIndexingStatus(Empty request) {
    logger.debug("===== NQAGrpcInterfaceManager getIndexingStatus");
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NQaApiServiceBlockingStub stub = NQaApiServiceGrpc.newBlockingStub(channel);
      IndexingResponse result = stub.getIndexingStatus(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== NQAGrpcInterfaceManager getIndexingStatus :: RPC failed: {}",
          e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("getIndexingStatus e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public IndexingResponse indexing(IndexingQaSet request) {
    logger.debug("===== NQAGrpcInterfaceManager indexing :: IndexingQaSet {}", request);
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NQaApiServiceBlockingStub stub = NQaApiServiceGrpc.newBlockingStub(channel);
      IndexingResponse result = stub.indexing(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== NQAGrpcInterfaceManager indexing :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("indexing e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public CheckQuestionResponse checkQuestion(SearchQuestionRequest request) {
    logger.debug("===== NQAGrpcInterfaceManager checkQuestion");
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NQaServiceBlockingStub stub = NQaServiceGrpc.newBlockingStub(channel);
      CheckQuestionResponse result = stub.checkQuestion(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== NQAGrpcInterfaceManager checkQuestion :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("checkQuestion e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

  public CheckAnswerResponse checkAnswer(SearchAnswerRequest request) {
    logger.debug("===== NQAGrpcInterfaceManager checkAnswer");
    final ManagedChannel channel = GrpcUtils.getChannel(this.host, this.port);
    try {
      NQaServiceBlockingStub stub = NQaServiceGrpc.newBlockingStub(channel);
      CheckAnswerResponse result = stub.checkAnswer(request);
      if (channel != null) {
        GrpcUtils.closeChannel(channel);
      }
      return result;
    } catch (StatusRuntimeException e) {
      logger.error("===== NQAGrpcInterfaceManager checkAnswer :: RPC failed: {}", e.getStatus());
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e1) {
        // TODO Auto-generated catch block
        logger.error("checkAnswer e1 : ", e1);
      } finally {
        if (channel != null) {
          GrpcUtils.closeChannel(channel);
        }
      }
      return null;
    }
  }

}
