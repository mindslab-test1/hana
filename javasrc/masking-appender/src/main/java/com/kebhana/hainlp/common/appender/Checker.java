package com.kebhana.hainlp.common.appender;

import java.io.IOException;

public class Checker {

  public static void main(String[] args) throws IOException {

    final String[] numbers = {
        "acct_no   12 adfwe",
        "acct_no   123414-1359773-513-123 가나다",
        "sfsdfsfsdf acct_no : 123414-1359773-513-123",
        "ACCT_NO - 123414-1359773-513-123",
        "AcCt_nO, 123414-1359773-513-123",
        "AcCt_nO ,123414-1359773-513-123",
        "AcCt_nO,123414-1359773-513-123",
        "AcCt_nO, 123414-1359773-513-123"
    };

    for (String number : numbers) {
      System.out.printf("%s ==> %s\n", number, MaskingUtils.accountNumber(number));
    }

  }
}
