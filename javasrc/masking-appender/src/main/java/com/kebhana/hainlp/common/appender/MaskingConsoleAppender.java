package com.kebhana.hainlp.common.appender;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.ConsoleAppender;

public class MaskingConsoleAppender extends ConsoleAppender<ILoggingEvent> {

  public boolean printMasking;

  public void setPrintMasking(boolean printMasking) {
    this.printMasking = printMasking;
  }

  public boolean getPrintMasking() {
    return printMasking;
  }

  @Override
  protected void append(ILoggingEvent event) {
    if (event != null && event.getArgumentArray() != null && event.getArgumentArray().length > 0) {
      if (getPrintMasking()) {
        // MaskingUtils.masking
        for (int index = 0; index < event.getArgumentArray().length; ++index) {
          if (event.getArgumentArray()[index] instanceof String) {
            event.getArgumentArray()[index] =
                MaskingUtils.masking(MaskingUtils.ALL,
                    (String) event.getArgumentArray()[index]);
          } else if (event.getArgumentArray()[index] != null) {
            event.getArgumentArray()[index] =
                MaskingUtils.masking(MaskingUtils.ALL,
                    event.getArgumentArray()[index].toString());
          }
        }
      }
    }
    super.append(event);
  }
}
