package ai.maum.m2u;

import ai.maum.m2u.service.NQADialogAgentImpl;
import ai.maum.m2u.utils.ServerMetaInterceptor;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.ServerInterceptors;
import io.grpc.netty.NettyServerBuilder;
import io.grpc.protobuf.services.ProtoReflectionService;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BasicQAv2DialogAgent {

  private static final Logger logger = LoggerFactory.getLogger(BasicQAv2DialogAgent.class);

  private static final String PORT_REGEX = "^(6553[0-5]|655[0-2]\\d|65[0-4]\\d\\d|6[0-4]\\d{3}|[1-5]\\d{4}|[2-9]\\d{3}|1[1-9]\\d{2}|10[3-9]\\d|102[4-9])$";

  private Server server;

  private void start(int servicePort) throws IOException {
    logger.info("da-version3 Server starting");

    ServerBuilder sb = NettyServerBuilder.forPort(servicePort)
        .maxConnectionIdle(10000, TimeUnit.MILLISECONDS)
        .maxConnectionAge(10000, TimeUnit.MILLISECONDS);

    sb.addService(ServerInterceptors
        .intercept(new NQADialogAgentImpl(), new ServerMetaInterceptor()));
    // grpc cli instance
    sb.addService(ProtoReflectionService.newInstance());

    server = sb.build().start();

    logger.info("DialogAgentV3 Server started, listening on = {}", servicePort);
    Runtime.getRuntime().addShutdownHook(new Thread(() -> {
      // Use stderr here since the logger may have been reset by its
      // JVM shutdown hook.
      logger.info("*** shutting down gRPC server since JVM is shutting down");
      BasicQAv2DialogAgent.this.stop();
      logger.info("*** server shut down");
    }));
  }
  private void stop() {
    if (server != null) {
      server.shutdown();
    }
  }

  /**
   * Await termination on the main thread since the grpc library uses daemon threads.
   */
  private void blockUntilShutdown() throws InterruptedException {
    if (server != null) {
      server.awaitTermination();
    }
  }

  /**
   * Main launches the server from the command line.
   */
  public static void main(String[] args) throws IOException, InterruptedException {
    for (int i = 0; i < args.length; i++) {
      if (args[i].equals("-p")) {
        String port = args[++i];
        if (Pattern.matches(PORT_REGEX, port)) {
          final BasicQAv2DialogAgent server = new BasicQAv2DialogAgent();
          server.start(Integer.parseInt(port));
          server.blockUntilShutdown();
        } else {
          logger.error("=======port error====== : " + port);
        }
        return;
      }
    }

    logger.error("Please Use ./ask -p [port]  You used option : " + args[0]);
  }
}
