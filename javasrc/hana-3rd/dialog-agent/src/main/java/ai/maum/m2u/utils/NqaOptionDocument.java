package ai.maum.m2u.utils;

import ai.maum.m2u.config.PropertyManager;
import maum.brain.qa.nqa.Nqa.LayerQueryUnit;
import maum.brain.qa.nqa.Nqa.QueryType;
import maum.brain.qa.nqa.Nqa.QueryUnit;
import maum.brain.qa.nqa.Nqa.AttributeQueryUnit;
import java.util.ArrayList;
import java.util.List;
//import lombok.Data;
//
//@Data

public class NqaOptionDocument {
    private int ntop = PropertyManager.getInt("nqa.eca.ntop");
    private float score = PropertyManager.getFloat("nqa.eca.score");
    private float maxSize = PropertyManager.getFloat("nqa.eca.maxsize");
    private String category = "ECA";
    private QueryType queryType = QueryType.ALL;
//    private QueryTarget queryTarget = QueryTarget.Q;
    private QueryUnit.Builder userQuery = QueryUnit.newBuilder().setWeight(PropertyManager.getFloat("nqa.eca.tagweight"));
    private List<String> ids = new ArrayList<>();
    private AttributeQueryUnit.Builder attribute =  AttributeQueryUnit.newBuilder();
    private LayerQueryUnit.Builder layer = LayerQueryUnit.newBuilder();
    private List<QueryUnit> ner = new ArrayList<>();
    private List<QueryUnit> tags = new ArrayList<>();

    public QueryUnit.Builder getUserQuery() {
        return userQuery;
    }

    public void setUserQuery(QueryUnit.Builder userQuery) {
        this.userQuery = userQuery;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public QueryType getQueryType() { return queryType; }

    public void setQueryType(QueryType queryType) {
        this.queryType = queryType;
    }

//    public QueryTarget getQueryTarget() {
//        return queryTarget;
//    }
//
//    public void setQueryTarget(QueryTarget queryTarget) {
//        this.queryTarget = queryTarget;
//    }

    public List<String> getIds() { return ids; }

    public void setIds(List<String> ids) { this.ids = ids; }

    public int getNtop() {
        return ntop;
    }

    public void setNtop(int ntop) {
        this.ntop = ntop;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public float getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(float maxSize) {
        this.maxSize = maxSize;
    }

    public AttributeQueryUnit.Builder getAttribute() {
        return attribute;
    }

    public void setAttribute(AttributeQueryUnit.Builder attribute) {
        this.attribute = attribute;
    }

    public LayerQueryUnit.Builder getLayer() {
        return layer;
    }

    public void setLayer(LayerQueryUnit.Builder layer) {
        this.layer = layer;
    }

    public List<QueryUnit> getNer() {
        return ner;
    }

    public void setNer(List<QueryUnit> ner) {
        this.ner = ner;
    }

    public List<QueryUnit> getTags() {
        return tags;
    }

    public void setTags(List<QueryUnit> tags) {
        this.tags = tags;
    }

}
