package ai.maum.m2u.service;
import ai.maum.m2u.config.PropertyManager;
import ai.maum.m2u.utils.LayerNullInfo;
import ai.maum.m2u.utils.NqaOptionDocument;
import ai.maum.m2u.utils.ServerMetaInterceptor;
import ai.maum.rpc.ResultStatusListProto;
import com.google.protobuf.*;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import java.util.*;
import java.util.concurrent.TimeUnit;
import maum.brain.qa.nqa.NQaServiceGrpc;
import maum.brain.qa.nqa.NQaServiceGrpc.NQaServiceBlockingStub;
import maum.brain.qa.nqa.Nqa.QueryTarget;
import maum.brain.qa.nqa.Nqa.SearchAnswerResponse;
import maum.brain.qa.nqa.Nqa.SearchAnswerRequest;
import maum.brain.qa.nqa.Nqa.QueryUnit;
import maum.brain.qa.nqa.Nqa.SearchAnswerResponse.Answer;
import maum.brain.qa.nqa.Nqa.SearchQuestionRequest;
import maum.brain.qa.nqa.Nqa.SearchQuestionResponse.Question;
import maum.brain.qa.nqa.Nqa.SearchQuestionResponse;
import maum.common.LangOuterClass.Lang;
import maum.m2u.common.CardOuterClass.Card;
import maum.m2u.common.CardOuterClass.SelectCard;
import maum.m2u.common.CardOuterClass.SelectCard.Item;
import maum.m2u.common.Dialog.Session;
import maum.m2u.common.Dialog.Speech;
import maum.m2u.common.DirectiveOuterClass.AvatarSetExpressionPayload;
import maum.m2u.common.DirectiveOuterClass.Directive;
import maum.m2u.common.DirectiveOuterClass.Directive.DirectivePayload;
import maum.m2u.common.EventOuterClass.DialogAgentForwarderParam;
import maum.m2u.common.Userattr.DataType;
import maum.m2u.da.Provider;
import maum.m2u.da.Provider.AgentKind;
import maum.m2u.da.Provider.DialogAgentProviderParam;
import maum.m2u.da.Provider.DialogAgentState;
import maum.m2u.da.Provider.DialogAgentStatus;
import maum.m2u.da.Provider.RuntimeParameterList;
import maum.m2u.da.v3.DialogAgentProviderGrpc.DialogAgentProviderImplBase;
import maum.m2u.da.v3.Talk;
import maum.m2u.da.v3.Talk.CloseSkillResponse;
import maum.m2u.da.v3.Talk.OpenSkillResponse;
import maum.m2u.da.v3.Talk.SpeechResponse;
import maum.m2u.da.v3.Talk.TalkResponse;
import maum.rpc.ErrorDetails;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NQADialogAgentImpl extends DialogAgentProviderImplBase {

  static final private Logger logger = LoggerFactory.getLogger(NQADialogAgentImpl.class);

  private DialogAgentProviderParam.Builder provider = DialogAgentProviderParam.newBuilder();
  private Provider.InitParameter initParam;
  private DialogAgentState state;
  private String skill;
  private int count = 0;
  private LayerNullInfo layerNullInfo = new LayerNullInfo();

  public NQADialogAgentImpl() {
    logger.debug("DA V3 Server Constructor");
    state = DialogAgentState.DIAG_STATE_IDLE;
    logger.debug("DialogAgentState: {}", state);
    // Initialize Static variable
    ai.maum.rpc.ResultStatus.setModule(maum.rpc.Status.Module.M2U);
    ai.maum.rpc.ResultStatus.setProcess(6);
  }

  /**
   * Web console을 통해 DA를 Dialog Agent Instance 의 형태로 실행할 때, 관련 설정값을 이용하여 Instnace를 초기화하도록 실행되도록 하는
   * 함수 입력된 사항에 대해 Talk() 함수에서 사용할 수 있도록 InitParameter의 정보를 변수에 저장하는 과정 수행 Init() 함수에서
   * DIAG_STATE_INITIALIZING로 상태를 변경해주고 (초기화중) Init() 이 성공적으로 되면 DIAG_STATE_RUNNING 상태로 변경해줘야 합니다.
   * (동작중)
   */
  @Override
  public void init(Provider.InitParameter request,
      StreamObserver<DialogAgentProviderParam> responseObserver) {
    logger.info("DA V3 Server Init");
    logger.trace("Init request: {}", request);

    try {
      state = DialogAgentState.DIAG_STATE_INITIALIZING; // DA instance state 초기화

      provider.setName("name") // provider 정보 setting
          .setDescription("control intention return DA")
          .setVersion("3.0")
          .setSingleTurn(true)
          .setAgentKind(AgentKind.AGENT_SDS)
          .setRequireUserPrivacy(true);

      initParam = request;
      state = DialogAgentState.DIAG_STATE_RUNNING;

      //throw new Exception("exception test");

      logger.trace("Init response {}", provider);
      responseObserver.onNext(provider.build()); // provider를 response로 return
      responseObserver.onCompleted();

    } catch (Exception e) { // 예외 처리
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10001, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("init")
            .setLine(108)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        e1.printStackTrace();
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 현재 Dialog Agent Instance 의 상태를 확인할 수 있는 함수 Dialog Agent Instance 동작에 따라 상태 변경 필요 생성자에서
   * DIAG_STATE_IDLE 상태로 초기화 해줘야 합니다. (준비 상태)
   */
  @Override
  public void isReady(Empty request, StreamObserver<Provider.DialogAgentStatus> responseObserver) {
    logger.info("DA V3 Server IsReady");

    try {
      DialogAgentStatus.Builder response = DialogAgentStatus.newBuilder();

      response.setState(state); // DA instance 상태 저장

      logger.debug("IsReady response: {}", response);
      responseObserver.onNext(response.build()); // response return
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10002, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("isReady")
            .setLine(148)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        e1.printStackTrace();
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 동작중인 DA Instance을 종료시키는 함수 Terminate() 에서는 상태를 DIAG_STATE_TERMINATED로 변경해줘야 합니다.
   */
  @Override
  public void terminate(Empty request, StreamObserver<Empty> responseObserver) {
    logger.info("DA V3 Server Terminate");

    try {
      state = DialogAgentState.DIAG_STATE_TERMINATED; // DA instance state 변경
      responseObserver.onNext(Empty.newBuilder().build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10003, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("terminate")
            .setLine(183)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        e1.printStackTrace();
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 실제로 DA Instnace가 돌아가기 위해 필요한 paramerters를 정의 db 정보 정의
   */
  @Override
  public void getRuntimeParameters(Empty request,
      StreamObserver<RuntimeParameterList> responseObserver) {
    logger.info("DA V3 Server GetRuntimeParameters");

    try {
      RuntimeParameterList.Builder result = RuntimeParameterList.newBuilder();

      Provider.RuntimeParameter.Builder db_host = Provider.RuntimeParameter.newBuilder(); // db ip
      db_host.setName("db_host")
          .setType(DataType.DATA_TYPE_STRING)
          .setDesc("Database Host")
          .setDefaultValue("171.64.122.134")
          .setRequired(true);
      result.addParams(db_host);

      Provider.RuntimeParameter.Builder db_port = Provider.RuntimeParameter.newBuilder(); // db port
      db_port.setName("db_port")
          .setType(DataType.DATA_TYPE_INT)
          .setDesc("Database Port")
          .setDefaultValue("7701")
          .setRequired(true);
      result.addParams(db_port);

      Provider.RuntimeParameter.Builder db_user = Provider.RuntimeParameter.newBuilder(); // db user
      db_user.setName("db_user")
          .setType(DataType.DATA_TYPE_STRING)
          .setDesc("Database User")
          .setDefaultValue("minds")
          .setRequired(true);
      result.addParams(db_user);

      Provider.RuntimeParameter.Builder db_pwd = Provider.RuntimeParameter.newBuilder(); // db pwd
      db_pwd.setName("db_pwd")
          .setType(DataType.DATA_TYPE_AUTH)
          .setDesc("Database Password")
          .setDefaultValue("minds67~")
          .setRequired(true);
      result.addParams(db_pwd);

      Provider.RuntimeParameter.Builder db_database = Provider.RuntimeParameter
          .newBuilder(); // db name
      db_database.setName("db_database")
          .setType(DataType.DATA_TYPE_STRING)
          .setDesc("Database Database name")
          .setDefaultValue("ascar")
          .setRequired(true);
      result.addParams(db_database);

      logger.trace("GetRuntimeParameters response: {}", result);
      responseObserver.onNext(result.build()); // response return
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10004, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("getRuntimeParameter")
            .setLine(262)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        e1.printStackTrace();
        responseObserver.onError(e);
      }
    }
  }

  /**
   * DA가 동작하는 데 필요한 여러가지 옵션 값을 처리하는 DA Provider Parameter를 반환
   */
  @Override
  public void getProviderParameter(Empty request,
      StreamObserver<DialogAgentProviderParam> responseObserver) {
    logger.info("DA V3 Server GetProviderParameter");

    try {
      // init, isReady 등 여러 함수에서 정의된 provider 값 return
      logger.trace("GetProviderParameter response {}", provider);
      responseObserver.onNext(provider.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      ai.maum.rpc.ResultStatus status = null;
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10005, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("getProviderParameter")
            .setLine(299)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        e1.printStackTrace();
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 세션이 새로이 시작했을 때에 메시지를 전달 받고, 응답을 줄 수 있습니다. 이 호출은 세션 전체를 시작할 때 들어오는 요청이고 모든 DA가 이 요청을 받지는 않습니다.
   */
  @Override
  public void openSession(Talk.OpenSessionRequest request,
      StreamObserver<Talk.TalkResponse> responseObserver) {
    logger.info("DA V3 Server OpenSession");

    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("OpenSession request: {}", request);

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10101,
              "chatbot or skill or session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("openSession")
              .setLine(339)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          e.printStackTrace();
          responseObserver.onError(e);
        }
      }

      Talk.TalkResponse.Builder response = Talk.TalkResponse.newBuilder();
      response.setResponse(
          SpeechResponse.newBuilder()
              .setSpeech(
                  Speech.newBuilder().setLang(request.getUtter().getLang())
                      .setUtter("질문을 입력해주세요~!").build()) // open message
              .setSessionUpdate(
                  Session.newBuilder()
                      .setId(request.getSession().getId()).build())
              .setMeta( // meta 정보 저장
                  Struct.newBuilder().putFields("context_test_1", // key
                      // value, setIntValue, setStringValue 등으로 type 설정
                      Value.newBuilder().setStringValue("ctx_1")
                          .build()).build()).build());
      logger.trace("OpenSession response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10006, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("openSession")
            .setLine(378)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        e1.printStackTrace();
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 해당하는 스킬을 최초로 호출 할 때 호출된다. 싱글턴 일 경우에는 호출되지 않는다. 대화를 시작하기 전에 초기화해야 하는 작업이 있을 경우에 호출한다.
   */
  @Override
  public void openSkill(Talk.OpenSkillRequest request,
      StreamObserver<Talk.OpenSkillResponse> responseObserver) {
    logger.info("DA V3 Server OpenSkill");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("OpenSkill request: {}", request);

    OpenSkillResponse.Builder response = OpenSkillResponse.newBuilder();

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10102,
              "chatbot or skill or session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("openSkill")
              .setLine(419)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          e.printStackTrace();
          responseObserver.onError(e);
        }
      }
      skill = request.getSkill();
      response.setMeta( // meta 정보 저장
          Struct.newBuilder().putFields("meta_info",
              Value.newBuilder().setStringValue("This is openSkill")
                  .build()).build())
          .setSessionUpdate( // request의 session으로 업데이트
              Session.newBuilder().setId(
                  request.getSession().getId())
                  .build()).build();

      logger.trace("OpenSkill response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10007, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("openSkill")
            .setLine(453)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        e1.printStackTrace();
        responseObserver.onError(e);
      }
    }
  }

  /**
   * BQA와 실질적으로 대화하는 함수.
   * TalkRequest의 session에서 이전 계층 정보를 받아온다.
   * 이후 생성된 계층 정보는 TalkResponse.SpeechResponse의 session_update에 넣어준다.
   * 한번 답변이 완료된 경우 새로운 새션으로 대화를 시작해야함.
   **/
  @Override
  public void talk(Talk.TalkRequest request,
      StreamObserver<Talk.TalkResponse> responseObserver) {
    logger.info("DA V3 Server Talk");
//    logger.trace("Talk request: {}", request);
//    logger.trace("Talk User Meta info(Replacement) : {}", request.getUtter().getMeta());

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10103,
              "chatbot or skill or session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("Talk")
              .setLine(500)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          e.printStackTrace();
          responseObserver.onError(e);
        }
        return;
      }

      if (request.getUtter().getUtter().equals("error")) {
        try {
          status = new ai.maum.rpc.ResultStatus(maum.rpc.Status.ExCode.LOGICAL_ERROR,
              10106,
              "Error for Result Status.");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile =
              ErrorDetails.DebugInfoFile.newBuilder()
                  .setFunction("Talk")
                  .setLine(523)
                  .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          e.printStackTrace();
          responseObserver.onError(e);
        }
        return;
      }

      if (request.getUtter().getUtter().equals("deadline")) {
        try {
          Thread.sleep(35000);
          responseObserver.onCompleted();
        } catch (Exception e) {
          e.printStackTrace();
          responseObserver.onError(e);
        }
        return;
      }

      Talk.TalkResponse.Builder response = TalkResponse.newBuilder();

      Speech.Builder speech = Speech.newBuilder();
      Card.Builder card = Card.newBuilder();
      Directive.Builder directive = Directive.newBuilder();
      Struct.Builder context = Struct.newBuilder();

      String question = request.getUtter().getUtter();  // 질문 or 계층정보

      List<String> resultQueryList = new ArrayList<>();
      Struct.Builder structBuilder = Struct.newBuilder();
      boolean closeFlag = false;

      logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
      logger.info(request.getSession().getContext().toString());  // context에 담긴 정보 출력(이전 데이터)
      logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

      SearchAnswerResponse searchAnswerResponse = bqaConnect(request.getSession().getContext(), question, context, resultQueryList, structBuilder);  // BQA 검색
      count=0;
      String finalAnswer = "";
      if (searchAnswerResponse.getAnswerResultCount() > 0) {
        List<String> layerList = new ArrayList<>();
        HashSet<String> finalSet = checkLayer(searchAnswerResponse, context, layerList);
        if (finalSet.size()>0) {
          logger.debug("답변 여러개 카드 송출");
          card.setSelect(insertLayerCard(finalSet, context, layerList, structBuilder));
          finalAnswer = getMatchNoMatchString(context);
        } else {
          logger.debug("답변 한개 답변!");
          finalAnswer = searchAnswerResponse.getAnswerResult(0).getAnswer();
          context.putFields("layerNum", Value.newBuilder().setStringValue("Finish").build());
          context.putFields("Qid_Aid_Map", Value.newBuilder().setStringValue("Finish").build());
        }
      } else {
        logger.debug("답변 없음!!");
        finalAnswer = "답변을 찾을 수 없습니다.";
        context.putFields("layerNum", Value.newBuilder().setStringValue("Finish").build());
        context.putFields("Qid_Aid_Map", Value.newBuilder().setStringValue("Finish").build());
      }

      speech.setLang(Lang.ko_KR)
          .setUtter(finalAnswer)
          .setSpeechUtter("TTS 출력: " + request.getUtter().getUtter())
          .setReprompt(request.getUtter().getUtter().equals("expected response"));

      directive.setInterface("Avatar") // directive 저장
          .setOperation("SetExpression")
          .setParam(DialogAgentForwarderParam.newBuilder()
              .setChatbot("NQAChatbot")
              .setSkill("bqa")
              .setIntent("intent")
              .setSessionId(1234).build())
          .setPayload(DirectivePayload.newBuilder()
              .setAvatarSetExpression(AvatarSetExpressionPayload.newBuilder()
                  .setExpression("smile")
                  .setDesciption("avata is happy")
//                  .setDesciption(fullQuery + fullValue)
                  .build()).build()).build();


      LinkedHashMap<String, Value> stepMap = new LinkedHashMap<>();
      for (int i=0; i<resultQueryList.size();i++) {
//        stepMap.put("step_" + String.valueOf(i),Value.newBuilder().setStringValue(resultQueryList.get(i)).build());
        stepMap.put(String.valueOf(i) + "_step",Value.newBuilder().setStringValue(resultQueryList.get(i)).build());
      }
      structBuilder.putFields("all_step", Value.newBuilder().setStructValue(Struct.newBuilder().putAllFields(stepMap).build()).build());

      response.setResponse(SpeechResponse.newBuilder()
          .setSpeech(speech)
          .addCards(card)
          .addDirectives(directive)
          .setCloseSkill(closeFlag)
          .setMeta(structBuilder.build())
//          .setMeta(Struct.newBuilder().putAllFields(context.getFieldsMap().get("allResult").getStructValue().getFieldsMap()))
          .setSessionUpdate(Session.newBuilder().setContext(context.build())))
          .build();

//      logger.info("Talk response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10008, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("talk")
            .setLine(714)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        e1.printStackTrace();
        responseObserver.onError(e);
      }
    }
  }

  private void setNqaDocument(NqaOptionDocument nqaOptionDocument,String key, String value) {
    switch (key) {
      case "A1":
        nqaOptionDocument.getAttribute().setAttr1(QueryUnit.newBuilder().setPhrase(value).build());
        break;
      case "A2":
        nqaOptionDocument.getAttribute().setAttr2(QueryUnit.newBuilder().setPhrase(value).build());
        break;
      case "A3":
        nqaOptionDocument.getAttribute().setAttr3(QueryUnit.newBuilder().setPhrase(value).build());
        break;
      case "A4":
        nqaOptionDocument.getAttribute().setAttr4(QueryUnit.newBuilder().setPhrase(value).build());
        break;
      case "A5":
        nqaOptionDocument.getAttribute().setAttr5(QueryUnit.newBuilder().setPhrase(value).build());
        break;
      case "A6":
        nqaOptionDocument.getAttribute().setAttr6(QueryUnit.newBuilder().setPhrase(value).build());
        break;
      case "L1":
        nqaOptionDocument.getLayer().setLayer1(QueryUnit.newBuilder().setPhrase(value).build());
        break;
      case "L2":
        nqaOptionDocument.getLayer().setLayer2(QueryUnit.newBuilder().setPhrase(value).build());
        break;
      case "L3":
        nqaOptionDocument.getLayer().setLayer3(QueryUnit.newBuilder().setPhrase(value).build());
        break;
      case "L4":
        nqaOptionDocument.getLayer().setLayer4(QueryUnit.newBuilder().setPhrase(value).build());
        break;
      case "L5":
        nqaOptionDocument.getLayer().setLayer5(QueryUnit.newBuilder().setPhrase(value).build());
        break;
      case "L6":
        nqaOptionDocument.getLayer().setLayer6(QueryUnit.newBuilder().setPhrase(value).build());
        break;
//      case "TAG":
//        nqaOptionDocument.getTags().add(QueryUnit.newBuilder().setPhrase(value).build());
//        break;
      case "Q":
      case "A":
      case "QA":
      case "6":
      case "7":
      case "SA":
      case "SQ":
      case "LA":
      case "AA":
      case "NQ":
      case "NA":
        nqaOptionDocument.getUserQuery().setPhrase(value);
        List<String> attr1List = new ArrayList<>();
        String[] attr1Array = {"인터넷 뱅킹", "스마트폰 뱅킹" ,"폰뱅킹","용어 정의"};
        for (String attr1 : attr1Array) {
          if (value.contains(attr1)) {
            attr1List.add(attr1);
          }
        }
        nqaOptionDocument.getAttribute().setAttr1(QueryUnit.newBuilder().setPhrase(String.join(" ",attr1List)).build());
        break;
      case "AID":
        nqaOptionDocument.getIds().addAll(Arrays.asList(value.split(",")));
        break;
      case "SCORE":
        nqaOptionDocument.setScore(Float.valueOf(value));
        break;
      case "WEIGHT":
        nqaOptionDocument.getUserQuery().setWeight(Float.valueOf(value));
        break;
      case "SIZE":
        nqaOptionDocument.setMaxSize(Float.valueOf(value));
        break;
    }
  }

  private SearchAnswerResponse searchAnswerTask(String userQuery, boolean ismatch, QueryTarget queryTarget,
      NQaServiceBlockingStub stub, Struct.Builder contextOut,
      List<String> resultQueryList, Struct.Builder structBuilder) {
    SearchAnswerRequest.Builder searchAnswerRequestBuilder = SearchAnswerRequest.newBuilder().setQueryTarget(queryTarget);
    logger.debug("Answer QueryTarget BeforeFill => " + queryTarget.name());
    fillASlot(searchAnswerRequestBuilder, userQuery);

    SearchAnswerResponse searchAnswerResponse = stub.searchAnswer(searchAnswerRequestBuilder.build());

    Map<String, String> resultPorcessLog = searchAnswerResponse.getResultProcessMap();
    for (Map.Entry<String, String> entry : resultPorcessLog.entrySet()) {
      resultQueryList.add(entry.getKey() + "=>" + entry.getValue());
    }
    String queryTargetName = "";

    if (!ismatch && searchAnswerResponse.getAnswerResultCount()>0 && queryTarget!=QueryTarget.AID) {  // 처음 답이 있을 경우 + AID검색이 아닐경우
      contextOut.putFields("Synonym_result", Value.newBuilder().setStringValue(resultPorcessLog.get("Synonym_result")).build());  // 카드 출력용 검색단어
      contextOut.putFields("All_Result", Value.newBuilder().setStructValue(searchAnswerResponse.getAllResult()).build());  // 검색결과 저장(매칭단어모음:id)
      queryTargetName = "Answer_details_";

      List<String> AidList = new ArrayList<>();

      for (Answer answer : searchAnswerResponse.getAnswerResultList()) {
        AidList.add(answer.getId());
      }
      contextOut.putFields("Answer_ids", Value.newBuilder().setStringValue(String.join(",",AidList)).build());
    }

    if (!(queryTarget == QueryTarget.Q) && !(queryTarget == QueryTarget.GRAMQ) && !ismatch && searchAnswerResponse.getAnswerResultCount()>0) { // Q검색이 아니면서 처음 답이 있을경우
      put3TopResult(contextOut, structBuilder, searchAnswerResponse.getAnswerResultList());
    }

    putAnswerResultView(structBuilder, searchAnswerResponse, queryTargetName + queryTarget.name(), contextOut);
    return searchAnswerResponse;
  }

  private void put3TopResult(Struct.Builder contextOut, Struct.Builder structBuilder, List<Answer> answerList) {
    HashMap<String, Answer> currentAnswerMap = new HashMap<>();
    for (Answer answer : answerList) {
      currentAnswerMap.put(answer.getId(), answer);
    }

    List<String> originAnsweridList = Arrays.asList(contextOut.getFieldsMap().get("Answer_ids").getStringValue().split(","));
    Map<String, Value> qIdInfoMap = new HashMap<>();
    if (contextOut.containsFields("Q_Info_Map")) {
      qIdInfoMap = contextOut.getFieldsMap().get("Q_Info_Map").getStructValue().getFieldsMap();
    }
    int i = 0;
    String query = "<br>";
    for (String aId : originAnsweridList) {
      if (currentAnswerMap.containsKey(aId)) {
        List<String> queryList = new ArrayList<>();
        Answer answer = currentAnswerMap.get(aId);
        if (qIdInfoMap.size()>0) {
          ListValue qValueList =  qIdInfoMap.get(aId).getListValue();
          queryList.add("Qscore:" + qValueList.getValues(3).getStringValue());
          queryList.add("Aid:" + aId);
          queryList.add("Qid:" + qValueList.getValues(0).getStringValue());
          queryList.add("Qstring:" + qValueList.getValues(1).getStringValue());
          queryList.add("Qmorph:" + qValueList.getValues(2).getStringValue());
        } else {
          queryList.add("Ascore:" + String.valueOf(answer.getScore()));
          queryList.add("Aid:" + aId);
        }
        queryList.add("Astring:" + answer.getAnswer());
        queryList.add("Aattr1:" + answer.getAttributes().getAttr1());
        queryList.add("Aattr2:" + answer.getAttributes().getAttr2());
        queryList.add("Aattr3:" + answer.getAttributes().getAttr3());
        queryList.add("Aattr4:" + answer.getAttributes().getAttr4());
        queryList.add("Aattr5:" + answer.getAttributes().getAttr5());
        queryList.add("Aattr6:" + answer.getAttributes().getAttr6());
        queryList.add("Alayer1:" + answer.getLayers().getLayer1());
        queryList.add("Alayer2:" + answer.getLayers().getLayer2());
        queryList.add("Alayer3:" + answer.getLayers().getLayer3());
        queryList.add("Alayer4:" + answer.getLayers().getLayer4());
        queryList.add("Alayer5:" + answer.getLayers().getLayer5());
        queryList.add("Alayer6:" + answer.getLayers().getLayer6());

        query += " ";
        query += String.join("  ", queryList);
        query += "<br>";

        i++;
        if (i==3) {  // 상위3개까지만
          break;
        }
      }
    }
    structBuilder.putFields("TOP3_Value", Value.newBuilder().setStringValue(query).build());
  }

  // Question 검색 처리
  private void searchQuestionTask(String userQuery, boolean ismatch, QueryTarget queryTarget, List<String> idList,
      NQaServiceBlockingStub stub, Struct.Builder contextOut,
      List<String> resultQueryList, Struct.Builder structBuilder) {
    SearchQuestionRequest.Builder searchQuestionRequestBuilder = SearchQuestionRequest.newBuilder().setQueryTarget(queryTarget);
    logger.debug("Question QueryTarget BeforeFill => " + queryTarget.name());
    fillQSlot(searchQuestionRequestBuilder, userQuery);

    SearchQuestionResponse searchQuestionResponse = stub.searchQuestion(searchQuestionRequestBuilder.build());

    Map<String, String> resultPorcessLog = searchQuestionResponse.getResultProcessMap();
    for (Map.Entry<String, String> entry : resultPorcessLog.entrySet()) {  // put All Log Step
      resultQueryList.add(entry.getKey() + "=>" + entry.getValue());
    }

    String queryTargetName = "";
    if (!ismatch && searchQuestionResponse.getIdsCount()>0) {  // 처음으로 답이 있을경우
      idList.addAll(searchQuestionResponse.getIdsList());
      contextOut.putFields("Synonym_result", Value.newBuilder().setStringValue(resultPorcessLog.get("Synonym_result")).build());  // 카드 출력용 검색단어
      contextOut.putFields("All_Result", Value.newBuilder().setStructValue(searchQuestionResponse.getAllResult()).build());  // 검색결과 저장(매칭단어모음:id)
      contextOut.putFields("Answer_ids", Value.newBuilder().setStringValue(String.join(",", searchQuestionResponse.getIdsList())).build()).build();  // Score기준 answerid 저장(Top3 출력용)
      Struct.Builder qidAidMap = Struct.newBuilder();
      Struct.Builder qInfoMap = Struct.newBuilder();
      for (Question question : searchQuestionResponse.getQuestionsList()) {
        qidAidMap.putFields(question.getId(), Value.newBuilder().setStringValue(question.getAnswerId()).build());
        qInfoMap.putFields(question.getAnswerId(), Value.newBuilder().setListValue(ListValue.newBuilder()
            .addValues(0,Value.newBuilder().setStringValue(question.getId()).build())  //q_id
            .addValues(1,Value.newBuilder().setStringValue(question.getQuestion()).build())  //Q_string
            .addValues(2,Value.newBuilder().setStringValue(question.getQuestionMorp()).build())  //Q_morph
            .addValues(3,Value.newBuilder().setStringValue(String.valueOf(question.getScore())).build())  //Q_score
            .build()).build());
      }
      contextOut.putFields("Qid_Aid_Map", Value.newBuilder().setStructValue(qidAidMap).build());  // qid, aid 매칭맵
      contextOut.putFields("Q_Info_Map", Value.newBuilder().setStructValue(qInfoMap).build());  // aid, qid 매칭맵(Top3출력을 위해 부가정보들 추가(string,morph,score))
      queryTargetName = "Question_details_";
    }
    putQuestionResultView(structBuilder, searchQuestionResponse,queryTargetName + queryTarget.name());
  }

  // QueryTarget 파싱 함수
  private QueryTarget parseQueryTarget(String queryTarget) {
    switch (queryTarget) {
      case "Q":
        return QueryTarget.Q;
      case "SQ":
        return QueryTarget.GRAMQ;
      case "NQ":
      case "NA":
        return QueryTarget.NER;
      case "A":
        return QueryTarget.A;
      case "SA":
        return QueryTarget.GRAMA;
      case "AA":
        return QueryTarget.ATTRALL;
      case "LA":
        return QueryTarget.LAYERALL;
      case "AID":
        return QueryTarget.AID;
//      case "TAG":
//        return QueryTarget.TAG;
    }
    return QueryTarget.Q;
  }

  // 모든 단일 QA 처리(Q일경우 A까지 결과 받아옴)
  // 검색 타입에 따른 검색(Q,A,LA,...)
  private SearchAnswerResponse searchNQA(String userQuery, boolean ismatch, String queryTarget, NQaServiceBlockingStub stub, Struct.Builder contextOut,
      List<String> resultQueryList, Struct.Builder structBuilder) {
    SearchAnswerResponse searchAnswerResponse = SearchAnswerResponse.newBuilder().build();
    List<String> idList = new ArrayList<>();
    switch (queryTarget) {
      case "Q":
      case "SQ":
      case "NQ":  // Question검색일 경우 Question검색 후 Answer까지 검색하여 결과 반환
        searchQuestionTask(userQuery, ismatch, parseQueryTarget(queryTarget), idList, stub, contextOut, resultQueryList, structBuilder);
        if (idList.size()>0) {
          userQuery = "@AID:" + String.join(",",idList);
          ismatch = true;
          searchAnswerResponse = searchAnswerTask(userQuery, ismatch, QueryTarget.AID, stub, contextOut, resultQueryList, structBuilder);
        }
        break;
//      case "A":
//      case "SA":
//      case "NA":
//      case "AA":
//      case "LA":
//      case "AID":
//      case "TAG":
      default:  // Answer검색일 경우 A만 검색하여 결과 반환
        searchAnswerResponse = searchAnswerTask(userQuery, ismatch, parseQueryTarget(queryTarget), stub, contextOut, resultQueryList, structBuilder);
        break;
    }
    return searchAnswerResponse;
  }

  // NQA검색 실행 (queryTarget을 List형태로 받아와 순차적으로 검색 실행)
  private SearchAnswerResponse searchNQATargets(List<String> queryTargetList, String userQuery, List<String> resultQueryList, Struct.Builder structBuilder, Struct.Builder contextOut) {
    ManagedChannel channel = ManagedChannelBuilder.forAddress("10.231.88.63", 50052).usePlaintext(true).build();
//    ManagedChannel channel = ManagedChannelBuilder.forAddress("10.122.64.212", 50052).usePlaintext(true).build();
    NQaServiceBlockingStub stub = NQaServiceGrpc.newBlockingStub(channel);
    SearchAnswerResponse searchAnswerResponse = SearchAnswerResponse.newBuilder().build();
    boolean isMatched = false;

    for(String queryTarget : queryTargetList) {  //순차적으로 검색
      SearchAnswerResponse searchAnswerResponseTemp = searchNQA(userQuery, isMatched, queryTarget, stub, contextOut, resultQueryList, structBuilder);
      if (!isMatched && searchAnswerResponseTemp.getAnswerResultCount()>0) {
        logger.info("searchAsnwerResponse Fisrt answer step--> " + queryTarget);
        searchAnswerResponse = searchAnswerResponseTemp;
        isMatched = true;
      }
    }
    try {
      channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return searchAnswerResponse;
  }

  // 검색 타입, 순서를 결정
  private void setQueryTargetList(List<String> queryTargetList, String userQuery) {
    String[] userQueryArray = userQuery.split(":");
    switch (userQueryArray[0].replace("@","").trim()) {
      case "Q":
      case "SQ":
      case "NQ":
      case "A":
      case "SA":
      case "NA":
      case "AA":
      case "LA":
      case "AID":
//      case "TAG":
        queryTargetList.add(userQueryArray[0].replace("@","").trim());
        break;
      case "6":
        queryTargetList.addAll(Arrays.asList("NQ","Q","NA","A","AA","LA"));
        break;
      case "7":
      case "":
      default:
//        queryTargetList.addAll(Arrays.asList("NQ","Q","NA","A","AA","LA","SQ","SA"));
//        queryTargetList.addAll(Arrays.asList("Q","A","LA","SQ","SA"));
        queryTargetList.addAll(Arrays.asList(PropertyManager.getStringArray("nqa.eca.query")));
        break;
    }
  }

  /**
   * NQA와 통신하는 함수
   * 이전 계층 정보를 포함한 context, 질문(계층), 이후계층을 저장할 context를 입력받음.
   * 답변 결과들을 리턴한다.
   **/
  private SearchAnswerResponse bqaConnect(Struct context, String userQuery, Struct.Builder contextOut,
      List<String> resultQueryList, Struct.Builder structBuilder) {
    logger.info("question ==> " + userQuery);
    SearchAnswerResponse searchAnswerResponse = SearchAnswerResponse.newBuilder().build();

    String nowLayernum = "";  // 현재 입력 받아온 계층 넘버
    List<String> idList = new ArrayList<>();  // 답변 검색할 ID 리스트

    List<String> queryTargetList = new ArrayList<>();

    if (!context.getFieldsMap().containsKey("layerNum") || "Finish".equalsIgnoreCase(context.getFieldsMap().get("layerNum").getStringValue())) {  // 첫 질문
      if (!userQuery.startsWith("@")) {  // 질문시작에 @없을때 Default로 7설정
        userQuery = "@7:" + userQuery;
      }
      setQueryTargetList(queryTargetList, userQuery);
      searchAnswerResponse = searchNQATargets(queryTargetList, userQuery, resultQueryList, structBuilder, contextOut);
    } else {  // 계층 선택인 경우
      if (Arrays.asList(layerNullInfo.getLayerNullArray()).contains(userQuery)) {  // 해당없음->Null변환
        userQuery = "Null";
      }

      // context 정보들 파싱
      nowLayernum = context.getFieldsMap().get("layerNum").getStringValue();
      if (context.containsFields("All_Result")) {
        contextOut.putFields("All_Result", context.getFieldsMap().get("All_Result"));
      }
      if (context.containsFields("Qid_Aid_Map")) {
        contextOut.putFields("Qid_Aid_Map", context.getFieldsMap().get("Qid_Aid_Map"));
      }
      if (context.containsFields("Q_Info_Map")) {
        contextOut.putFields("Q_Info_Map", context.getFieldsMap().get("Q_Info_Map"));
      }
      if (context.containsFields("Answer_ids")) {
        contextOut.putFields("Answer_ids", context.getFieldsMap().get("Answer_ids"));
      }

      logger.info("IdResult ==> " + idList.toString());

      if (Arrays.asList(context.getFieldsMap().get("idList").getStringValue().split(",")).size() > 0) { // Answer_id 결과가 있는경우
        userQuery = "@AID:" + context.getFieldsMap().get("idList").getStringValue() + " " + makeLayerString(context, nowLayernum, userQuery); // 검색 String으로 합성
        setQueryTargetList(queryTargetList, userQuery);
        searchAnswerResponse = searchNQATargets(queryTargetList, userQuery, resultQueryList, structBuilder, contextOut);
        contextOut.putFields("Synonym_result", context.getFieldsMap().get("Synonym_result"));  // 색인단어 유지

        Map<String, String> resultPorcessLog = searchAnswerResponse.getResultProcessMap();
        for (Map.Entry<String, String> entry : resultPorcessLog.entrySet()) {
          resultQueryList.add(entry.getKey() + "=>" + entry.getValue());
        }
      }
    }

    for(Answer answer : searchAnswerResponse.getAnswerResultList()) {
      idList.add(answer.getId());
    }
    contextOut.putFields("idList",Value.newBuilder().setStringValue(String.join(",",idList)).build());

    return searchAnswerResponse;
  }

  // 검색 단어 매칭 결과 리턴해주는 함수 (Match : **매칭된 단어** NonMatch : **매칭안된 단어**)
  private String getMatchNoMatchString(Struct.Builder context) {
    String allWords = context.getFieldsMap().get("Synonym_result").getStringValue();
    Map<String, Value> allResult = context.getFieldsMap().get("All_Result").getStructValue().getFieldsMap();
    List<String> idList = Arrays.asList(context.getFieldsMap().get("idList").getStringValue().split("\\s*,\\s*"));
    Map<String, Value> qIdaIdMap = new HashMap<>();
    if (context.containsFields("Qid_Aid_Map")) {
      qIdaIdMap = context.getFieldsMap().get("Qid_Aid_Map").getStructValue().getFieldsMap();
    }
    logger.debug("AllWords => " + allWords);
    logger.debug("MatchWordSet =>" + allResult.toString());
    Set<String> allWordsSet = new HashSet<>(Arrays.asList(allWords.replace("[","").replace("]","").split("\\s*,\\s*")));
    Set<String> matchWordsSet = new HashSet<>();
    for (String words : allResult.keySet()) {
      for (String id : allResult.get(words).getStringValue().split("\\s*,\\s*"))
        if (idList.contains(chechAnswerId(qIdaIdMap, id))) {
          matchWordsSet.addAll(Arrays.asList(words.split("\\s*,\\s*")));
          break;
        }
    }
    allWordsSet.removeAll(matchWordsSet);
    String matchWordResult = "Match : " + String.join(",",matchWordsSet) + "  NonMatch : " + String.join(",", allWordsSet);
    return matchWordResult;
  }

  // 화면에 보여줄 Question 검색 결과 파싱 및 저장
  private void putQuestionResultView(Struct.Builder structBuilder, SearchQuestionResponse searchQuestionResponse, String queryTarget) {
    Map<String, Value> allResultMap = searchQuestionResponse.getAllResult().getFieldsMap();
    HashMap<String, Question> qResponseMap = new HashMap<>();
    for (Question quesiton : searchQuestionResponse.getQuestionsList()) {
      qResponseMap.put(quesiton.getId(),quesiton);
    }
    String fullQuery = "<br>";
    for (Map.Entry<String, Value> entryValue : allResultMap.entrySet()) {
      fullQuery += " morph:" + entryValue.getKey() + "<br>";
      for (String qid : entryValue.getValue().getStringValue().split(",")) {
        //Q 파싱해서 출력
        Question question = qResponseMap.get(qid);
        List<String> qValueList = new ArrayList<>();
        qValueList.add("Aid:" + question.getAnswerId());
        qValueList.add("Qid:" + question.getId());
        qValueList.add("Qstring:" + question.getQuestion());
        qValueList.add("Qmorph:" + question.getQuestionMorp());
        qValueList.add("Qner:" + question.getNer());
        qValueList.add("Qattr1:" + question.getAttribute().getAttr1());
        qValueList.add("Qattr2:" + question.getAttribute().getAttr2());
        qValueList.add("Qattr3:" + question.getAttribute().getAttr3());
        qValueList.add("Qattr4:" + question.getAttribute().getAttr4());
        qValueList.add("Qattr5:" + question.getAttribute().getAttr5());
        qValueList.add("Qattr6:" + question.getAttribute().getAttr6());
        qValueList.add("Qscore:" + String.valueOf(question.getScore()));
        fullQuery += " ";
        fullQuery += String.join("  ", qValueList);
        fullQuery += "<br>";
      }
    }
    structBuilder.putFields(String.valueOf(count) + "_" + queryTarget, Value.newBuilder().setStringValue(fullQuery).build());
    count++;
  }

  private String chechAnswerId(Map<String, Value> qIdaIdMap, String aid) {
    return qIdaIdMap.size() > 0 ? qIdaIdMap.get(aid).getStringValue() : aid;
  }

  // 화면에 보여줄 Answer 검색 결과 파싱 및 저장
  private void putAnswerResultView(Struct.Builder structBuilder, SearchAnswerResponse searchAnswerResponse, String queryTarget, Struct.Builder contextOut) {
    Map<String, Value> allResultMap = new HashMap<>();
    Map<String, Value> qIdaIdMap = new HashMap<>();
    if ("AID".equalsIgnoreCase(queryTarget)) {
      allResultMap = contextOut.getFieldsMap().get("All_Result").getStructValue().getFieldsMap();
      if (contextOut.containsFields("Qid_Aid_Map")) {
        qIdaIdMap = contextOut.getFieldsMap().get("Qid_Aid_Map").getStructValue().getFieldsMap();
      }
    } else {
      allResultMap = searchAnswerResponse.getAllResult().getFieldsMap();
    }
    logger.debug("MakeView -> querytarget" + queryTarget);
    logger.debug(allResultMap.toString());
    HashMap<String, Answer> qAnswerMap = new HashMap<>();
    for (Answer answer : searchAnswerResponse.getAnswerResultList()) {
      qAnswerMap.put(answer.getId(), answer);
    }
    String fullQuery = "<br>";
    for (Map.Entry<String, Value> entryValue : allResultMap.entrySet()) {
      fullQuery += " morph:" + entryValue.getKey() + "<br>";
      for (String aid : entryValue.getValue().getStringValue().split(",")) {
        String answerid = chechAnswerId(qIdaIdMap, aid);
        if (qAnswerMap.containsKey(answerid)) {
          //A 파싱해서 출력
          Answer answer = qAnswerMap.get(answerid);
          List<String> aValueList = new ArrayList<>();
          aValueList.add("Aid:" + answer.getId());
          aValueList.add("Astring:" + answer.getAnswer());
//          aValueList.add("Amorph:" + answer.getAnserMorp);
          aValueList.add("Aner:" + answer.getNer());
          aValueList.add("Aattr1:" + answer.getAttributes().getAttr1());
          aValueList.add("Aattr2:" + answer.getAttributes().getAttr2());
          aValueList.add("Aattr3:" + answer.getAttributes().getAttr3());
          aValueList.add("Aattr4:" + answer.getAttributes().getAttr4());
          aValueList.add("Aattr5:" + answer.getAttributes().getAttr5());
          aValueList.add("Aattr6:" + answer.getAttributes().getAttr6());
          aValueList.add("Alayer1:" + answer.getLayers().getLayer1());
          aValueList.add("Alayer2:" + answer.getLayers().getLayer2());
          aValueList.add("Alayer3:" + answer.getLayers().getLayer3());
          aValueList.add("Alayer4:" + answer.getLayers().getLayer4());
          aValueList.add("Alayer5:" + answer.getLayers().getLayer5());
          aValueList.add("Alayer6:" + answer.getLayers().getLayer6());
          aValueList.add("Ascore:" + String.valueOf(answer.getScore()));
          fullQuery += " ";
          fullQuery += String.join("  ", aValueList);
          fullQuery += "<br>";
        }
      }
    }
    structBuilder.putFields(String.valueOf(count) + "_" + queryTarget, Value.newBuilder().setStringValue(fullQuery).build());
    count++;
  }

  // 여러 NQA검색 옵션들 세팅
  private void fillNqaOptionDocument(NqaOptionDocument nqaOptionDocument, String query) {
    logger.info("UserQuery => " + query);
    List<String> qList = Arrays.asList(query.split("@"));
    boolean first = true;

    if (qList.size() > 0) {
      for (String slot : qList) {
        if (first) {
          first = false;
        } else {
          String[] slotList = slot.split(":");
          setNqaDocument(nqaOptionDocument, slotList[0].trim(), slotList[1].trim());
        }
      }
    }
  }

  // Answer검색을 하기위한 조건 설정
  private void fillASlot(SearchAnswerRequest.Builder searchAnswerRequestBuilder, String query) {
    NqaOptionDocument nqaOptionDocument = new NqaOptionDocument();
    fillNqaOptionDocument(nqaOptionDocument, query);

    searchAnswerRequestBuilder.setNtop(nqaOptionDocument.getNtop());
    searchAnswerRequestBuilder.setCategory(nqaOptionDocument.getCategory());
    searchAnswerRequestBuilder.setQueryType(nqaOptionDocument.getQueryType());
    searchAnswerRequestBuilder.addAllIds(nqaOptionDocument.getIds());
//    searchQuestionRequestBuilder.setQueryTarget(nqaOptionDocument.getQueryTarget());
    searchAnswerRequestBuilder.setUserQuery(nqaOptionDocument.getUserQuery());
    searchAnswerRequestBuilder.setAttribute(nqaOptionDocument.getAttribute());
    searchAnswerRequestBuilder.setLayer(nqaOptionDocument.getLayer());
    searchAnswerRequestBuilder.addAllNer(nqaOptionDocument.getNer());
//    searchAnswerRequestBuilder.addAllTags(nqaOptionDocument.getTags());
  }

  // Question검색을 하기위한 조건 설정
  private void fillQSlot(SearchQuestionRequest.Builder searchQuestionRequestBuilder, String query) {
    NqaOptionDocument nqaOptionDocument = new NqaOptionDocument();
    fillNqaOptionDocument(nqaOptionDocument, query);

    searchQuestionRequestBuilder.setNtop(nqaOptionDocument.getNtop());
    searchQuestionRequestBuilder.setScore(nqaOptionDocument.getScore());
    searchQuestionRequestBuilder.setMaxSize(nqaOptionDocument.getMaxSize());
    searchQuestionRequestBuilder.setCategory(nqaOptionDocument.getCategory());
    searchQuestionRequestBuilder.setQueryType(nqaOptionDocument.getQueryType());
//    searchQuestionRequestBuilder.setQueryTarget(nqaOptionDocument.getQueryTarget());
    searchQuestionRequestBuilder.setUserQuery(nqaOptionDocument.getUserQuery());
    searchQuestionRequestBuilder.setAttributes(nqaOptionDocument.getAttribute());
    searchQuestionRequestBuilder.addAllNer(nqaOptionDocument.getNer());
//    searchQuestionRequestBuilder.addAllTags(nqaOptionDocument.getTags());
  }

  /**
   * 계층 정보 확인(계층 후보가 복수인지 단일인지 확인.)
   * @param searchAnswerResponse 답변 검색 결과들
   * @param context 세션에 저장할 context
   * @return 화면에 나타낼 카드(계층 후보) 목록
   */
  private HashSet<String> checkLayer(SearchAnswerResponse searchAnswerResponse, Struct.Builder context,List<String> layerList) {

    ArrayList<String> layer1 = new ArrayList<>();
    ArrayList<String> layer2 = new ArrayList<>();
    ArrayList<String> layer3 = new ArrayList<>();
    ArrayList<String> layer4 = new ArrayList<>();
    ArrayList<String> layer5 = new ArrayList<>();
    ArrayList<String> layer6 = new ArrayList<>();

    List<Answer> answerList = searchAnswerResponse.getAnswerResultList();

    for (Answer answer : answerList) {
      layer1.add(answer.getLayers().getLayer1());
      layer2.add(answer.getLayers().getLayer2());
      layer3.add(answer.getLayers().getLayer3());
      layer4.add(answer.getLayers().getLayer4());
      layer5.add(answer.getLayers().getLayer5());
      layer6.add(answer.getLayers().getLayer6());
    }

    HashMap<String, ArrayList<String>> layer = new HashMap<>();

    layer.put("L1", layer1);
    layer.put("L2", layer2);
    layer.put("L3", layer3);
    layer.put("L4", layer4);
    layer.put("L5", layer5);
    layer.put("L6", layer6);

    HashSet<String> final_set = new HashSet<>();

    for(int i=1; i<7; i++) {
      HashSet<String> set = new HashSet<>(layer.get("L" + String.valueOf(i)));
      logger.debug("layer_list ==> " + set.toString());
      if(set.size() > 1) {
        final_set = set;
        context.putFields("layerNum",Value.newBuilder().setStringValue("L" + String.valueOf(i)).build());
        logger.info("layer_num ==> " + context.getFieldsMap().get("layerNum").getStringValue());
        break;
      } else {
        for (String temp : set) {
          context.putFields("L" + String.valueOf(i),Value.newBuilder().setStringValue(temp).build());
          layerList.add("("+checkNull2LayerName(i,temp)+")");
        }
      }
    }
    logger.info("final_set ==> " + final_set.toString());
    return final_set;
  }

  // Null에 해당하는 각 Layer String 리턴하는 함수
  private String checkNull2LayerName(int layerNum, String layerName) {
    if ("Null".equalsIgnoreCase(layerName)) {
      switch (layerNum) {
        case 1:
          return layerNullInfo.getLayerNullArray()[0];
        case 2:
          return layerNullInfo.getLayerNullArray()[1];
        case 3:
          return layerNullInfo.getLayerNullArray()[2];
        case 4:
          return layerNullInfo.getLayerNullArray()[3];
        case 5:
          return layerNullInfo.getLayerNullArray()[4];
        case 6:
          return layerNullInfo.getLayerNullArray()[5];
      }
    }
    return layerName;
  }

  // Layer 관련 정보 파싱(경로, 선택카드)
  private SelectCard.Builder insertLayerCard(HashSet<String> set, Struct.Builder context, List<String> layerList, Struct.Builder structBuilder) {
    SelectCard.Builder selectCard = SelectCard.newBuilder();

    try {
      structBuilder.putFields("LayerSet", Value.newBuilder().setStringValue(String.join(",",set)).build());
      selectCard.setTitle(selectLayerName(context) + " 골라주세요")
          .setHeader("Layer:" + String.join( "->",layerList)).setHorizontal(true).build();
      for (String setName : set) {
        selectCard.addItems(Item.newBuilder()
            .setTitle(checkNull2LayerName(Integer.valueOf(context.getFieldsMap().get("layerNum").getStringValue().replace("L","")),setName))
//            .setSelectedUtter(setName)).build();
            .setSelectedUtter(checkNull2LayerName(Integer.valueOf(context.getFieldsMap().get("layerNum").getStringValue().replace("L","")),setName))).build();
      }
    } catch (Exception e) {
      logger.error(e.toString());
    }
    return selectCard;
  }

  /**
   * 계층의 속성이름 리턴
   */
  private String selectLayerName(Struct.Builder context) {
    HashMap<String, String> layerName = new HashMap<>();
    layerName.put("L1","계층1");
    layerName.put("L2","계층2");
    layerName.put("L3","계층3");
    layerName.put("L4","계층4");
    layerName.put("L5","계층5");
    layerName.put("L6","계층6");

    return layerName.get(context.getFieldsMap().get("layerNum").getStringValue());
  }

  /**
   * 검색결과 IdResult 객체 리스트를 String ID 리스트로 반환
   * @param idResultList
   * @return 답변 String ID 리스트
   */
  private List<String> passingIdResult2List(List<String> idResultList) {
    List<String> idList = new ArrayList<String>();
    for (String idResult : idResultList) {
      idList.add(idResult);
    }
    return idList;
  }

  private String makeLayerString(Struct context, String nowLayer, String layerName) {
    String layerQuery = "";
    for (int i=1; i<7; i++) {
      if (context.getFieldsMap().containsKey("layer" + String.valueOf(i))) {
        layerQuery += "@L" + String.valueOf(i) + ":" + context.getFieldsMap().get("layer" + String.valueOf(i)).getStringValue() + " ";
        logger.info("layer" + String.valueOf(i) + "==>" + context.getFieldsMap().get("layer" + String.valueOf(i)).getStringValue());
      } else if (StringUtils.equalsIgnoreCase(nowLayer, "L" + String.valueOf(i))) {
        layerQuery += "@L" + String.valueOf(i) + ":" + layerName + " ";
      } else {
        logger.info("layer" + String.valueOf(i) + "==>" + " ");
      }
    }
    return layerQuery;
  }

  /**
   * DA에서 내려보낸 디렉티브에 대한 실행 결과를 받아와서 처리할 수 있도록 한다. 다음과 같은 동작이 예가 될 수 있다. 로봇이 특정한 위치로 이동하라고 명명하고 수행이
   * 완료되었을 경우에 내보낸다.
   **/
  @Override
  public void event(Talk.EventRequest request,
      StreamObserver<Talk.TalkResponse> responseObserver) {

    logger.info("DA V3 Server Event");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("Event request: {}", request);

    Talk.TalkResponse.Builder response = TalkResponse.newBuilder();

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10104, "session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("event")
              .setLine(756)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          e.printStackTrace();
          responseObserver.onError(e);
        }
      }

      response.setResponse(
          SpeechResponse.newBuilder().setSpeech( // event에 대한 응답 메세지
              Speech.newBuilder().setUtter("This is event response")
                  .build())
              .setMeta(Struct.newBuilder().putFields("token", // 추가적인 정보 전달
                  Value.newBuilder().setStringValue(
                      request.getEvent().getPayload().getLauncherAuthorized().getAccessToken())
                      .build()).build())
              .setSessionUpdate( // session 업데이트
                  Session.newBuilder().setId(
                      request.getSession().getId())
                      .build()).build()).build();

      logger.trace("Event response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10009, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("event")
            .setLine(794)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        e1.printStackTrace();
        responseObserver.onError(e);
      }
    }
  }

  /**
   * 현재 SKILL이 종료되었음을 알려준다. SKILL이 정상적으로 스스로 종료할 경우에는 호출하지 않으므로 매우 주의해야 한다.
   **/
  @Override
  public void closeSkill(Talk.CloseSkillRequest request,
      StreamObserver<Talk.CloseSkillResponse> responseObserver) {
    logger.info("DA V3 Server CloseSkill");
    logger.debug("{}: {}",
        ServerMetaInterceptor.META_OPERAION_SYNC_ID,
        ServerMetaInterceptor.OPERAION_SYNC_ID.get());
    logger.trace("CloseSkill request: {}", request);

    CloseSkillResponse.Builder response = CloseSkillResponse.newBuilder();

    ai.maum.rpc.ResultStatus status = null;

    try {
      Long sessionId = request.getSession().getId();
      if (sessionId == null || request.getSkill() == null || request.getChatbot() == null) {
        // Detail Any object add
        try {
          status = new ai.maum.rpc.ResultStatus(
              maum.rpc.Status.ExCode.INVALID_ARGUMENT, 10105,
              "chatbot or skill or session.id is null");

          maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
              .setFunction("event")
              .setLine(835)
              .build();

          status.addDetail(infoFile);

          StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
          responseObserver.onError(sre);
        } catch (Exception e) {
          e.printStackTrace();
          responseObserver.onError(e);
        }
      }

      skill = null;
      response.setSessionUpdate(
          Session.newBuilder()
              .setId(request.getSession().getId()) // session update
              .setContext(request.getSession().getContext()) // session context update
              .build()).build();

      logger.trace("CloseSkill response: {}", response);
      responseObserver.onNext(response.build());
      responseObserver.onCompleted();
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
      // Detail Any object add
      try {
        status = new ai.maum.rpc.ResultStatus(
            maum.rpc.Status.ExCode.RUNTIME_ERROR, 10009, e.getMessage());

        maum.rpc.ErrorDetails.DebugInfoFile infoFile = ErrorDetails.DebugInfoFile.newBuilder()
            .setFunction("closeSkill")
            .setLine(866)
            .build();

        status.addDetail(infoFile);

        StatusRuntimeException sre = ResultStatusListProto.toStatusRuntimeException(status);
        responseObserver.onError(sre);
      } catch (Exception e1) {
        e1.printStackTrace();
        responseObserver.onError(e);
      }
    }
  }
}
