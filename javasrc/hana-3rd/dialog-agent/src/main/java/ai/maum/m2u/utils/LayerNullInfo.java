package ai.maum.m2u.utils;

public class LayerNullInfo {

  private String[] layerNullArray = {
      "해당없무 없음",
      "해당상세업무 없음",
      "해당처리방법 없음",
      "해당채널 없음",
      "해당실물 없음",
      "해당자격 없음"};

  public String[] getLayerNullArray() {
    return layerNullArray;
  }

  public void setLayerNullArray(String[] layerNullArray) {
    this.layerNullArray = layerNullArray;
  }

}
