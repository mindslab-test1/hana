package ai.maum.m2u.config;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.List;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropertyManager {

  static final Logger logger = LoggerFactory.getLogger(PropertyManager.class);

  private static String absoluteMindsPath = "MAUM_ROOT";
  private static String absoluteEtcPath = "/etc/";
  private static String absoluteConfigFileName = "m2u.conf";

  private static PropertiesConfiguration propertyConfig = null;

  private static void load() {
    if (propertyConfig == null) {
      logger.debug("### service-admin config file load");
      propertyConfig = getConfig();
    }
  }

  private static PropertiesConfiguration getConfig() {
    PropertiesConfiguration config = null;
    String confPath = getSystemConfPath();

    logger.debug("---------------------------------------");
    logger.debug("getSystemConfPath :: {}", confPath);
    logger.debug("getSystemConfFileName :: {}", absoluteConfigFileName);
    logger.debug("---------------------------------------");

    try {
      config = new PropertiesConfiguration(confPath + absoluteConfigFileName);
    } catch (ConfigurationException e) {
      logger.error("{} => ", e.getMessage(), e);
    } catch (Exception e) {
      logger.error("{} => ", e.getMessage(), e);
    }
    return config;
  }

  private static String getSystemConfPath() {
    String path = System.getenv(absoluteMindsPath);
    path += absoluteEtcPath;
    return path;
  }

  public static String getString(String propertyName) {
    load();
    return propertyConfig.getString(propertyName);
  }

  public static String[] getStringArray(String propertyName) {
    load();
    return propertyConfig.getStringArray(propertyName);
  }

  public static int getInt(String propertyName) {
    load();
    return propertyConfig.getInt(propertyName);
  }

  public static double getDouble(String propertyName) {
    load();
    return propertyConfig.getDouble(propertyName);
  }

  public static long getLong(String propertyName) {
    load();
    return propertyConfig.getLong(propertyName);
  }

  public static float getFloat(String propertyName) {
    load();
    return propertyConfig.getFloat(propertyName);
  }

  public static BigDecimal getBigDecimal(String propertyName) {
    load();
    return propertyConfig.getBigDecimal(propertyName);
  }

  public static List<Object> getList(String propertyName) {
    load();
    return propertyConfig.getList(propertyName);
  }

  public static Boolean getBoolean(String propertyName) {
    load();
    return propertyConfig.getBoolean(propertyName);
  }

  public static String getDecrypt(String propertyName) throws Exception {
    load();
    return decrypt(propertyConfig.getString(propertyName));
  }

  private static String decrypt(String str)
      throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException,
      IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
    Decoder decoder = Base64.getDecoder();
    byte[] decodedBytes = decoder.decode(str);
    Key key;
    IvParameterSpec ivParameterSpec;

    List<Byte> ivList = new ArrayList<>();
    List<Byte> keyList = new ArrayList<>();
    List<Byte> encList = new ArrayList<>();

    for (int x = 0; x < (16 * 3); x++) {
      int mod = x % 3;
      if (mod == 0) {
        encList.add(decodedBytes[x]);
      } else if (mod == 1) {
        ivList.add(decodedBytes[x]);
      } else if (mod == 2) {
        keyList.add(decodedBytes[x]);
      }
    }
    for (int x = (16 * 3); x < decodedBytes.length; x++) {
      encList.add(decodedBytes[x]);
    }

    key = new SecretKeySpec(ArrayUtils.toPrimitive(keyList.toArray(new Byte[keyList.size()])),
        "AES");
    ivParameterSpec = new IvParameterSpec(
        ArrayUtils.toPrimitive(ivList.toArray(new Byte[ivList.size()])));

    Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
    cipher.init(Cipher.DECRYPT_MODE, key, ivParameterSpec);
    String result = new String(
        cipher.doFinal(ArrayUtils.toPrimitive(encList.toArray(new Byte[encList.size()]))),
        "UTF-8");

    return result;
  }
}

