syntax = "proto3";

import "google/protobuf/empty.proto";
import "maum/common/lang.proto";

package maum.brain.nlp;

enum NlpFeature {
  // nlp1 only
  NLPF_SPACE = 0;
  NLPF_WORD = 1;
  // nlp2
  NLPF_MORPHEME = 2;
  NLPF_NAMED_ENTITY = 3;
  NLPF_CHUNK = 4;
  NLPF_WORD_SENSE_DISAMBIGUATION = 5;
  NLPF_PARSER = 6;
  // nlp3
  NLPF_SENTENCE_RECOGNITION = 7;
  NLPF_DEPENDENCY_PARSER = 8;
  NLPF_SEMANTIC_ROLE_LABELING = 9;
  NLPF_ZERO_ANAPHORA = 10;

  // 감성분석, 이 값은 InputText의 감성분석 요청 대상에서 제외된다.
  NLPF_SETIMENT = 100;
  // DNN 기반의 감성분석, 이 값은 InputText의 감성분석 요청 대상에서 제외된다.
  NLPF_DNN = 101;
}

// According to Level, NLP Analysis
// NLP_ANALYSIS_ALL : MORPHEME, NAMED_ENTITY, CHUNK
// NLP_ANALYSIS_WORD(Level 1) : SPACE, WORD, MORPHEME, MORPHEME_EVAL(only nlp kor #3)
// NLP_ANALYSIS_NAMED_ENTITY(Level 2) : Level 1 + NAMED_ENTITY
// NLP_ANALYSIS_WORD_SENSE_DISAMBIGUATION(Level 3) : Level2 + WORD_SENSE_DISAMBIGUATION
// NLP_ANALYSIS_DEPENDENCY_PARSER(Level 4) : Level 3 + DEPENDENCY_PARSER
// NLP_ANALYSIS_SEMANTIC_ROLE_LABELING(Level 5) : Level 4 + SEMANTIC_ROLE_LABELING
// NLP_ANALYSIS_ZERO_ANAPHORA(Level 6) : Level 5 + ZERO_ANAPHORA

enum NlpAnalysisLevel {
  NLP_ANALYSIS_ALL = 0;
  NLP_ANALYSIS_MORPHEME = 1;
  NLP_ANALYSIS_NAMED_ENTITY = 2;
  NLP_ANALYSIS_WORD_SENSE_DISAMBIGUATION = 3;
  NLP_ANALYSIS_DEPENDENCY_PARSER = 4;
  NLP_ANALYSIS_SEMANTIC_ROLE_LABELING = 5;
  NLP_ANALYSIS_ZERO_ANAPHORA = 6;
  // not work
  NLP_ANALYSIS_RELATION = 100;
  NLP_ANALYSIS_SENTIMENTAL_ANALYSIS = 101;
}

// According to Level, Extract Keyword Frequency

enum KeywordFrequencyLevel {
  KEYWORD_FREQUENCY_ALL = 0;
  KEYWORD_FREQUENCY_NONE = 1;
}

message NlpProvider {
  string name = 1;
  string vendor = 2;
  string version = 3;
  string description = 4;
  maum.common.LangCode lang = 5;
  string support_encoding = 6;
}

///
// Input Text
//
message InputText {
  string text = 1;
  maum.common.LangCode lang = 2;
  bool split_sentence = 11;
  bool use_tokenizer = 12;
  bool use_space = 13; //nlp3


  // NLP의 분석 레벨 값
  //
  // 대체적으로 분석 레벨은 숫자가 높을 수록 더 상세한 요청을 보낼 수 있다.
  // 하지만, 여러가지 특징 중에서 의미있는 것만을 처리할 수가 없는 문제가
  // 있다. 예를 들어, ZERO_ANAPHORA 기능만 사용하겠다고 하는 경우에는
  // SR, MORP, ZERO_ANAPHORA만 요청하면 해결된다.
  // 하지만, ZERO_ANAPHORA를 레벨로 요청하게 되면
  // 모든 기능을 다 쓰도록 하게 되는 문제가 발생하게 된다.
  // 또한 서버가 NER 기능을 실질적으로 지원하지 않도록 구성해 놓은 경우에도
  // NER을 요청하게 되는 단점이 있게 된다.
  NlpAnalysisLevel level = 20 [deprecated=true];

  // NLP1에서만 지원하고, NLP2, NLP3에서는 사용하지 않는다.
  // 속도의 문제가 있기 때문에 기본적으로 사용하지 않는 게 좋다.
  KeywordFrequencyLevel keyword_frequency_level = 30;

  // NLP 요청 특질들
  // 이 값이 0이 아닌 값으로 요청되면
  // 위의 `level`은 무시된다.
  //
  // 이 요청은 `NlpFeature`들의 숫자를 BIT 연산하여 더한 요청값이다.
  // `NLPF_MORPHEME | NLPF_NAMED_ENTITY | NLPF_DEPENDENCY_PARSER` 만 요청하고 싶으면
  //
  // (1<<NLPF_MORPHEME || 1<<NLPF_NAMED_ENTITY || 1<< NLPF_DEPENDENCY_PARSER)
  //
  // 값을 전송하면 된다.
  // 서버는 자동으로 NLPF_MORPHEME가 누락된 경우에 이를 채워서 사용해야 한다.
  // 이값이 없으면 무조건 에러가 나기 때문이다.
  //
  int32 feature_mask = 40;
}

// ==================================================================
// Language Analysis
// ==================================================================
message KeywordFrequency {
  int32 seq = 1; // seq of sentence
  string keyword = 2; // keyword
  int32 frequency = 3; // keyword frequency
  string word_type = 4; // named entity type
  string word_type_nm = 5; // named of named entity(ex. COMPANY)
}

/**
 * 어절 정보
 * seq : 어절의 id
 * text : 어절
 * begin : 어절을 구성하는 첫형태소 id
 * end : 어절을 구성하는 끝 형대소 id
 */
message Word {
  int32 seq = 1; // kor nlp #1,3
  string text = 2; // kor nlp #1,3
  string tagged_text = 3; // kor nlp #2
  string type = 4; // kor nlp #1,3
  int32 begin = 11; // kor nlp #1,3
  int32 end = 12; // kor nlp #1,3
  int32 begin_sid = 21; // kor nlp #2
  int32 end_sid = 22; // kor nlp #2
  int32 position = 31; // eng nlp #2
}

/**
 * 형태소 정보
 * seq : 형태소 id
 * lemma : 형태소 단어
 * type : 형태소 태그
 * posion : 문장에서 byte position
 * weight : 0 ~ 1 사이의 형태소 분석 결과의 신뢰도
 */
message Morpheme {
  int32 seq = 1;
  string lemma = 2;
  string type = 3;
  int32 position = 4;
  int32 wid = 5; // kor nlp #2
  double weight = 11; // kor nlp #3
}

// kor nlp #3
message MorphemeEval {
  int32 seq = 1;
  string target = 2;
  string result = 3;
  int32 word_id = 4;
  int32 m_begin = 5;
  int32 m_end = 6;
}

/**
 * 개체명 정보
 * seq : 개체명 id ( 출현 순서대로 부여 )
 * text : 개체명 단어
 * type : 개체명 타입
 * begin : 개체명을 구성하는 첫형태소 seq
 * end : 개체명을 구성하는 끝형태소 seq
 * weight : 개체명 분석결과의 신뢰도
 * common_noun : 고유명사(0), 일반명사(1)
 */
message NamedEntity {
  int32 seq = 1;
  string text = 2;
  string type = 3;
  int32 begin = 4;
  int32 end = 5;
  double weight = 11; // kor nlp #1,3
  int32 common_noun = 12; // kor nlp #1,3
  int32 begin_sid = 21; // kor nlp #2
  int32 end_sid = 22; // kor nlp #2
}

message NamedEntityTag {
  string name = 1;
  string full_name = 2;
  int32 index = 3;
  string description = 4;
}

// kor nlp #3
/**
 * 어휘의미 분석 정보
 * seq : wsd 단위 id
 * text : wsd 대상 단어 텍스트
 * type : wsd 대상 단어 형태소 태그
 * scode : wsd 대상 단어의 표준국어대사전 어깨번호
 * weight : 어휘 의미 분석 결과 신뢰도
 * position : 문장 내 byte position
 * begin : wsd 대상 단어의 첫 형태소 seq
 * end : wsd 대상 단어의 끝 형태소 seq
 */
message WordSenseDisambiguation {
  int32 seq = 1;
  string text = 2;
  string type = 3;
  string scode = 4;
  double weight = 5;
  int32 position = 6;
  int32 begin = 7;
  int32 end = 8;
}

/**
 * seq : chunk id
 * type : chunk type
 * text : chunk 문자열( 형태소 분석 결과로부터 복원)
 * begin : chunk를 구성하는 첫 형태소 id
 * end : chunk를 구성하는 끝 형태소 id
 */
message Chunk {
  int32 seq = 1;
  string text = 2;
  string type = 3;
  int32 begin = 4;
  int32 end = 5;
  double weight = 11; // kor nlp #3
}

/**
 * 의존관계 정보
 * seq : 어절의 id
 * text : 어절
 * head : 부모 어절의 seq
 * label : 의존관계
 * mod : 자식 어절의 seq
 * weight : 의존구문분석 결과 신뢰도 (높을수록 신뢰)
 */
message DependencyParser {
  int32 seq = 1;
  string text = 2;
  int32 head = 3;
  string label = 4;
  repeated int32 mods = 5;
  double weight = 11; // kor nlp #3
}

// kor nlp #3
message PhraseElement {
  string text = 1;
  string label = 2;
  int32 begin = 3;
  int32 end = 4;
  string ne_type = 5;
}

// kor nlp #3
message PhraseClause {
  int32 seq = 1;
  string label = 2;
  string text = 3;
  int32 begin = 4;
  int32 end = 5;
  int32 key_begin = 6;
  int32 head_phrase = 7;
  repeated int32 sub_phrases = 8;
  double weight = 9;
  repeated PhraseElement elements = 10;
}

/**
 * type : argument type
 * word_id : argument 어절 id
 * text : argument 어절
 */
message SRLArgument {
  string type = 1;
  int32 word_id = 2;
  string text = 3;
}

/**
 * 의미역인식 정보
 * verb : 대상용언
 * sense : 용언에 대한 의미번호
 * word_id : 용언 어절 id
 * weight : 결과에 대한 신뢰도 점수
 * argument : predicate에 대한 argument 정보
 */
message SRL {
  string verb = 1;
  int32 sense = 2;
  int32 word_id = 3;
  double weight = 4;
  repeated SRLArgument arguments = 5;
}

// kor nlp #3
message SRLInformationExtraction {
  string pred = 1;
  int32 sense = 2;
  int32 word_id = 3;
  double weight = 4;
  repeated SRLArgument arguments = 5;
}

// kor nlp #3
message RelationProperty {
  string type = 1;
  string value = 2;
  string value_type = 3;
  int32 begin = 4;
  int32 end = 5;
}

// kor nlp #3
message Relation {
  string type = 1;
  string trigger = 2;
  string sub = 3;
  string sub_type = 4;
  int32 begin = 5;
  int32 end = 6;
  double weight = 7;
  repeated RelationProperty rel_props = 8;
}

// kor nlp #1
message SentimentalAnalysis {
  int32 type = 1;
  double degree = 2;
  double weight = 3;
  int32 purity = 4;
  string anchor = 5;
  int32 anchor_begin = 6;
  int32 anchor_end = 7;
  string trigger = 8;
  int32 trigger_begin = 9;
  int32 trigger_end = 10;
  string target = 11;
  int32 target_begin = 12;
  int32 target_end = 13;
  string aspect = 14;
  string aspect_expr = 15;
  string aspect_value = 16;
  int32 aspect_begin = 17;
  int32 aspect_end = 18;
}

// kor nlp #3
/**
 * 무형대용어 복원 정보
 * seq : 무형대용어 복원 결과 id ( 출현 순서대로)
 * verb_wid : 문장 내 필수격이 생략된 용언의 어절 번호
 * ant_sid : 대용어로 인식된 어절을 포함한 선행 문장의 id
 * and_wid : 선행 문장의 대용어 어절 id
 * type : verb_wid 용언의 대용어 인식 결과 격 정보 (s:주격, o:목적격)
 * istitle : 문서의 타이틀이 무형대용어로 이용가능한지 여부(0: 불가능, 1: 가능)와 가능할 경우
 * ant_sid:-1, ant_wid:-1 일 경우 문서 타이틀을 복원함
 * weight : 무형대용어 분석 결과 신뢰도
 */
message ZeroAnaphora {
  int32 seq = 1;
  int32 verb_wid = 2;
  int32 ant_sid = 3;
  int32 ant_wid = 4;
  string type = 5;
  int32 istitle = 6;
  double weight = 7;
}

message Sentence {
  int32 seq = 1;
  string text = 2;
  repeated Word words = 3;
  repeated Morpheme morps = 4;
  repeated MorphemeEval morph_evals = 5; // kor nlp #3
  repeated NamedEntity nes = 6;
  repeated WordSenseDisambiguation wsds = 7; // kor nlp #3
  repeated Chunk chunks = 8;
  repeated DependencyParser dependency_parsers = 9;
  repeated PhraseClause phrase_clauses = 10; // kor nlp #3
  repeated SRL srls = 11;
  repeated SRLInformationExtraction srl_infos = 12; // kor nlp #3
  repeated Relation relations = 13;
  repeated SentimentalAnalysis sas = 14;
  repeated ZeroAnaphora zas = 15; // kor nlp #3
}

message Document {
  string category = 1;
  float category_weight = 2;
  maum.common.LangCode lang = 4;
  repeated Sentence sentences = 11;
  repeated KeywordFrequency keyword_frequencies = 12;
}

message NlpFeatures {
  repeated NlpFeature features = 1;
}

message NlpFeatureSupport {
  NlpFeature feature = 1;
  bool support = 2;
}

message NlpFeatureSupportList {
  repeated NlpFeatureSupport supports = 1;
}

message NamedEntityTagList {
  repeated NamedEntityTag tags = 1;
}

enum NlpDictType {
  DIC_UNKNOWN = 0;
  DIC_CUSTOM_MORPHEME = 1;
  DIC_CUSTOM_COMPOUND_NOUN = 2;
  DIC_NE_PRE_PROC = 11;
  DIC_NE_POST_PROC = 12;
  DIC_SYNONYM = 21;
}

message NlpDict {
  NlpDictType type = 1;
  string download_url = 2;
  string callback_url = 100;
}

message TaggedList {
  repeated string result = 1;
}

// NLP3 전처리, 후처리 패턴 사전 정보
message NerDict {
  message Dictionary {
    // 카테고리
    string category = 1;
    // 정규식 패턴
    repeated string values = 2;
  }
  repeated Dictionary text = 1;
  string callback_url = 2;
}

// NLP3 오류 태그 변경 사전 정보
message ChangedNerDict {
  message Dictionary {
    // 변경 할 카테고리
    string category = 1;
    // 기존 카테고리
    string old_category = 2;
    // 단어
    string word = 3;

  }
  repeated Dictionary text = 1;
  string callback_url = 2;
}

service NaturalLanguageProcessingService {
  rpc GetProvider (google.protobuf.Empty) returns (NlpProvider);
  rpc Analyze (InputText) returns (Document);
  rpc AnalyzeMultiple (stream InputText) returns (stream Document);
  rpc HasSupport (NlpFeatures) returns (NlpFeatureSupportList);
  rpc ApplyDict (NlpDict) returns (google.protobuf.Empty);
  rpc GetNamedEntityTagList (google.protobuf.Empty) returns (NamedEntityTagList);
  rpc GetPosTaggedList (Document) returns (TaggedList);
  rpc GetNerTaggedList (Document) returns (TaggedList);
  rpc GetPosNerTaggedList (Document) returns (TaggedList);
  rpc UpdateNerDict (NerDict) returns (google.protobuf.Empty);
  // NLP3 후처리 사전 업데이트
  rpc UpdatePostNerDict (NerDict) returns (google.protobuf.Empty);
  // NLP3 전처리 사전 업데이트
  rpc UpdatePreNerDict (NerDict) returns (google.protobuf.Empty);
  // NLP3 개체명 태그 변경 사전 업데이트
  rpc UpdatePostChangeNerDict (ChangedNerDict) returns (google.protobuf.Empty);
}


message Text {
  string text = 1;
}

message TextArray {
  string text = 1;
  repeated string sents = 2;
}

service SplitSentenceService {
  rpc Split (Text) returns (TextArray);
}
