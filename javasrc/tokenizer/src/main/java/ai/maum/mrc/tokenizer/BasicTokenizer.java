package ai.maum.mrc.tokenizer;

import java.util.ArrayList;
import java.util.List;


/**
 * Runs basic tokenization (punctuation splitting, lower casing, etc.).
 */
public class BasicTokenizer {

  private static final boolean DEFAULT_DO_LOWER_CASE = false;
  private static final boolean DEFAULT_INCLUDE_NON_BMP_CHARACTER = true;

  private boolean doLowerCase;
  private boolean includeNonBmpCharacter;

  /**
   * Constructs a BasicTokenizer.
   *
   * @param doLowerCase Whether to lower case the input. [default = false]
   */
  public BasicTokenizer(boolean doLowerCase, boolean includeNonBmpCharacter) {
    this.doLowerCase = doLowerCase;
    this.includeNonBmpCharacter = includeNonBmpCharacter;
  }

  public BasicTokenizer() {
    this.doLowerCase = DEFAULT_DO_LOWER_CASE;
    this.includeNonBmpCharacter = DEFAULT_INCLUDE_NON_BMP_CHARACTER;
  }

  public BasicTokenizer setDoLowerCase(boolean value) {
    doLowerCase = value;
    return this;
  }

  public BasicTokenizer setIncludeNonBmpCharacter(boolean value) {
    includeNonBmpCharacter = value;
    return this;
  }

  /**
   * Tokenize a piece of text.
   */
  public List<String> tokenize(String text) {
    text = Utils.cleanText(text, includeNonBmpCharacter);

    // This was added on November 1st, 2018 for the multilingual and Chinese
    // models. This is also applied to the English models now, but it doesn't
    // matter since the English models were not trained on any Chinese data
    // and generally don't have any Chinese data in them (there are Chinese
    // characters in the vocabulary because Wikipedia does have some Chinese
    // words in the English Wikipedia.).
    text = Utils.tokenizeChineseChars(text);

    List<String> origTokens = Utils.whiteSpaceTokenize(text);
    List<String> splitTokens = new ArrayList<>();
    for (String token : origTokens) {
      if (doLowerCase) {
        token = token.toLowerCase();
        token = Utils.runStripAccents(token);
      }
      List<String> splited = Utils.runSplitOnPunctuation(token);
      splitTokens.addAll(splited);
    }

    String reconstructed = String.join(" ", splitTokens);

    return Utils.whiteSpaceTokenize(reconstructed);
  }


}
