package ai.maum.mrc.tokenizer;

import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class Utils {

  /**
   * Checks whether `chars` is a whitespace character.
   */
  public static boolean isWhiteSpace(int codePoint) {
    // \t, \n, and \r are technically control characters
    // but we treat them as whitespace since they are generally considered as such.
    if (codePoint == ' ' || codePoint == '\t' || codePoint == '\n' || codePoint == '\r') {
      return true;
    }

    return Character.getType(codePoint) == Character.SPACE_SEPARATOR;
  }

  /**
   * Checks whether `char` is a control character.
   */
  public static boolean isControl(int codePoint) {
    // These are technically control characters
    // but we count them as whitespace characters.
    if (codePoint == '\t' || codePoint == '\n' || codePoint == '\r') {
      return false;
    }

    int category = Character.getType(codePoint);

    return category == Character.CONTROL || category == Character.FORMAT
        || category == Character.SURROGATE || category == Character.PRIVATE_USE
        || category == Character.UNASSIGNED;
  }

  /**
   * Checks whether `char` is a punctuation character.
   */
  public static boolean isPunctuation(int codePoint) {
    // We treat all non-letter/number ASCII as punctuation.
    // Characters such as "^", "$", and "`" are not in the Unicode
    // Punctuation class but we treat them as punctuation anyways, for
    // consistency.
    if ((codePoint >= 33 && codePoint <= 47)
        || (codePoint >= 58 && codePoint <= 64)
        || (codePoint >= 91 && codePoint <= 96)
        || (codePoint >= 123 && codePoint <= 126)) {
      return true;
    }

    int category = Character.getType(codePoint);
    return (category >= Character.DASH_PUNCTUATION && category <= Character.OTHER_PUNCTUATION)
        || (category >= Character.INITIAL_QUOTE_PUNCTUATION
        && category <= Character.FINAL_QUOTE_PUNCTUATION);
  }

  /**
   * Checks whether CP is the codepoint of a CJK character.
   */
  public static boolean isChineseChar(int codePoint) {
    // This defines a "chinese character" as anything in the CJK Unicode block:
    //   https://en.wikipedia.org/wiki/CJK_Unified_Ideographs_(Unicode_block)
    //
    // Note that the CJK Unicode block is NOT all Japanese and Korean characters,
    // despite its name. The modern Korean Hangul alphabet is a different block,
    // as is Japanese Hiragana and Katakana. Those alphabets are used to write
    // space-separated words, so they are not treated specially and handled
    // like the all of the other languages.
    return (codePoint >= 0x4E00 && codePoint <= 0x9FFF)
        || (codePoint >= 0x3400 && codePoint <= 0x4DBF)
        || (codePoint >= 0x20000 && codePoint <= 0x2A6DF)
        || (codePoint >= 0x2A700 && codePoint <= 0x2B73F)
        || (codePoint >= 0x2B740 && codePoint <= 0x2B81F)
        || (codePoint >= 0x2B820 && codePoint <= 0x2CEAF)
        || (codePoint >= 0xF900 && codePoint <= 0xFAFF)
        || (codePoint >= 0x2F800 && codePoint <= 0x2FA1F);
  }

  /**
   * Checks whether CP is the codepoint of a non-BMP character.
   */
  public static boolean isNonBmpChar(int codePoint) {
    return !Character.isBmpCodePoint(codePoint);
  }

  /**
   * Runs basic whitespace cleaning and splitting on a piece of text.
   */
  public static List<String> whiteSpaceTokenize(String text) {
    text = text.trim();

    if (text.isEmpty()) {
      return new ArrayList<>();
    }

    String[] splited = text.split("\\s+");

    return Arrays.asList(splited);
  }

  /**
   * Performs invalid character removal and whitespace cleanup on text.
   */
  public static String cleanText(String text, boolean includeNonBmpCharacter) {
    StringBuilder cleaned = new StringBuilder();

    int pos = 0;
    int length = text.length();
    while (pos < length) {
      int codePoint = text.codePointAt(pos);
      pos = text.offsetByCodePoints(pos, 1);
      if (codePoint == 0 || codePoint == 0xfffd || isControl(codePoint)) {
        continue;
      }
      if (!includeNonBmpCharacter && isNonBmpChar(codePoint)) {
        continue;
      }
      if (isWhiteSpace(codePoint)) {
        cleaned.append(' ');
      } else {
        cleaned.appendCodePoint(codePoint);
      }
    }

    return cleaned.toString();
  }

  /**
   * Adds whitespace around any CJK character.
   */
  public static String tokenizeChineseChars(String text) {
    StringBuilder tokenized = new StringBuilder();

    int pos = 0;
    int length = text.length();
    while (pos < length) {
      int codePoint = text.codePointAt(pos);
      pos = text.offsetByCodePoints(pos, 1);
      if (isChineseChar(codePoint)) {
        tokenized.append(' ').appendCodePoint(codePoint).append(' ');
      } else {
        tokenized.appendCodePoint(codePoint);
      }
    }

    return tokenized.toString();
  }

  /**
   * Strips accents from a piece of text.
   */
  public static String runStripAccents(String text) {
    StringBuilder stripped = new StringBuilder();

    text = Normalizer.normalize(text, Form.NFD);
    int pos = 0;
    int length = text.length();
    while (pos < length) {
      int codePoint = text.codePointAt(pos);
      pos = text.offsetByCodePoints(pos, 1);
      if (Character.getType(codePoint) == Character.NON_SPACING_MARK) {
        continue;
      }
      stripped.appendCodePoint(codePoint);
    }

    text = stripped.toString();
    text = Normalizer.normalize(text, Form.NFC);

    return text;
  }

  /**
   * Splits punctuation on a piece of text.
   */
  public static List<String> runSplitOnPunctuation(String text) {
    List<String> splited = new ArrayList<>();
    StringBuilder sb = new StringBuilder();

    int pos = 0;
    int length = text.length();
    while (pos < length) {
      int codePoint = text.codePointAt(pos);
      new String(Character.toChars(codePoint));

      if (isPunctuation(codePoint)) {
        if (sb.length() > 0) {
          splited.add(sb.toString());
          sb.setLength(0);
        }
        sb.appendCodePoint(codePoint);
        splited.add(sb.toString());
        sb.setLength(0);
      } else {
        sb.appendCodePoint(codePoint);
      }
      pos = text.offsetByCodePoints(pos, 1);
    }

    if (sb.length() > 0) {
      splited.add(sb.toString());
    }

    return splited;
  }

}
