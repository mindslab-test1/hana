package ai.maum.mrc.tokenizer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;


public class Vocab {

  private final Map<String, Integer> vocab;
  private final Map<Integer, String> invVocab;

  /**
   * Loads a vocabulary file into a dictionary.
   */
  public Vocab(String vocabFile) throws FileNotFoundException {
    vocab = new HashMap<>();
    invVocab = new HashMap<>();

    FileInputStream fis = new FileInputStream(vocabFile);
    Scanner scanner = new Scanner(fis);

    int id = 0;
    while (scanner.hasNext()) {
      String token = scanner.next().trim();
      vocab.put(token, id);
      invVocab.put(id, token);
      id++;
    }

    scanner.close();
  }

  public boolean contains(String token) {
    return vocab.containsKey(token);
  }

  public List<Integer> convertToIds(List<String> tokens) {
    List<Integer> ids = new ArrayList<>();

    for (String token : tokens) {
      ids.add(vocab.get(token));
    }

    return ids;
  }

  public List<String> convertToTokens(List<Integer> ids) {
    List<String> tokens = new ArrayList<>();

    for (int id : ids) {
      tokens.add(invVocab.get(id));
    }

    return tokens;
  }
}
