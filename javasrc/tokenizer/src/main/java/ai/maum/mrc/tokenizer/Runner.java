package ai.maum.mrc.tokenizer;

import java.io.UnsupportedEncodingException;
import org.apache.commons.text.StringEscapeUtils;

public class Runner {

  private static final String TEST_TEXT = "세계적 지질자원의 ‘보고(寶庫)’인 한탄강 일대가 \uD83D\uDCA3 "
      + "국내 네 번째 유네스코 세계지질공원으로 지난 7일 인증을 받았다.\n"
      + "한탄강이 흐르는 경기 포천시 유역 493.24㎢, 연천군 유역 273.65㎢, 강원 철원군 유역 398.72㎢ 등 "
      + "총 1165.61㎢다. 여의도 면적(2.9㎢)의 400배에 달한다.\n"
      + "비둘기낭 폭포, 재인 폭포, 고석정 등 26곳이 지질·문화 명소로 등재됐다. "
      + "앞서 한탄강 일대는 독특한 지질과 지형적 가치로 "
      + "2015년 12월 환경부가 연천, 포천, 철원을 아우르는 1164.74㎢를 국가지질공원으로 인증했다.";
  private static final String TEST_TEXT2 = "| [ROW] SMS 문자통지 서비스 수수료"
      + " | [ROW] 입출금 거래내역 문자통지 서비스 이용료 | [ROW] 정액제(월) | [COL] 금액 | [CON] 800원\n"
      + "| [ROW] SMS 문자통지 서비스 수수료 | [ROW] 입출금 거래내역 문자통지 서비스 이용료 | [ROW] 정액제(월)"
      + " | [COL] 비고 | [CON] 건수 무제한(자세한 사항은 하단 참고)";

//  public static void main(String[] args) throws FileNotFoundException {
//    String text = args.length > 0 ? args[0] : TEST_TEXT;
//
//    System.out.println("text: " + text);
//    System.out.println("length: " + text.length());
//    int i = 0;
//    List<String> tokens = new FullTokenizer("./pysrc/vocab.txt").tokenize(text);
//    System.out.println("\ntokens: " + tokens.size());
//    for (String token : tokens) {
//      System.out.printf("%d [%s]", i, token);
//      i++;
//      System.out.print((i > 0 && i % 10 == 0) ? System.lineSeparator() : " ");
//    }
//    if (i % 10 != 0) {
//      System.out.println();
//    }
//  }

  private static String escapeMs949(String text) throws UnsupportedEncodingException {
    StringBuilder escaping = new StringBuilder();

    int length = text.length();
    int pos = 0;
    int codePoint;
    while (pos < length) {
      codePoint = text.codePointAt(pos);
      String utf8 = new String(Character.toChars(codePoint));
      String ms949 = new String(utf8.getBytes("MS949"), "MS949");
      if (!utf8.equals(ms949)) {
        escaping.append("&#").append(codePoint).append(';');
      } else {
        escaping.appendCodePoint(codePoint);
      }
      pos = text.offsetByCodePoints(pos, 1);
    }

    return escaping.toString();
  }


  public static void main(String[] args) throws UnsupportedEncodingException {
//    String testText = "가\u2013\uD83D\uDCA3나";
    String testText = "–💣전투종족(Martial race)은 \u2013\uD83D\uDCA3 세포이 항쟁 이후 영국령 인도의 군 관계자들이 만든 명칭으로 각 카스트를 \u0027전투\u0027, \u0027비전투\u0027의 두 그룹으로 분류하던 것으로부터 유래하였다.";

    System.out.println("org: " + testText);

//    String escapedText = Normalizer.normalize(testText, Form.NFD);
//    String escapedText = new String(testText.getBytes("EUC-KR"), "EUC-KR");
//    String escapedText = URLEncoder.encode(testText, "UTF-8");
//    String escapedText = StringEscapeUtils.escapeHtml4(testText);
    String escapedText = escapeMs949(testText);
    System.out.println("escaped: " + escapedText);

    String unescapedText = StringEscapeUtils.unescapeHtml4(testText);
    System.out.println("unescaped: " + unescapedText);
    System.out.println("isSame: " + unescapedText.equals(testText));

//    int length = testText.length();
//    int pos = 0;
//    while (pos < length) {
//      int codePoint = testText.codePointAt(pos);
//      System.out.println(new String(Character.toChars(codePoint))
//          + " : " + Character.getName(codePoint)
//          + " " +  Character.UnicodeBlock.of(codePoint).toString());
//
//      pos = testText.offsetByCodePoints(pos, 1);
//    }
  }

}
