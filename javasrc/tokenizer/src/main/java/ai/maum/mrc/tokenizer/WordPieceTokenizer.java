package ai.maum.mrc.tokenizer;

import java.util.ArrayList;
import java.util.List;

/**
 * Runs WordPiece tokenization.
 */
public class WordPieceTokenizer {

  private static final String DEFAULT_UNK_TOKEN = "[UNK]";
  private static final int MAX_INPUT_CHARS_PER_WORD = 200;

  private final Vocab vocab;
  private String unkToken;
  private int maxInputCharsPerWord;

  public WordPieceTokenizer(Vocab vocab, String unkToken, int maxInputCharsPerWord) {
    this.vocab = vocab;
    this.unkToken = unkToken;
    this.maxInputCharsPerWord = maxInputCharsPerWord;
  }

  public WordPieceTokenizer(Vocab vocab) {
    this.vocab = vocab;
    this.unkToken = DEFAULT_UNK_TOKEN;
    this.maxInputCharsPerWord = MAX_INPUT_CHARS_PER_WORD;
  }

  public WordPieceTokenizer setUnkToken(String value) {
    unkToken = value;
    return this;
  }

  public WordPieceTokenizer setMaxInputCharsPerWord(int value) {
    maxInputCharsPerWord = value;
    return this;
  }

  /**
   * Tokenize a piece of text into its word pieces.
   *
   * <p>This uses a greedy longest-match-first algorithm
   * to perform tokenization using the given vocabulary.
   *
   * @param text <p>A single token or whitespace separated tokens.
   * <p> This should have already been passed through `BasicTokenizer`.
   * @return A list of word-piece tokens.
   */
  public List<String> tokenize(String text) {
    List<String> output = new ArrayList<>();
    List<String> subTokens = new ArrayList<>();

    int i = 0;
    List<String> tokens = Utils.whiteSpaceTokenize(text);
    for (String token : tokens) {
      final int len = token.length();
      i++;
      if (token.length() > maxInputCharsPerWord) {
        output.add(unkToken);
        continue;
      }

      boolean isBad = false;
      int start = 0;
      subTokens.clear();
      while (start < len) {
        int end = token.length();
        String curSubString = null;
        while (start < end) {
          String subString = token.substring(start, end);
          if (start > 0) {
            subString = "##" + subString;
          }
          if (vocab.contains(subString)) {
            curSubString = subString;
            break;
          }
          end--;
        }
        if (curSubString == null) {
          isBad = true;
          break;
        }
        subTokens.add(curSubString);
        start = end;
      }

      if (isBad) {
        output.add(unkToken);
      } else {
        output.addAll(subTokens);
      }
    }

    return output;
  }

}
