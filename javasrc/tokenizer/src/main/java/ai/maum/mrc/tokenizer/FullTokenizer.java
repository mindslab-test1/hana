package ai.maum.mrc.tokenizer;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Runs end-to-end tokenziation.
 */
public class FullTokenizer {

  private static final boolean DEFAULT_DO_LOWER_CASE = false;
  private static final boolean DEFAULT_INCLUDE_NON_BMP_CHARACTER = false;
  private static final int DEFAULT_MAX_SEQ_LEN = 0;

  private final Vocab vocab;
  private boolean doLowerCase;
  private boolean includeNonBmpCharacter;
  private int maxSeqLen;
  private final BasicTokenizer basicTokenizer;
  private final WordPieceTokenizer wordPieceTokenizer;

  public FullTokenizer(String vocabFile, boolean doLowerCase, boolean includeNonBmpCharacter,
      int maxSeqLen)
      throws FileNotFoundException {
    this.doLowerCase = doLowerCase;
    this.includeNonBmpCharacter = includeNonBmpCharacter;
    this.maxSeqLen = maxSeqLen;
    vocab = new Vocab(vocabFile);
    basicTokenizer = new BasicTokenizer();
    wordPieceTokenizer = new WordPieceTokenizer(vocab);
  }

  public FullTokenizer(String vocabFile)
      throws FileNotFoundException {
    this.doLowerCase = DEFAULT_DO_LOWER_CASE;
    this.includeNonBmpCharacter = DEFAULT_INCLUDE_NON_BMP_CHARACTER;
    this.maxSeqLen = DEFAULT_MAX_SEQ_LEN;
    vocab = new Vocab(vocabFile);
    basicTokenizer = new BasicTokenizer();
    wordPieceTokenizer = new WordPieceTokenizer(vocab);
  }

  public FullTokenizer setDoLowerCase(boolean value) {
    doLowerCase = value;
    return this;
  }

  public FullTokenizer setIncludeNonBmpCharacter(boolean value) {
    includeNonBmpCharacter = value;
    return this;
  }

  public FullTokenizer setMaxSeqLen(int value) {
    maxSeqLen = value;
    return this;
  }

  public List<String> tokenize(String text) throws NullPointerException {
    List<String> tokens = new ArrayList<>();
    List<String> basicTokens = basicTokenizer
        .setDoLowerCase(doLowerCase)
        .setIncludeNonBmpCharacter(includeNonBmpCharacter)
        .tokenize(text);
    for (String token : basicTokens) {
      List<String> wordPieceTokens = wordPieceTokenizer.tokenize(token);
      for (String subToken : wordPieceTokens) {
        tokens.add(subToken);
      }
      if (maxSeqLen > 0 && tokens.size() >= (maxSeqLen - 2)) {
        System.out.println("Max. Length Exceeded.");
        break;
      }
    }

    return tokens;
  }

  public List<Integer> convertTokensToIds(List<String> tokens) {
    return vocab.convertToIds(tokens);
  }

  public List<String> convertIdsToTokens(List<Integer> ids) {
    return vocab.convertToTokens(ids);
  }
}
