package com.appender;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.ConsoleAppender;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RemoteDbAppender extends ConsoleAppender<ILoggingEvent> {

  private static AppenderServiceImpl service = null;

  @Override
  public void start() {
    super.start();
    log.info("Start RemoteDbAppender...");
    service = new AppenderServiceImpl();
  }

  @Override
  public void doAppend(ILoggingEvent eventObject) {
    super.doAppend(eventObject);

    try {
      Level tempLevel = eventObject.getLevel();

      if (tempLevel == Level.INFO) {
        Object[] argArray = eventObject.getArgumentArray();
        service.putLog(argArray);

      } else {
        log.warn("RemoteDbAppender log level supports only INFO...");
        return;
      }
    } catch (Exception e) {
      log.error("FAILED Log Remote : ", e);
    }
  }
}
