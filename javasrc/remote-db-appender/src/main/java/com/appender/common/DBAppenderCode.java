package com.appender.common;

public class DBAppenderCode {

  // CLAS_RSLT_CD 분류결과 코드
  public static final String CLAS_RSLT_CD_SKILL = "01"; // skill
  public static final String CLAS_RSLT_CD_HINT = "02"; // hint

  // HAI_1_TI_CLAS_CD HAI 1차 분류 코드 Key값
  public static final String HAI_1_TI_CLAS_CD_KEY = "hint1";
  public static final String HAI_2_TI_CLAS_CD_KEY = "hint2";
  public static final String HAI_3_TI_CLAS_CD_KEY = "hint3";

  // MSGTP_DV_CD 메세지 타입 구분 코드
  public static final String MSGTP_DV_CD_TEXT = "0001";
  public static final String MSGTP_DV_CD_SPEECH = "0002";

  // meta data 공용 Key값
  public static final String META_CALL_ID_KEY = "CALL_ID";
  public static final String META_CNSR_UID_KEY = "CNSR_UID";
  public static final String META_HAI_CUST_NO_KEY = "HAI_CUST_NO";
  public static final String META_CUST_NO_KEY = "CUST_NO";
  public static final String META_SPEECH_KEY = "speech";
  public static final String META_CARDS_KEY = "cards";
  public static final String META_UTTER_KEY = "utter";
  public static final String META_SPEECH_UTTER_KEY = "speechUtter";
}
