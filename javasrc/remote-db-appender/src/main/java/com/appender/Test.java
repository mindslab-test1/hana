package com.appender;

import com.appender.dao.InlpClasLogPtclDao;
import com.appender.dao.InlpTalkLogDtlsDao;
import com.appender.dao.InlpTalkLogPtclDao;
import com.appender.dao.MybatisConnectionFactory;
import com.appender.entity.InlpClasLogPtcl;
import com.appender.entity.InlpTalkLogDtls;
import com.appender.entity.InlpTalkLogPtcl;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.awt.image.AreaAveragingScaleFilter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Test {

  public static void main(String[] args) throws Exception {
    int result;
    String grobalId = getUUID().replaceAll("-", "");
    String callId = getUUID().replaceAll("-", "");
    ObjectMapper mapper = new ObjectMapper();

    // HAI 분류로그내역 insert
    InlpClasLogPtclDao inlpClasLogPtclDao = new InlpClasLogPtclDao(
        MybatisConnectionFactory.getSqlSessionFactory());
    InlpClasLogPtcl inlpClasLogPtclParam = new InlpClasLogPtcl();

    inlpClasLogPtclParam.setSESS_ID(getUUID());
    inlpClasLogPtclParam.setGLOB_ID(grobalId);
    inlpClasLogPtclParam.setHAI_SVC_GRP_CD("123");
    inlpClasLogPtclParam.setHAI_CHNL_DV_CD("123");
    inlpClasLogPtclParam.setSKILL_CD("123456");
    inlpClasLogPtclParam.setCALL_ID(callId);
    inlpClasLogPtclParam.setCLAS_RSLT_CD("12");
    inlpClasLogPtclParam.setCLAS_RTO(5.2);
    inlpClasLogPtclParam.setHAI_1_TI_CLAS_CD("12");
    inlpClasLogPtclParam.setHAI_1_TI_CLAS_RTO(5.2);
    inlpClasLogPtclParam.setHAI_2_TI_CLAS_CD("12");
    inlpClasLogPtclParam.setHAI_2_TI_CLAS_RTO(5.2);
    inlpClasLogPtclParam.setHAI_3_TI_CLAS_CD("12");
    inlpClasLogPtclParam.setHAI_3_TI_CLAS_RTO(5.2);
    inlpClasLogPtclParam.setQNA_QUES_CTT("내계좌 잔액조회 해주세요");
    inlpClasLogPtclParam.setQNA_QUES_CTT_POS_TAG("QNA_QUES_CTT_POS_TAG1");
    inlpClasLogPtclParam.setQNA_QUES_CTT_DESC_NM("QNA_QUES_CTT_DESC_NM1");
    inlpClasLogPtclParam.setREG_EMP_NO("1234567");
    inlpClasLogPtclParam.setUPD_EMP_NO("1234567");

    ArrayList<InlpClasLogPtcl> clasLogs = new ArrayList<InlpClasLogPtcl>();
    clasLogs.add(inlpClasLogPtclParam);

    result = inlpClasLogPtclDao.insert(clasLogs);

    if (result > 0) {
      log.debug("Success insert InlpClasLogPtcl cnt :: {}", result);
    } else {
      log.debug("Fail insert InlpClasLogPtcl ");
    }

    // HAI 대화로그내역 insert
    InlpTalkLogPtclDao inlpTalkLogPtclDao = new InlpTalkLogPtclDao(
        MybatisConnectionFactory.getSqlSessionFactory());
    InlpTalkLogPtcl inlpTalkLogPtclParam = new InlpTalkLogPtcl();

    inlpTalkLogPtclParam.setSESS_ID(getUUID());
    inlpTalkLogPtclParam.setGLOB_ID(grobalId);
    inlpTalkLogPtclParam.setTALK_STR_DRTM("2018-06-15 00:00:00:0000");
    inlpTalkLogPtclParam.setHAI_SVC_GRP_CD("123");
    inlpTalkLogPtclParam.setHAI_CHNL_DV_CD("123");
    inlpTalkLogPtclParam.setSKILL_CD("123456");
    inlpTalkLogPtclParam.setCALL_ID(callId);
    inlpTalkLogPtclParam.setCNSR_UID("0123456789");
    inlpTalkLogPtclParam.setHAI_CUST_NO("0123456789");
    inlpTalkLogPtclParam.setCUST_NO("0123456789");
    inlpTalkLogPtclParam.setQNA_QUES_CTT("내계좌 잔액조회 해주세요");
    inlpTalkLogPtclParam.setQNA_ANSW_CTT("홍길동님의 계좌의 잔액은 1,000,000,000원 입니다");
    inlpTalkLogPtclParam.setCPLT_YN("Y");
    inlpTalkLogPtclParam.setREG_EMP_NO("1234567");
    inlpTalkLogPtclParam.setUPD_EMP_NO("1234567");

    ArrayList<InlpTalkLogPtcl> talkLogs = new ArrayList<InlpTalkLogPtcl>();
    talkLogs.add(inlpTalkLogPtclParam);

    result = inlpTalkLogPtclDao.insert(talkLogs);

    if (result > 0) {
      log.debug("Success insert InlpTalkLogPtcl cnt :: {}", result);
    } else {
      log.debug("Fail insert InlpTalkLogPtcl ");
    }

    // HAI 대화로그상세 insert
    InlpTalkLogDtlsDao InlpTalkLogDtlsDao = new InlpTalkLogDtlsDao(
        MybatisConnectionFactory.getSqlSessionFactory());
    InlpTalkLogDtls inlpTalkLogDtlsParam = new InlpTalkLogDtls();

    inlpTalkLogDtlsParam.setSESS_ID(getUUID());
    inlpTalkLogDtlsParam.setGLOB_ID(grobalId);
    inlpTalkLogDtlsParam.setTALK_SEQ_NO(123);
    inlpTalkLogDtlsParam.setHAI_SVC_GRP_CD("123");
    inlpTalkLogDtlsParam.setHAI_CHNL_DV_CD("123");
    inlpTalkLogDtlsParam.setSKILL_CD("123456");
    inlpTalkLogDtlsParam.setCALL_ID(callId);
    inlpTalkLogDtlsParam.setMSGTP_DV_CD("1234");
    inlpTalkLogDtlsParam.setQNA_ANSW_CTT("홍길동님의 계좌의 잔액은 1,000,000,000원 입니다.");
    inlpTalkLogDtlsParam.setQNA_ANSW_CTT_DE_TAG("QNA_ANSW_CTT_DE_TAG1");
    inlpTalkLogDtlsParam.setREG_EMP_NO("1234567");
    inlpTalkLogDtlsParam.setUPD_EMP_NO("1234567");

    ArrayList<InlpTalkLogDtls> talkDLogs = new ArrayList<InlpTalkLogDtls>();
    talkDLogs.add(inlpTalkLogDtlsParam);
    result = InlpTalkLogDtlsDao.insert(talkDLogs);

    if (result > 0) {
      log.debug("Success insert InlpTalkLogDtls cnt :: {}", result);
    } else {
      log.debug("Fail insert InlpTalkLogDtls ");
    }


  }

  private static String getUUID() {
    return UUID.randomUUID().toString();
  }

}
