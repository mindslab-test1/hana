package com.appender;

import com.appender.common.DBAppenderCode;
import com.appender.dao.InlpClasLogPtclDao;
import com.appender.dao.InlpTalkLogDtlsDao;
import com.appender.dao.InlpTalkLogPtclDao;
import com.appender.dao.MybatisConnectionFactory;
import com.appender.entity.InlpClasLogPtcl;
import com.appender.entity.InlpTalkLogDtls;
import com.appender.entity.InlpTalkLogPtcl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.protobuf.Struct;
import com.google.protobuf.Value;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import maum.m2u.router.v3.Intentfinder.ClassificationRecord;
import maum.m2u.router.v3.Intentfinder.FoundIntent;
import maum.m2u.router.v3.Intentfinder.IntentFindingHistory.RunResult;
import maum.m2u.router.v3.Session.DialogSession;
import maum.m2u.router.v3.Session.SessionTalk;


@Slf4j
public class AppenderServiceImpl implements AppenderService {

  private InlpClasLogPtclDao inlpClasLogPtclDao = null;
  private InlpTalkLogDtlsDao inlpTalkLogDtlsDao = null;
  private InlpTalkLogPtclDao inlpTalkLogPtclDao = null;
  private static ObjectMapper mapper = new ObjectMapper();

  AppenderServiceImpl() {
    inlpClasLogPtclDao = new InlpClasLogPtclDao(MybatisConnectionFactory.getSqlSessionFactory());
    inlpTalkLogDtlsDao = new InlpTalkLogDtlsDao(MybatisConnectionFactory.getSqlSessionFactory());
    inlpTalkLogPtclDao = new InlpTalkLogPtclDao(MybatisConnectionFactory.getSqlSessionFactory());
  }

  @Override
  public void putLog(Object[] argArray) throws Exception {
    if (argArray == null) {
      throw new Exception("RemoteDbAppender Log Args Null!!");
    } else if (argArray.length < 3) {
      throw new Exception(
          "RemoteDbAppender Log Args Not enough!! [Arg Size :: " + argArray.length + "]");
    } else {

      try {
        DialogSession session = (DialogSession) argArray[0];
        List<SessionTalk> talks = (List<SessionTalk>) argArray[1];
        List<ClassificationRecord> records = (List<ClassificationRecord>) argArray[2];

        insertClasLog(session, talks, records);
        insertTalkLog(session, talks);
      } catch (Exception e) {
        throw e;
      }
    }
  }

  private void insertTalkLog(DialogSession session, List<SessionTalk> talks) {
    if (session == null) {
      log.warn("Parameter is null!!");
    } else {

      try {

        ArrayList<InlpTalkLogPtcl> ptclParams = new ArrayList<InlpTalkLogPtcl>();
        ArrayList<InlpTalkLogDtls> dtlsparams = new ArrayList<InlpTalkLogDtls>();

        for (SessionTalk talk : talks) {
          InlpTalkLogPtcl ptcl = new InlpTalkLogPtcl();
          Struct inputMeta = talk.getInputMeta();
          ArrayList<SessionTalk> tempList = new ArrayList<SessionTalk>();
          tempList.add(talk);

          String sessionId = Long.toString(talk.getSessionId());
          String operatonSyncId = talk.getOperationSyncId();
          String chatbot = session.getChatbot();
          String channel = session.getChannel();
          String skill = talk.getSkill();
          String callId = findCallId(tempList, talk.getOperationSyncId());

          ptcl.setSESS_ID(sessionId);
          ptcl.setGLOB_ID(operatonSyncId);
          ptcl.setTALK_STR_DRTM(talk.getStart().toString());
          ptcl.setHAI_SVC_GRP_CD(chatbot);
          ptcl.setHAI_CHNL_DV_CD(channel);
          ptcl.setSKILL_CD(skill);
          ptcl.setCALL_ID(callId);
          ptcl.setCNSR_UID(inputMeta
              .getFieldsOrDefault(DBAppenderCode.META_CNSR_UID_KEY, Value.newBuilder().build())
              .getStringValue());
          ptcl.setHAI_CUST_NO(
              inputMeta.getFieldsOrDefault(DBAppenderCode.META_HAI_CUST_NO_KEY,
                  Value.newBuilder().build())
                  .getStringValue());
          ptcl.setCUST_NO(inputMeta
              .getFieldsOrDefault(DBAppenderCode.META_CUST_NO_KEY, Value.newBuilder().build())
              .getStringValue());
          ptcl.setQNA_QUES_CTT(talk.getIn());
          ptcl.setQNA_ANSW_CTT(talk.getOut());
          ptcl.setCPLT_YN(talk.getCloseSkill() ? "Y" : "N");

          ptclParams.add(ptcl);

          // detail card 갯수만큼
          HashMap<String, Object> map =
              new ObjectMapper().readValue(talk.getExtraOut(), HashMap.class);

          List<Object> cards = (List) map.get(DBAppenderCode.META_CARDS_KEY);
          HashMap<String, Object> speech = (HashMap<String, Object>) map
              .get(DBAppenderCode.META_SPEECH_KEY);
          String msgtDvCd = "".equals(speech.get(DBAppenderCode.META_SPEECH_UTTER_KEY))
              ? DBAppenderCode.MSGTP_DV_CD_TEXT : DBAppenderCode.MSGTP_DV_CD_SPEECH;
          int idx = 0;

          for (Object card : cards) {
            InlpTalkLogDtls dtls = new InlpTalkLogDtls();
            HashMap<String, Object> cardMap = (HashMap<String, Object>) card;

            dtls.setSESS_ID(sessionId);
            dtls.setGLOB_ID(talk.getOperationSyncId());
            dtls.setTALK_SEQ_NO(idx);
            dtls.setHAI_SVC_GRP_CD(chatbot);
            dtls.setHAI_CHNL_DV_CD(channel);
            dtls.setSKILL_CD(skill);
            dtls.setCALL_ID(callId);
            dtls.setMSGTP_DV_CD(msgtDvCd);
            dtls.setQNA_ANSW_CTT((String) cardMap.get(DBAppenderCode.META_UTTER_KEY));

            dtlsparams.add(dtls);
            idx++;
          }
        }

        log.debug("@# InlpTalkLogPtcl cnt :: {}", ptclParams.size());
        log.debug("@# InlpTalkLogDtls cnt :: {}", dtlsparams.size());

        inlpTalkLogPtclDao.insert(ptclParams);
        inlpTalkLogDtlsDao.insert(dtlsparams);

      } catch (Exception e) {
        e.getMessage();
      }
    }
  }

  /**
   * 분류결과 로그 insert
   */
  private void insertClasLog(DialogSession session, List<SessionTalk> talks,
      List<ClassificationRecord> records) {
    if (session == null || records == null || records.size() < 1) {
      log.info("Parameter is null!!");
    } else {
      try {
        ArrayList<InlpClasLogPtcl> params = new ArrayList<InlpClasLogPtcl>();

        for (ClassificationRecord record : records) {
          InlpClasLogPtcl inlpClasLogPtcl = new InlpClasLogPtcl();

          inlpClasLogPtcl.setSESS_ID(Long.toString(record.getSessionId()));
          inlpClasLogPtcl.setGLOB_ID(record.getOperationSyncId());
          inlpClasLogPtcl.setHAI_SVC_GRP_CD(record.getChatbot());
          inlpClasLogPtcl.setHAI_CHNL_DV_CD(session.getChannel());
          inlpClasLogPtcl.setCALL_ID(findCallId(talks, record.getOperationSyncId()));
          if (record.hasSkill()) {
            FoundIntent skill = record.getSkill();
            List<RunResult> history = skill.getHistory().getRunResultsList();

            inlpClasLogPtcl.setSKILL_CD(skill.getSkill());
            inlpClasLogPtcl.setCLAS_RSLT_CD(
                skill.getHintsCount() > 0 ? DBAppenderCode.CLAS_RSLT_CD_HINT
                    : DBAppenderCode.CLAS_RSLT_CD_SKILL);
            inlpClasLogPtcl.setCLAS_RTO(skill.getProbability());
            inlpClasLogPtcl.setHAI_1_TI_CLAS_CD(
                skill.getHintsOrDefault(DBAppenderCode.HAI_1_TI_CLAS_CD_KEY, ""));
            inlpClasLogPtcl.setHAI_1_TI_CLAS_RTO(
                findProbablility(history, DBAppenderCode.HAI_1_TI_CLAS_CD_KEY));
            inlpClasLogPtcl.setHAI_2_TI_CLAS_CD(
                skill.getHintsOrDefault(DBAppenderCode.HAI_2_TI_CLAS_CD_KEY, ""));
            inlpClasLogPtcl.setHAI_2_TI_CLAS_RTO(
                findProbablility(history, DBAppenderCode.HAI_2_TI_CLAS_CD_KEY));
            inlpClasLogPtcl.setHAI_3_TI_CLAS_CD(
                skill.getHintsOrDefault(DBAppenderCode.HAI_3_TI_CLAS_CD_KEY, ""));
            inlpClasLogPtcl.setHAI_3_TI_CLAS_RTO(
                findProbablility(history, DBAppenderCode.HAI_3_TI_CLAS_CD_KEY));

          }
          inlpClasLogPtcl.setQNA_QUES_CTT(record.getUtter());
        }

        log.debug("@# InlpClasLogPtcl cnt :: {}", params.size());
        inlpClasLogPtclDao.insert(params);

      } catch (Exception e) {
        log.warn(e.getMessage());
      }
    }
  }

  /**
   * talk에 meta로 넘어오는 callId를 operation_sync_id로 찾는다
   */
  private String findCallId(List<SessionTalk> talks, String globalId) {
    String resultId = "";

    if (talks.size() < 1 || "".equals(globalId)) {
      log.warn("Parameter is null!!");
      return resultId;
    } else {
      try {
        for (SessionTalk talk : talks) {
          if (globalId.equals(talk.getOperationSyncId())) {
            String extraOutStr = talk.getExtraOut();
            HashMap<String, Object> map = mapper.readValue(extraOutStr, HashMap.class);
            // 실제로 meta에 callId가 어떤 key값으로 들어오는지 확인필요함
            resultId = (String) ((HashMap<String, Object>) map.get("meta"))
                .get(DBAppenderCode.META_CALL_ID_KEY);
            break;
          }
        }
      } catch (IOException e) {
        log.warn(e.getMessage());
      } catch (Exception e) {
        log.warn(e.getMessage());
      } finally {
        return resultId;
      }
    }
  }

  /**
   * hintName을 가지고 history에서 Probablility를 찾는다
   */
  private float findProbablility(List<RunResult> runResults, String hintName) {
    float result = 1;

    if (runResults == null || runResults.size() < 1 || "".equals(hintName)) {
      log.warn("Parameter is null!!");
      return result;
    } else {
      try {
        for (RunResult runResult : runResults) {
          if (hintName.equals(runResult.getHint())) {
            result = runResult.getProbability();
          }
        }
      } catch (Exception e) {
        log.warn(e.getMessage());
      } finally {
        return result;
      }
    }
  }
}
