package com.appender.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
// HAI 분류로그생성
// message ClassificationRecord
public class InlpClasLogPtcl {

  private String SESS_ID; // 세션 ID [session_id]
  private String GLOB_ID; // 글로벌 ID [operaton_sync_id]
  private String HAI_SVC_GRP_CD; // HAI 서비스 그룹 코드 [chatbot]
  private String HAI_CHNL_DV_CD; // HAI 채널 구분 코드 [param channel]
  private String SKILL_CD; // 스킬 코드 [classified.hasSkill().skill] 있을때만 셋팅
  private String CALL_ID; // 콜 ID [talk.extraOut.meta.callId]
  private String CLAS_RSLT_CD; // 분류 결과 코드 [classified.hasSkill().skill.hintsSize] hints가 있으면 02 없으면 01
  private double CLAS_RTO; // 분류 비율 [disable]
  private String HAI_1_TI_CLAS_CD; // HAI 1차 분류 코드 [hasSkill().hints<hint1>]
  private double HAI_1_TI_CLAS_RTO; // HAI 1차 분류 비율 [hasSkill().history.run_results.hint]
  private String HAI_2_TI_CLAS_CD; // HAI 2차 분류 코드 [hasSkill().hints<hint2>]
  private double HAI_2_TI_CLAS_RTO; // HAI 2차 분류 비율 [hasSkill().history.run_results.hint]
  private String HAI_3_TI_CLAS_CD; // HAI 3차 분류 코드 [hasSkill().hints<hint3>]
  private double HAI_3_TI_CLAS_RTO; // HAI 3차 분류 비율 [hasSkill().history.run_results.hint]
  private String QNA_QUES_CTT; // QNA 질문내용 [utter]
  private String QNA_QUES_CTT_POS_TAG; // QNA 질문내용 Pos Tagged [disable]
  private String QNA_QUES_CTT_DESC_NM; // QNA 질문내용 Named Entity [disable]
  private String REG_EMP_NO; // 등록 직원 번호
  private String SYS_REG_DTM; // 시스템 등록 일시
  private String UPD_EMP_NO; // 변경 직원 번호
  private String SYS_UPD_DTM; // 시스템 변경 일시
}
