package com.appender.entity;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import oracle.sql.CHAR;

@Getter
@Setter
@ToString
// HAI 대화로그내역
// message SessionTalk
public class InlpTalkLogPtcl {

  private String SESS_ID; // 세션 ID [session_id]
  private String GLOB_ID; // 글로벌 ID [session_id]
  private String TALK_STR_DRTM; // 대화 시작 시간 [started_at]
  private String HAI_SVC_GRP_CD; // HAI 서비스 그룹 코드 [session.chatbot]
  private String HAI_CHNL_DV_CD; // HAI 채널 구분 코드 [session.channel]
  private String SKILL_CD; // 스킬 코드 [skill]
  private String CALL_ID; // 콜 ID [input_meta.CALL_ID]
  private String CNSR_UID; // 상담원 사용자 ID [input_meta.CNSR_UID]
  private String HAI_CUST_NO; // HAI 고객 번호 [input_meta.HAI_CUST_NO]
  private String CUST_NO; // 고객 번호 [input_meta.CUST_NO]
  private String QNA_QUES_CTT; // QNA 질문 내용 [in]
  private String QNA_ANSW_CTT; // QNA 답변 내용 [out]
  private String CPLT_YN; // 완료 여부 [close_skill]
  private String REG_EMP_NO; // 등록 직원 번호
  private String SYS_REG_DTM; // 시스템 등록 일시
  private String UPD_EMP_NO; // 변경 직원 번호
  private String SYS_UPD_DTM; // 시스템 변경 일시
}
