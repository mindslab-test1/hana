package com.appender.entity;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
// HAI 대화로그상세
// message SessionTalk
// extra_out의 갯수만큼 생성된다
public class InlpTalkLogDtls {

  private String SESS_ID; // 세션 ID [session_id]
  private String GLOB_ID; // 글로벌 ID [operation_sync_id]
  private Integer TALK_SEQ_NO; // 대화 일련 번호 [extra_out.idx]
  private String HAI_SVC_GRP_CD; // HAI 서비스 그룹 코드 [session.chatbot]
  private String HAI_CHNL_DV_CD; // HAI 채널 구분 코드 [session.channel]
  private String SKILL_CD; // 스킬 코드 [talk.skill]
  private String CALL_ID; // 콜 ID [input_meta.CALL_ID]
  private String MSGTP_DV_CD; // 메세지 타입 구분 코드 [extra_out.cards[0].typeCode]
  private String QNA_ANSW_CTT; // QNA 답변 내용 [extra_out.cards[0]]
  private String QNA_ANSW_CTT_DE_TAG; // QNA 답변 내용 DETAG *disable
  private String REG_EMP_NO; // 등록 직원 번호
  private String SYS_REG_DTM; // 시스템 등록 일시
  private String UPD_EMP_NO; // 변경 직원 번호
  private String SYS_UPD_DTM; // 시스템 변경 일시
}
