package com.appender.dao;

import com.appender.entity.InlpClasLogPtcl;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

@Slf4j
public class InlpClasLogPtclDao {

  private SqlSessionFactory sqlSessionFactory = null;

  public InlpClasLogPtclDao(SqlSessionFactory sqlSessionFactory) {
    this.sqlSessionFactory = sqlSessionFactory;
  }

  public int insert(ArrayList<InlpClasLogPtcl> inlpClasLogPtcls) {
    int id = -1;
    SqlSession session = sqlSessionFactory.openSession();

    try {
      HashMap<String, ArrayList<InlpClasLogPtcl>> paramMap = new HashMap();
      paramMap.put("list", inlpClasLogPtcls);
      id = session.insert("inlpClasLogPtclMapper.insert", paramMap);
    } finally {
      session.commit();
      session.close();
    }

    return id;
  }

}
