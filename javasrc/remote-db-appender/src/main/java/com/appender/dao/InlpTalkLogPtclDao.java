package com.appender.dao;

import com.appender.entity.InlpTalkLogPtcl;
import java.util.ArrayList;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

@Slf4j
public class InlpTalkLogPtclDao {

  private SqlSessionFactory sqlSessionFactory = null;

  public InlpTalkLogPtclDao(SqlSessionFactory sqlSessionFactory) {
    this.sqlSessionFactory = sqlSessionFactory;
  }

  public int insert(ArrayList<InlpTalkLogPtcl> inlpTalkLogPtcls) {
    int id = -1;
    SqlSession session = sqlSessionFactory.openSession();

    try {
      HashMap<String, ArrayList<InlpTalkLogPtcl>> paramMap = new HashMap();
      paramMap.put("list", inlpTalkLogPtcls);
      id = session.insert("inlpTalkLogPtclMapper.insert", paramMap);

    } finally {
      session.commit();
      session.close();
    }

    return id;
  }
}
