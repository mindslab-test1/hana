package com.appender.dao;

import com.appender.entity.InlpTalkLogDtls;
import java.util.ArrayList;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

@Slf4j
public class InlpTalkLogDtlsDao {

  private SqlSessionFactory sqlSessionFactory = null;

  public InlpTalkLogDtlsDao(SqlSessionFactory sqlSessionFactory) {
    this.sqlSessionFactory = sqlSessionFactory;
  }

  public int insert(ArrayList<InlpTalkLogDtls> inlpTalkLogDtlss) {
    int id = -1;
    SqlSession session = sqlSessionFactory.openSession();

    try {
      HashMap<String, ArrayList<InlpTalkLogDtls>> paramMap = new HashMap();
      paramMap.put("list", inlpTalkLogDtlss);
      id = session.insert("inlpTalkLogDtlsMapper.insert", paramMap);
    } finally {
      session.commit();
      session.close();
    }

    return id;
  }
}
