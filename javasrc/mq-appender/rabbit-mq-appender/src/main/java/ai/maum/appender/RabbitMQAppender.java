package ai.maum.appender;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.ConsoleAppender;

public class RabbitMQAppender extends ConsoleAppender<ILoggingEvent> {

  private static RabbitMQClient client = null;
  //private static Channel channel = null;

  @Override
  public void start() {
    super.start();
    System.out.println("#@ start set rabbitMQ");

    // RabbitMQClient 객체 생성
    client = new RabbitMQClient();
  }

  @Override
  public void doAppend(ILoggingEvent eventObject) {
    super.doAppend(eventObject);

    Level tempLevel = eventObject.getLevel();

    // WARN, ERROR 인 경우만 rabbitMQ 요청을 보낸다
    if (tempLevel == Level.ERROR || tempLevel == Level.WARN) {
      System.out.println(
          "#@ START doAppend Level :" + tempLevel + ", Msg :" + eventObject
              .getMessage());

      // TODO 메시지 규격은??
      client.pushDataToQueue(eventObject.getMessage().getBytes());

    } else {
      // ERROR, WARN 이 아닌 경우
      return;
    }
  }
}

