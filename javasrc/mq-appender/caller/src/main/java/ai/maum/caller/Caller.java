package ai.maum.caller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Caller {

  private static final Logger logger = LoggerFactory.getLogger(Caller.class);

  public static void main(String[] args) {
    int count = 0;

    while (true) {
      ++count;

      logger.error("#@ Test MQ Queue Count is :" +  count);

      try {
        Thread.sleep(3000);
      } catch (Exception e) {
      }

      if (count == 5) {
        break;
      }
    }
  }
}
