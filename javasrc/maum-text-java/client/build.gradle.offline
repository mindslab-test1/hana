group "ai.maum"

buildscript {
  repositories {
    maven { url new File(project.projectDir, "./libs/plugins") }
  }
  dependencies {
    // ASSUMES GRADLE 2.12 OR HIGHER. Use plugin version 0.7.5 with earlier
    // gradle versions
    classpath "com.google.protobuf:protobuf-gradle-plugin:0.8.3"
    classpath "com.github.jengelman.gradle.plugins:shadow:2.0.1"
    classpath "com.google.guava:guava:18.0"
    classpath "com.google.gradle:osdetector-gradle-plugin:1.4.0"
    classpath "commons-lang:commons-lang:2.6"
    classpath "org.codehaus.groovy:groovy-backports-compat23:2.4.4"
    classpath "kr.motd.maven:os-maven-plugin:1.4.0.Final"
    classpath "org.codehaus.plexus:plexus-utils:3.0.17"
    classpath "org.eclipse.sisu:org.eclipse.sisu.plexus:0.0.0.M5"
    classpath "javax.enterprise:cdi-api:1.0"
    classpath "org.sonatype.sisu:sisu-guice:3.1.0"
    classpath "org.eclipse.sisu:org.eclipse.sisu.inject:0.0.0.M5"
    classpath "org.codehaus.plexus:plexus-component-annotations:1.5.5"
    classpath "org.codehaus.plexus:plexus-classworlds:2.4"
    classpath "javax.annotation:jsr250-api:1.0"
    classpath "javax.inject:javax.inject:1"
    classpath "aopalliance:aopalliance:1.0"
  }
}

repositories {
  maven { url new File(project.projectDir, "./libs/plugins") }
}


apply plugin: "java"

compileJava.options.encoding = "UTF-8"

// IMPORTANT: You probably want the non-SNAPSHOT version of gRPC. Make sure you
// are looking at a tagged version of the example and not "master"!

// Feel free to delete the comment at the next line. It is just for safely
// updating the version in our release process.

dependencies {
  compile fileTree(dir: "./libs", includes: [
    "grpc-stub-1.8.0.jar",
    "grpc-protobuf-1.8.0.jar",
    "grpc-netty-1.8.0.jar",
    "HikariCP-2.7.3.jar",
    "retrofit-2.3.0.jar",
    "converter-gson-2.3.0.jar",
    "commons-configuration-1.10.jar",
    "commons-collections-3.2.2.jar",
    "mybatis-3.4.5.jar",
    "mysql-connector-java-6.0.6.jar",
    "slf4j-api-1.7.25.jar",
    "logback-classic-1.2.3.jar",
    "xmlrpc-client-3.1.3.jar",
    "grpc-core-1.8.0.jar",
    "protobuf-java-3.4.0.jar",
    "guava-19.0.jar",
    "protobuf-java-util-3.4.0.jar",
    "proto-google-common-protos-0.1.9.jar",
    "grpc-protobuf-lite-1.8.0.jar",
    "netty-codec-http2-4.1.16.Final.jar",
    "netty-handler-proxy-4.1.16.Final.jar",
    "okhttp-3.8.0.jar",
    "gson-2.7.jar",
    "logback-core-1.2.3.jar",
    "xmlrpc-common-3.1.3.jar",
    "grpc-context-1.8.0.jar",
    "error_prone_annotations-2.0.19.jar",
    "jsr305-3.0.0.jar",
//    "instrumentation-api-0.4.3.jar",
    "opencensus-api-0.8.0.jar",
    "opencensus-contrib-grpc-metrics-0.8.0.jar",
    "netty-codec-http-4.1.16.Final.jar",
    "netty-handler-4.1.16.Final.jar",
    "netty-transport-4.1.16.Final.jar",
    "netty-codec-socks-4.1.16.Final.jar",
    "okio-1.13.0.jar",
    "ws-commons-util-1.0.2.jar",
    "netty-codec-4.1.16.Final.jar",
    "netty-buffer-4.1.16.Final.jar",
    "netty-resolver-4.1.16.Final.jar",
    "junit-3.8.1.jar",
    "xml-apis-1.0.b2.jar",
    "netty-common-4.1.16.Final.jar",
    "commons-logging-1.1.1.jar",
  ])
  compile fileTree(dir: "./libs/plugins/commons-lang", includes: [
    "**/commons-lang-2.6.jar"
  ])
  compile fileTree(dir: "./libs/oracle", includes: [
    "ojdbc8.jar"
  ])

  testCompile fileTree(dir: "./libs", includes: [
    "junit-4.12.jar",
    "mockito-core-2.12.0.jar",
    "hamcrest-core-1.3.jar",
    "byte-buddy-1.7.9.jar",
    "byte-buddy-agent-1.7.9.jar",
    "objenesis-2.6.jar",
  ])
}

apply plugin: "com.google.protobuf"

protobuf {
  protoc {
    artifact = 'com.google.protobuf:protoc:3.5.0'
  }
  plugins {
    grpc {
      artifact = "io.grpc:protoc-gen-grpc-java:1.8.0"
    }
  }
  generateProtoTasks {
    all()*.plugins {
      grpc {
        // To generate deprecated interfaces and static bindService method,
        // turn the enable_deprecated option to true below:
        option 'enable_deprecated=false'
      }
    }
  }
}

// Inform IntelliJ projects about the generated code.
apply plugin: "idea"

idea {
  module {
    // Not using generatedSourceDirs because of
    // https://discuss.gradle.org/t/support-for-intellij-2016/15294/8
    sourceDirs += file("${projectDir}/build/generated/source/proto/main/java");
    sourceDirs += file("${projectDir}/build/generated/source/proto/main/grpc");
  }
}

apply plugin: "eclipse"

// Provide convenience executables for trying out the examples.
apply plugin: "application"
apply plugin: "com.github.johnrengelman.shadow"

def appName = "dialog-trigger"

sourceCompatibility = 1.8
targetCompatibility = 1.8
startScripts.enabled = false
mainClassName = "ai.maum.trigger.TriggerServer"

jar {
  manifest {
    attributes "Title": appName, "Main-Class": mainClassName,
      "Version": "1.0.0", "BuildTime": new Date().format("yyyy-MM-dd HH:mm:ss")
  }
  archiveName appName + ".jar"
  dependsOn configurations.runtime
  from {
    configurations.compile.collect { it.isDirectory() ? it : zipTree(it) }
  }
}