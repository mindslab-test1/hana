package ai.maum.utils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import ai.maum.preference.PreferenceHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Spacing {
  private static final Logger logger = LoggerFactory.getLogger(Spacing.class);
  private static volatile Map<String, String> PATTERN = null;

  static {
    init(PreferenceHelper.getStringKeyValue(Context.NullContext, "spacing.pattern.file"));
  }

  private static void init(String filePath) {
    BufferedReader reader = null;
    try {
      if (filePath == null || filePath.isEmpty())
        return;
      reader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "UTF-8"));
      Map<String, String> pattern = new HashMap<>();
      String line = null;
      while ((line = reader.readLine()) != null) {
        String[] temp = line.split(" -> ");
        if (temp.length == 2) {
          String key = temp[0];
          String value = temp[1];
          pattern.put(key, value);
        }
      }
      if (pattern.size() >= 0) {
        PATTERN = pattern;
      }
    } catch (Exception e) {
      logger.error(Utils.getStackTrace(e));
    } finally {
      if (reader != null) {
        try {
          reader.close();
        } catch (Exception e) {
          logger.error(Utils.getStackTrace(e));
        }
      }
    }
  }

  public static String doSpace(String text) {
    if (text == null)
      return "";
    if (PATTERN != null && !text.isEmpty()) {
      Set<String> keys = PATTERN.keySet();
      for (String key : keys) {
        if (text.contains(key)) {
          text = text.replaceAll(key, PATTERN.get(key));
        }
      }
    }
    return text;
  }

  public static void prepare() {
    doSpace("");
  }
}
