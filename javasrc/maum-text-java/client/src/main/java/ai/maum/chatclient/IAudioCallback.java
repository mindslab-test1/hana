package ai.maum.chatclient;

import com.google.protobuf.ByteString;

/**
 * AudioTalk interface.
 */
public interface IAudioCallback {
  /**
   * Audiotalk repsone.
   *
   * @param utter audio utter data.
   */
  void onAudio(ByteString utter);

  /**
   * Audiotalk error
   *
   * @param message error message.
   */
  void onAudioError(String message);

  /**
   * Audiotalk respone end.
   */
  void onAudioEnd();
}
