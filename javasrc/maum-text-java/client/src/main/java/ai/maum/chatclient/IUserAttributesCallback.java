package ai.maum.chatclient;

/*
 * Created by yjhwang on 2016-12-19.
 */

import maum.m2u.facade.Dialog;

/**
 * Userattributes callback interface
 */
public interface IUserAttributesCallback {
  /**
   * Get user attributes
   *
   * @param value user attributes value.
   */
  void onGetUserAttributes(Dialog.ChatbotUserAttributes value);

  /**
   * Error.
   *
   * @param message
   */
  void onError(String message);
}
