package ai.maum.chatclient;

import ai.maum.chatclient.RestDialog.Caller;
import ai.maum.chatclient.RestDialog.ReceiveTextUtter;
import ai.maum.chatclient.RestDialog.SendTextUtter;
import ai.maum.chatclient.RestDialog.Session;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RestDialogService {
  @POST("chatbot/{chatbot}/session")
  Call<Session> openWithAuth(
      @Path("chatbot") String chatbot,
      @Query("_chatbot") String _chatbot,
      @Body Caller caller);

  @POST("session/{session_id}/talk")
  Call<ReceiveTextUtter> textTalk(
      @Path("session_id") String sessionId,
      @Body SendTextUtter textUtter);

}
