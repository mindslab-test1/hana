package ai.maum.preference;

import java.util.HashMap;
import java.util.Map;

import ai.maum.utils.Context;

/*
 * Created by yjhwang on 2016-12-27.
 */
public class PreferenceHelper {
  private static final Map<String, Object> preferences = new HashMap<>();

  // �솚寃쎌꽕�젙 ���옣 �뙆�씪
  public static final String AI_CLOUD_CHATAPP_PREFERENCES = "ai_cloud_chatapp_preferences";
  // Signin remeber me checked
  public static final String KEY_SIGNIN_REMEMBER_ME_CHECKED = "signin_rembember_me_checked";
  // Prev Signin method
  public static final String KEY_SIGNIN_REMEMBER_ME_LASTEST_METHOD = "signin_last_success_method";
  // Signin input field id
  public static final String KEY_SIGNIN_REMEMBER_ME_ID = "signin_rembember_me_id";
  // Signin input field password
  public static final String KEY_SIGNIN_REMEMBER_ME_PASSWORD = "signin_rembember_me_password";
  // GPS enable next question disable checked.
  public static final String KEY_GPS_NEXT_QUESTION_DISABLE_CHECKED = "gps_next_question_disable_checked";
  // Access Token
  public static final String KEY_NETWORK_ACCESS_TOKEN = "netowrk_access_token";
  // Login User name
  public static final String KEY_LOGIN_USER_NAME = "username";
  // Login User email
  public static final String KEY_LOGIN_USER_EMAIL = "email";
  // Login User id
  public static final String KEY_LOGIN_USER_ID = "userid";
  // Login name
  public static final String KEY_LOGIN_NAME = "name";
  // Profile Bio
  public static final String KEY_LOGIN_USER_BIO = "bio";
  // Profile Bio
  public static final String KEY_LOGIN_USER_DESCRIPTION = "description";
  // Profile Avatar image src
  public static final String KEY_LOGIN_USER_AVATAR_IMG_SRC = "avatarImgSrc";
  // Profile Avatar image file id
  public static final String KEY_LOGIN_USER_AVATAR_FILEID = "avatarFileId";
  // GPS �쐞�룄媛�
  public static final String KEY_LAST_LOCATION_LATITUDE = "last_latitude";
  // GPS 寃쎈룄媛�
  public static final String KEY_LAST_LOCATION_LONGITUDE = "last_longitude";
  // HTTP Protocol type.
  public static final String KEY_NET_REST_IS_HTTPS = "net_rest_is_https";
  // REST API Url.
  public static final String KEY_NET_REST_URL = "net_rest_url";
  // REST Port.
  public static final String KEY_NET_REST_PORT = "net_rest_port";
  // GRPC Url.
  public static final String KEY_NET_GRPC_URL = "net_grpc_host";
  // GRPC Port.
  public static final String KEY_NET_GRPC_PORT = "net_grpc_port";

  /**
   * Integer value getter.
   *
   * @param context context
   * @param key     key
   * @return integer value
   */
  public static int getIntKeyValue(Context context, String key) {
    try {
      return (int) preferences.get(key);
    } catch (Exception e) {
      return -1;
    }
  }

  /**
   * String value getter.
   *
   * @param context context
   * @param key     key
   * @return string value
   */
  public static String getStringKeyValue(Context context, String key) {
    String value = null;
    try {
      value = (String) preferences.get(key);
    } catch (Exception e) {
    }
    return value != null ? value : "";
  }

  /**
   * boolean value getter.
   *
   * @param context context
   * @param key     key
   * @return boolean value
   */
  public static boolean getBooleanKeyValue(Context context, String key) {
    try {
      return (boolean) preferences.get(key);
    } catch (Exception e) {
      return false;
    }
  }

  /**
   * Integer value setter.
   *
   * @param context context
   * @param key     key
   * @param value   value
   */
  public static void setIntKeyValue(Context context, String key, Integer value) {
    preferences.put(key, value);
  }

  /**
   * String value setter.
   *
   * @param context context
   * @param key     key
   * @param value   value
   */
  public static void setStringKeyValue(Context context, String key, String value) {
    preferences.put(key, value);
  }

  /**
   * boolean value setter.
   *
   * @param context context
   * @param key     key
   * @param value   value
   */
  public static void setBooleanKeyValue(Context context, String key, Boolean value) {
    preferences.put(key, value);
  }
}
