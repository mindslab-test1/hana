package ai.maum.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import com.mysql.cj.jdbc.Driver;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.HikariPoolMXBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


// create table `Rejections` ( `deviceId` varchar(64) NOT NULL, `count` tinyint(1) NOT NULL, `rejectedAt` datetime NOT NULL,
// PRIMARY KEY (`deviceId`));


public class QueryUtil {
  private static final Logger logger = LoggerFactory.getLogger(QueryUtil.class);
  private static HikariDataSource dataSource;

  private abstract static class QueryRunner<T> {
    private Connection conn;
    private PreparedStatement stat;
    private ResultSet rs;
    private T result;

    protected Connection getConnection() throws Exception {
      if (conn != null) return conn;
      if (dataSource != null) {
        conn = dataSource.getConnection();
        return conn;
      }
      throw new SQLException("DataSource is not ready.");
    }

    private void closeConnection() {
      if (conn != null) {
        try {
          conn.close();
        } catch (Exception e) {
          logger.error(Utils.getStackTrace(e));
        } finally {
          conn = null;
        }
      }
    }

    protected void setStatement(PreparedStatement stat) {
      this.stat = stat;
    }

    protected void setResultSet(ResultSet rs) {
      this.rs = rs;
    }

    protected void setResult(T value) {
      this.result = value;
    }

    protected Statement getStatement() {
      return stat;
    }

    protected ResultSet getResultSet() {
      return rs;
    }

    protected T getResult() {
      return result;
    }

    protected abstract void run() throws Exception;
  }

  public static void prepare() {
    HikariConfig config = new HikariConfig();

    config.setDriverClassName(Driver.class.getName());
    config.setJdbcUrl("jdbc:mysql://localhost:3306/dialog_inventory");
    config.setUsername("maum");
    config.setPassword("ggoggoma");
    config.setPoolName("Dialog-Inventory-DB-Pool");
    config.setConnectionTimeout(5 * 1000L);
    config.setAutoCommit(true);
    config.setMaximumPoolSize(100);
    config.setIdleTimeout(10 * 60 * 1000L);
    config.setMaxLifetime(60 * 60 * 1000L);
    config.addDataSourceProperty("cachePrepStmts", true);
    config.addDataSourceProperty("prepStmtCacheSize", 250);
    config.addDataSourceProperty("prepStmtCacheSqlLimit", 2048);
    config.addDataSourceProperty("verifyServerCertificate", false);
    config.addDataSourceProperty("useSSL", false);

    dataSource = new HikariDataSource(config);
  }

  public static void finish() {
    if (dataSource != null) {
      dataSource.close();
    }
  }

  private static <T> T executeQuery(QueryRunner<T> runner, T defaultValue) {
    try {
      runner.setResult(defaultValue);
      runner.run();
    } catch (SQLTimeoutException e) {
      logger.error(Utils.getStackTrace(e));
    } catch (SQLException e) {
      logger.error(Utils.getStackTrace(e));
    } catch (Exception e) {
      logger.error(Utils.getStackTrace(e));
    } finally {
      if (runner.getResultSet() != null) {
        try {
          runner.getResultSet().close();
        } catch (Exception e) {
          logger.error(Utils.getStackTrace(e));
        }
      }
      if (runner.getStatement() != null) {
        try {
          runner.getStatement().close();
        } catch (Exception e) {
          logger.error(Utils.getStackTrace(e));
        }
      }
      runner.closeConnection();
    }
    return runner.getResult();
  }

  public static int getDualInteger() {
    return executeQuery(new QueryRunner<Integer>() {
      @Override
      protected void run() throws Exception {
        String query = "select 1 from dual";
        PreparedStatement pstat = getConnection().prepareStatement(query);
        setStatement(pstat);
        ResultSet rs = pstat.executeQuery();
        setResultSet(rs);
        rs.next();
        setResult(rs.getInt(1));
      }
    }, -1);
  }

  public static void listSession() {
    executeQuery(new QueryRunner<Void>() {
      @Override
      protected void run() throws Exception {
        String query = "select * from Session";
        PreparedStatement pstat = getConnection().prepareStatement(query);
        setStatement(pstat);
        ResultSet rs = pstat.executeQuery();
        setResultSet(rs);
//        while (rs.next()) {
//          logger.info(rs.getInt(1) + " " + rs.getString(4) + " " + rs.getString(5));
//        }
      }
    }, null);
  }

  public static Map<String, Object> getRejections(final String deviceId) {
    return executeQuery(new QueryRunner<Map<String, Object>>() {
      @Override
      protected void run() throws Exception {
        String query = "select count, rejectedAt from Rejections where deviceId='" + deviceId + "'";
        PreparedStatement pstat = getConnection().prepareStatement(query);
        setStatement(pstat);
        ResultSet rs = pstat.executeQuery();
        setResultSet(rs);
        Map<String, Object> result = new HashMap<>();
        if (rs.next()) {
          result.put("count", rs.getInt(1));
          result.put("rejectedAt", rs.getTime(2));
        }
        setResult(result);
      }
    }, new HashMap<>());
  }

  public static boolean increaseRejectionCount(final String deviceId) {
    return executeQuery(new QueryRunner<Boolean>() {
      @Override
      protected void run() throws Exception {
        String query = "insert into Rejections values ('" + deviceId
          + "', 1, now()) on duplicate key update count=count+1, rejectedAt=now()";
        PreparedStatement pstat = getConnection().prepareStatement(query);
        setStatement(pstat);
        setResult(pstat.execute());
      }
    }, false);
  }

  public static boolean clearRejectionCount(final String deviceId) {
    return executeQuery(new QueryRunner<Boolean>() {
      @Override
      protected void run() throws Exception {
        String query = "delete from Rejections where deviceId='" + deviceId + "'";
        PreparedStatement pstat = getConnection().prepareStatement(query);
        setStatement(pstat);
        setResult(pstat.execute());
      }
    }, false);
  }

  // for Test
  public static void main(String[] args) throws Exception {
    QueryUtil.prepare();

    for (int i = 0; i < 100; i++) {
//      new Thread(() -> QueryUtil.listSession()).start();
      new Thread(() -> {
        logger.trace("dual: {}", QueryUtil.getDualInteger());
      }).start();
    }

    HikariPoolMXBean poolProxy = dataSource.getHikariPoolMXBean();

    while (poolProxy.getActiveConnections() > 1) {
      logger.info("T:" + poolProxy.getTotalConnections()
        + " A:" + poolProxy.getActiveConnections()
        + " I:" + poolProxy.getIdleConnections()
        + " W:" + poolProxy.getThreadsAwaitingConnection());
      Thread.sleep(10);
    }
    QueryUtil.finish();
  }
}