package ai.maum.chatclient;

import com.google.common.util.concurrent.SettableFuture;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import ai.maum.network.NetConfigInfo;
import ai.maum.preference.PreferenceHelper;
import ai.maum.utils.Context;
import maum.m2u.facade.Dialog;
import maum.m2u.facade.Dialog.AccessFrom;
import maum.m2u.facade.Dialog.AudioUtter;
import maum.m2u.facade.Dialog.Caller;
import maum.m2u.facade.Dialog.Session;
import maum.m2u.facade.Dialog.SessionKey;
import maum.m2u.facade.Dialog.SessionStat;
import maum.m2u.facade.Dialog.TextUtter;
import maum.m2u.facade.DialogServiceGrpc;
import maum.m2u.facade.DialogServiceGrpc.DialogServiceStub;
import io.grpc.Channel;
import io.grpc.ClientInterceptors;
import io.grpc.ManagedChannel;
import io.grpc.Metadata;
import io.grpc.netty.NettyChannelBuilder;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classes for AsyncClient
 */
public class AsyncClient {
  /* For log */
  private static final Logger logger = LoggerFactory.getLogger(AsyncClient.class);
  /* Context */
  private Context context;
  /* Dialog stub */
  private DialogServiceStub asyncStub;
  /* Stub channel */
  private ManagedChannel originalChannel;
  /* Session id */
  private Long sessionId;
  private Metadata receivedMetadata;
  /* Audio sample-rate */
  private static final Integer audioSampleRate = 16000;
  /* Service group */
  private String chatbot = "";

  /* Language type */
  public enum ChatLanguage {
    kor, eng
  }

  /* chat language type save */
  private ChatLanguage chatLanguage = ChatLanguage.kor;

  /**
   * Constructor
   */
  public AsyncClient(Context context) {
    final Channel underlyingChannel;

    this.context = context;
    originalChannel = NettyChannelBuilder
      .forAddress(NetConfigInfo.getGrpcUrl(), NetConfigInfo.GRPC_PORT)
      .usePlaintext(true)
      .build();

    HeaderClientInterceptor headerClientInterceptor = new HeaderClientInterceptor();
    headerClientInterceptor.setMetadataGetter(new IMetadataGetter() {
      // Meta data receiver
      @Override
      public void onMetaData(Metadata md) {
        receivedMetadata = md;
      }
    });

    underlyingChannel = ClientInterceptors.intercept(originalChannel, headerClientInterceptor);
    asyncStub = DialogServiceGrpc.newStub(underlyingChannel);
  }

  public Metadata getMetadata() {
    return receivedMetadata;
  }

  /**
   * Grpc shutdown.
   */
  public void shutdown() throws InterruptedException {
    if (!(originalChannel.isShutdown() || originalChannel.isTerminated())) {
      originalChannel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }
  }

  /**
   * Grpc open with chatbot.
   */
  public void open() {
    open(ChatLanguage.kor, chatbot); // default
  }

  /**
   * Grpc open with language.
   */
  public void open(ChatLanguage lang) {
    open(lang, chatbot);
  }

  /**
   * Grpc open
   */
  public void open(final ChatLanguage lang, String chatbot) {
    final SettableFuture<Void> finishFuture = SettableFuture.create();

    // Get Login Name.
    String username = PreferenceHelper.getStringKeyValue(context, PreferenceHelper.KEY_LOGIN_USER_NAME);

    // for test
    username = "sbyoon";

    Caller accessCaller = Caller.newBuilder().setName(username).setAccessFrom(AccessFrom.MOBILE_ANDROID)
      .setChatbot(chatbot).build();

    // Get Access-Token
    String accessToken = PreferenceHelper.getStringKeyValue(context, PreferenceHelper.KEY_NETWORK_ACCESS_TOKEN);

    accessToken = "qu54MS6Yloj0PKLmcEAom67MeYNaEXU0NcM3GOo2kQDsNPI1lNlgU755sYZ65zjF";

    // Get Location.
    String latitudeValue = PreferenceHelper.getStringKeyValue(context, PreferenceHelper.KEY_LAST_LOCATION_LATITUDE);
    String longitudeValue = PreferenceHelper.getStringKeyValue(context,
      PreferenceHelper.KEY_LAST_LOCATION_LONGITUDE);

    // logger.trace("username: {}", username);
    // logger.trace("AccessToken: {}", accessToken);

    Metadata.Key<String> audioHz = Metadata.Key.of("in.samplerate", Metadata.ASCII_STRING_MARSHALLER);
    Metadata.Key<String> language = Metadata.Key.of("in.lang", Metadata.ASCII_STRING_MARSHALLER);
    Metadata.Key<String> latitude = Metadata.Key.of("in.user.location.latitude", Metadata.ASCII_STRING_MARSHALLER);
    Metadata.Key<String> longitude = Metadata.Key.of("in.user.location.longitude",
      Metadata.ASCII_STRING_MARSHALLER);
    Metadata.Key<String> authToken = Metadata.Key.of("x-elsa-authentication-token",
      Metadata.ASCII_STRING_MARSHALLER);
    Metadata.Key<String> provider = Metadata.Key.of("x-elsa-authentication-provider",
      Metadata.ASCII_STRING_MARSHALLER);

    Metadata header = new Metadata();
    header.put(audioHz, "" + audioSampleRate);
    header.put(language, lang.name());
    header.put(authToken, accessToken);
    header.put(provider, "maum");
    header.put(latitude, latitudeValue);
    header.put(longitude, longitudeValue);

    // Meta header 를 붙여 오픈하여야 한다.
    asyncStub = MetadataUtils.attachHeaders(asyncStub, header);
    // logger.trace("[GRPC Header] {}", header.toString());
    // blockingStub_.open(accessCaller);
    asyncStub.open(accessCaller, new StreamObserver<Session>() {
      @Override
      public void onNext(Session session) {
        sessionId = session.getId();

        Metadata header = new Metadata();
        Metadata.Key<String> sessionKey = Metadata.Key.of("in.sessionid", Metadata.ASCII_STRING_MARSHALLER);
        header.put(sessionKey, "" + sessionId);

        asyncStub = MetadataUtils.attachHeaders(asyncStub, header);
        chatLanguage = lang;
        // logger.trace("[GRPC Header] {}", header.toString());
      }

      @Override
      public void onError(Throwable err) {
        err.printStackTrace();
        finishFuture.setException(err);
      }

      @Override
      public void onCompleted() {
        // System.out.println("stub open completed");
        finishFuture.set(null);
      }
    });

    try {
      finishFuture.get(10, TimeUnit.SECONDS);
    } catch (Exception e) {
      logger.error("Chatting-Server is not responding. Please try again later.", e);
    }

  }

  /**
   * Grpc openWithAuth
   */
  public void openWithAuth(final Caller accessCaller, final ChatLanguage lang) throws Exception {
    final SettableFuture<Void> finishFuture = SettableFuture.create();

    asyncStub.openWithAuth(accessCaller, new StreamObserver<Session>() {
      @Override
      public void onNext(Session session) {
        sessionId = session.getId();

        Metadata header = new Metadata();
        Metadata.Key<String> sessionKey = Metadata.Key.of("in.sessionid", Metadata.ASCII_STRING_MARSHALLER);
        header.put(sessionKey, "" + sessionId);

        asyncStub = MetadataUtils.attachHeaders(asyncStub, header);
        chatLanguage = lang;
        // logger.trace("[GRPC Header] {}", header.toString());
      }

      @Override
      public void onError(Throwable err) {
        err.printStackTrace();
        finishFuture.setException(err);
      }

      @Override
      public void onCompleted() {
        // logger.trace("stub open completed");
        finishFuture.set(null);
      }
    });

    finishFuture.get(10, TimeUnit.SECONDS);
  }

  public void openWithAuth(final String chatbot, final ChatLanguage lang) throws Exception {
    openWithAuth(Caller.newBuilder().setName("elsa text").setAccessFrom(AccessFrom.TEXT_MESSENGER)
      .setChatbot(chatbot).build(), lang);
  }

  public void assignSession(final long _sessionId, final ChatLanguage lang) {
    Metadata header = new Metadata();
    Metadata.Key<String> sessionKey = Metadata.Key.of("in.sessionid", Metadata.ASCII_STRING_MARSHALLER);
    header.put(sessionKey, "" + _sessionId);
    asyncStub = MetadataUtils.attachHeaders(asyncStub, header);
    chatLanguage = lang;
    sessionId = _sessionId;
  }

  public boolean updateSession(final Long sessionId) {
    if (asyncStub == null) {
      return false;
    }

    SessionKey.Builder builder = SessionKey.newBuilder();
    builder.setSessionId(sessionId);
    final SessionKey key = builder.build();

    final SettableFuture<Boolean> finishFuture = SettableFuture.create();

    asyncStub.updateSession(key, new StreamObserver<Session>() {

      @Override
      public void onNext(Session session) {
        finishFuture.set(session.getId() == sessionId);
      }

      @Override
      public void onError(Throwable err) {
        finishFuture.setException(err);
      }

      @Override
      public void onCompleted() {
      }
    });

    try {
      return finishFuture.get(10, TimeUnit.SECONDS);
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
      logger.error("TIMEOUT to get response of updateSession.", e);
      return false;
    } catch (ExecutionException | TimeoutException e) {
      logger.error("TIMEOUT to get response of updateSession.", e);
      return false;
    }
  }

  /**
   * AudioTalk
   *
   * @param ac callback
   * @return AudioUtter data.
   */
  public StreamObserver<AudioUtter> getSenderAudioTalk(final IAudioCallback ac) {
    return asyncStub.audioTalk(new StreamObserver<AudioUtter>() {
      @Override
      public void onNext(AudioUtter utter) {
        ac.onAudio(utter.getUtter());
      }

      @Override
      public void onError(Throwable err) {
        ac.onAudioError(err.toString());
      }

      @Override
      public void onCompleted() {
//        logger.trace("audio talk completed");
        ac.onAudioEnd();
      }
    });
  }

  /**
   * AudioTextTalk
   *
   * @param atc callback
   * @return AudioUtter data.
   */
  public StreamObserver<AudioUtter> getSenderAudioTextTalk(final IAudioTextCallback atc) {
    return asyncStub.audioToTextTalk(new StreamObserver<TextUtter>() {
      @Override
      public void onNext(TextUtter utter) {
//         logger.trace("utter: {}", utter.getUtter());
        atc.onText(utter.getUtter());
      }

      @Override
      public void onError(Throwable err) {
        atc.onError(err.toString());
      }

      @Override
      public void onCompleted() {
//        logger.trace("audio-text talk completed");
      }
    });
  }

  /**
   * TextTalk
   *
   * @param tc callback
   * @return TextUtter data.
   */
  public StreamObserver<TextUtter> getSenderTextTalk(final ITextCallback tc) {
    Metadata.Key<String> sessionId = Metadata.Key.of("in.sessionid", Metadata.ASCII_STRING_MARSHALLER);
    Metadata.Key<String> language = Metadata.Key.of("in.lang", Metadata.ASCII_STRING_MARSHALLER);

//    logger.trace("session: {}", getSessionId().toString());
//    logger.trace("language: {}", chatLanguage.name());
    Metadata header = new Metadata();
    header.put(sessionId, getSessionId().toString());
    header.put(language, chatLanguage.name());

    asyncStub = MetadataUtils.attachHeaders(asyncStub, header);

    return asyncStub.textTalk(new StreamObserver<TextUtter>() {
      @Override
      public void onNext(TextUtter utter) {
//        logger.trace("utter: {}", utter.getUtter());
        tc.onText(utter.getUtter());
      }

      @Override
      public void onError(Throwable err) {
        tc.onError(err.toString());
      }

      @Override
      public void onCompleted() {
//        logger.trace("text talk completed");
      }
    });
  }

  /**
   * Text to Audio talk.
   *
   * @param ac callback
   * @return TextUtter data.
   */
  public StreamObserver<TextUtter> getSenderTextToAudioTalk(final IAudioCallback ac) {
    return asyncStub.textToAudioTalk(new StreamObserver<AudioUtter>() {
      @Override
      public void onNext(AudioUtter utter) {
        logger.trace("getSenderTextToAudioTalk onNext");
        ac.onAudio(utter.getUtter());
      }

      @Override
      public void onError(Throwable err) {
        logger.error("getSenderTextToAudioTalk onError");
        ac.onAudioError(err.toString());
      }

      @Override
      public void onCompleted() {
        logger.trace("getSenderTextToAudioTalk completed");
        ac.onAudioEnd();
      }
    });
  }

  /**
   * imageToTextTalk
   *
   * @param tc callback
   */
  public StreamObserver<Dialog.Image> getSenderImageTalk(final ITextCallback tc) {
    return asyncStub.imageToTextTalk(new StreamObserver<TextUtter>() {
      @Override
      public void onNext(TextUtter value) {
        logger.trace("ImageTalk: {}", value.getUtter());
      }

      @Override
      public void onError(Throwable t) {
        logger.error("ImageTalk talk error");
        tc.onError(t.toString());
      }

      @Override
      public void onCompleted() {
        logger.trace("ImageTalk completed");
      }
    });
  }

  /**
   * Close asyncClient.
   */
  public void close() {
    if (asyncStub == null || sessionId == null) {
      return;
    }

    SessionKey.Builder builder = SessionKey.newBuilder();
    builder.setSessionId(sessionId);
    final SessionKey key = builder.build();

//    logger.trace("stub close SessionKey : {}", key.getSessionId());
//    SessionStat stat = blockingStub_.close(key);
    asyncStub.close(key, new StreamObserver<SessionStat>() {
      @Override
      public void onNext(SessionStat stat) {
//        여기서 대답을 얻는다.
//        logger.info("stub close code : {} {} {}", key.getSessionId(), stat.getResultCode(), stat.getFullMessage());
      }

      @Override
      public void onError(Throwable err) {
        logger.error("stub close", err);
      }

      @Override
      public void onCompleted() {
      }
    });
  }

  /**
   * 서비스 그룹을 셋팅한다.
   *
   * @param chatbot
   */
  public void setChatbot(String chatbot) {
    this.chatbot = chatbot;
  }

  /**
   * Get chatLanguage.
   */
  public ChatLanguage getChatLanguage() {
    return chatLanguage;
  }

  /**
   * Set chatLanguage.
   */
  public void setChatLanguage(ChatLanguage chatLanguage) {
    this.chatLanguage = chatLanguage;
  }

  /**
   * SessionId getter.
   *
   * @return session id.
   */
  public Long getSessionId() {
    if (sessionId == null) {
      return null;
    }

    return sessionId;
  }
}
