package ai.maum.chatclient;

/*
 * Created by yjhwang on 2016-12-19.
 */

public interface IGetChatbotsCallback {
  void onGetChatbots(java.util.List<maum.m2u.facade.Dialog.Chatbot> list);

  void onError(String message);
}
