package ai.maum.utils;

import io.grpc.Channel;
import io.grpc.ClientInterceptors;
import io.grpc.ManagedChannel;
import io.grpc.netty.NettyChannelBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public abstract class GrpcClient<T> {
  private static final Logger logger = LoggerFactory.getLogger(GrpcClient.class);
  private static final long clientKeepAliveTime = 20 * 60 * 1000L; // 20 mins
  private static final long clientApplicableGap = 10 * 1000L; // 10 seconds

  private ManagedChannel originalChannel;
  private volatile long aliveTo;
  protected T stub;

  protected GrpcClient() {
  }

  protected abstract T newStub(final Channel underlyingChannel);

  protected void init(String host, int port) {
    originalChannel = NettyChannelBuilder
      .forAddress(host, port)
      .usePlaintext(true)
      .build();
    stub = newStub(ClientInterceptors.intercept(originalChannel));
    refreshLifetime();
  }

  /**
   * shutdown
   * Shutdown GRPC Channel
   */
  protected void shutdown() {
    originalChannel.shutdown();
    originalChannel = null;
    stub = null;
  }

  /**
   * refreshLifetime
   */
  protected void refreshLifetime() {
    aliveTo = System.currentTimeMillis() + clientKeepAliveTime;
  }

  /**
   * isApplicable
   *
   * @return boolean value whether is applicable.
   */
  protected boolean isApplicable() {
    if (!originalChannel.isShutdown() && !originalChannel.isTerminated()) {
      if (aliveTo > System.currentTimeMillis()) {
        aliveTo += clientApplicableGap;
        return true;
      }
    }
    return false;
  }
}
