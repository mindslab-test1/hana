package ai.maum.bridge;

import java.net.SocketTimeoutException;

import ai.maum.chatclient.RestDialogService;
import ai.maum.utils.InputPrompter;
import ai.maum.chatclient.RestDialog.Caller;
import ai.maum.chatclient.RestDialog.ReceiveTextUtter;
import ai.maum.chatclient.RestDialog.SendTextUtter;
import ai.maum.chatclient.RestDialog.Session;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestDialogTask implements Runnable {
  String sessionId = "";
  RestDialogService service;

  private void setupClient() {
    Retrofit retrofit = new Retrofit.Builder().baseUrl("http://10.122.65.10:9999/api/dialog/")
        .addConverterFactory(GsonConverterFactory.create()).build();

    service = retrofit.create(RestDialogService.class);
    Call<Session> call = service.openWithAuth("hana_textbanking", "hana_textbanking",
        new Caller("elas text", 2, "hana_textbanking"));
    try {
      Response<Session> response = call.execute();
      if (response.isSuccessful()) {
        Session session = response.body();
        sessionId = session.getId();
        System.out.println("\n[hana_textbanking] Open Success.");
        System.out.println("Session ID:" + sessionId);
      }
    } catch (SocketTimeoutException e) {
      System.out.println(e.getMessage());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private static void printReply(Response<ReceiveTextUtter> response) {
    if (response.isSuccessful()) {
      ReceiveTextUtter textUtter = response.body();
      System.out.println("AI <<< : [/] : " + textUtter.getUtter());
      // System.out.println(" meta[out.talk.skill] = {"
      // + MetaDataHelper.getStringValueForKey(receivedMetadata, "out.talk.skill") +
      // "}");
    } else {
      System.out.println("ERROR ### : [/] : " + response.toString());
      // System.out.println(" meta[out.talk.error] = {"
      // + MetaDataHelper.getStringValueForKey(receivedMetadata, "out.talk.error") +
      // "}");
    }
    System.out.println();
  }


  @Override
  public void run() {
    setupClient();

    if (sessionId.isEmpty()) {
      System.out.println("Open Failed. Bye~~\n");
      return;
    }

    InputPrompter prompter = new InputPrompter(
        "\nHelp:\n"
            + "q: Stop chatting\n"
            // + "n: Restart session\n"
            // + "e: Set language as English\n"
            // + "k: Set language as Korean\n"
            + "any text: talk talk talk\n\n",
        "{" + sessionId + "} [q|User] >>> ");

    Call<ReceiveTextUtter> call;
    for (; ; ) {
      String line = prompter.promptToGetLine();
      if (line.equals("q")) {
        System.out.println("Quit. Bye~~");
        break;
      }
      call = service.textTalk(sessionId, new SendTextUtter(line, "kor"));
      try {
        Response<ReceiveTextUtter> response = call.execute();
        printReply(response);
      } catch (SocketTimeoutException e) {
        System.out.println(e.getMessage());
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

  }

}
