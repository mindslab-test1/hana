package ai.maum.utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import ai.maum.preference.PreferenceHelper;

public class TestTalk {
  private static String talkFile = PreferenceHelper.getStringKeyValue(Context.NullContext, "test.talk.file");
  private static BufferedReader testTalk = null;

  public static String getTalk() {
    if (testTalk == null) {
      try {
        testTalk = new BufferedReader(new FileReader(talkFile));
      } catch (FileNotFoundException e) {
        e.printStackTrace();
        return "";
      }
    }

    try {
      String line = testTalk.readLine();
      if (line == null) {
        testTalk.close();
        testTalk = null;
        return getTalk();
      }
      return line;
    } catch (Exception e) {
      e.printStackTrace();
      if (testTalk != null) {
        try {
          testTalk.close();
          testTalk = null;
          return getTalk();
        } catch (IOException e1) {
          e1.printStackTrace();
        }
      }
    }

    return "";
  }
}
