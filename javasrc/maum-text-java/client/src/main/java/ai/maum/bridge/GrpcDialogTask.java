package ai.maum.bridge;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.google.common.util.concurrent.SettableFuture;

import ai.maum.chatclient.AsyncClient;
import ai.maum.chatclient.IGetChatbotsCallback;
import ai.maum.chatclient.IMetadataGetter;
import ai.maum.chatclient.ITextCallback;
import ai.maum.chatclient.ChatbotFinderClient;
import ai.maum.chatclient.AsyncClient.ChatLanguage;
import ai.maum.model.MetaDataHelper;
import ai.maum.utils.Context;
import ai.maum.utils.InputPrompter;
import maum.m2u.facade.Dialog;
import maum.m2u.facade.Dialog.Skill;
import maum.m2u.facade.Dialog.Chatbot;
import maum.m2u.facade.Dialog.TextUtter;
import io.grpc.Metadata;
import io.grpc.stub.StreamObserver;

public class GrpcDialogTask implements Runnable {
  private InputPrompter prompter;
  private AsyncClient chatClient;

  private List<Chatbot> getChatbotList() {
    SettableFuture<List<Chatbot>> chatbotList = SettableFuture.create();

    // Create chat client session.
    ChatbotFinderClient chatbotFinderClient = new ChatbotFinderClient(Context.NullContext,
        new IMetadataGetter() {
          // Meta data receiver
          @Override
          public void onMetaData(Metadata md) {
            // logger.info(md.toString());

          }
        });

    // Get Chatbot
    chatbotFinderClient.getSenderChatbots(new IGetChatbotsCallback() {

      @Override
      public void onGetChatbots(final List<Chatbot> list) {
        chatbotList.set(list);
      }

      @Override
      public void onError(String message) {
      }
    });

    try {
      return chatbotList.get(10, TimeUnit.SECONDS);
    } catch (InterruptedException | ExecutionException | TimeoutException e) {
      e.printStackTrace();
      return null;
    } finally {
      try {
        // chatbotFinderClient.shutdown();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  private void describeChatbot(Chatbot item, int index) {
    System.out.println(">>> " + index + ": " + item.getName() + ", " + item.getDescription());
    Iterator<Skill> iterator = item.getSkillsList().iterator();
    while (iterator.hasNext()) {
      Skill skill = iterator.next();
      System.out.println("        " + skill.getName() + " {" + skill.getSkill() + ", " + skill.getLang() + ", "
          + skill.getVersion() + ", " + skill.getDescription() + "}");
    }
  }

  private String selectChatbot(List<Chatbot> list) {

    if (list == null || list.size() < 1) {
      return "";
    }

    int index = 0;
    Iterator<Chatbot> iterator = list.iterator();
    while (iterator.hasNext()) {
      ++index;
      describeChatbot(iterator.next(), index);
    }

    prompter = new InputPrompter("\nSelect Service Group [1-" + index + "|q]: ");
    try {
      return list.get(Integer.parseInt(prompter.promptToGetLine()) - 1).getName();
    } catch (NumberFormatException e) {
      return "q";
    } catch (Exception e) {
      e.printStackTrace();
    }

    return "";
  }

  /**
   * Setup chat client.
   */
  protected void setupChatClient(String chatbot) throws Exception {
    if (chatClient != null) {
      chatClient.close();
      chatClient.shutdown();
    }
    chatClient = new AsyncClient(Context.NullContext);

    // Connect client
    chatClient.openWithAuth(chatbot, ChatLanguage.kor);
  }

  /**
   * Text message send and response data.
   */
  protected SettableFuture<String> sendTextMessage(final String sendMessage) {
    final SettableFuture<String> reply = SettableFuture.create();

    // Check chat session.
    if (chatClient.getSessionId() == null) {
      reply.setException(new Throwable("Session ID is invalid from beginning."));
      return reply;
    }

    if (sendMessage.trim().isEmpty()) {
      reply.setException(new Throwable("Message is Empty. Not sending."));
      return reply;
    }

    // Calendar calendar = Calendar.getInstance();
    // SimpleDateFormat messageDateFormat = new SimpleDateFormat("hh:mm aa");
    // final String messageTime = messageDateFormat.format(calendar.getTime());
    StreamObserver<Dialog.TextUtter> sender = chatClient.getSenderTextTalk(new ITextCallback() {
      @Override
      public void onText(final String receiveMessage) {
        // Check chat session.
        if (chatClient.getSessionId() == null) {
          reply.setException(new Throwable("Session ID is invalid from onText."));
          return;
        }
        reply.set(receiveMessage);
      }

      @Override
      public void onError(final String message) {
        reply.setException(new Throwable(message));
      }
    });

    TextUtter utter = TextUtter.newBuilder().setUtter(sendMessage).build();
    sender.onNext(utter);
    sender.onCompleted();

    return reply;
  }

  /**
   * Assign Chat client.
   */
  protected void assignChatClient(Long sessionId) {
    System.out.println("assignChatClient session id:" + sessionId);
    chatClient = new AsyncClient(Context.NullContext);
    chatClient.assignSession(sessionId, ChatLanguage.kor);
    System.out.println("session id:" + chatClient.getSessionId());
  }

  /**
   * Shutdown Chat client.
   */
  protected void shutdownChatClient() {
    try {
      if (chatClient != null) {
        chatClient.shutdown();
        chatClient = null;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void printReply(SettableFuture<String> replyFuture) {
    try {
      String reply = replyFuture.get(10, TimeUnit.SECONDS);
      System.out.println("AI <<< : [/] : " + reply);
      System.out.println("        meta[in.lang] = {"
          + MetaDataHelper.getStringValueForKey(chatClient.getMetadata(), "in.lang") + "}");
      System.out.println("        meta[in.sessionid] = {"
          + MetaDataHelper.getStringValueForKey(chatClient.getMetadata(), "in.sessionid") + "}");
      System.out.println("        meta[out.talk.skill] = {"
          + MetaDataHelper.getStringValueForKey(chatClient.getMetadata(), "out.talk.skill") + "}");
    } catch (Exception e) {
      System.out.println("ERROR ### : [/] : " + e.getMessage());
      System.out.println("        meta[out.talk.error] = {"
          + MetaDataHelper.getStringValueForKey(chatClient.getMetadata(), "out.talk.error") + "}");
    }
    System.out.println();
  }

  @Override
  public void run() {
    String chatbot = selectChatbot(getChatbotList());

    if (chatbot.isEmpty()) {
      System.out.println("Chatbot is not available.");
      System.out.println("Bye~~");
      return;
    }

    if (chatbot.equals("q")) {
      System.out.println("Quit. Bye~~");
      return;
    }

    try {
      setupChatClient(chatbot);
      Long sessionId = chatClient.getSessionId();
      shutdownChatClient();
      prompter.setDescription("\nHelp:\n" + "q: Stop chatting\n"
          // + "n: Restart session\n"
          // + "e: Set language as English\n"
          // + "k: Set language as Korean\n"
          + "any text: talk talk talk\n\n");
      prompter.setPrompt("{" + sessionId + "} [q|User] >>> ");
      for (; ; ) {
        String line = prompter.promptToGetLine();
        if (line.equals("q")) {
          System.out.println("Quit. Bye~~");
          break;
        }
        assignChatClient(sessionId);
        printReply(sendTextMessage(line));
        shutdownChatClient();
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      shutdownChatClient();
    }
  }
}
