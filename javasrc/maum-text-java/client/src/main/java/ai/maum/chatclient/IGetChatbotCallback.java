package ai.maum.chatclient;

/*
 * Created by yjhwang on 2016-12-19.
 */

public interface IGetChatbotCallback {
  void onGetChatbotList(java.util.List<maum.m2u.facade.Dialog.Chatbot> list);

  void onError(String message);
}
