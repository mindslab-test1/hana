package ai.maum.model;

/**
 * Class for decoding MetaData.
 */
public class DecodeMetaData {
  /*
  [MetaData Field 정의]
    in.sessionid	User 접속 session ID	1000001
    in.lang	        입력 언어	kor, eng
    in.samplerate	입력된 음성의 sample rate	8000, 16000
    in.user.location.latitude	사용자 위치(위도)	37.5665350
    in.user.location.longitude	사용자 위치(경도)	126.9779690
    in.text	        STT 음성 인식 결과
    in.talk.origin	입력된 원본 텍스트	서울날씨어때

    out.talk.proofread	교정된 입력 텍스트	서울 날씨 어때
    out.talk.skill	입력 텍스트에 대한 skill 분류값	날씨
    out.talk.intention	입력 텍스트에 대한 의도 분류값	날씨정보
    out.text	        text 출력 데이터	알람이 3시로 설정되었습니다. AudioOutput 인 경우에만 내려갑니다.
    out.embed.type	embedded 출력 데이터 타입
                        html5 (embed.data.body, embed.data.style) 이 필요함.
                        audio (embed.data.url, embed.data.mime) 이 필요함.
                        link (embed.data.url) 이 필요함.
    out.embed.data.body	embedded 출력 데이터 body	"<html> </html>", raw html
    out.embed.data.url	embedded 출력 데이터 url	"http://where.domain.com/aaa.pcm"
    out.embed.data.style	embedded 출력 데이터 style	사용안함
    out.embed.data.mime	embedded 출력 데이터 mime	"application/mp3"
    out.embed.data.samplerate	embedded 출력 sample rate	"44100" (G711인 경우에)
   */

  private String inSessionid;
  private String inLang;
  private String inSamplerate;
  private String inUserLocationLatitude;
  private String inUserLocationLongitude;
  private String inText;
  private String inTalkOrigin;

  private String outTalkProofread;
  private String outTalkSkill;
  private String outTalkIntention;
  private String outText;
  private String outEmbedType;
  private String outEmbedDataBody;
  private String outEmbedDataUrl;
  private String outEmbedDataStyle;
  private String outEmbedDataMime;
  private String outEmbedDataSamplerate;

  /**
   * InSessionid getter.
   *
   * @return string result.
   */
  public String getInSessionid() {
    return inSessionid;
  }

  /**
   * InSessionid setter.
   *
   * @param inSessionid
   */
  public void setInSessionid(String inSessionid) {
    this.inSessionid = inSessionid;
  }

  /**
   * InLang getter.
   *
   * @return string result.
   */
  public String getInLang() {
    return inLang;
  }

  /**
   * InLang setter.
   *
   * @param inLang
   */
  public void setInLang(String inLang) {
    this.inLang = inLang;
  }

  /**
   * InSamplerate getter.
   *
   * @return string result.
   */
  public String getInSamplerate() {
    return inSamplerate;
  }

  /**
   * InSamplerate setter
   *
   * @param inSamplerate
   */
  public void setInSamplerate(String inSamplerate) {
    this.inSamplerate = inSamplerate;
  }

  /**
   * InUserLocationLatitude getter.
   *
   * @return string result.
   */
  public String getInUserLocationLatitude() {
    return inUserLocationLatitude;
  }

  /**
   * InUserLocationLatitude setter.
   *
   * @param inUserLocationLatitude
   */
  public void setInUserLocationLatitude(String inUserLocationLatitude) {
    this.inUserLocationLatitude = inUserLocationLatitude;
  }

  /**
   * InUserLocationLongitude getter.
   *
   * @return string result.
   */
  public String getInUserLocationLongitude() {
    return inUserLocationLongitude;
  }

  /**
   * InUserLocationLongitude setter.
   *
   * @param inUserLocationLongitude
   */
  public void setInUserLocationLongitude(String inUserLocationLongitude) {
    this.inUserLocationLongitude = inUserLocationLongitude;
  }

  /**
   * InText getter.
   *
   * @return string result.
   */
  public String getInText() {
    return inText;
  }

  /**
   * InText setter.
   *
   * @param inText
   */
  public void setInText(String inText) {
    this.inText = inText;
  }

  /**
   * InTalkOrigin getter.
   *
   * @return string result.
   */
  public String getInTalkOrigin() {
    return inTalkOrigin;
  }

  /**
   * InTalkOrigin setter.
   *
   * @param inTalkOrigin
   */
  public void setInTalkOrigin(String inTalkOrigin) {
    this.inTalkOrigin = inTalkOrigin;
  }

  /**
   * OutTalkProofread getter.
   *
   * @return string result.
   */
  public String getOutTalkProofread() {
    return outTalkProofread;
  }

  /**
   * OutTalkProofread setter.
   *
   * @param outTalkProofread
   */
  public void setOutTalkProofread(String outTalkProofread) {
    this.outTalkProofread = outTalkProofread;
  }

  /**
   * OutTalkSkill getter.
   *
   * @return string result.
   */
  public String getOutTalkSkill() {
    return outTalkSkill;
  }

  /**
   * OutTalkSkill setter.
   *
   * @param outTalkSkill
   */
  public void setOutTalkSkill(String outTalkSkill) {
    this.outTalkSkill = outTalkSkill;
  }

  /**
   * OutTalkIntention getter.
   *
   * @return string result.
   */
  public String getOutTalkIntention() {
    return outTalkIntention;
  }

  /**
   * OutTalkIntention setter.
   *
   * @param outTalkIntention
   */
  public void setOutTalkIntention(String outTalkIntention) {
    this.outTalkIntention = outTalkIntention;
  }

  /**
   * OutText getter.
   *
   * @return string result.
   */
  public String getOutText() {
    return outText;
  }

  /**
   * OutText setter.
   *
   * @param outText
   */
  public void setOutText(String outText) {
    this.outText = outText;
  }

  /**
   * OutEmbedType getter.
   *
   * @return string result.
   */
  public String getOutEmbedType() {
    return outEmbedType;
  }

  /**
   * OutEmbedType setter.
   *
   * @param outEmbedType
   */
  public void setOutEmbedType(String outEmbedType) {
    this.outEmbedType = outEmbedType;
  }

  /**
   * OutEmbedDataBody getter.
   *
   * @return string result.
   */
  public String getOutEmbedDataBody() {
    return outEmbedDataBody;
  }

  /**
   * OutEmbedDataBody setter.
   *
   * @param outEmbedDataBody
   */
  public void setOutEmbedDataBody(String outEmbedDataBody) {
    this.outEmbedDataBody = outEmbedDataBody;
  }

  /**
   * OutEmbedDataUrl getter.
   *
   * @return string result.
   */
  public String getOutEmbedDataUrl() {
    return outEmbedDataUrl;
  }

  /**
   * OutEmbedDataUrl setter.
   *
   * @param outEmbedDataUrl
   */
  public void setOutEmbedDataUrl(String outEmbedDataUrl) {
    this.outEmbedDataUrl = outEmbedDataUrl;
  }

  /**
   * OutEmbedDataStyle getter.
   *
   * @return string result.
   */
  public String getOutEmbedDataStyle() {
    return outEmbedDataStyle;
  }

  /**
   * OutEmbedDataStyle setter.
   *
   * @param outEmbedDataStyle
   */
  public void setOutEmbedDataStyle(String outEmbedDataStyle) {
    this.outEmbedDataStyle = outEmbedDataStyle;
  }

  /**
   * OutEmbedDataMime getter.
   *
   * @return string result.
   */
  public String getOutEmbedDataMime() {
    return outEmbedDataMime;
  }

  /**
   * OutEmbedDataMime setter.
   *
   * @param outEmbedDataMime
   */
  public void setOutEmbedDataMime(String outEmbedDataMime) {
    this.outEmbedDataMime = outEmbedDataMime;
  }

  /**
   * OutEmbedDataSamplerate getter.
   *
   * @return string result.
   */
  public String getOutEmbedDataSamplerate() {
    return outEmbedDataSamplerate;
  }

  /**
   * OutEmbedDataSamplerate setter.
   *
   * @param outEmbedDataSamplerate
   */
  public void setOutEmbedDataSamplerate(String outEmbedDataSamplerate) {
    this.outEmbedDataSamplerate = outEmbedDataSamplerate;
  }

}
