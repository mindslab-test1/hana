package ai.maum.chatclient;

import com.google.gson.annotations.SerializedName;

public class RestDialog {
  public static class Caller {
    @SerializedName("chatbot")
    private String __chatbot;
    @SerializedName("name")
    private String __name;
    /**
     * <code>ANONYMOUS = 0;</code>
     * <code>WEB_BROSWER = 1;</code>
     * <code>TEXT_MESSENGER = 2;</code>
     * <code>MOBILE_ANDROID = 11;</code>
     * <code>MOBILE_IPHONE = 12;</code>
     * <code>SPEAKER = 20;</code>
     * <code>TELEPHONE = 30;</code>
     * <code>EXT_API = 1000;</code>
     */
    @SerializedName("accessFrom")
    private int __accessFrom;
    @SerializedName("phone_no")
    private String __phoneNo;
    @SerializedName("email")
    private String __email;
    @SerializedName("properties")
    private String __properties;

    public Caller(String name, int accessFrom, String chatbot) {
      __name = name;
      __accessFrom = accessFrom;
      __chatbot = chatbot;
    }
  }

  public static class SendTextUtter {
    @SerializedName("message")
    private String __message;
    @SerializedName("language")
    private String __language;

    public SendTextUtter(String message, String language) {
      __message = message;
      __language = language;
    }
  }

  public class Session {
    @SerializedName("id")
    private String __id;
    @SerializedName("chatbot")
    private String __chatbot;
    @SerializedName("user_key")
    private String __userKey;
    @SerializedName("accessFrom")
    private String __accessFrom;
    @SerializedName("start_time")
    private String __startTime;
    //		@SerializedName("duration") private String __duration;
    @SerializedName("human_assisted")
    private boolean __humanAssisted;

    public String getId() {
      return __id;
    }

    public String getChatbot() {
      return __chatbot;
    }

    public String getUserKey() {
      return __userKey;
    }

    public String getAccessFrom() {
      return __accessFrom;
    }

    public String getStartTime() {
      return __startTime;
    }

    public boolean getHumanAssisted() {
      return __humanAssisted;
    }
  }

  public class ReceiveTextUtter {
    @SerializedName("utter")
    private String __utter;
    @SerializedName("at")
    private String __at;
    @SerializedName("receive")
    private boolean __receive;
    @SerializedName("error")
    private boolean __error;
//		@SerializedName("meta") private String __meta;

    public String getUtter() {
      return __utter;
    }

    public String getAt() {
      return __at;
    }

    public boolean getReceive() {
      return __receive;
    }

    public boolean getError() {
      return __error;
    }
  }
}
