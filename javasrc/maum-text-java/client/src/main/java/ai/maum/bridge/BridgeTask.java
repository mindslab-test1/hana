package ai.maum.bridge;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import ai.maum.utils.Utils;
import com.google.common.util.concurrent.SettableFuture;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

import ai.maum.chatclient.AsyncClient;
import ai.maum.chatclient.AsyncClient.ChatLanguage;
import ai.maum.chatclient.ITextCallback;
import ai.maum.model.MetaDataHelper;
import ai.maum.preference.PreferenceHelper;
import ai.maum.utils.Context;
import ai.maum.utils.Spacing;
import maum.m2u.facade.Dialog;
import maum.m2u.facade.Dialog.AccessFrom;
import maum.m2u.facade.Dialog.Caller;
import maum.m2u.facade.Dialog.TextUtter;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BridgeTask implements Runnable {
  private static final Logger logger = LoggerFactory.getLogger(BridgeTask.class);
  private static final int TALK_SESSION_KEEP_TIME = 10 * 60 * 1000; // 10 mins
  private static final int TALK_SESSION_FRESH_LIMIT = 9 * 60 * 1000; // 9 mins
  private static final int TALK_SESSION_FAIL_RETRY = 2;
  private static final int TALL_REPLY_WAIT_TIMEOUT = 10; // 10s
  private static final int TALK_MAXIMUM_COUNT = PreferenceHelper.getIntKeyValue(Context.NullContext, "dialog.talk.count.maximum");

  private static final int SESSION_NOT_EXIST = -1;

  private final BridgeMessage bmsg;
  int retry;

  private static class TalkSession {
    final String key;
    final Long sessionId;

    long lastTwinkle;
    boolean valid = true;
    AsyncClient chatClient;

    TalkSession(String _key, AsyncClient _chatClient) {
      this.key = _key;
      this.chatClient = _chatClient;
      this.sessionId = _chatClient.getSessionId();
      refreshLastTwinkle();
    }

    String getMetadata(String key) {
      return chatClient != null && chatClient.getMetadata() != null ?
        MetaDataHelper.getStringValueForKey(chatClient.getMetadata(), key) : "";
    }

    void deployChatClient() {
      if (chatClient == null) {
        chatClient = new AsyncClient(Context.NullContext);
        try {
          chatClient.assignSession(sessionId, ChatLanguage.kor);
        } catch (Exception e) {
          logger.error(Utils.getStackTrace(e));
          chatClient = null;
        }
      }
    }

    void shutdownChatClient() {
      try {
        if (chatClient != null) {
          chatClient.shutdown();
        }
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
        logger.error(Utils.getStackTrace(e));
      }
      chatClient = null;
    }

    void refreshLastTwinkle() {
      lastTwinkle = System.currentTimeMillis();
    }

    void setLastTwinkle(long timeMillis) {
      if (timeMillis <= 0L)
        valid = false;
      lastTwinkle = timeMillis;
    }

    boolean isFresh() {
      if (!valid)
        return false;

      boolean fresh = false;
      long calmSpan = System.currentTimeMillis() - lastTwinkle;

      if (calmSpan < TALK_SESSION_FRESH_LIMIT) {
        fresh = true;
      } else {
        try {
          fresh = chatClient.updateSession(sessionId);
        } catch (Exception e) {
          logger.error(Utils.getStackTrace(e));
        }
      }

      if (!fresh) {
        shutdownChatClient();
        valid = false;
      }

      return fresh;
    }

    boolean checkKeepAlive() {
      long calmSpan = System.currentTimeMillis() - lastTwinkle;
      return calmSpan < TALK_SESSION_KEEP_TIME;
    }

    public String toString() {
      return TalkSession.class.getSimpleName() + "@" + Integer.toHexString(hashCode()) + " " + key + " "
        + sessionId + " " + lastTwinkle + " " + valid;
    }
  }

  private static class TalkMaxExceededException extends Exception {
    private static final long serialVersionUID = -8076249537474574027L;

    public TalkMaxExceededException(String message) {
      super(message);
    }
  }

  public BridgeTask(BridgeMessage bmsg) {
    this.bmsg = bmsg;
  }

  private SettableFuture<String> sendTextMessage(final BridgeMessage bmsg, final TalkSession session) {
    final String talk = bmsg.getUmsMessage();
    final String aliases = bmsg.getAliases();

    logger.info("{{}} {} {}", session.sessionId, bmsg.getAliasCount(), aliases);
    logger.info("{{}} >>> {}", session.sessionId, talk);

    final SettableFuture<String> reply = SettableFuture.create();

    // if(SessionManager.getSessionTalkCount(session.key) > TALK_MAXIMUM_COUNT) {
    // reply.setException(new TalkMaxExceededException("Talk Max. Count(" +
    // TALK_MAXIMUM_COUNT + "exceeded."));
    // return reply;
    // }

    // if(session.sessionId == null) {
    // bmsg.setRetnCode(BridgeMessage.RETN_CODE_FAIL);
    // reply.setException(new Throwable("Session ID is invalid in beginning."));
    // return reply;
    // }

    if (talk.trim().isEmpty()) {
      bmsg.setRetnCode(BridgeMessage.RETN_CODE_FAIL);
      reply.setException(new Throwable("Message is Empty. Not sending."));
      return reply;
    }

    StreamObserver<Dialog.TextUtter> sender = session.chatClient.getSenderTextTalk(new ITextCallback() {
      @Override
      public void onText(String receivedMessage) {
        // if(session.sessionId == null) {
        // bmsg.setRetnCode(BridgeMessage.RETN_CODE_FAIL);
        // reply.setException(new Throwable("Session ID is invalid in onText."));
        // return;
        // }
        reply.set(receivedMessage);
      }

      @Override
      public void onError(String err) {
        bmsg.setRetnCode(BridgeMessage.RETN_CODE_FAIL);
        reply.setException(new Throwable(err));
      }
    }); // , talk, aliases);
    TextUtter utter = TextUtter.newBuilder().setUtter(Spacing.doSpace(talk)).build();
    sender.onNext(utter);
    sender.onCompleted();

    return reply;
  }

  private boolean performReply(SettableFuture<String> reply, BridgeMessage bmsg, TalkSession session) {
    JsonObject json = null;

    try {
      String daReply = reply.get(TALL_REPLY_WAIT_TIMEOUT, TimeUnit.SECONDS);
      session.refreshLastTwinkle();
      // SessionManager.updateSession(session.key, session.sessionId);

      try {
/*
        json = new JsonParser().parse(daReply).getAsJsonObject();
				if (json.get("response_cd") == null) {
					throw new JsonSyntaxException("response_cd is not exist.");
				}
				JsonElement responseCtt = json.get("response_ctt");
				if (responseCtt == null) {
					throw new JsonSyntaxException("response_ctt is not exist.");
				}
				if (responseCtt.isJsonArray()) {
					if (responseCtt.getAsJsonArray().get(0) == null) {
						throw new JsonSyntaxException("response_ctt is Malformed.");
					}
					json.add("response_ctt", responseCtt.getAsJsonArray().get(0));
				}
				logger.debug("{" + session.sessionId + "} AI {" + session.getMetadata("out.talk.skill") + "} <<< "
						+ json.get("msg_ctt").getAsString());
*/
        logger.info("{{}} AI {{}} <<< {}",
          session.sessionId,
          session.getMetadata("out.talk.skill"),
          daReply.replaceAll("\\r\\n|\\r|\\n", " ^"));
        bmsg.setRetnCode(BridgeMessage.RETN_CODE_SUCCESS);
      } catch (JsonSyntaxException e) {
        logger.error(Utils.getStackTrace(e));
        logger.info("{{}} ERROR JSON: {}", session.sessionId, daReply);
        logger.info("    meta[out.talk.skill] = {}", session.getMetadata("out.talk.skill"));
        logger.info("    meta[out.talk.error] = {}", session.getMetadata("out.talk.error"));
        bmsg.setRetnCode(BridgeMessage.RETN_CODE_FAIL);
      }
    } catch (TimeoutException e) {
      logger.error(Utils.getStackTrace(e));
      bmsg.setRetnCode(BridgeMessage.RETN_CODE_FAIL);
    } catch (ExecutionException e) {
      Throwable cause = e.getCause();
      if (cause instanceof StatusRuntimeException && cause.getMessage().equals("NOT_FOUND: Session Not Found")) {
        logger.info("{{}} ", session.sessionId, e.getCause().getMessage());
        removeSession(session.key);
        if (retry >= TALK_SESSION_FAIL_RETRY) {
          logger.info("{{}} No More Retry. Finish!", session.sessionId);
          bmsg.setRetnCode(BridgeMessage.RETN_CODE_FAIL);
        } else {
          return false;
        }
      } else if (cause instanceof TalkMaxExceededException) {
        logger.info(e.getCause().getMessage());
        bmsg.setRetnCode(BridgeMessage.RETN_CODE_FAIL);
      }
    } catch (Exception e) {
      logger.error(Utils.getStackTrace(e));
      bmsg.setRetnCode(BridgeMessage.RETN_CODE_FAIL);
    }

    bmsg.setSessionId(session.sessionId);
    BridgeMessage.reply(bmsg, json);

    return true;
  }

  public void run() {
    TalkSession session = null;
    try {
      if (bmsg.isValid()) {
        String key = bmsg.getKey();
        // TalkSession session = null;
        // long sessionId = SessionManager.getSession(key);
        session = getSession(key);
        long sessionId = session != null ? session.sessionId : SESSION_NOT_EXIST;

        for (retry = 1; retry <= TALK_SESSION_FAIL_RETRY; ++retry) {
          synchronized (sessionCache) {
            // if (sessionId != SessionManager.SESSION_NOT_EXIST) {
            // session = getSession(key);
            if (sessionId != SESSION_NOT_EXIST) {
              if (session != null) {
                while (session.chatClient != null)
                  Thread.sleep(100);
                session.deployChatClient();
              } else {
                session = assignSession(bmsg, sessionId);
                addSession(key, session);
              }
              if (!session.isFresh()) {
                removeSession(session.key);
                session = null;
              }
            }
            if (session == null) {
              session = establishSession(bmsg);
              addSession(key, session);
              // SessionManager.addSession(key, session.sessionId);
            }
          }
          synchronized (session) {
            SettableFuture<String> reply = sendTextMessage(bmsg, session);
            boolean done = performReply(reply, bmsg, session);
            session.shutdownChatClient();
            if (done) {
              break;
            } else {
              logger.info("{{}} Retry with new Session.", session.sessionId);
              // sessionId = SessionManager.SESSION_NOT_EXIST;
              sessionId = SESSION_NOT_EXIST;
              session = null;
            }
          }
        }
      } else {
        BridgeMessage.reply(bmsg, null);
      }
    } catch (Exception e) {
      if (session != null) session.shutdownChatClient();
      logger.error(Utils.getStackTrace(e));
      logger.info("Task has unexpectedly Stopped.");
    } finally {
      bmsg.closeSocket();
    }
  }

  // static functions
  private static TalkSession establishSession(BridgeMessage bmsg) throws Exception {
    AsyncClient chatClient = new AsyncClient(Context.NullContext);
    chatClient.openWithAuth(Caller.newBuilder().setAccessFrom(AccessFrom.TEXT_MESSENGER)
      .setChatbot(PreferenceHelper.getStringKeyValue(Context.NullContext, "dialog.service.chatbot"))
      .setName(bmsg.getKey()).setPhoneNo("0000").build(), ChatLanguage.kor);

    TalkSession session = new TalkSession(bmsg.getKey(), chatClient);

    logger.info("Session Established: {} ==>> {{}}", session.key, session.sessionId);

    return session;
  }

  private static TalkSession assignSession(BridgeMessage bmsg, long sessionId) throws Exception {
    AsyncClient chatClient = new AsyncClient(Context.NullContext);
    chatClient.assignSession(sessionId, ChatLanguage.kor);
    TalkSession session = new TalkSession(bmsg.getKey(), chatClient);
    // session.setLastTwinkle(SessionManager.getSessionUpdateTime(session.key));

    logger.info("Session Assigned: {} ==>> {{}}", session.key, session.sessionId);

    return session;
  }

  private static void shutdownSession(TalkSession session) {
    try {
      logger.info("Session Shutdown: {} ==>> {{}}", session.key, session.sessionId);
      // if(session.valid) {
      // session.chatClient.close();
      // }
      session.shutdownChatClient();
    } catch (Exception e) {
      logger.error(Utils.getStackTrace(e));
    }
  }

  private static final Map<String, TalkSession> sessionCache = new HashMap<>();
  private static final List<TalkSession> staleSessions = new LinkedList<>();

  private static void addSession(String key, TalkSession session) {
    synchronized (sessionCache) {
      TalkSession oldSession = sessionCache.put(key, session);
      if (oldSession != null)
        staleSessions.add(oldSession);
    }
  }

  private static TalkSession getSession(String key) {
    synchronized (sessionCache) {
      return sessionCache.get(key);
    }
  }

  private static void removeSession(String key) {
    synchronized (sessionCache) {
      TalkSession session = sessionCache.remove(key);
      if (session != null)
        staleSessions.add(session);
    }
  }

  public static int getSessionCacheCount() {
    synchronized (sessionCache) {
      return sessionCache.size();
    }
  }

  public static void cleanUpSession() {
    synchronized (sessionCache) {
      Set<String> keys = new HashSet<String>(sessionCache.keySet());
      for (String key : keys) {
        try {
          TalkSession session = sessionCache.get(key);
          if (session.valid) {
            if (!session.checkKeepAlive()) {
              // session.setLastTwinkle(SessionManager.getSessionUpdateTime(key));
              if (!session.checkKeepAlive()) {
                removeSession(key);
              }
            }
          } else {
            removeSession(key);
          }
        } catch (Exception e) {
          logger.error(Utils.getStackTrace(e));
        }
      }
      for (TalkSession session : staleSessions) {
        shutdownSession(session);
        // SessionManager.deactivateSession(session.key, session.sessionId);
      }
      staleSessions.clear();
    }
  }
}
