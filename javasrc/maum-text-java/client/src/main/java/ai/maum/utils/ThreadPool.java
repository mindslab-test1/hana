package ai.maum.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;

public class ThreadPool {
  private static final Logger logger = LoggerFactory.getLogger(ThreadPool.class);
  private static volatile ThreadPoolExecutor executorService;

  public static void init(int corePoolSize, int maximumPoolSize) {
    if (executorService == null || executorService.isTerminated()) {
      executorService = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, 10L, TimeUnit.MINUTES,
        new SynchronousQueue<Runnable>());
    }
  }

  public static void finish(boolean now) {
    if (executorService == null) return;
    if (now) {
      executorService.shutdownNow();
    } else {
      executorService.shutdown();
      try {
        executorService.awaitTermination(60, TimeUnit.SECONDS);
      } catch (Exception e) {
        logger.error(Utils.getStackTrace(e));
      }
    }
  }

  public static Executor getExecutor() {
    return executorService;
  }

  public static void proceed(Runnable task) {
    if (executorService.getActiveCount() < executorService.getMaximumPoolSize()) {
      executorService.submit(task);
    } else {
      logger.info("Pool is full.");
    }
  }

  public static void report() {
    logger.info("A{} S{}/{} L{}",
      executorService.getActiveCount(),
      executorService.getPoolSize(),
      executorService.getMaximumPoolSize(),
      executorService.getLargestPoolSize());
  }

}
