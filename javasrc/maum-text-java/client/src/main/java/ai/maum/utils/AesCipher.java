package ai.maum.utils;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import com.google.gson.Gson;

public class AesCipher {

  private IvParameterSpec ivParameter;
  private Key keySpec;

  public AesCipher(String key) {
    try {
      if (key.length() >= 32) {
        key = key.substring(0, 32);
      } else if (key.length() >= 24) {
        key = key.substring(0, 24);
      }

      SecretKeySpec keySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
      ivParameter = new IvParameterSpec(key.substring(0, 16).getBytes());
      this.keySpec = keySpec;
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
  }

  public String encrypt(String str)
      throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException,
      IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
    Cipher cipher;
    cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
    cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivParameter);

    byte[] encrypted = cipher.doFinal(str.getBytes("UTF-8"));
    String Str = new String(Base64.getEncoder().encode(encrypted));

    return Str;
  }

  public String decrypt(String str)
      throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException,
      IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
    Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
    cipher.init(Cipher.DECRYPT_MODE, keySpec, ivParameter);

    byte[] byteStr = Base64.getDecoder().decode(str.getBytes());
    String Str = new String(cipher.doFinal(byteStr), "UTF-8");

    return Str;
  }

  public static void makeExtent() {
    Map<String, String> extent = new LinkedHashMap<String, String>();
    InputPrompter prompter = new InputPrompter();
    String items[] = {"host", "port"};
    String line;
    for (String item : items) {
      prompter.setPrompt(item.toUpperCase() + "?");
      line = prompter.promptToGetLine();
      if (line.isEmpty()) {
        System.out.println(item.toUpperCase() + "is Empty. Stop!");
        return;
      }
      extent.put(item, line);
    }
    AesCipher cipher = new AesCipher("sfd");
    try {
      System.out.println("extent=" + cipher.encrypt(new Gson().toJson(extent)));
    } catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
        | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException
        | UnsupportedEncodingException e) {
      e.printStackTrace();
    }
  }

  public static String getExtent(String extent) {
    try {
      return new AesCipher("sfd").decrypt(extent);
    } catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
        | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException
        | UnsupportedEncodingException e) {
      e.printStackTrace();
      return "";
    }
  }
}
