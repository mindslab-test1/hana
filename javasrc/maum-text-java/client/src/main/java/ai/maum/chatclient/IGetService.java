package ai.maum.chatclient;

import maum.m2u.facade.Dialog;

/**
 * Service interface
 */
public interface IGetService {
  /**
   * @param cbDetail chatbot detail info.
   */
  void onGetChatbotDetail(Dialog.Chatbot cbDetail);

  /**
   * Error
   *
   * @param message error message.
   */
  void onError(String message);
}
