package ai.maum.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class InputPrompter {
  static public final String DEFAULT_PROMT = ">>>";
  BufferedReader consoleIn = new BufferedReader(new InputStreamReader(System.in));
  private String prompt;
  private String description;
  private boolean printDescription = false;

  public InputPrompter() {
    setPrompt(prompt);
  }

  public InputPrompter(String prompt) {
    setPrompt(prompt);
  }

  public InputPrompter(String description, String prompt) {
    setPrompt(prompt);
    setDescription(description);
  }

  public InputPrompter setPrompt(String prompt) {
    this.prompt = prompt;
    return this;
  }

  public InputPrompter setDescription(String description) {
    this.description = description;
    printDescription = true;
    return this;
  }

  public String promptToGetLine() {
    if (printDescription) {
      System.out.print(description);
      printDescription = false;
    }
    System.out.print(prompt);
    try {
      return consoleIn.readLine();
    } catch (Exception e) {
      e.printStackTrace();
      return "";
    }
  }
}
