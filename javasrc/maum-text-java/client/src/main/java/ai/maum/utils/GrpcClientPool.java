package ai.maum.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;


public class GrpcClientPool<T extends GrpcClient> {
  private static final Logger logger = LoggerFactory.getLogger(GrpcClientPool.class);
  private static final long cleanUpPeriod = 10 * 60 * 1000L; // 10-minutes

  private int defaultPoolSize = 3;
  private int maxPoolSize;
  private LinkedBlockingDeque<T> inReady;
  private LinkedBlockingQueue<T> inUse;
  private Timer cleanUpTimer;
  private ClientFactory<T> factory;
  private String clientTypeName;
  private String clientHost;
  private int clientPort;
  private boolean initialized = false;


  public interface ClientFactory<U extends GrpcClient> {
    U create();
  }


  private GrpcClientPool() {
  }

  public GrpcClientPool(ClientFactory<T> factory) {
    this.factory = factory;
    String typeName = this.factory.getClass().getSimpleName();
    clientTypeName = typeName.substring(0, typeName.indexOf('$'));
  }

  private T newClient() {
    try {
      T client = factory.create();
      client.init(clientHost, clientPort);
      return client;
    } catch (Exception e) {
      logger.error("Failed to get a new client<" + clientTypeName + "> .", e);
    }
    return null;
  }

  private TimerTask cleanUpTask = new TimerTask() {
    @Override
    public void run() {
      synchronized (inReady) {
        while (true) {
          T client = inReady.peek();
          if (client == null || client.isApplicable()) {
            break;
          }
          inReady.remove(client);
          client.shutdown();
        }
        int i = inUse.size() + inReady.size();
        for (; i < defaultPoolSize; i++) {
          T client = newClient();
          if (client != null) inReady.add(client);
        }
      }
    }
  };

  public String getClientTypeName() {
    return clientTypeName;
  }

  public GrpcClientPool<T> init(int maxPoolSize, String host, int port) {
    this.maxPoolSize = maxPoolSize;
    if (this.maxPoolSize < defaultPoolSize * 2) {
      defaultPoolSize = 1;
    }
    clientHost = host;
    clientPort = port;
    inReady = new LinkedBlockingDeque<>();
    inUse = new LinkedBlockingQueue<>();
    cleanUpTimer = new Timer();
    cleanUpTimer.schedule(cleanUpTask, 0, cleanUpPeriod);
    initialized = true;
    return this;
  }

  public void finish() {
    cleanUpTimer.cancel();
    inReady.forEach(client -> client.shutdown());
    inUse.forEach(client -> client.shutdown());
    clientTypeName = null;
    initialized = false;
  }

  public synchronized T getClient() throws Exception {
    if (!initialized) {
      throw new IllegalStateException("Pool<" + clientTypeName + "> is not initialized.");
    }

    T client = inReady.pollLast();

    for (int i = 0; client == null && i < 10; i++) {
      client = inReady.pollLast(10, TimeUnit.MILLISECONDS);
      if (client == null) {
        if (inUse.size() < maxPoolSize) {
          client = newClient();
        }
      } else {
        if (!client.isApplicable()) {
          client.shutdown();
          client = null;
        }
      }
    }
    inUse.add(Objects.requireNonNull(client, "Failed to borrow a client<" + clientTypeName + ">."));
    return client;
  }

  public synchronized void releaseClient(T client) {
    if (!initialized) {
      logger.info(Utils.getStackTrace(new IllegalStateException("Pool<" + clientTypeName + "> is not initialized.")));
      return;
    }
    try {
      Objects.requireNonNull(client, "Can't release null client<" + clientTypeName + ">.");
    } catch (Exception e) {
      logger.debug(Utils.getStackTrace(e));
      return;
    }
    Thread.currentThread().getStackTrace();

    inUse.remove(client);
    if ((inUse.size() + inReady.size()) < maxPoolSize) {
      client.refreshLifetime();
      inReady.add(client);
    } else {
      client.shutdown();
    }
  }

}
