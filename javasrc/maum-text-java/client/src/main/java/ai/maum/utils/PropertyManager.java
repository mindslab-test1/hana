package ai.maum.utils;

import java.math.BigDecimal;
import java.util.List;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropertyManager {
  static final Logger logger = LoggerFactory.getLogger(PropertyManager.class);

  private static String maumRootEnv = "MAUM_ROOT";
  private static String configPath = "/etc/";
  private static String configFilename = "m2u.conf";

  private static PropertiesConfiguration propertyConfig = null;

  private static void load() {
    if (propertyConfig == null) {
      logger.debug("### m2u map config file load");
      propertyConfig = getConfig();
    }
  }

  private static PropertiesConfiguration getConfig() {
    PropertiesConfiguration config = null;
    String confPath = getSystemConfPath();

    logger.debug("---------------------------------------");
    logger.debug("getSystemConfPath :: {}", confPath);
    logger.debug("getSystemConfFileName :: {}", configFilename);
    logger.debug("---------------------------------------");

    try {
      config = new PropertiesConfiguration(confPath+ configFilename);
    } catch (ConfigurationException e) {
      for (String m : e.getMessages()) {
        logger.debug(m);
      }
      System.err.println("Cannot load config " + confPath+ configFilename);
      System.exit(1);
    } catch (Exception e) {
      logger.debug(e.getMessage());
      e.printStackTrace();
      System.err.println("Cannot load config " + confPath+ configFilename);
      System.exit(1);
    }
    return config;
  }

  private static String getSystemConfPath(){
    String path = System.getenv(maumRootEnv);
    if (path == null) {
      throw new IllegalStateException("MAUM_ROOT environment variable is not defined!");
    }
    path += configPath;
    return path;
  }

  public static String getString(String propertyName) {
    load();
    return propertyConfig.getString(propertyName);
  }

  public static String[] getStringArray(String propertyName) {
    load();
    return propertyConfig.getStringArray(propertyName);
  }

  public static int getInt(String propertyName) {
    load();
    return propertyConfig.getInt(propertyName);
  }

  public static double getDouble(String propertyName) {
    load();
    return propertyConfig.getDouble(propertyName);
  }

  public static long getLong(String propertyName) {
    load();
    return propertyConfig.getLong(propertyName);
  }

  public static float getFloat(String propertyName) {
    load();
    return propertyConfig.getFloat(propertyName);
  }

  public static BigDecimal getBigDecimal(String propertyName) {
    load();
    return propertyConfig.getBigDecimal(propertyName);
  }

  public static List<Object> getList(String propertyName) {
    load();
    return propertyConfig.getList(propertyName);
  }

  public static Boolean getBoolean(String propertyName) {
    load();
    return propertyConfig.getBoolean(propertyName);
  }

}
