package ai.maum.bridge;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import ai.maum.utils.TestTalk;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BridgeMessage {
  private static final Logger logger = LoggerFactory.getLogger(BridgeMessage.class);

  private static final byte SPACE = 0x20;
  private static final byte[] REPLAY_CD = "0000".getBytes();

  public static final String RETN_CODE_SUCCESS = "0";
  public static final String RETN_CODE_FAIL = "-1";

  private static final Map<String, String> RetnMessageMap;

  static {
    RetnMessageMap = new HashMap<>();
    RetnMessageMap.put(RETN_CODE_SUCCESS, "정상적으로 처리되었습니다.");
    RetnMessageMap.put(RETN_CODE_FAIL, "처리에 오류가 발생했습니다.");
  }

  static class BMsg {
    static BMsg parseHeader(byte[] bmsg) {
      return new BMsg();
    }

    static byte[] parseBody(byte[] bmsg) {
      return bmsg;
    }
  }

  private Socket socket;
  //	private BMsg header;
  private byte[] body;
  private String umsMessage;
  private int aliasCount;
  private String[] aliases;
  private String key;
  private boolean valid = false;
  private String retnCode;
  private long sessionId = -1;

  public BridgeMessage(byte[] bmsg, Socket sck) {
    int process = 0;
    try {
      socket = sck;
//			header = BMsg.parseHeader(bmsg);
      byte[] data = BMsg.parseBody(bmsg);
      if (getFileds(data)) {
        valid = true;
      }
    } catch (Exception e) {
      logger.error("Parsing Process: {}", process, e);
      setRetnCode(RETN_CODE_FAIL);
    }
  }

  public BridgeMessage(String retnCode, Socket sck) {
    setRetnCode(retnCode);
    socket = sck;
  }

  public BridgeMessage(String talk) {
    Random generator = new Random();
    key = String.format("010%09d", generator.nextInt(900000000) + 1000000000);
    umsMessage = talk;
    aliasCount = 0;
    aliases = new String[0];
    valid = true;
  }

  private String getStringValue(byte[] body, int start, int len) {
    int end = start + len;
    while (end > start) {
      --end;
      if (body[end] != SPACE) {
        break;
      }
      --len;
    }

    try {
      return new String(body, start, len, "EUC-KR");
    } catch (UnsupportedEncodingException e) {
      logger.error("Failed to get String value.", e);
      return new String(body, start, len);
    }
  }

  private boolean getFileds(byte[] body) {
    umsMessage = getStringValue(body, 0, 0);
    if (umsMessage.isEmpty()) {
      logger.error("UMS Message is Empty.");
      setRetnCode(RETN_CODE_FAIL);
      return false;
    }

    try {
      aliasCount = Integer.parseInt(new String(body, 0, 0));
      if (aliasCount < 0 || aliasCount > 5) {
        throw new Exception("Invalid alias number: " + aliasCount);
      }
    } catch (Exception e) {
      logger.error("Parsing Alias number", e);
      setRetnCode(RETN_CODE_FAIL);
      return false;
    }

    aliases = new String[aliasCount];
    for (int i = 0; i < aliasCount; ++i) {
      aliases[i] = getStringValue(body, 0, 0);
      if (aliases[i].isEmpty()) {
        setRetnCode(RETN_CODE_FAIL);
        return false;
      }
    }

    key = "_";

    return true;
  }

  public boolean isValid() {
    return valid;
  }

  public String getKey() {
    return key;
  }

  public String getUmsMessage() {
    return umsMessage;
  }

  public String getAliases() {
    StringBuilder builder = new StringBuilder();

    for (String alias : aliases) {
      builder.append(alias);
      builder.append(" ");
    }

    if (builder.length() > 0) {
      builder.setLength(builder.length() - 1);
      return builder.toString();
    }

    return "";
  }

  public String getAliasCount() {
    return "Aliases[" + aliasCount + "]: ";
  }

  public void report() {
    if (!isValid()) {
      logger.trace("\tMessage is Invalid.");
      return;
    }

    logger.trace("\tKey: {}", getKey());
  }

  public void setRetnCode(String code) {
    retnCode = code;
  }

  public boolean hasRetnCode() {
    return retnCode != null;
  }

  public boolean isRetnCodeSuccess() {
    return hasRetnCode() && retnCode.equals(RETN_CODE_SUCCESS);
  }

  public void setSessionId(long sessionId) {
    this.sessionId = sessionId;
  }

  public void closeSocket() {
    if (socket != null) {
      try {
        socket.close();
      } catch (Exception e) {
        logger.error("closeSocket", e);
      }
    }
  }


  public static BridgeMessage receive(ServerSocket server) throws Exception {
    BridgeMessage bmsg;
    boolean forTest = true;
    if (!forTest) {
      Socket sck = null;
      byte[] rcvData = new byte[1024];
      try {
        sck = server.accept();
        BufferedInputStream buffIn = new BufferedInputStream(sck.getInputStream());
        int rcvDataLen = buffIn.read(rcvData);
        logger.trace("   ------ Message received: {}bytes -----", rcvDataLen);

        // FileOutputStream bw = new FileOutputStream("rcvdata.bmsg");
        // bw.write(rcvData);
        // bw.close();
        bmsg = new BridgeMessage(rcvData, sck);

      } catch (SocketTimeoutException e) {
        return null;
      } catch (Exception e) {
        logger.error("Receiving Messages", e);
        bmsg = new BridgeMessage(BridgeMessage.RETN_CODE_FAIL, sck);
      }
    } else {
      String talk = TestTalk.getTalk();
      if (talk.isEmpty()) {
        return null;
      } else {
        Thread.sleep(100);
        logger.trace("   ------ Message received: {}bytes -----", talk.getBytes().length);
        bmsg = new BridgeMessage(talk);
      }

//      FileInputStream br = new FileInputStream("rcvdata.bmsg");
//      byte[] rcvData = new byte[br.available()];
//      if (br.read(rcvData) > 0) {
//        logger.trace(" ------ Message received: {}bytes -----", rcvData.length);
//      } else {
//        logger.warn("Read data file error.");
//      }
//      br.close();
//      bmsg = new BridgeMessage(rcvData, null);
    }
    bmsg.report();

    return bmsg;
  }


  // meaningless pseudo-code
  private static byte[] getReplyBody(BridgeMessage bmsg, JsonObject json) throws Exception {
    byte[] body = bmsg.body;
    System.arraycopy(REPLAY_CD, 0, body, 10, 5);
    System.arraycopy(bmsg.retnCode.getBytes("EUC-KR"), 0, body, 10, 5);
//        byte[] rspsMsg = RetnMessageMap.get(bmsg.retnCode).getBytes("EUC-KR");
    Arrays.fill(body, 1, 100, SPACE);

    if (bmsg.isRetnCodeSuccess()) {
      logger.trace("{{}} response_cd: ", bmsg.sessionId, json.get("response_cd").getAsString());
      logger.trace("{{}} response_msg: ", +bmsg.sessionId, new Gson().toJson(json.get("response_msg")));
      try {
        byte[] field = new Gson().toJson(json.get("response_cd")).getBytes("EUC-KR");
        System.arraycopy(field, 0, body, 0, field.length < 10 ? field.length : 10);
        field = json.get("response_msg").getAsString().getBytes("EUC-KR");
        System.arraycopy(field, 0, body, 0, field.length < 10 ? field.length : 10);
      } catch (UnsupportedEncodingException e) {
        logger.error("EUC-KR is Unsupported.", e);
        byte[] field = new Gson().toJson(json.get("response_cd")).getBytes();
        System.arraycopy(field, 0, body, 0, field.length < 10 ? field.length : 10);
        field = json.get("response_msg").getAsString().getBytes();
        System.arraycopy(field, 0, body, 0, field.length < 10 ? field.length : 10);
      }
    } else {
      logger.trace("{{}} {}:{}", bmsg.sessionId, bmsg.retnCode, RetnMessageMap.get(bmsg.retnCode));
    }

    return body;
  }

  public static void reply(BridgeMessage bmsg, JsonObject json) {
    try {
      if (bmsg.socket == null) {
        if (json != null) {
          logger.info("{{}} response_cd: ", bmsg.sessionId, json.get("response_cd").getAsString());
          logger.info("{{}} response_msg: ", bmsg.sessionId, new Gson().toJson(json.get("response_msg")));
        }
        logger.info("{{}} NOT SENDING: Socket is null.", bmsg.sessionId);
        return;
      }

      byte[] body = getReplyBody(bmsg, json);

      // FileOutputStream bw = new FileOutputStream("sendData.bmsg");
      // bw.write(totalPacket);
      // bw.close();
      BufferedOutputStream buffOut = new BufferedOutputStream(bmsg.socket.getOutputStream());
      buffOut.write(body);
      logger.info("{{}} SUCCESS: Sending Message", bmsg.sessionId);
    } catch (Exception e) {
      logger.error("{{}} FAILED: Sending Message", bmsg.sessionId, e);
    }
  }
}
