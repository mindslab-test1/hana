package ai.maum.chatclient;

import io.grpc.Metadata;

/**
 * Meta interface
 */
public interface IMetadataGetter {
  /**
   * Medata getting.
   *
   * @param md metadata
   */
  void onMetaData(Metadata md);
}
