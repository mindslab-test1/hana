package ai.maum.network;

import ai.maum.utils.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classes for NetConfigInfo
 */
public final class NetConfigInfo {
  private static final Logger logger = LoggerFactory.getLogger(NetConfigInfo.class);

  //PROFILE 설정
  private final static String profiles = "DEV";
//  private final static String profiles = "STG";
//  private final static String profiles = "PROD";

  /***********************************************************************************************
   * 설정 정보
   **********************************************************************************************/
  //protocal
//  public final static String PROTOCOL = "HTTP";
  public static String PROTOCOL = "HTTPS";

  public static String DEV_API_URL = "beta.maum.ai";
//  private static String STG_API_URL = "beta.maum.ai";
//  private static String PROD_API_URL = "maum.ai";

  public static Integer REST_PORT = 443;

  // Grpc connection server info settings.
  public static String DEV_GRPC_HOST_ADDRESS = "beta.maum.ai";
  //  private static String STG_GRPC_HOST_ADDRESS = "beta.maum.ai";
//  private static String PROD_GRPC_HOST_ADDRESS = "maum.ai";
  // GRPC Port
  public static Integer GRPC_PORT = 9901;
  // Profile image download url.
  private static final String PROFILE_IMAGE_URL = "/api/v1/SvcUser";
  // Chatbot Image Icon URL.
  public final static String CHATBOT_ICON_URL_PREFIX_128 = "https://cdn.maum.ai/icons/128";
  // Chatbot ScreenShot Image URL.(Ex:http://cdn.mindslab.ai/images/chatbots/{{name}}.png)
  public final static String CHATBOT_SCREENSHOT_URL_PREFIX = "https://cdn.maum.ai/images/chatbots";

  // API 공통 URI
  public final static String API_COMMON_URI = "/api/v1";

  public final static int TIMEOUT = 30; // 기본 30초
  public final static String CHARSET = "UTF-8";  // 기본 UTF-8

  public final static int RESCODE_EXCEPTION = 999; //Network 오류 시 초기설정 코드(Network Exception)

  //PUT, GET, POST, PATCH, DELETE
  public final static String HTTP_METHOD_PUT = "PUT";
  public final static String HTTP_METHOD_GET = "GET";
  public final static String HTTP_METHOD_POST = "POST";
  public final static String HTTP_METHOD_PATCH = "PATCH";
  public final static String HTTP_METHOD_DELETE = "DELETE";

  /**
   * Net config init.
   *
   * @param context
   */
  public static int initNetConfig(Context context) {
    // Set same as Default.
    PROTOCOL = "HTTPS";
    DEV_API_URL = "beta.maum.ai";
    REST_PORT = 443;
    DEV_GRPC_HOST_ADDRESS = "beta.maum.ai";
    GRPC_PORT = 9901;

    return 0;
  }

  /**
   * Set http protocol.
   *
   * @param isHttps
   */
  public static void setHttpProtocolType(Context context, boolean isHttps) {
    if (isHttps == false) {
      PROTOCOL = "HTTPS";
    } else {
      PROTOCOL = "HTTP";
    }

    //Set rest http protocol type.
//    PreferenceHelper.setBooleanKeyValue(context, PreferenceHelper.KEY_NET_REST_IS_HTTPS, isHttps);
  }

  /**
   * REST API Url
   *
   * @return rest api url stirng.
   */
  public static String getApiUrl() {
    String protocol = "";
    protocol += PROTOCOL.toLowerCase() + "://";
    protocol += DEV_API_URL;

    logger.info("getApiUrl(): {}", protocol);

    return protocol;
  }

  /**
   * Set rest api url.
   *
   * @param context
   * @param url
   * @param port
   */
  public static void setApiUrlPort(Context context, String url, int port) {
    DEV_API_URL = url;
    REST_PORT = port;

    //Set rest url.
//    PreferenceHelper.setStringKeyValue(context, PreferenceHelper.KEY_NET_REST_URL, url);
    //Set rest port.
//    PreferenceHelper.setIntKeyValue(context, PreferenceHelper.KEY_NET_REST_PORT, port);
  }

  /**
   * GRPC URL
   *
   * @return grpc url string.
   */
  public static String getGrpcUrl() {
//    logger.info("getGrpcUrl(): {}", DEV_GRPC_HOST_ADDRESS);
    return DEV_GRPC_HOST_ADDRESS;
  }

  /**
   * Set grpc url & port
   *
   * @param url
   * @param port
   */
  public static void setGrpcUrlPort(Context context, String url, Integer port) {
    DEV_GRPC_HOST_ADDRESS = url;
    GRPC_PORT = port;

    //Set grpc url.
//    PreferenceHelper.setStringKeyValue(context, PreferenceHelper.KEY_NET_GRPC_URL, url);
    //Set grpc port.
//    PreferenceHelper.setIntKeyValue(context, PreferenceHelper.KEY_NET_GRPC_PORT, port);
  }

  /**
   * Profile Image Url prefix.
   *
   * @return profile image url string.
   */
  public static String getProfileImageUrl() {
    String url = NetConfigInfo.getApiUrl() + PROFILE_IMAGE_URL;
    logger.info("getProfileImageUrl(): {}", url);
    return url;
  }
}
