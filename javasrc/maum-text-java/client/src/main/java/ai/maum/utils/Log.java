package ai.maum.utils;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.SimpleFormatter;

import ai.maum.preference.PreferenceHelper;

public class Log {
  private static final long A_DAY_IN_MILLIS = 24 * 60 * 60 * 1000L;
  private static final int DEFAULT_MAX_SIZE = 200 * 1024 * 1024;
  private static final int MAX_KEEP_LOG_NUM = 1000;
  private static final java.util.logging.Logger logger;
  private static FileHandler fh = null;
  private static Timer dailyTimer = new Timer();

  static {
    System.setProperty("java.util.logging.SimpleFormatter.format", "%1$tF %1$tT %5$s%6$s%n ");

    logger = java.util.logging.Logger.getLogger("maum-bridge");
    logger.setUseParentHandlers(false);
    logger.setLevel(Level.FINE);

    if (PreferenceHelper.getBooleanKeyValue(Context.NullContext, "log.stdout") ||
      PreferenceHelper.getBooleanKeyValue(Context.NullContext, "exec.mode.console")) {
      Handler handler = new ConsoleHandler();
      handler.setLevel(Level.FINE);
      logger.addHandler(handler);
    }

    if (!PreferenceHelper.getBooleanKeyValue(Context.NullContext, "exec.mode.console")) {
      attachDailyFileHandler();
      setDailyTimer();
    }
  }

  private static void setDailyTimer() {
    TimerTask task = new TimerTask() {
      @Override
      public void run() {
        attachDailyFileHandler();
        setDailyTimer();
      }
    };

    Calendar tomorrow = new GregorianCalendar();
    tomorrow.setTimeInMillis(System.currentTimeMillis() + A_DAY_IN_MILLIS);
    tomorrow.set(Calendar.HOUR_OF_DAY, 0);
    tomorrow.set(Calendar.MINUTE, 0);
    tomorrow.set(Calendar.SECOND, 0);
    tomorrow.set(Calendar.MILLISECOND, 0);

    dailyTimer.schedule(task, tomorrow.getTime());
  }

  private static void attachDailyFileHandler() {
    synchronized (logger) {
      try {
        String logPath = PreferenceHelper.getStringKeyValue(Context.NullContext, "log.path");
        if (logPath.isEmpty())
          logPath = "./log";
        logPath += System.getProperty("file.separator");
        new File(logPath).mkdirs();
        int maxSize = PreferenceHelper.getIntKeyValue(Context.NullContext, "log.size.maximum");
        FileHandler fnNew = new FileHandler(
          logPath + "maum-bridge.log." + new SimpleDateFormat("yyyyMMdd").format(new Date()),
          maxSize < 0 ? DEFAULT_MAX_SIZE : maxSize, MAX_KEEP_LOG_NUM,
          true);
        if (fnNew != null) {
          fnNew.setFormatter(new SimpleFormatter());
          fnNew.setLevel(Level.FINE);
          // fnNew.setErrorManager(new ErrorManager());
          if (fh != null) {
            logger.removeHandler(fh);
            fh.close();
          }
          fh = fnNew;
          logger.addHandler(fh);
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  public static void verbose(String tag, String msg) {
    logger.severe("VRB " + tag + ": " + msg);
    fh.flush();
  }

  public static void debug(String tag, String msg) {
    logger.severe("DBG " + tag + ": " + msg);
    fh.flush();
  }

  public static void info(String tag, String msg) {
    logger.severe("INF " + tag + ": " + msg);
    fh.flush();
  }

  public static void error(String tag, String msg) {
    logger.severe("ERR " + tag + ": " + msg);
    fh.flush();
  }

  public static void error(String tag, String msg, Object param1) {
    logger.log(Level.SEVERE, "ERR " + tag + ": ", param1);
    fh.flush();
  }

  public static void exception(String tag, Exception e) {
    StringWriter msg = new StringWriter();
    e.printStackTrace(new PrintWriter(msg));
    logger.severe("EXP " + tag + ": " + msg.toString());
    fh.flush();
  }

  public static void terminate() {
    synchronized (logger) {
      dailyTimer.cancel();
      logger.removeHandler(fh);
      fh.close();
      fh = null;
    }
  }
}