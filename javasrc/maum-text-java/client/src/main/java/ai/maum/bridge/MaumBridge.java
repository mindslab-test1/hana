package ai.maum.bridge;

import ai.maum.utils.Utils;
import io.grpc.Metadata;

import java.io.FileInputStream;
import java.net.URL;
import java.security.InvalidParameterException;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import com.google.common.util.concurrent.SettableFuture;

import ai.maum.chatclient.IGetChatbotsCallback;
import ai.maum.chatclient.IMetadataGetter;
import ai.maum.chatclient.ChatbotFinderClient;
import ai.maum.network.NetConfigInfo;
import ai.maum.preference.PreferenceHelper;
import ai.maum.utils.Context;
import ai.maum.utils.InputPrompter;
import maum.m2u.facade.Dialog.Chatbot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MaumBridge {
  private static final Logger logger = LoggerFactory.getLogger(MaumBridge.class);
  private static final String serviceHost;
  private static final int servicePort;
  private static final String chatbot;
  private static final int maximumPoolSize = 100;
  private static final long CLEANUP_PERIOD = 10000;
  private static final long REPORT_PERIOD = 60000;

  private ThreadPoolExecutor executorService;
  private long nextReportTime = System.currentTimeMillis();
  private long nextCleanUpTime = nextReportTime + CLEANUP_PERIOD;

  static private final boolean propsLoaded;
  static private final Exception loadException;

  static {
    String host, _chatbot;
    int port;
    boolean loaded;
    Exception exp;
    try {
      Properties props = new Properties();
      props.load(new FileInputStream(System.getProperty("propsPath")));

      host = props.getProperty("dialog.service.host");
      port = Integer.parseInt(props.getProperty("dialog.service.port"));
      _chatbot = props.getProperty("dialog.service.chatbot");

      if (host.isEmpty() || port < 1 || _chatbot.isEmpty()) {
        throw new InvalidParameterException();
      }

      PreferenceHelper.setStringKeyValue(Context.NullContext, "dialog.service.chatbot", _chatbot);
      PreferenceHelper.setStringKeyValue(Context.NullContext, "log.path", props.getProperty("log.path"));
      PreferenceHelper.setStringKeyValue(Context.NullContext, "log.size.maximum", props.getProperty("log.size.maximum"));
      PreferenceHelper.setBooleanKeyValue(Context.NullContext, "log.stdout", Boolean.parseBoolean(props.getProperty("log.stdout")));
      PreferenceHelper.setStringKeyValue(Context.NullContext, "test.talk.file", props.getProperty("test.talk.file"));

      exp = null;
      loaded = true;
    } catch (Exception e) {
      exp = e;
      host = null;
      port = -1;
      _chatbot = null;
      loaded = false;
    }
    serviceHost = host;
    servicePort = port;
    chatbot = _chatbot;
    propsLoaded = loaded;
    loadException = exp;
  }

  /**
   * logVersions
   *
   * @param console
   */
  protected void logVersion(boolean console) {
    try {
      String version = "{Version information is not found.}";
      Enumeration<URL> resources = MaumBridge.class.getClassLoader().getResources("META-INF/MANIFEST.MF");
      while (resources.hasMoreElements()) {
        Manifest manifest = new Manifest(resources.nextElement().openStream());
        Attributes attrs = manifest.getMainAttributes();
        if (attrs.getValue("BuildTime") != null) {
          version = attrs.getValue("Title") + " Version " + attrs.getValue("Version") + " / "
            + attrs.getValue("BuildTime");
          break;
        }
      }
      if (console) {
        System.out.println(version);
      } else {
        logger.info(version);
      }
    } catch (Exception e) {
      logger.error(Utils.getStackTrace(e));
    }
  }

  protected void setShutdownHook() {
    // final long start = System.nanoTime();
    Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
      public void run() {
        shutdownThreadPoolNow();
        // logger.info("Program execution took %f seconds.", System);
        logger.error("Execution has been Terminated.");
        Runtime.getRuntime().halt(-1);
      }
    }));
  }

  protected void prepareThreadPool() {
    executorService = new ThreadPoolExecutor(0, maximumPoolSize, 10L, TimeUnit.MINUTES,
      new SynchronousQueue<Runnable>());
    logger.info("[maum-bridge] Thread Pool created with maxi-size {}.", maximumPoolSize);
  }

  protected void postSendTalkToThreadPool(BridgeMessage bmsg) {
    executorService.execute(new BridgeTask(bmsg));
  }

  protected void cleanUpAndReport() {
    long now = System.currentTimeMillis();
    if (nextCleanUpTime <= now) {
      BridgeTask.cleanUpSession();
      nextCleanUpTime = now + CLEANUP_PERIOD;
    }
    if (nextReportTime <= now) {
      logger.info("Active Sessions:{} ThreadPool:{}/{}"
        , BridgeTask.getSessionCacheCount()
        , executorService.getActiveCount()
        , executorService.getPoolSize());
      nextReportTime = now + REPORT_PERIOD;
    }
  }

  protected void __shutdownThreadPool(boolean now) {
    if (executorService == null)
      return;
    synchronized (executorService) {
      if (now) {
        executorService.shutdownNow();
      } else {
        executorService.shutdown();
        try {
          executorService.awaitTermination(60, TimeUnit.SECONDS);
        } catch (Exception e) {
          logger.error(Utils.getStackTrace(e));
        }
      }
    }
  }

  protected void shutdownThreadPoolNow() {
    __shutdownThreadPool(true);
  }

  protected void shutdownThreadPool() {
    __shutdownThreadPool(false);
  }

  protected void setupNetwork() {
    NetConfigInfo.initNetConfig(Context.NullContext);
    NetConfigInfo.setHttpProtocolType(Context.NullContext, false);
    NetConfigInfo.setGrpcUrlPort(Context.NullContext, serviceHost, servicePort);
  }


  /**
   * Get chatbot list.
   */
  protected SettableFuture<List<Chatbot>> getChatbot() {
    SettableFuture<List<Chatbot>> chatbotList = SettableFuture.create();

    // Create chat client session.
    ChatbotFinderClient chatbotFinderClient = new ChatbotFinderClient(Context.NullContext,
      new IMetadataGetter() {
        // Meta data receiver
        @Override
        public void onMetaData(Metadata md) {
          // logger.info(md.toString());

        }
      });

    // Get Chatbot
    chatbotFinderClient.getSenderChatbots(new IGetChatbotsCallback() {
      @Override
      public void onGetChatbots(final List<Chatbot> list) {
        chatbotList.set(list);
      }

      @Override
      public void onError(String message) {
        chatbotList.setException(new Throwable(message));
      }
    });

    return chatbotList;
  }

  /**
   * Get chatbot list.
   */
  protected boolean checkChatbot() {
    try {
      List<Chatbot> list;
      int waits[] = {10, 50, 120, 120, 300};
      for (int wait : waits) {
        list = getChatbot().get(wait, TimeUnit.SECONDS);
        if (list != null) {
          for (Chatbot item : list) {
            if (item.getName().equals(chatbot)) {
              return true;
            }
          }
          break;
        }
      }
      return false;
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();
      logger.error(Utils.getStackTrace(e));
      return false;
    } catch (ExecutionException | TimeoutException e) {
      logger.error(Utils.getStackTrace(e));
      return false;
    }
  }

  protected void runBridge() throws Exception {
    logVersion(false);
    setupNetwork();
    setShutdownHook();
    if (!checkChatbot()) {
      logger.error("[maum-bridge] Service Group {{}} is not operational.", chatbot);
      logger.error("[maum-bridge] Faile to start Server.");
      return;
    }
    logger.info("[maum-bridge] Service Group {{}} is operational.", chatbot);
    boolean activated = true;
    prepareThreadPool();
    do {
      BridgeMessage bmsg = BridgeMessage.receive(null);
      if (bmsg != null) {
        postSendTalkToThreadPool(bmsg);
      } else {
        Thread.sleep(100);
      }
      cleanUpAndReport();
    } while (activated);
    shutdownThreadPool();
  }

  protected void chatOnConsole() throws Exception {
    PreferenceHelper.setBooleanKeyValue(Context.NullContext, "exec.mode.console", true);
    if (!MaumBridge.propsLoaded) {
      throw MaumBridge.loadException;
    }
    setupNetwork();
    InputPrompter prompter = new InputPrompter(
      "\nSelections:\n" + "   1: GRPC dialog\n" + "   2: REST dialog\n" + "   else : quit\n\n",
      "Select [1|2] >>> ");
    String line = prompter.promptToGetLine();
    switch (line) {
      case "1":
        new GrpcDialogTask().run();
        break;
      case "2":
        new RestDialogTask().run();
        break;
      default:
        System.out.println("Quit. Bye~~\n");
        break;
    }
  }

  /**
   * If provided, the first element of {@code args} is the name to use in the
   * greeting.
   */
  public static void main(String[] args) throws Exception {
    try {
      if (args.length > 0) {
        switch (args[0]) {
          case "console":
            new MaumBridge().chatOnConsole();
            break;
          case "version":
            new MaumBridge().logVersion(true);
            break;
        }
        return;
      }

      if (!MaumBridge.propsLoaded) {
        logger.error("Loading properties failed.");
        throw MaumBridge.loadException;
      }
      new MaumBridge().runBridge();
    } catch (Exception e) {
      logger.error(Utils.getStackTrace(e));
    }
  }
}
