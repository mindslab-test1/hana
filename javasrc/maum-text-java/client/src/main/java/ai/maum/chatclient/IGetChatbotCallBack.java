package ai.maum.chatclient;

import maum.m2u.facade.Dialog;

/**
 * Chatbot interface
 */
public interface IGetChatbotCallBack {
  /**
   * @param chatbot chatbot info.
   */
  void onGetChatbot(Dialog.Chatbot chatbot);

  /**
   * Error
   *
   * @param message error message.
   */
  void onError(String message);
}
