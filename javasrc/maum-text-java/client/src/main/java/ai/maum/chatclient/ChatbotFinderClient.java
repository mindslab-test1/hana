package ai.maum.chatclient;


import com.google.protobuf.Empty;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import ai.maum.network.NetConfigInfo;
import ai.maum.preference.PreferenceHelper;
import ai.maum.utils.Context;
import maum.m2u.facade.Dialog;
import maum.m2u.facade.Dialog.Chatbot;
import maum.m2u.facade.Dialog.ChatbotList;
import maum.m2u.facade.ChatbotFinderGrpc;
import maum.m2u.facade.ChatbotFinderGrpc.ChatbotFinderStub;

import io.grpc.Channel;
import io.grpc.ClientInterceptors;
import io.grpc.ManagedChannel;
import io.grpc.Metadata;
import io.grpc.netty.NettyChannelBuilder;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classes for ChatbotFinderClient
 */
public class ChatbotFinderClient {
  /* For log tag */
  private final Logger logger = LoggerFactory.getLogger(ChatbotFinderClient.class);
  /* Context */
  private Context context;
  /* Chatbot stub */
  private ChatbotFinderStub chatbotFinderStub;
  /* Stub channel */
  private ManagedChannel originalChannel;
  /* Service group */
  private String chatbot = "meari";

  /* Language type */
  public enum ChatLanguage {
    kor,
    eng
  }

  /* chat language type save */
  private ChatLanguage chatLanguage = ChatLanguage.kor;

  /**
   * Constructor
   */
  public ChatbotFinderClient(Context context, IMetadataGetter mg) {
    final Channel underlyingChannel;

//    logger.log(Level.INFO, NetConfigInfo.getGrpcUrl() + ":" + NetConfigInfo.GRPC_PORT);

    this.context = context;
    originalChannel = NettyChannelBuilder
      .forAddress(NetConfigInfo.getGrpcUrl(), NetConfigInfo.GRPC_PORT)
      .usePlaintext(true)
      .build();

    HeaderClientInterceptor headerClientInterceptor = new HeaderClientInterceptor();
    headerClientInterceptor.setMetadataGetter(mg);

    underlyingChannel = ClientInterceptors.intercept(originalChannel, headerClientInterceptor);
    chatbotFinderStub = ChatbotFinderGrpc.newStub(underlyingChannel);
  }

  /**
   * Grpc shutdown.
   */
  public void shutdown() throws InterruptedException {
    if (!(originalChannel.isShutdown() || originalChannel.isTerminated())) {
      originalChannel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }
  }

  /**
   * Get all chatbots.
   *
   * @param isAll
   * @param chatbotList
   * @param chatbotCallback
   */
  public void getSenderAllChatbots(boolean isAll, final List<HashMap> chatbotList, final IGetChatbotsCallback chatbotCallback) {
    Dialog.ChatbotFindParams.Builder builder = Dialog.ChatbotFindParams.newBuilder();
    builder.setAll(isAll);
    if (isAll == false) {
      ArrayList<String> addedList = new ArrayList<String>();
      for (int i = 0; i < chatbotList.size(); i++) {
        HashMap info = (HashMap) chatbotList.get(i);

        if (info.containsKey("name") == true) {
          String chatbotName = info.get("name").toString();
          if (addedList.contains(chatbotName) == false) {
            logger.info("getSenderAllChatbots():" + chatbotName);
            addedList.add(chatbotName);
            builder.addNames(chatbotName);
          }
        }
      }
    }
    Dialog.ChatbotFindParams request = builder.build();

    chatbotFinderStub.getAllChatbots(request, new StreamObserver<ChatbotList>() {
      @Override
      public void onNext(ChatbotList responseData) {
        logger.info("getSenderAllChatbots:" + responseData.getChatbotsList());
        chatbotCallback.onGetChatbots(responseData.getChatbotsList());
      }

      @Override
      public void onError(Throwable err) {
        logger.info("getSenderAllChatbots error:" + err.toString());
      }

      @Override
      public void onCompleted() {
        logger.info("getSenderAllChatbots completed");
      }
    });
  }

  /**
   * Get Chatbots
   */
  public void getSenderChatbots(final IGetChatbotsCallback chatbotCallback) {
    Empty.Builder builder = Empty.newBuilder();
    Empty empty = builder.build();

    chatbotFinderStub.getChatbots(empty, new StreamObserver<ChatbotList>() {
      @Override
      public void onNext(ChatbotList responseData) {
//        System.out.println("getChatbots:"+ responseData.getChatbotsList());
        chatbotCallback.onGetChatbots(responseData.getChatbotsList());
      }

      @Override
      public void onError(Throwable err) {
//        System.out.println("getChatbots error:" + err.toString());
        chatbotCallback.onError(err.toString());
      }

      @Override
      public void onCompleted() {
//        System.out.println("getChatbots completed");
      }
    });
  }

  /**
   * Get Chatbot
   */
  public void getSenderChatbot(final String chatbotName,
                               final IGetChatbotCallBack getChatbotCallback) {
    // Get Access-Token
    String accessToken = PreferenceHelper.getStringKeyValue(context,
      PreferenceHelper.KEY_NETWORK_ACCESS_TOKEN);

    /* Set meta data */
    Metadata.Key<String> authToken = Metadata.Key.of("x-maum-authentication-token", Metadata.ASCII_STRING_MARSHALLER);
    Metadata.Key<String> provider = Metadata.Key.of("x-maum-authentication-provider", Metadata.ASCII_STRING_MARSHALLER);

    Metadata header = new Metadata();
    header.put(authToken, accessToken);
    header.put(provider, "maum");
    chatbotFinderStub = MetadataUtils.attachHeaders(chatbotFinderStub, header);
    logger.info("[GRPC Header]" + header.toString());

    /* Make Request Param */
    Dialog.ChatbotKey.Builder builder = Dialog.ChatbotKey.newBuilder();
    builder.setName(chatbotName);
    Dialog.ChatbotKey cbKey = builder.build();

    /* Request getChatbot */
    chatbotFinderStub.getChatbot(cbKey, new StreamObserver<Chatbot>() {
      @Override
      public void onNext(Chatbot chatbot) {
        System.out.println("onNext getSenderChatbot:" + chatbot.toString());
        getChatbotCallback.onGetChatbot(chatbot);
      }

      @Override
      public void onError(Throwable err) {
        System.out.println("onError getSenderChatbot:" + err.toString());
        getChatbotCallback.onError(err.toString());
      }

      @Override
      public void onCompleted() {
        System.out.println("onCompleted getSenderChatbot");
      }
    });
  }

  /**
   * Get User Attributes
   */
  public void getSenderUserAttributes(final String chatbotName,
                                      final IUserAttributesCallback userAttrCallback) {
    // Get Access-Token
    String accessToken = PreferenceHelper.getStringKeyValue(context, PreferenceHelper.KEY_NETWORK_ACCESS_TOKEN);

    /* Set meta data */
    Metadata.Key<String> authToken = Metadata.Key.of("x-maum-authentication-token", Metadata.ASCII_STRING_MARSHALLER);
    Metadata.Key<String> provider = Metadata.Key.of("x-maum-authentication-provider", Metadata.ASCII_STRING_MARSHALLER);

    Metadata header = new Metadata();
    header.put(authToken, accessToken);
    header.put(provider, "maum");
    chatbotFinderStub = MetadataUtils.attachHeaders(chatbotFinderStub, header);
    logger.info("[GRPC Header]" + header.toString());

    /* Make Request Param */
    Dialog.ChatbotKey.Builder builder = Dialog.ChatbotKey.newBuilder();
    builder.setName(chatbotName);
    Dialog.ChatbotKey cbKey = builder.build();

    /* Request getChatbot */
    chatbotFinderStub.getUserAttributes(cbKey, new StreamObserver<Dialog.ChatbotUserAttributes>() {
      @Override
      public void onNext(Dialog.ChatbotUserAttributes value) {
        System.out.println("onNext getSkillUserAttrsCount:" + value.getSkillUserAttrsCount());
        System.out.println("onNext getSkillUserAttrsList:" + value.getSkillUserAttrsList());
        System.out.println("onNext getSkillUserAttrsList:" + value.toString());
        System.out.println("onNext getChatbot:" + value.getChatbot());
        userAttrCallback.onGetUserAttributes(value);
      }

      @Override
      public void onError(Throwable t) {
        System.out.println("onError userAttributes:" + t.toString());
        userAttrCallback.onError(t.toString());
      }

      @Override
      public void onCompleted() {
        System.out.println("onCompleted userAttributes");
      }
    });
  }
}


