package ai.mindslab.hana;

import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

import java.io.IOException;
import java.util.logging.Logger;

import com.google.protobuf.Empty;

import elsa.facade.Dialog.AccessFrom;
import elsa.facade.Dialog.AudioUtter;
import elsa.facade.Dialog.Caller;
import elsa.facade.Dialog.ServiceGroupList;
import elsa.facade.Dialog.Session;
import elsa.facade.Dialog.SessionKey;
import elsa.facade.Dialog.SessionStat;
import elsa.facade.Dialog.TextUtter;
import elsa.facade.DialogServiceGrpc;
import elsa.facade.ServiceGroupFinderGrpc;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;

/**
 * Server that manages startup/shutdown of a {@code Greeter} server.
 */
public class ChatServer {
  private static final Logger logger = Logger.getLogger(ChatServer.class.getSimpleName());

  private Server server;


//int64 id = 1;
//string svc_group = 2;
//string user_key = 3;
//AccessFrom accessFrom = 4;
//map<string, string> contexts = 5;
//google.protobuf.Timestamp start_time = 6;
//google.protobuf.Duration duration = 7;
//bool human_assisted = 8;

  static class DialogServiceImpl extends DialogServiceGrpc.DialogServiceImplBase {
	@Override
    public void open(Caller request, StreamObserver<Session> responseObserver) {
      logger.info("DialogService open() called.");
      Session session = Session.newBuilder()
    		  				.setId(1879)
							.setSvcGroup("MAUM-TEXT")
							.setUserKey("USER-KEY:00000000")
//							.setAccessFrom()
//							.setContext()
//							.setStartTime()
//							.setDuration()
							.setHumanAssisted(false)
							.build();
      responseObserver.onNext(session);
      responseObserver.onCompleted();
	}

    @Override
    public StreamObserver<TextUtter> textTalk(StreamObserver<TextUtter> responseObserver) {
      logger.info("DialogService textTalk() called.");
      return asyncUnimplementedStreamingCall(DialogServiceGrpc.METHOD_TEXT_TALK, responseObserver);
    }
  }

  static class ServiceGroupFinderImpl extends ServiceGroupFinderGrpc.ServiceGroupFinderImplBase {
    @Override
    public void getServiceGroups(Empty request, StreamObserver<ServiceGroupList> responseObserver) {
        logger.info("ServiceGroupFinder getServiceGroups() called.");
        responseObserver.onCompleted();
//      asyncUnimplementedUnaryCall(ServiceGroupFinderGrpc.METHOD_GET_SERVICE_GROUPS, responseObserver);
    }
  }
  
  private void start() throws IOException {
    /* The port on which the server should run */
    int port = 9901;
    server = ServerBuilder
    			.forPort(port)
				.addService(new DialogServiceImpl())
				.addService(new ServiceGroupFinderImpl())
    			.build()
    			.start();
    logger.info("Server started, listening on " + port);
    Runtime.getRuntime().addShutdownHook(new Thread() {
      @Override
      public void run() {
        // Use stderr here since the logger may have been reset by its JVM shutdown hook.
        System.err.println("*** shutting down gRPC server since JVM is shutting down");
        ChatServer.this.stop();
        System.err.println("*** server shut down");
      }
    });
  }

  private void stop() {
    if (server != null) {
      server.shutdown();
    }
  }

  /**
   * Await termination on the main thread since the grpc library uses daemon threads.
   */
  private void blockUntilShutdown() throws InterruptedException {
    if (server != null) {
      server.awaitTermination();
    }
  }

  /**
   * Main launches the server from the command line.
   */
  public static void main(String[] args) throws IOException, InterruptedException {
    final ChatServer server = new ChatServer();
    server.start();
    server.blockUntilShutdown();
  }
  
}
