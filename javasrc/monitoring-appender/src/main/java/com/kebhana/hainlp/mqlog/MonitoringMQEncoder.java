package com.kebhana.hainlp.mqlog;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.encoder.LayoutWrappingEncoder;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Error, Warn 로그를 정리하여 MQ로 전달한다
 */
public class MonitoringMQEncoder extends LayoutWrappingEncoder<ILoggingEvent> {

  private Charset charset;
  private static MonitoringMQClient client = null;
  private String tempIp = "";

  public static final int X_OPER_SYNC_ID_POSITION = 2;

  @Override
  public void start() {
    super.start();

    InetAddress local = null;
    try {
      local = InetAddress.getLocalHost();
    } catch (UnknownHostException e) {
      e.printStackTrace();
    }

    // 시스템 IP 생성
    tempIp = local.getHostAddress();

    // RabbitMQClient 객체 생성
    client = new MonitoringMQClient();
  }

  /**
   * 문자열을 charset에 따라 byte[]로 변환한다.
   */
  private byte[] convertToBytes(String s) {
    if (charset == null) {
      return s.getBytes();
    } else {
      return s.getBytes(charset);
    }
  }

  /**
   * Encoder에 사용할 Character set 설정
   */
  @Override
  public void setCharset(Charset charset) {
    super.setCharset(charset);
    this.charset = getCharset();
  }

  /**
   * Error, Warn 로그를 정리하여 MQ로 전달한다
   * 각 세션의 구분은 파이프로 한다
   * 글로벌ID
   * 호스트명 : total
   * 오류구분 : 시스템오류는 S, 비지니스오류는 B
   * 시스템아이피
   * 프로세스명 : MAP
   * 에러메시지 (최대 4000자)
   * 에러발생시간 : YYYYMMDDHH24MISS
   * @param event
   * @return
   */
  @Override
  public byte[] encode(ILoggingEvent event) {
    //System.out.println("#@ START encode Level :" + event.getLevel() + ", Msg :" + event.getFormattedMessage());

    StringBuffer buffer = new StringBuffer();
    // 변수 초기화
    String tempXOperSyncId = "";
    // ErrorCode가 정립되기 전까지 Business Logic Error로 처리
    String tempErrorType = "B";

    // 날짜 생성
    Date nowDate = new Date();
    SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMddHHmmss");

    //System.out.println("#@ event.getArgumentArray().length :" + event.getArgumentArray().length);

    // 1.X-Operation-Id가 있는지 확인
    if (event.getArgumentArray().length >= 3) {
      Object[] tempArray = event.getArgumentArray();
      tempXOperSyncId = (String) tempArray[X_OPER_SYNC_ID_POSITION];
    }

    String tempErrMsg = event.getFormattedMessage();

    // Error Message는 최대 4000자
    if (tempErrMsg.length() > 4000) {
      tempErrMsg = tempErrMsg.substring(0, 3999);
    }

    // MQ 메시지 포멧 생성
    buffer.append(tempXOperSyncId);
    buffer.append("|");
    buffer.append("total");
    buffer.append("|");
    buffer.append(tempErrorType);
    buffer.append("|");
    buffer.append(tempIp);
    buffer.append("|");
    buffer.append("MAP");
    buffer.append("|");
    buffer.append(tempErrMsg);
    buffer.append("|");
    buffer.append(sdf1.format(nowDate));

    client.pushDataToQueue(String.valueOf(buffer).getBytes());

    return convertToBytes(buffer.toString());
  }
}