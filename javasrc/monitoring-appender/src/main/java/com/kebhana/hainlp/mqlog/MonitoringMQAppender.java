package com.kebhana.hainlp.mqlog;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.ConsoleAppender;

/**
 * WARN, ERROR 경우 rabbitMQ 요청하는 Appender
 */
public class MonitoringMQAppender extends ConsoleAppender<ILoggingEvent> {
  private static MonitoringMQClient client = null;

  @Override
  public void start() {
    // RabbitMQClient 객체 생성
    client = new MonitoringMQClient();

    super.start();
  }

  /**
   * WARN, ERROR 인 경우만 rabbitMQ 요청을 보낸다
   */
  @Override
  protected void append(ILoggingEvent event) {
    Level tempLevel = event.getLevel();

    if (tempLevel == Level.ERROR || tempLevel == Level.WARN) {
      //System.out.println("#@ START doAppend Level :" + tempLevel + ", Msg :" + event.getMessage());
    } else {
      // ERROR, WARN 이 아닌 경우
      return;
    }


    super.append(event);
  }
}

