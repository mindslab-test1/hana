package com.kebhana.hainlp.mqlog;

import com.kebhana.hainlp.mqlog.config.PropertyManager;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class MonitoringMQClient {

  // TODO Server 정보는??
  // RabbitMQ Server MQ_IP 정보
  public static final String MQ_IP = "mq.appender.ip";
  // RabbitMQ Server MQ_PORT 정보
  public static final String MQ_PORT = "mq.appender.port";
  // RabbitMQ Server MQ_USER_NAME 정보
  public static final String MQ_USER_NAME = "mq.appender.username";
  // RabbitMQ Server MQ_PASSWORD 정보
  public static final String MQ_PASSWORD = "mq.appender.password";
  // RabbitMQ Server MQ_VIRTUAL_HOST 정보
  public static String MQ_VIRTUAL_HOST = "mq.appender.virtualhost";
  // Queue Name 정보
  public static final String MQ_TRANSATION_QUEUE_NAME = "HANA2_AI_ERROR";

  // MQ connect 변수 선언
  private Connection connection = null;
  private Channel channel = null;
  private ConnectionFactory factory = null;

  /**
   * 데이터 Queue 적재 함수 해당 함수에서 mq factory, connection, channel 객체를 생성한다
   */
  public void pushDataToQueue(byte[] data) {
    try {
      if (this.factory == null) {
        // connection 생성을 위한 factory 객체 생성
        this.factory = new ConnectionFactory();
        this.factory.setHost(PropertyManager.getString(MQ_IP));
        this.factory.setPort(PropertyManager.getInt(MQ_PORT));
        this.factory.setUsername(PropertyManager.getString(MQ_USER_NAME));
        this.factory.setPassword(PropertyManager.getString(MQ_PASSWORD));
        this.factory.setVirtualHost(PropertyManager.getString(MQ_VIRTUAL_HOST));
      }

      if (this.connection == null || !this.connection.isOpen()) {
        // connection 객체 생성
        this.connection = factory.newConnection();
      }

      if (this.channel == null || !this.channel.isOpen()) {
        // channel 객체 생성
        this.channel = connection.createChannel();
      }

      // queueDeclare의 queue name이 같아야 sever에 적재 된다
      channel.queueDeclare(MQ_TRANSATION_QUEUE_NAME, true, false, false, null);
      channel.basicPublish("", MQ_TRANSATION_QUEUE_NAME, null, data);
      if (channel.isOpen()) {
        this.channel.close();
      }
    } catch (Exception e) {
      System.out.println("pushDataToQueue Exception :" + e.toString());
    }
  }
}
