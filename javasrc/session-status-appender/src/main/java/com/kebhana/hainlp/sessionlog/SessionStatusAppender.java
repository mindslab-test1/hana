package com.kebhana.hainlp.sessionlog;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.ConsoleAppender;
import java.util.HashMap;
import java.util.Map;

public class SessionStatusAppender extends ConsoleAppender<ILoggingEvent> {

  private MQClient client;

  @Override
  public void start() {
    super.start();

    // RabbitMQClient 객체 생성
    System.out.println("#@ start set SessionStatusAppender's rabbitMQ");
    client = new MQClient();
  }

  @Override
  protected void append(ILoggingEvent event) {
    Object[] arguments = event.getArgumentArray();

    if (arguments.length != 3) {
      System.out.println("#@ append Argument number is invalid.");
    }
    Map<String, Integer> summary = (HashMap<String, Integer>) arguments[2];

    // 변수 초기화
    StringBuffer buffer = new StringBuffer();

    buffer.append("{\"SYS_DV\":\"AI\",");
    buffer.append("\"sendTime\":\"" + arguments[0] + "\",");
    buffer.append("\"DATA_LIST\":[");
    for (String key : summary.keySet()) {
      buffer.append("{\"" + key + "\":");
      buffer.append("\"" + summary.get(key) + "\"},");
    }
    if (!summary.isEmpty()) {
      buffer.deleteCharAt(buffer.length() - 1);
    }
    buffer.append("]}");

    client.pushDataToQueue(String.valueOf(buffer).getBytes());
  }
}

