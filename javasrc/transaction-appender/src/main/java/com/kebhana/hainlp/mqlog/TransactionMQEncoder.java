package com.kebhana.hainlp.mqlog;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.encoder.LayoutWrappingEncoder;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * MARKER가 CallLog로 전달된 로그를 조합하여 MQ로 전송한다
 */
public class TransactionMQEncoder extends LayoutWrappingEncoder<ILoggingEvent> {

  private Charset charset;
  private static TransactionMQClient client = null;

  @Override
  public void start() {
    super.start();
    System.out.println("#@ start set TransactionMQEncoder's rabbitMQ");
    // RabbitMQClient 객체 생성
    client = new TransactionMQClient();
  }

  /**
   * 문자열을 charset에 따라 byte[]로 변환한다.
   */
  private byte[] convertToBytes(String s) {
    if (charset == null) {
      return s.getBytes();
    } else {
      return s.getBytes(charset);
    }
  }

  /**
   * Encoder에 사용할 Character set 설정
   */
  @Override
  public void setCharset(Charset charset) {
    super.setCharset(charset);
    this.charset = getCharset();
  }

  /**
   * event로 전달된 Marker, Message, Arguments, MDC Map 등을 이용하여 로그를 조합한다.
   * 각 섹션의 구분은 파이프로 한다
   * 세션ID : 내부세션ID
   * 글로벌ID : X-Operation-Sync-Id
   * 서비스그룹 : CHATBOT명
   * 채널구분 : Device의Channel
   * 호스트명 : total로 고정 (by 장태성 수석)
   * 일자 : date 8 YYYYMMDD
   * 시간 : time 6 HHMISS
   * 서비스명 : MAP
   * 거래성공여부 : Y/N
   * 관련skill_ID : Utter의 Meta
   */
  @Override
  public byte[] encode(ILoggingEvent event) {
    //System.out.println("#@ START encode Level :" + event.getLevel() + ", Msg :" + event.getFormattedMessage());

    // 변수 초기화
    StringBuffer buffer = new StringBuffer();
    String tempSessionId = "";
    String tempOperationSyncId = "";
    String tempChatbot = "";
    String tempChannel = "";
    String tempHost = "total";
    String tempService = "";
    String tempIsSuccess = "";
    String tempSkill = "";

    // START Check MDC
    Map<String, String> map = event.getMDCPropertyMap();

    for (String key : map.keySet()) {
      // MDC로 부터 전달받은 변수 설정 (Key, Value)
      // Argument로 전달하지 못하는 값은 Kay, Value 형식의 MDC로 전달된다
      // 지정된 Key값으로 Value 값을 획득한다 (추후 필요한 데이터를 MDC를 통해 추가 가능)
      if (key.equalsIgnoreCase("sessionId")) {
        tempSessionId = map.get(key);
      } else if (key.equalsIgnoreCase("operationSyncId")) {
        tempOperationSyncId = map.get(key);
      } else if (key.equalsIgnoreCase("chatbot")) {
        tempChatbot = map.get(key);
      } else if (key.equalsIgnoreCase("channel")) {
        tempChannel = map.get(key);
      } else if (key.equalsIgnoreCase("service")) {
        tempService = map.get(key);
      } else if (key.equalsIgnoreCase("isSuccess")) {
        tempIsSuccess = map.get(key);
      } else if (key.equalsIgnoreCase("skill")) {
        tempSkill = map.get(key);
      }
    }
    // 날짜 생성
    Date nowDate = new Date();
    SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd");
    SimpleDateFormat sdf2 = new SimpleDateFormat("HHmmss");

    buffer.append(tempSessionId);
    buffer.append("|");
    buffer.append(tempOperationSyncId);
    buffer.append("|");
    buffer.append(tempChatbot);
    buffer.append("|");
    buffer.append(tempChannel);
    buffer.append("|");
    buffer.append(tempHost);
    buffer.append("|");
    buffer.append(sdf1.format(nowDate));
    buffer.append("|");
    buffer.append(sdf2.format(nowDate));
    buffer.append("|");
    buffer.append(tempService);
    buffer.append("|");
    buffer.append(tempIsSuccess);
    buffer.append("|");
    buffer.append(tempSkill);

    client.pushDataToQueue(String.valueOf(buffer).getBytes());

    return convertToBytes(buffer.toString());
  }
}