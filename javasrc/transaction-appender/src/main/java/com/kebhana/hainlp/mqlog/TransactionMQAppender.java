package com.kebhana.hainlp.mqlog;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.ConsoleAppender;

/**
 * MARKER가 CallLog인 로그를 MQ로 전달하는 Appender
 */
public class TransactionMQAppender extends ConsoleAppender<ILoggingEvent> {

  private static final String allowedMarker = "CallLog";

  @Override
  public void start() {
    super.start();
  }

  /**
   * Marker가 매칭되는 로그만 append 한다.
   */
  @Override
  protected void append(ILoggingEvent event) {
    if (!event.getMarker().contains(allowedMarker)) {
      // 해당 MARKER가 아닌 경우 프로세스 종료
      return;
    }

    super.append(event);
  }
}

