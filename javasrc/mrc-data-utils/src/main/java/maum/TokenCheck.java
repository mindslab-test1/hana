package maum;

import java.io.FileNotFoundException;
import java.util.List;
import ai.maum.mrc.tokenizer.FullTokenizer;


public class TokenCheck {

  private static final String TEST_TEXT = "AA01.테스트";
  private static String path = "C:\\";

  public static void main(String[] args) throws FileNotFoundException {
    String text = args.length > 0 ? args[0] : TEST_TEXT;
    int i = 0;
    List<String> tokens = new FullTokenizer(path).tokenize(text);
    for (String token : tokens) {
//			System.out.println(i+ " ["+ token + "]");
      i++;
//			System.out.println((i > 0 && i % 10 == 0) ? System.lineSeparator() : " ");
    }
    if (i % 10 != 0) {
      System.out.println();
    }

  }

}
