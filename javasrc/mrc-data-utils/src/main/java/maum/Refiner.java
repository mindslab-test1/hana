package maum;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import common.HtmlToPlainText;

public class Refiner {

  private static final String FILE_PATH = "C:\\hana\\문서\\이메일상담_2019.txt";

  private static void refineChatting(String target) throws FileNotFoundException, IOException {
    FileInputStream fis = new FileInputStream(target);
    Scanner scanner = new Scanner(fis);
//		FileWriter fw = new FileWriter("refinde.txt");
    String filePath = "C:\\hana\\문서\\refinde_chat.txt";
    FileWriter fw = new FileWriter(filePath);

    System.out.println("dump 2-lines");
    System.out.println(scanner.nextLine());
    System.out.println(scanner.nextLine());
    try {
      int no = 0;
      while (scanner.hasNextLine()) {
        String line = scanner.nextLine();
        String[] fields = line.split("\\[\\[\\[TAB]]]");
				/*int idx = 0;
				for(String field: fields){
					System.out.println(idx + ": " + field);
					idx++;
				}
				System.out.println(line + "\n");*/

        if (fields.length == 12) {
          fw.write(fields[7] + System.lineSeparator());
        } else {
          System.out.println("Invalid Format at " + (no + 2) + ".");
        }
        no++;
      }
    } catch (Exception e) {
      // TODO: handle exception
      e.printStackTrace();
    } finally {
      scanner.close();
      fw.close();
    }

  } //refineChatting()

  private static void refineEmail(String target) throws FileNotFoundException, IOException {
    FileInputStream fis = new FileInputStream(target);
    Scanner scanner = new Scanner(fis);
//		FileWriter fw = new FileWriter("refinde.txt");
    String filePath = "C:\\hana\\문서\\refinde_email.txt";
    FileWriter fw = new FileWriter(filePath);

    System.out.println("dump 2-lines");
    System.out.println(scanner.nextLine());
    System.out.println(scanner.nextLine());

    try {
      int no = 0;
      while (no < 5 && scanner.hasNextLine()) {
        String line = scanner.nextLine();
        String[] fields = line.split("\\[\\[\\[TAB]]]");
        if (no == 3) {
          StringBuilder contentBuilder = new StringBuilder();
          int idx = 0;
          for (String field : fields) {
            String content = field.trim();
            if (!content.isEmpty()) {
              if (idx < 2) {
                System.out.println(idx + ": " + content);
                fw.append(String.valueOf(idx))
                    .append(": ")
                    .append(content)
                    .append(System.lineSeparator())
                    .append(System.lineSeparator());
              } else {
                contentBuilder.append(content);
              }
            }
            idx++;
          }

          if (contentBuilder.length() > 0) {
            String allContent = contentBuilder.toString();
//						System.out.println("content: " + allContent);
            fw.append("HTML: ")
                .append(allContent)
                .append(System.lineSeparator())
                .append(System.lineSeparator());
            Document doc = Jsoup.parse(allContent);
            allContent = new HtmlToPlainText().getPlainText(doc);

            fw.append("before: ")
                .append(allContent)
                .append(System.lineSeparator())
                .append(System.lineSeparator())
                .append(System.lineSeparator());

            System.out.println("content: " + allContent);
            fw.append("content: ")
                .append(allContent)
                .append(System.lineSeparator());
          }

        }
        no++;
      }

      scanner.close();
      fw.close();
    } catch (Exception e) {
      // TODO: handle exception
      e.printStackTrace();
    } finally {
//			scanner.close();
//			fw.close();
    }
  } //refineEmail()

  public static void main(String[] args) throws FileNotFoundException, IOException {
    String path = args.length > 0 ? args[0] : FILE_PATH;
    System.out.println("file path : " + path);
//			refineChatting(path);
    refineEmail(path);
  }

}
