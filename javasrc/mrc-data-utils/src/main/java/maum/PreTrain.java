package maum;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import common.HtmlToPlainText;

public class PreTrain {

  private static void refineChatting(BufferedReader br, BufferedWriter bw)
      throws FileNotFoundException, IOException {
    int no = 0;
    String line = null;
    while (no < 5 && (line = br.readLine()) != null) {
      String[] fields = line.split("\\[\\[\\[TAB]]]");
      if (fields.length == 12) {
        bw.append(fields[7]).append(System.lineSeparator());
      } else {
        System.out.println("Invalid Format at " + (no + 2) + ".");
      }
      no++;
    }
  }

  private static void refineEmail(BufferedReader br, BufferedWriter bw)
      throws FileNotFoundException, IOException {
    int no = 0;
    String line = null;
    while (no < 5 && (line = br.readLine()) != null) {
      String[] fields = line.split("\\[\\[\\[TAB]]]");
      if (no > 1) {
        StringBuilder contentBuilder = new StringBuilder();
        int idx = 0;
        for (String field : fields) {
          if (!"".equals(field)) {
            String content = field.trim();
            if (!content.isEmpty() && idx > 0) {
              contentBuilder.append(content);
            }
          }
          idx++;
        }
        if (contentBuilder.length() > 0) {
          String allContent = contentBuilder.toString();
          Document doc = Jsoup.parse(allContent);
          allContent = new HtmlToPlainText().getPlainText(doc);
          allContent = allContent.replaceAll("(\r\n|\r|\n|\n\r|)", "");
          allContent = allContent.replace("니다.   ", "니다.");
          allContent = allContent.replace("니다.  ", "니다.");
          allContent = allContent.replace("니다. ", "니다.");
          allContent = allContent.replace("세요.   ", "세요.");
          allContent = allContent.replace("세요.  ", "세요.");
          allContent = allContent.replace("세요. ", "세요.");
          allContent = allContent.replace("니다.", "니다." + System.lineSeparator());
          allContent = allContent.replace("세요.", "세요." + System.lineSeparator());

          if (!allContent.endsWith("니다." + System.lineSeparator()) && !allContent
              .endsWith("세요." + System.lineSeparator())) {
            allContent = allContent + System.lineSeparator();
          }
          bw.append(allContent).append(System.lineSeparator());
        }
      }
      no++;
    }
  }

  public static void main(String[] args) {
    // TODO Auto-generated method stub
    String defaultPath = "C:\\workFolder\\plain text\\이메일상담_2019.txt";
    String path = args.length > 0 ? args[0] : defaultPath;
    String writePath = "C:\\workFolder\\plain text\\20200924_2019.txt";

    FileReader fr = null;
    FileWriter fw = null;
    BufferedReader br = null;
    BufferedWriter bw = null;

    Date date = null;

    try {
      fr = new FileReader(path);
      br = new BufferedReader(fr);

      fw = new FileWriter(writePath);
      bw = new BufferedWriter(fw);

      date = new Date();
      long startTime = date.getTime();
      System.out.println("시작 시간 : " + startTime);

//			refineChatting(br, bw);
      refineEmail(br, bw);

      date = new Date();
      long endTime = date.getTime();
      System.out.println("종료 시간 : " + endTime);

      long diff = endTime - startTime;
      long sec = diff / 1000;
      System.out.println("파일 생성 시간 : " + sec + "초");

    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      if (br != null) {
        try {
          br.close();
        } catch (IOException e) {
        }
      }
      if (fr != null) {
        try {
          fr.close();
        } catch (IOException e) {
        }
      }
      if (bw != null) {
        try {
          bw.close();
        } catch (IOException e) {
        }
      }
      if (fw != null) {
        try {
          fw.close();
        } catch (IOException e) {
        }
      }
    }
  }

}






