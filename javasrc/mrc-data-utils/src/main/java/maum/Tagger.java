package maum;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import com.google.protobuf.util.JsonFormat;
import ai.maum.mrc.tokenizer.FullTokenizer;
import maum.grpc.protobuf.Passage;
import maum.grpc.protobuf.XmlConvert;

public class Tagger {

  private static final int maxLevel = 3;
  private static final HashMap<String, String> tableTaggingHints = new HashMap<String, String>();
  private static String title, level1 = "", level2 = "", tag;
  private static String oriTitle, oriLevel1 = "", oriLevel2 = "", oriTag;
  private static String historyLaw = "";
  private static String statNo = "";
  private static int passageSize = 0, maxPassageSize = 0, totalPassageSize = 0;
  private static int totalPassageCount = 0, totalDocCount = 0;
  private static int tableOrderInPassage;
  private static int totalTableCount = 0;
  private static boolean doTokenize = false;
  private static FullTokenizer tokenizer;
  private static int tokenSize = 0, maxTokenSize = 0, totalTokenSize = 0;
  private static int tokenOver512Size = 0;
  //토큰추가
  private static int passageTokenSize = 0, maxPassageTokenSize = 0, totalPassageTokenSize = 0;
  private static int tokenSizeOverCnt = 0;
  private static StringBuilder contextBuilder;
  private static boolean writeContext = true;

  static {
    //예시로 몇개만 샘플로 만듦.
    tableTaggingHints.put("AA1 하나은행 창구 수신\t1.1 은행계좌개설\t1.1.1 신규예금개설\t1", "SKIP");
    tableTaggingHints
        .put("AA1 하나은행 창구 수신\t1.2 은행계좌개설\t1.2.1 신규예금개설\t1", "COL_HEADER:1,EACH_COLUMN");
    tableTaggingHints.put("AA1 하나은행 창구 수신\t1.3 은행계좌삭제\t1.3.1 신규예금개설\t1", "COL_HEADER:2");
    tableTaggingHints.put("AA1 하나은행 창구 수신\t1.4 은행계좌변경\t1.4.1 신규예금개설\t1", "COMMON_ROW");
    tableTaggingHints.put("AA1 하나은행 창구 수신\t1.4 은행계좌변경\t1.4.1 신규예금개설\t2", "EACH_COLUMN,COMMON_ROW");
    tableTaggingHints
        .put("AA1 하나은행 창구 수신\t1.4 은행계좌변경\t1.4.1 신규예금개설\t3", "EACH_COLUMN,COMMON_ROW:1");
    tableTaggingHints.put("AA1 하나은행 창구 수신\t1.4 은행계좌변경\t1.4.1 신규예금개설\t3", "ROW_HEADER:1");

    /**
     * 힌트 종류 정리
     * [
     * 	"SKIP",
     * 	"COL_HEADER:2",
     * 	"COL_HEADER:1,HEADING:1",
     * 	"COL_HEADER:1,EACH_COLUMN",
     * 	"ROW_HEADER:1",
     * 	"ROW_HEADER:1,HEADING:1",
     * 	"ROW_HEADER:1,EACH_COLUMN",
     * 	"ROW_HEADER:0,EACH_ROW",
     * 	"COMMON_COLUMN",
     * 	"COMMON_ROW",
     * 	"EACH_COLUMN,COMMON_ROW",
     * 	"EACH_COLUMN,COMMON_ROW:1"
     * ]
     */

  }

  private static String getCellText(Node tblCell) {
    //cell text 파싱
    StringBuilder pcontJoiner = new StringBuilder();
    StringBuilder gtextJoiner = new StringBuilder();
    List<Node> tblCellPcontNodes = tblCell.selectNodes("P_CONT");
    for (Node pcont : tblCellPcontNodes) {
      gtextJoiner.setLength(0);
      List<Node> pcontGtextNodes = pcont.selectNodes("G_TEXT");
      for (Node gtext : pcontGtextNodes) {
        gtextJoiner.append(gtext.getText());
      }
      String gText = gtextJoiner.toString().trim();
      // System.out.print("gText: ["+ gText + "]");
      if (!gText.isEmpty()) {
        pcontJoiner.append(gText).append("\n");
      }
    }

    String cellText = pcontJoiner.toString().trim();
    if (cellText.isEmpty() || cellText.equals(".") || cellText.equals("-")) {
      cellText = "";
    }
//		System.out.println("cellText: ["+ cellText + "]");

    return cellText;
  }// getCellText()

  /**
   * 선두열을 태깅하고 변경된 표 데이터를 리턴한다.
   *
   * @param table 표 데이터
   * @param hints 태깅 힌트
   * @return 선두열을 제외한 표 데이터
   */

  private static String[][] tagHeading(String[][] table, TableTaggingHints hints,
      StringBuilder tagBuilder) {
    int rowCount = hints.rowCount;
    int columnCount = hints.columnCount;
    int heading = hints.heading;

    for (int row = 0; row < heading; row++) {
      for (int col = 0; col < columnCount; col++) {
        if (!table[row][col].isEmpty() && (col == 0 || !table[0][col].equals(table[0][col - 1]))) {
          tagBuilder.append(col > 0 ? " " : "").append(table[0][col]);
        }
      }
      tagBuilder.append("\n");
    }

    String[][] newTable = new String[rowCount - heading][columnCount];
    for (int row = heading; row < rowCount; row++) {
      System.arraycopy(table[row], 0, newTable[row - heading], 0, columnCount);
    }

    hints.rowCount = newTable.length;

    return newTable;
  } //tagHeading()

  /**
   * 공통 컬럼을 가져온다.
   *
   * @param table 표 데이터
   * @param hints 태깅 힌트
   * @return 공통 컬럼 데이터
   */
  private static String[][] getCommonColumn(String[][] table, TableTaggingHints hints) {
    int rowCount = hints.rowCount;
    int columnCount = hints.columnCount;
    int newColumnCount = columnCount - 1;
    int common = newColumnCount;

    String[][] commonColumn = new String[1][rowCount];

    for (int row = 0; row < rowCount; row++) {
      commonColumn[0][row] = table[row][common];
    }

    hints.columnCount = newColumnCount;

    return commonColumn;
  }

  /**
   * 공통 컬럼을 태깅한다.
   *
   * @param commonColumn 공통 컬럼 데이터
   * @param hints 태깅 힌트
   */
  private static void tagCommonColumn(String[] commonColumn, TableTaggingHints hints,
      StringBuilder tagBuilder) {
    int rowCount = hints.rowCount;
    int headerDepth = hints.columnHeaderDepth;

    for (int header = 0; header < headerDepth; header++) {
      if (header == 0 || (!commonColumn[header].equals(commonColumn[header - 1]) && (
          (header + 1) != headerDepth || !commonColumn[header].equals(commonColumn[header + 1])))) {
        tagBuilder.append("| [COL] ").append(commonColumn[header]).append(" ");
      }
    }
    // 내용
    for (int row = headerDepth; row < rowCount; row++) {
      if (!commonColumn[row].isEmpty() && (row == headerDepth || !commonColumn[row]
          .equals(commonColumn[row - 1]))) {
        tagBuilder.append(headerDepth > 0 ? "| [COL] " : "").append(commonColumn[row]).append(" ");
      }
    }
    tagBuilder.append(headerDepth > 0 ? "|" : "").append("\n");
  } //tagCommonColumn()

  /**
   * 공통 행을 가져온다.
   *
   * @param table 표 데이터
   * @param hints 태깅 힌트
   * @return 공통 행 데이터
   */
  private static String[][] getCommonRow(String[][] table, TableTaggingHints hints) {
    int rowCount = hints.rowCount;
    int columnCount = hints.columnCount;
    int newRowCount = rowCount - hints.commonRow;
    int common = newRowCount;

    String[][] rowColumn = new String[hints.commonRow][columnCount];

    for (int row = 0; row < hints.commonRow; row++) {
      for (int col = 0; col < columnCount; col++) {
        rowColumn[row][col] = table[common + row][col];
      }
    }
    hints.rowCount = newRowCount;
    return rowColumn;
  } //getCommonRow()

  /**
   * 공통 컬럼을 태깅한다.
   *
   * @param commonColumn 공통 컬럼 데이터
   * @param hints 태깅 힌트
   */
  private static void tagCommonRow(String[][] commonRow, TableTaggingHints hints,
      StringBuilder tagBuilder) {
    int columnCount = hints.columnCount;
    int rowCount = hints.commonRow;
    int headerDepth = hints.rowHeaderDepth;

    for (int row = 0; row < rowCount; row++) {
      for (int header = 0; header < headerDepth; header++) {
        if (header == 0 || (!commonRow[row][header].equals(commonRow[row][header - 1]) && (
            (header + 1) != headerDepth || !commonRow[row][header]
                .equals(commonRow[row][header + 1])))) {
          tagBuilder.append("| [ROW] ").append(commonRow[row][header]).append(" ");
        }
      }
      // 내용
      for (int col = headerDepth; col < columnCount; col++) {
        if (!commonRow[row][col].isEmpty()
            && (col == headerDepth || !commonRow[row][col].equals(commonRow[row][col - 1]))) {
          tagBuilder.append(headerDepth > 0 ? "| [CON] " : "").append(commonRow[row][col])
              .append(" ");
        }
      }
      tagBuilder.append(headerDepth > 0 ? "|" : "").append("\n");
    }
  } //tagCommonRow()

  /**
   * Column Header 1개만 있는 기본표를 태깅한다.
   *
   * <pre>
   * 가장 많은 유형의 표로 추정된다.
   * </pre>
   *
   * @param table 표 데이터
   * @param hints 태깅 힌트
   * @return 태깅된 문자열
   */
  private static String tagBasicTable(String[][] table, TableTaggingHints hints)
      throws IOException {
    StringBuilder tagBuilder = new StringBuilder();

    //heading
    if (hints.heading > 0) {
      table = tagHeading(table, hints, tagBuilder);
    }

    //공통 행 or 컬럼
    String[][] commons = hints.commonColumn ? getCommonColumn(table, hints)
        : hints.commonRow > 0 ? getCommonRow(table, hints) : null;

    int rowCount = hints.rowCount;
    int columnCount = hints.columnCount;

    for (int row = 1; row < rowCount; row++) {
      for (int col = 0; col < columnCount; col++) {
        //내용
        if (!table[row][col].isEmpty()) {
          tagBuilder.append("| [COL] ").append(table[0][col]).append(" ");
          tagBuilder.append("| [CON] ").append(table[row][col]).append(" ");
        }
      }
      tagBuilder.append("|\n");
    }

    if (commons != null) {
      if (hints.commonColumn) {
        tagCommonColumn(commons[0], hints, tagBuilder);
      } else {
        tagCommonRow(commons, hints, tagBuilder);
      }
    }

    return tagBuilder.toString();

  } //tagBasicTable()

  /**
   * Column 단위로 구성된 표를 태깅한다.<br> 가장 많은 유형의 표로 추정된다.
   *
   * @param table 표 데이터
   * @param hints 태깅 힌트
   * @return 태깅된 문자열
   */

  private static String tagEachColumnTable(String[][] table, TableTaggingHints hints)
      throws IOException {
    StringBuilder tagBuilder = new StringBuilder();

    //heading
    if (hints.heading > 0) {
      table = tagHeading(table, hints, tagBuilder);
    }

    // 공통 행 or 컬럼
    String[][] commons = hints.commonColumn ? getCommonColumn(table, hints)
        : hints.commonRow > 0 ? getCommonRow(table, hints) : null;

    int rowCount = hints.rowCount;
    int columnCount = hints.columnCount;

    //동일한 컬럼 제거
    for (int col = 1; col < columnCount; col++) {
      boolean sameColumn = true;
      for (int row = 0; row < rowCount; row++) {
        if (!table[row][col].equals(table[row][col - 1])) {
          sameColumn = false;
          break;
        }
      }
      if (sameColumn) {
        columnCount--;
        if (columnCount > col) {
          for (int row = 0; row < rowCount; row++) {
            System.arraycopy(table[row], col + 1, table[row], col, columnCount - col);
          }
        }
      }
    }

    int headerDepth = hints.columnHeaderDepth;

    for (int col = 0; col < columnCount; col++) {
      // 컬럼 헤더 태깅
      for (int header = 0; header < headerDepth; header++) {
        if (header == 0 || (!table[header][col].equals(table[header - 1][col]) && (
            (header + 1) != headerDepth || !table[header][col].equals(table[header + 1][col])))) {
          tagBuilder.append("| [COL] ").append(table[header][col]).append(" ");
        }
      }
      // 내용
      for (int row = headerDepth; row < rowCount; row++) {
        if (!table[row][col].isEmpty() && (row == headerDepth || !table[row][col]
            .equals(table[row - 1][col]))) {
          tagBuilder.append(headerDepth > 0 ? "| [CON] " : "").append(table[row][col]).append(" ");
        }
      }
      tagBuilder.append(headerDepth > 0 ? "|" : "").append("\n");
    }

    if (commons != null) {
      if (hints.commonColumn) {
        tagCommonColumn(commons[0], hints, tagBuilder);
      } else {
        tagCommonRow(commons, hints, tagBuilder);
      }
    }

    return tagBuilder.toString();
  } //tagEachColumnTable()

  /**
   * Column 단위로 구성된 표를 태깅한다.<br> 가장 많은 유형의 표로 추정된다.
   *
   * @param table 표 데이터
   * @param hints 태깅 힌트
   * @return 태깅된 문자열
   */
  private static String tagColumnHeaderTable(String[][] table, TableTaggingHints hints)
      throws IOException {
    StringBuilder tagBuilder = new StringBuilder();

    //heading
    if (hints.heading > 0) {
      table = tagHeading(table, hints, tagBuilder);
      System.out.printf("tagColumnHeaderTable [%d x %d]\n", hints.rowCount, hints.columnCount);
    }

    // 공통 행 or 컬럼
    String[][] commons = hints.commonColumn ? getCommonColumn(table, hints)
        : hints.commonRow > 0 ? getCommonRow(table, hints) : null;

    int rowCount = hints.rowCount;
    int columnCount = hints.columnCount;
    int headerDepth = hints.columnHeaderDepth;

    for (int row = headerDepth; row < rowCount; row++) {
      for (int col = 0; col < columnCount; col++) {
        if (col == 0 || !table[row][col].equals(table[row][col - 1])) {
          //컬럼 헤더 태깅
          for (int header = 0; header < headerDepth; header++) {
            if (!table[header][col].isEmpty() && (header == 0 || !table[header][col]
                .equals(table[header - 1][col]))
                && (col == 0 || !table[header][col].equals(table[header][col - 1]))) {
              tagBuilder.append("| [COL] ").append(table[header][col]).append(" ");
            }
          }
          //내용
          if (!table[row][col].isEmpty()) {
            tagBuilder.append("| [CON] ").append(table[row][col]).append(" ");
          }
        }
      }
      tagBuilder.append("|\n");
    }

    if (commons != null) {
      if (hints.commonColumn) {
        tagCommonColumn(commons[0], hints, tagBuilder);
      } else {
        tagCommonRow(commons, hints, tagBuilder);
      }
    }

    return tagBuilder.toString();
  } //tagColumnHeaderTable()

  /**
   * Row Header만 있는 표로 태깅한다.
   *
   * @param table 표 데이터
   * @param hints 태깅 힌트
   * @return 태깅된 문자열
   */
  private static String tagRowHeaderTable(String[][] table, TableTaggingHints hints)
      throws IOException {
    StringBuilder tagBuilder = new StringBuilder();

    //heading
    if (hints.heading > 0) {
      table = tagHeading(table, hints, tagBuilder);
    }

    // 공통 행 or 컬럼
    String[][] commons = hints.commonColumn ? getCommonColumn(table, hints)
        : hints.commonRow > 0 ? getCommonRow(table, hints) : null;

    int rowCount = hints.rowCount;
    int columnCount = hints.columnCount;
    int headerDepth = hints.rowHeaderDepth;

    for (int row = 0; row < rowCount; row++) {
      // Row 헤더 태깅
      for (int header = 0; header < headerDepth; header++) {
        if (header == 0 || (!table[row][header].equals(table[row][header - 1]) && (
            (header + 1) != headerDepth
                || (!table[row][header].equals(table[row][headerDepth]) && !table[row][headerDepth]
                .isEmpty())))) {
          tagBuilder.append("| [ROW] ").append(table[row][header]).append(" ");
        }
      }
      // 내용
      for (int col = headerDepth; col < columnCount; col++) {
        if (!table[row][col].isEmpty()) {
          tagBuilder.append(headerDepth > 0 ? "| [CON] " : "").append(table[row][col]).append(" ");
        }
      }
      tagBuilder.append(headerDepth > 0 ? "|" : "").append("\n");
    }

    if (commons != null) {
      if (hints.commonColumn) {
        tagCommonColumn(commons[0], hints, tagBuilder);
      } else {
        tagCommonRow(commons, hints, tagBuilder);
      }
    }

    return tagBuilder.toString();
  } //tagRowHeaderTable()

  private static void tagTable(String[][] table, boolean isSimple, StringBuilder lineBuilder)
      throws IOException {
    TableTaggingHints hints = new TableTaggingHints(table.length, table[0].length, isSimple);
    {
      final String taggingHint = tableTaggingHints
          .getOrDefault(oriTag + "\t" + tableOrderInPassage, "");
      hints.parseHint(taggingHint);
    }

    if (hints.isSkip) {
      return;
    }
    //컬럼이 하나인 테이블
    if (hints.columnCount == 1) {
      for (int row = 0; row < hints.rowCount; row++) {
        lineBuilder.append(table[row][0]).append("\n");
      }
    } else if (hints.eachColumn) {
      lineBuilder.append(tagEachColumnTable(table, hints));
    } else if (hints.rowHeaderDepth > 0 || hints.eachRow) {
      lineBuilder.append(tagRowHeaderTable(table, hints));
    } else {
      // 표 기본 유형 : Column Header가 1개만 있는 경우
      if (hints.columnHeaderDepth == 1 && hints.isSimple) {
        lineBuilder.append(tagBasicTable(table, hints));
      } else {
        lineBuilder.append(tagColumnHeaderTable(table, hints));
      }
    }
  } //tagTable()

  ;

  private static String[][] getfilledTable(List<Node> tblRowNodes, int rowCount, int columnCount) {
    String[][] table = new String[rowCount][columnCount];

    int rowIndex = 0;
    for (Node tblRow : tblRowNodes) {
      List<Node> tblCellNodes = tblRow.selectNodes("TBL_CELL");
      int columnIndex = 0;
      for (Node tblCell : tblCellNodes) {
        // rowspan, colspan으로 이미 채워진 cell 건너띄기
        while (table[rowIndex][columnIndex] != null) {
          columnIndex++;
        }
        // row전체가 span으로 채워져 있으면 ???
        if (columnIndex >= columnCount) {
          System.out.println("TABLE abnormal span found.");
          rowIndex++;
          continue;
        }

        String cellText = getCellText(tblCell);

        // cell값을 colspan, rowspan에 따라 table에 채우기
        int rowSpan = tblCell.numberValueOf("@ROWSPAN").intValue();
        int colSpan = tblCell.numberValueOf("@COLSPAN").intValue();
        if (rowSpan == 0) {
          rowSpan = 1;
        }
        if (colSpan == 0) {
          colSpan = 1;
        }
        for (int row = rowIndex; row < rowIndex + rowSpan; row++) {
          for (int col = columnIndex; col < columnIndex + colSpan; col++) {
            table[row][col] = cellText;
          }
        }
        columnIndex += colSpan;
      }
      rowIndex++;
    }

    return table;
  } //getfilledTable()

  private static void printTable(String[][] table) {
    final int rowCount = table.length;
    final int columnCount = table[0].length;

//		System.out.printf("table: [%2d x %2d]\n", rowCount, columnCount);
    for (int row = 0; row < rowCount; row++) {
      for (int col = 0; col < columnCount; col++) {
        String briefText = table[row][col] != null ? table[row][col]
            .substring(0, Math.min(table[row][col].length(), 12)) : "{null}";
        System.out.printf("[%d:%d - %s]", row, col, briefText);
      }
      System.out.println();
    }
  } //printTable()

  /**
   * 표를 파싱하여 패시지를 출력한다.
   *
   * @param childNode 표 노드
   * @param lineBuilder XML 노드를 조합하여 한 문장을 만든다.
   * @param fw 출력 파일 Writer
   */
  private static void parseTable(Node childNode, StringBuilder lineBuilder, FileWriter fw)
      throws Exception {
    totalTableCount++;
    tableOrderInPassage++;
    List<Node> tblRowNodes = childNode.selectNodes("TBL_ROW");
    int rowCount = tblRowNodes.size();
    int columnCount;
    boolean isSimple = true;

    // 표의 행렬 크기 가져오기
    columnCount = 0;
    {
      List<Node> tblCells = tblRowNodes.get(0).selectNodes("TBL_CELL");
      for (Node cell : tblCells) {
        int colspan = cell.numberValueOf("@COLSPAN").intValue();
        if (colspan > 1) {
          isSimple = false;
        } else {
          colspan = 1;
        }
        columnCount += colspan;
      }
    }
    System.out.printf("표 규격 : [%2d x %2d]\n", rowCount, columnCount);

    // 표 배열에 값 채워 넣기
    String[][] table = getfilledTable(tblRowNodes, rowCount, columnCount);

    printTable(table);
    tagTable(table, isSimple, lineBuilder);
  } //parseTable()

  /**
   * 현재 패시지 태깅을 끝내고 새로운 패시지 태깅을 시작한다.
   *
   * @param lineBuilder XML 노드를 조합하여 한 문장을 만든다.
   * @param fw 출력 파일 Writer
   */
  private static void beginAndEndPassage(StringBuilder lineBuilder, FileWriter fw, boolean isLevel2,
      String stat_No) throws IOException {
    if (!isLevel2 && (!tag.isEmpty() || title.isEmpty())) {
      return;
    }
    totalPassageCount++;
    if (level1.isEmpty()) {
      level1 = "Nil.1";
      oriLevel1 = "Nil.1";
    }
    if (level2.isEmpty()) {
      level2 = "Nil.2";
      oriLevel2 = "Nil.2";
    }
    tag = String.join("\t", title.trim(), level1.trim(), level2.trim());
    oriTag = String.join("\t", oriTitle.trim(), oriLevel1.trim(), oriLevel2.trim());
    tableOrderInPassage = 0;
    if (totalPassageCount > 1) {
      if (writeContext && contextBuilder.length() > 9) {
        contextBuilder.append("</SECTION>");
        fw.append(contextBuilder.toString().replaceAll(">[\\t\\r\\n\\s]+", ">")).append("\n");
        if (!"".equals(statNo)) {
          fw.append("$$").append(statNo.trim()).append("\n");
        }
      }
      fw.append("\n");

      maxPassageSize = Math.max(maxPassageSize, passageSize);
      totalPassageSize += passageSize;
      if (doTokenize) {
        if (tokenSize > 512) {
          tokenOver512Size++;
        }
        maxTokenSize = Math.max(maxTokenSize, tokenSize);
        totalTokenSize += tokenSize;
      }
    }
    statNo = stat_No;
    if (!"".equals(historyLaw)) {
      if (tag.indexOf("Nil") > -1) {
        lineBuilder.append("@@").append(tag).append("\n");
      } else {
        lineBuilder.append("@@").append(tag.trim()).append(" <").append(historyLaw).append(">")
            .append("\n");
      }
    } else {
      lineBuilder.append("@@").append(tag).append("\n");
    }

    contextBuilder.setLength(0);
    contextBuilder.append("##").append("<SECTION>");

    passageSize = 0;
    if (doTokenize) {
      tokenSize = 0;
    }
  } //beginAndEndPassage()

  /**
   * 문단을 파싱하여 패시지 태그 및 패시지를 출력한다.
   *
   * @param childNode 문단노드
   * @param pcontNode 원문 XML을 추출하기 위한 P_CONT 노드
   * @param recursive true일 경우 원문 XML을 추가로 추출하지 않는다.
   * @param lineBuilder XML 노드를 조합하여 한 문장을 만든다.
   * @param fw 출력 파일 Writer
   */
  private static void parseChildNode(Node childNode, Node pcontNode, boolean recursive,
      StringBuilder lineBuilder, FileWriter fw) throws Exception {
    final String childNodeType = childNode.getName();

    if (lineBuilder.length() != 0 && !childNodeType.equals("G_TEXT") && !childNodeType
        .equals("M_TEXT")) {
      String line = lineBuilder.toString().trim();
      if (!line.isEmpty()) {
        if (doTokenize) {
          List<String> tokens = tokenizer.tokenize(line);
          int i = 0;
          for (String token : tokens) {
            System.out.print(i + " [" + token + "]");
            i++;
            System.out.print((i > 0 && i % 10 == 0) ? "\n" : " ");
          }
          if (i % 10 != 0) {
            System.out.println();
          }
          tokenSize += tokens.size();
        }
        fw.append(line).append("\n");
        passageSize += line.length();
      }
      lineBuilder.setLength(0);
    }

    if (childNodeType.equals("TOC_INFO")) {
      int level = Integer.parseInt(childNode.valueOf("@LEVEL"));
      String stat_No = childNode.valueOf("@STAT_NO");
      Node subjectNoNode = childNode.selectSingleNode("NOSTR/G_TEXT");
      List<Node> subjectNodes = childNode.selectNodes("SUBJT_TAG/*");
      List<Node> subjectTagNodes = childNode.selectNodes("SUBJT_TAG/ITEM/*"); //개정이력
      String subjectNo, subject;

      if (subjectNoNode == null) {
        subjectNo = "";
        System.out.print("{{{ 제목의 번호가 없습니다. }}}");
        System.out.println("{{{ " + title + " }}}");
      } else {
        subjectNo = subjectNoNode.getText();
      }

      if (subjectNodes.size() == 0) {
        subject = "";
        System.out.print("{{{ 제목이 없습니다. }}}");
        System.out.println("{{{ " + title + " , " + subjectNo + " }}}");
      } else {
        if (subjectNodes.size() > 1) {
          System.out.println("{{{ 제목이 여러 노드로 구성되어 있습니다. }}} " + subjectNodes.size());
        }
        StringBuilder builder = new StringBuilder();
        for (Node node : subjectNodes) {
          String name = node.getName();
          if (name.equals("G_TEXT") || name.equals("M_TEXT")) {
            builder.append(node.getText());
          } else {
            System.out.println("{{{ 처리하지 않은 제목의 노드 이름: " + name + " }}}");
          }
        }
        subject = builder.toString();

        StringBuilder itemAppend = new StringBuilder();
        StringBuilder itemAppendTxt = new StringBuilder();
        int cnt = 0;
        for (Node tagNode : subjectTagNodes) {
          String name = tagNode.getName();
          String itemDtText = "";
          if (name.equals("CDNM") && !"".equals(tagNode.getText())) {
            if (cnt == 0) {
              itemAppend.append(tagNode.getText()).append(" ");
            }
            cnt++;
          }
          if (name.equals("DT") && !"".equals(tagNode.getText())) {
            itemDtText = tagNode.getText();
            itemDtText = dateFormat(itemDtText, ".");
            itemAppendTxt.append(itemDtText).append(" ");
          }
        }
        subject = builder.toString();

        String txtComma = itemAppendTxt.toString().trim().replace(" ", ", ");
        itemAppend.append(String.join(" ", txtComma));
        historyLaw = itemAppend.toString().trim();
      }

      if (level == 1) {
        level1 = subjectNo + subject;
        level2 = "";
        tag = "";

        oriLevel1 = subjectNo + subject;
        oriLevel2 = "";
        oriTag = "";
        if (!"".equals(historyLaw)) {
          level1 += " <" + historyLaw + ">\n";
        }
      } else if (level == 2) {
        level2 = subjectNo + subject;
        oriLevel2 = subjectNo + subject;
        beginAndEndPassage(lineBuilder, fw, true, stat_No);
      } else if (level >= maxLevel) {
        throw new Exception("비정상적인 LEVEL 값 : " + level);
      }
      return;
    }

    // 비정형 개조식 시작 확인
    if (tag.isEmpty()) {
      if (childNodeType.equals("G_TEXT") || childNodeType.equals("M_TEXT")) {
        final String nline = childNode.valueOf("@NLINE");
        if (nline.equals("N")) {
          String sentence = childNode.getText().trim();
          if (!sentence.isEmpty() && !(sentence.startsWith("(") && sentence.endsWith("신규중단)"))
              && !(sentence.startsWith("<") && sentence.endsWith("신규중단>"))) {
            beginAndEndPassage(lineBuilder, fw, false, "");
          }
        }
      } else {
        beginAndEndPassage(lineBuilder, fw, false, "");
      }
      if (tag.isEmpty()) {
        return;
      }
    }

    if (childNodeType.equals("G_TEXT") || childNodeType.equals("M_TEXT")) {
      final String pkind = pcontNode.valueOf("@PKIND");
      final String nline = childNode.valueOf("@NLINE");
      if (nline.equals("N") || pkind.equals("02")) {
        String sentence = childNode.getText();
        lineBuilder.append(sentence);
      } else if (!nline.equals("Y")) {
        System.out.println("{{{ 알 수 없는 NLINE : " + nline + " }}}");
      }
    } else if (childNodeType.equals("T_TEXT")) {
      parseTable(childNode, lineBuilder, fw);
    } else if (childNodeType.equals("I_TEXT")) {
      System.out.println("{{{ 이미지 노드는 사용하지 않습니다. }}}");
    } else {
      System.out.println("{{{ 처리하지 않은 노드 타입 " + childNodeType + " }}}");
    }

    if (!recursive) {
      contextBuilder.append(pcontNode.asXML());
    }
  } //parseChildNode()

  /**
   * P_CONT 노드를 파싱한다.
   *
   * @param pcontNode P_CONT 노드
   * @param recursive true일 경우 원문 XML을 추가로 추출하지 않는다.
   * @param lineBuilder XML 노드를 조합하여 한 문장을 만든다.
   * @param fw 파일 출력 Writer
   */
  private static void parsePcont(Node pcontNode, StringBuilder lineBuilder, FileWriter fw)
      throws Exception {
    List<Node> childNodes = pcontNode.selectNodes("*");
    if (childNodes.size() == 0) {
      throw new Exception("문단 노드가 없습니다.");
    }

    boolean recursive;
    if (lineBuilder != null) {
      recursive = true;
      lineBuilder.setLength(0);
    } else {
      recursive = false;
      lineBuilder = new StringBuilder();
    }

    for (Node childNode : childNodes) {
      parseChildNode(childNode, pcontNode, recursive, lineBuilder, fw);
    }

    if (lineBuilder.length() != 0) {
      String line = lineBuilder.toString().trim();
      if (!line.isEmpty()) {
        if (doTokenize) {
          List<String> tokens = tokenizer.tokenize(line);
          int i = 0;
          for (String token : tokens) {
            System.out.print(i + " [" + token + "]");
            i++;
            System.out.print((i > 0 && i % 10 == 0) ? "\n" : " ");
          }
          if (i % 10 != 0) {
            System.out.println();
          }
          tokenSize += tokens.size();
        }
        fw.append(line).append("\n");
        passageSize += line.length();
      }
      lineBuilder.setLength(0);
    }
  } //parsePcont()

  /**
   * XML 문서 한 개를 파싱하여 tagging한다. 여기서 최상위 레벨값을 가져온다.
   *
   * @param target 대상 XML문서 경로명
   * @param fw 파일 출력 Writer
   */
  private static void parse(String target, FileWriter fw) {
    try {
      File inputFile = new File(target);
      SAXReader reader = new SAXReader();
      Document document = reader.read(inputFile);

      title = level1 = level2 = tag = "";
      oriTitle = oriLevel1 = oriLevel2 = oriTag = "";
      passageSize = 0;
      if (doTokenize) {
        tokenSize = 0;
      }

      totalDocCount++;
      if (totalDocCount > 1) {
        System.out.println();
        System.out.println();
      }
      System.out.println("문서파일 : " + inputFile.getName());

      // 최상위 레벨값을 가져온다.
      Node titleNode = document.selectSingleNode("/GENX_DOC/GENX_INFO/CONTMAST_INFO/CONT_KNM");
      Node titleNodeHistory = document.selectSingleNode("/GENX_DOC/GENX_INFO/REGHIST_INFO/EXE_DT");
      title = titleNode.getText();
      oriTitle = titleNode.getText();

      //1레벨 개정이력
      if (titleNodeHistory != null && !"".equals(titleNodeHistory.getText())) {
        String historyDate = titleNodeHistory.getText();
        historyDate = dateFormat(historyDate, ".");
        title += " <" + historyDate + ">";
      }

      // 개별 문단의 노드를 가져온다.
      List<Node> pcontNodes = document.selectNodes("/GENX_DOC/GENX_TOC/SECTION/P_CONT");

      for (Node node : pcontNodes) {
        parsePcont(node, null, fw);
      }
      if (writeContext && contextBuilder.length() > 9) {
        contextBuilder.append("</SECTION>");
        fw.append(contextBuilder.toString().replaceAll(">[\\t\\r\\n\\s]+", ">")).append("\n");
        if (!"".equals(statNo)) {
          fw.append("$$").append(statNo.trim()).append("\n");
        }
      }
      contextBuilder.setLength(0);
      maxPassageSize = Math.max(maxPassageSize, passageSize);
      totalPassageSize += passageSize;
      if (doTokenize) {
        if (tokenSize > 512) {
          tokenOver512Size++;
        }
      }
    } catch (Exception e) {
      // TODO: handle exception
      e.printStackTrace();
    }
  } //parse()

  /**
   * 지정한 경로에서 XML 문서의 목록을 가져온다. 대상 XML문서는 "20"으로 시작하고 ".xml"로 끝나는 파일로 한다.
   *
   * @param path XML 문서 목록을 가져올 대상 경로
   * @return XML 문서의 목록
   */
  private static List<String> getTargetXmlList(String path) throws IOException {
    List<String> targetXmlList = new ArrayList<String>();
    File dataFolder = new File(path);
    File[] xmlFolders = dataFolder.listFiles();

    if (xmlFolders == null) {
      System.out.println("데이터 폴더 목록을 가져오지 못 했습니다.");
      return targetXmlList;
    }

    for (File folder : xmlFolders) {
      File[] files = folder.listFiles();
      if (files == null) {
        continue;
      }
      for (File file : files) {
        String name = file.getName().toLowerCase();
        if (name.startsWith("20") && name.endsWith(".xml")) {
          targetXmlList.add(file.getCanonicalPath());
        }
      }
    }

    return targetXmlList;
  } //getTargetXmlList

  private static String dateFormat(String date, String type) {
    String returnTxt = "";
    if (!"".equals(date) || date != null) {
      returnTxt = date.substring(0, 4) + type + date.substring(4, 6) + type + date.substring(6, 8);
    }
    return returnTxt;
  } //dateFormat()

  /**
   * 토큰나이저 체크
   */
  private static int tokenCheck(String line, FullTokenizer tokenizer)
      throws NullPointerException, FileNotFoundException {
    List<String> tokens = new ArrayList<String>();
    tokens = tokenizer.tokenize(line);
    passageTokenSize += tokens.size();
    return tokens.size();
  } //tokenCheck()

  /**
   * plain text로 생성된 파일을 다시 한 번 읽어 json 형태로 출력한다.
   *
   * @param readFile 읽을 파일명
   * @param writeJson 출력 파일 json 파일명
   */
  private static void readPlainTxt(String readFile) {
    String writeJson = "C:\\workFolder\\json\\passage_20200917.json";
    FileReader fr = null;
    FileWriter fw = null;
    BufferedReader br = null;
    BufferedWriter bw = null;

    try {
      File file = new File(readFile);
      fr = new FileReader(file);
      br = new BufferedReader(fr);
      fw = new FileWriter(writeJson, false);
      bw = new BufferedWriter(fw);

      String line = null;
      //proto buffer
      Passage.Builder passageBuilder = Passage.newBuilder();
      XmlConvert.Builder xmlBuilder = XmlConvert.newBuilder();

      StringBuilder bodyTxt = new StringBuilder();
      StringBuilder bodyOriginTagTxt = new StringBuilder();
//			String packageHeaderTag = "";
      while ((line = br.readLine()) != null) {
        if (line.indexOf("@@") != -1) {
          String headerTxt = "";
          bodyTxt.setLength(0);
          headerTxt = line.replace("@@", "");
          passageBuilder.setTag(headerTxt);
        } else if (line.indexOf("##") != -1) {
          String orginTag = "";
          orginTag = line.replace("##", "");
          bodyOriginTagTxt.append(orginTag).append("\n");
        } else if (line.indexOf("$$") != -1) {
          String linkTag = "";
          linkTag = line.replace("$$", "");
          passageBuilder.setLink(linkTag);
        } else if ("".equals(line)) {
          if (!"".equals(bodyTxt.toString())) {
            int tokenSize = 0;
            tokenSize = tokenCheck(bodyTxt.toString(), tokenizer);
            if (tokenSize > 512) {
              ++tokenSizeOverCnt;
            }
            //토큰 상관없이 출력
            passageBuilder.setPassage(bodyTxt.toString());
            passageBuilder.setContext(bodyOriginTagTxt.toString());
            xmlBuilder.addPassages(passageBuilder);
          }
          bodyTxt.setLength(0);
          bodyOriginTagTxt.setLength(0);
        } else {
          bodyTxt.append(line).append("\n");
          if (tokenSize > 512) {
            ++tokenSizeOverCnt;
          }
        }
      }//while()

      if (!bodyTxt.toString().isEmpty() && !bodyOriginTagTxt.toString().isEmpty()) {
        //마지막 데이터 처리
        passageBuilder.setPassage(bodyTxt.toString());
        passageBuilder.setContext(bodyOriginTagTxt.toString());
        xmlBuilder.addPassages(passageBuilder);
      }

      bw.write(JsonFormat.printer().print(xmlBuilder));
      bw.flush();
    } catch (Exception e) {
      // TODO: handle exception
      e.printStackTrace();
    } finally {
      if (br != null) {
        try {
          br.close();
        } catch (IOException e) {
        }
      }
      if (fr != null) {
        try {
          fr.close();
        } catch (IOException e) {
        }
      }
      if (bw != null) {
        try {
          bw.close();
        } catch (IOException e) {
        }
      }
      if (fw != null) {
        try {
          fw.close();
        } catch (IOException e) {
        }
      }
    }
  } //readPlainTxt()

  /**
   * 지정한 경로에 저장된 XML 문서들을 패시지 태깅된 plain text로 변환 한다. 실행 예: ./tag U:\data
   *
   * @param args XML 문서가 저장된 경로
   */
  public static void main(String[] args) throws IOException, Exception {
    String writeFile = "C:\\work\\xml\\plain\\passage_20200918.txt";
    String filePath = "C:\\work\\xml";
    String path = "C:\\haiwas\\workspace\\maum\\libs\\vocab.txt";

    tokenizer = new FullTokenizer(path);
    FileWriter fw = new FileWriter(writeFile);
    contextBuilder = new StringBuilder();

    totalPassageCount = 0;
    maxPassageSize = 0;
    totalPassageSize = 0;
    totalDocCount = 0;

    try {
      List<String> targetXmlList = getTargetXmlList(filePath);
      int count = 0;
      for (String xml : targetXmlList) {
        count++;
        parse(xml, fw);
      }
      fw.flush();

      System.out.println("");
      System.out.println("");
      System.out.println("전체 문서 수 : " + totalDocCount);
      System.out.println("전체 패시지 수 : " + totalPassageCount);
      System.out.println("전체 패시지 길이 : " + totalPassageSize);
      System.out.println("평균 패시지 길이 : " + (float) totalPassageSize / totalPassageCount);
      System.out.println("가장 긴 패시지의 길이: " + maxPassageSize);
      System.out.println("전체 표 개수: " + totalTableCount);

      readPlainTxt(writeFile);
    } catch (Exception e) {
      // TODO: handle exception
      e.printStackTrace();
    } finally {
      fw.close();
    }
  } //main()

  private static class TableTaggingHints {

    int rowCount;
    int columnCount;
    boolean isSimple;
    int rowHeaderDepth = 0;
    int columnHeaderDepth = 1;
    int heading = 0;
    boolean eachColumn = false;
    boolean eachRow = false;
    boolean isSkip = false;
    boolean commonColumn = false;
    int commonRow = 0;
    String hintString = "";

    TableTaggingHints(int rowCount, int columnCount, boolean isSimple) {
      this.rowCount = rowCount;
      this.columnCount = columnCount;
      this.isSimple = isSimple;
    }

    public void parseHint(final String taggingHint) {
      if (taggingHint != null && !taggingHint.isEmpty()) {
        hintString = taggingHint;
        isSkip = taggingHint.equals("SKIP");

        int startIdx, endIdx;

        // Row 헤더 갯수 가져오기
        startIdx = taggingHint.indexOf("ROW_HEADER:");
        if (startIdx > -1) {
          startIdx += 11;
          endIdx = taggingHint.indexOf(",", startIdx);
          if (endIdx <= startIdx) {
            endIdx = taggingHint.length();
          }
          rowHeaderDepth = Integer.parseInt(taggingHint.substring(startIdx, endIdx));
        }

        // Column 헤더 갯수 가져오기
        startIdx = taggingHint.indexOf("COL_HEADER:");
        if (startIdx > -1) {
          startIdx += 11;
          endIdx = taggingHint.indexOf(",");
          if (endIdx <= startIdx) {
            endIdx = taggingHint.length();
          }
          columnHeaderDepth = Integer.parseInt(taggingHint.substring(startIdx, endIdx));
        } else {
          if (rowHeaderDepth > 0) {
            columnHeaderDepth = 0;
          }
        }

        // 개별 행/열 단위 태깅 여부 가져오기
        startIdx = taggingHint.indexOf("EACH_COLUMN");
        eachColumn = startIdx > -1;

        if (!eachColumn) {
          startIdx = taggingHint.indexOf("EACH_ROW");
          eachRow = startIdx > -1;
        }

        //선두 갯수
        startIdx = taggingHint.indexOf("HEADING:");
        if (startIdx > -1) {
          startIdx += 8;
          endIdx = taggingHint.indexOf(",");
          if (endIdx <= startIdx) {
            endIdx = taggingHint.length();
          }
          heading = Integer.parseInt(taggingHint.substring(startIdx, endIdx));
        }

        //공통 컬럼.
        startIdx = taggingHint.indexOf("COMMON_COLUMN");
        commonColumn = startIdx > -1;

        //공통 행
        startIdx = taggingHint.indexOf("COMMON_ROW:");
        if (startIdx > -1) {
          startIdx += 11;
          endIdx = taggingHint.indexOf(",");
          if (endIdx <= startIdx) {
            endIdx = taggingHint.length();
          }
          commonRow = Integer.parseInt(taggingHint.substring(startIdx, endIdx));
        }
      } else {
        hintString = "";
      }
    } //parseHint()

    public String toString() {
      return String.format("[%2d x %2d] <%d, %d> %s", rowCount, columnCount, rowHeaderDepth,
          columnHeaderDepth, hintString.isEmpty() ? hintString : "hints: " + hintString);
    }

  } //TableTaggingHints()

}
