package maum;

import java.io.FileNotFoundException;
import java.util.List;
import ai.maum.mrc.tokenizer.FullTokenizer;

public class Runner {

  private static final String TEST_TEXT = "AA1 1레벨 개조식 1.1개요 1.2 문의사항\n" +
      "테스트 문구를 넣어서 토큰 길이 체크를 한다.\n" +
      "결과값에 토큰 길이를 출력해서 확인 할 수 있다.";
  private static String path = "C:\\work\\vacab.txt";

  public static void main(String[] args) throws FileNotFoundException {
    // TODO Auto-generated method stub
    String text = args.length > 0 ? args[0] : TEST_TEXT;
    System.out.println("text : " + text);
    int i = 0;
    List<String> tokens = new FullTokenizer(path).tokenize(text);
    System.out.println("\ntokens : " + tokens.size());
    for (String token : tokens) {
      System.out.print(i + " [" + token + "]");
      i++;
      System.out.print((i > 0 && i % 10 == 0) ? System.lineSeparator() : " ");
    }
    if (i % 10 != 0) {
      System.out.println();
    }
  }

}
