package common;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

public class PropertyManager {

  private static String absoluteMindsPath = "MAUM_ROOT";
  private static String absoluteEtcPath = "/etc/";
  private static String absoluteConfigfilePath = "log-appender.conf";
  private static Properties prop = null;
  private static HashMap<String, List<String>> prop_map = new HashMap<String, List<String>>();

  private static void load() {
    if (prop == null) {
      String path =
          System.getenv(absoluteMindsPath) == null ? "/srv/maum" : System.getenv(absoluteMindsPath);
      path += (absoluteEtcPath + absoluteConfigfilePath);
      System.out.println("#@ PropertyManager load start : " + path);
      try {
        InputStream input = new FileInputStream(path);
        prop = new Properties();
        prop.load(input);
        for (String key : prop.stringPropertyNames()) {
          String[] arr = prop.getProperty(key).toString().split(",");
          prop_map.put(key, new ArrayList<String>(Arrays.asList(arr)));
        }
      } catch (IOException ie) {
        System.out.println("#@ PropertyManager load IO failed : " + path);
        ie.printStackTrace();
      } catch (Exception e) {
        // TODO: handle exception
        System.out.println("#@ PropertyManager load failed : " + path);
        e.printStackTrace();
      }
      System.out.println("#@ PropertyManager load done : " + path);
    } else {
      System.out.println("#@ PropertyManager already load.");
    }
  } //load()

  public static List<String> getValues(String key) {
    load();
    List<String> result = new ArrayList<String>();
    if (key != "" && prop_map.containsKey(key)) {
      result = prop_map.get(key);
    }
    return result;
  } //getValues()

}
